/*******************************************************************
  uLan Communication - user visible definitions

  ul_drvdef.h	- driver exported definitions and types to userspace

  (C) Copyright 1996-2016 by Pavel Pisa - project originator
        http://cmp.felk.cvut.cz/~pisa
  (C) Copyright 1996-2016 PiKRON Ltd.
        http://www.pikron.com
  (C) Copyright 2002-2016 Petr Smolik


  The uLan driver project can be used and distributed
  in compliance with any of next licenses
   - GPL - GNU Public License
     See file COPYING for details.
   - LGPL - Lesser GNU Public License
   - MPL - Mozilla Public License
   - and other licenses added by project originator

  Code can be modified and re-distributed under any combination
  of the above listed licenses. If contributor does not agree with
  some of the licenses, he/she can delete appropriate line.
  WARNING: if you delete all lines, you are not allowed to
  distribute code or sources in any form.
 *******************************************************************/

#ifndef _UL_DRVDEF_H
#define _UL_DRVDEF_H

#ifdef __cplusplus
extern "C" {
#endif

#include <ul_lib/ul_lib_config.h>

#ifndef _WIN32
 #ifndef CONFIG_OC_UL_DRV_SYSLESS
   #include <sys/ioctl.h>
 #endif /* CONFIG_OC_UL_DRV_SYSLESS */
#else /* _WIN32 */
 #include <windows.h>
 #include <winioctl.h>
#endif /* _WIN32 */

#if !defined(__uchar_defined) && !defined(uchar)
 typedef unsigned char uchar;
 #define uchar uchar
 #define __uchar_defined
#endif

#if !defined(__ushort_defined) && !defined(ushort)
 typedef unsigned short ushort;
 #define ushort ushort
 #define __ushort_defined
#endif

#if !defined(__uint_defined) && !defined(uint)
 typedef unsigned int uint;
 #define uint uint
 #define __uint_defined
#endif

#if !defined(__ulong_defined) && !defined(ulong)
 typedef unsigned long ulong;
 #define ulong ulong
 #define __ulong_defined
#endif

/* ul_mess_head flags definition defines proccessing 
   of message or its frames stored in bll */
#define UL_BFL_LOCK 0x8000	/* locked message is pointed only once */
#define UL_BFL_MSST 0x4000      /* Message must be received by some proccess */
#define UL_BFL_M2IN 0x2000      /* After succesfull proccessing move to proc_bll */
#define UL_BFL_LNMM 0x1000      /* Length of received frame must match expected len */
#define UL_BFL_FAIL 0x0800      /* Message cannot be proccessed - error */
#define UL_BFL_TAIL 0x0400      /* Multiframe message continues by next bll block */
#define UL_BFL_SND  0x0200      /* Send this frame */
#define UL_BFL_REC  0x0100      /* Receive answer frame into this bll block */
#define UL_BFL_SANL 0x0080      /* Source address not local */
#define UL_BFL_VERL 0x0040      /* Verify free space in buffer of destination station */
#define UL_BFL_NORE 0x0020      /* Do not try to repeat if error occurs */
#define UL_BFL_REWA 0x0010      /* If error occurs do wait with retry */
#define UL_BFL_PRQ  0x0002      /* Request imediate proccessing of frame by receiver station */
#define UL_BFL_ARQ  0x0001      /* Request imediate acknowledge by receiving station */

#define UL_BFL_PROC 0x0000      /* Flag used by ASM code version */

/*******************************************************************/
/* ioctl definitions */

#ifdef _WIN32

/* 32768-65535 are reserved for use by customers.*/
#define FILE_DEVICE_ULAN  0x0000A000

/* function codes 2048-4095 are reserved for customers. */
#define UL_IOCTL_INDEX  0xA00

#define UL_IO(function) \
	CTL_CODE(FILE_DEVICE_ULAN,UL_IOCTL_INDEX+function,METHOD_BUFFERED,FILE_ANY_ACCESS)
#define UL_IOR(function,size) \
	CTL_CODE(FILE_DEVICE_ULAN,UL_IOCTL_INDEX+function,METHOD_BUFFERED,FILE_ANY_ACCESS)
#define UL_IOW(function,size) \
	CTL_CODE(FILE_DEVICE_ULAN,UL_IOCTL_INDEX+function,METHOD_BUFFERED,FILE_ANY_ACCESS)
#define UL_IOWR(function,size) \
	CTL_CODE(FILE_DEVICE_ULAN,UL_IOCTL_INDEX+function,METHOD_BUFFERED,FILE_ANY_ACCESS)

#else /* _WIN32 */

#if defined(_IOC) && !defined(_IOR) && !defined(_IOW) /* NuttX */

#define	UL_IOCTL	0x7500

#define UL_IO(function)        _IOC(UL_IOCTL,function)
#define UL_IOR(function,size)  _IOC(UL_IOCTL,function)
#define UL_IOW(function,size)  _IOC(UL_IOCTL,function)
#define UL_IOWR(function,size) _IOC(UL_IOCTL,function)

#define UL_IO_DUMMY_ARG ,0

#else /* Linux */

#define	UL_IOCTL	0x75

#define UL_IO(function)        _IO(UL_IOCTL,function)
#define UL_IOR(function,size)  _IOR(UL_IOCTL,function,size)
#define UL_IOW(function,size)  _IOW(UL_IOCTL,function,size)
#define UL_IOWR(function,size) _IOWR(UL_IOCTL,function,size)

#endif /* Linux */

#endif /* _WIN32 */

#ifndef UL_IO_DUMMY_ARG
#define UL_IO_DUMMY_ARG
#endif

#define UL_DRV_VER	UL_IO(0x10)
#define UL_NEWMSG	UL_IOW(0x11,ul_msginfo)
#define UL_TAILMSG	UL_IOW(0x12,ul_msginfo)
#define UL_FREEMSG	UL_IO(0x13)
#define UL_ACCEPTMSG	UL_IOR(0x14,ul_msginfo)
#define UL_ACTAILMSG	UL_IOR(0x15,ul_msginfo)
#define UL_ADDFILT	UL_IOW(0x16,ul_msginfo)
#define UL_ABORTMSG	UL_IO(0x17)
#define UL_REWMSG	UL_IO(0x18)
#define UL_INEPOLL	UL_IO(0x19)
#define UL_WAITREC	UL_IO(0x1A)
#define UL_SETMYADR	UL_IO(0x1B)  /* arg = new station address */
#define UL_SETIDSTR	UL_IO(0x1C)  /* arg = "C" string */
#define UL_SETBAUDRATE	UL_IO(0x1D)  /* arg = new baudrate */
#define UL_SETPROMODE	UL_IO(0x1E)  /* arg = new pro_mode */
#define UL_SETSUBDEV	UL_IO(0x1F)  /* arg = sub device */
#define UL_QUERYPARAM	UL_IO(0x20)  /* arg = query */
#define UL_ROUTE	UL_IOWR(0x21,ul_route_range_t)  /* arg = route */
#define UL_KLOGBLL	UL_IO(0x41)
#define UL_STROKE	UL_IO(0x42)
#define UL_DEBFLG	UL_IO(0x43)  /* arg = new debug mask */
#define UL_HWTEST	UL_IO(0x44)  /* arg = test subcommand */

/* uLan queryparam services */
#define UL_QP_MYADR         0
#define UL_QP_BAUDRATE      1
#define UL_QP_PROMODE       2
#define UL_QP_SUBDEVNUM_MAX 3
#define UL_QP_MY_NET_BASE   4
#define UL_QP_DEF_GW_ADR    5
#define UL_QP_FULL_NET_ADR  6

/* Address format has to be interger for compatibility with kernel */
typedef int ul_msg_adr_t;

#define UL_MSG_ADR_LOCAL_MAX 100
#define UL_MSG_ADR_NET_MASK  (~0xffUL)

typedef struct ul_msginfo {
  ul_msg_adr_t dadr;		/* destignation address */
  ul_msg_adr_t sadr;		/* source address */
  int          cmd;		/* command/socket number */
  int          flg;		/* message flags */
  int          len;		/* length of frame */
  unsigned     stamp;		/* unigue message number */
} ul_msginfo;

/* Routing */
#define UL_ROUTE_OP_SET         0
#define UL_ROUTE_OP_GET         1

typedef struct ul_route_range_t {
   int operation;
   int route_flg;
   ul_msg_adr_t first;
   ul_msg_adr_t last;
   ul_msg_adr_t gw;
   int idx;
} ul_route_range_t;

/*******************************************************************/
/* command definitions */

/* standard command codes 
   00H .. 3FH    store to buffer 
   40H .. 7FH    store to buffer without ACK
   80H .. 9FH    proccess at onece
   A0H .. BFH    process with additional receive
   C0H .. FFH    process with additional send
*/

#define UL_CMD_RES	0x80	/* Reinitialize RS485 or connected module */
#define UL_CMD_SFT	0x81	/* Test free space in input buffer */
#define UL_CMD_EADR	0x82	/* Extended addresses follows LN ED.. ES.. CMD*/
#define UL_CMD_SID	0xF0	/* Send identification */
#define UL_CMD_SFI	0xF1	/* Send amount of free space in IB */
#define UL_CMD_TF0	0x98	/* End of stepping */
#define UL_CMD_TF1	0x99	/* Begin of stepping */
#define UL_CMD_STP	0x9A	/* Do step */
#define UL_CMD_DEB	0x9B	/* Additional debug commands */
#define UL_CMD_SPC	0xDA	/* Send state - for 8051 PCL PCH PSW ACC */

#define UL_CMD_RDM	0xF8	/* Read memory   T T B B L L */
#define UL_CMD_WRM	0xB8	/* Write mamory  T T B B L L */
#define UL_CMD_ERM      0x88    /* Erase memory T T B B L L */

#ifdef __cplusplus
} /* extern "C"*/
#endif

#endif /* _UL_DRVDEF_H */


