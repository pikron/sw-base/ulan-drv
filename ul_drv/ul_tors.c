/*******************************************************************
  uLan Communication - uL_DRV - multiplatform uLan driver

  ul_tors.c	- initialize and free of one driver

  (C) Copyright 1996-2004 by Pavel Pisa - project originator
        http://cmp.felk.cvut.cz/~pisa
  (C) Copyright 1996-2004 PiKRON Ltd.
        http://www.pikron.com
  (C) Copyright 2002-2004 Petr Smolik
  

  The uLan driver project can be used and distributed 
  in compliance with any of next licenses
   - GPL - GNU Public License
     See file COPYING for details.
   - LGPL - Lesser GNU Public License
   - MPL - Mozilla Public License
   - and other licenses added by project originator

  Code can be modified and re-distributed under any combination
  of the above listed licenses. If contributor does not agree with
  some of the licenses, he/she can delete appropriate line.
  WARNING: if you delete all lines, you are not allowed to
  distribute code or sources in any form.
 *******************************************************************/

static void ul_drv_reset_myadr(ul_drv *udrv, int newadr)
{
 #ifdef CONFIG_OC_UL_DRV_WITH_MULTI_DEV
  memset(udrv->my_adr_arr, 0, sizeof(udrv->my_adr_arr));
 #endif /*CONFIG_OC_UL_DRV_WITH_MULTI_DEV*/
  udrv->my_adr_arr[0]=newadr&~UL_ADR_NET_MASK;
 #ifdef UL_WITH_MULTI_NET
  udrv->my_net_base=newadr&UL_ADR_NET_MASK;
 #endif /*UL_WITH_MULTI_NET*/
}

/*******************************************************************/
/* default variants of some chip routines */

#ifdef UL_WITH_FRAME_FSM

int ul_drv_common_fnc_rqirq_noprobe(struct ul_drv *udrv)
{
  if(udrv->irq<=0)
    return UL_RC_EBADIRQ;
 #ifndef _WIN32
  return request_irq(udrv->irq, uld_irq_handler,
                            UL_IRQF_FLAGS , "ulan", udrv);
 #else /* _WIN32 */
  {
    NTSTATUS status;
    status = IoConnectInterrupt(&udrv->InterruptObject,
                        uld_irq_handler,            // ServiceRoutine
                        udrv,                       // ServiceContext
                        NULL,                       // SpinLock
                        udrv->irq,                  // Vector
                        udrv->Irql,                 // Irql
                        udrv->Irql,                 // SynchronizeIrql
                        udrv->InterruptMode,        // InterruptMode
                        udrv->InterruptShare,       // ShareVector
                        udrv->InterruptAffinity,    // ProcessorEnableMask
                        FALSE);                     // FloatingSave

    if (!NT_SUCCESS(status)) {
      udrv->InterruptObject=NULL;
      return UL_RC_EBADIRQ;
    }
    return 0;
  }
 #endif /* _WIN32 */
}

int ul_drv_common_fnc_rqirq_probe(struct ul_drv *udrv)
{
 #ifdef FOR_LINUX_KERNEL
  int ret=0;
  int test_cnt=3;
  unsigned long probe_irqs;

  while(test_cnt--&&udrv->irq<=0)
  { /* probe for IRQ */
    probe_irq_off(probe_irq_on());
    probe_irqs=probe_irq_on();
    if(udrv->chip_ops->fnc_genirq==NULL)
      return UL_RC_EBADIRQ;
    ret=udrv->chip_ops->fnc_genirq(udrv,1);
    udelay(100);
    udrv->irq=probe_irq_off(probe_irqs);
    ret=udrv->chip_ops->fnc_genirq(udrv,0);
    UL_PRINTF(KERN_INFO "uLan ul_drv_new : probed irq %d\n",udrv->irq);
  }
 #endif /* FOR_LINUX_KERNEL */
  if(udrv->irq<=0)
    return UL_RC_EBADIRQ;
  return ul_drv_common_fnc_rqirq_noprobe(udrv);
}

int ul_drv_common_fnc_freeirq(struct ul_drv *udrv)
{
 #ifndef _WIN32
  if(udrv->irq>0) {
    free_irq(udrv->irq,udrv); 
    ul_synchronize_irq(udrv->irq);
  }
 #else /* _WIN32 */
  if (udrv->InterruptObject!=NULL) {
    IoDisconnectInterrupt(udrv->InterruptObject);
    udrv->InterruptObject = NULL;
  }
 #endif /* _WIN32 */
  return 0;
}

#endif /*UL_WITH_FRAME_FSM*/

int ul_drv_common_fnc_setmyadr(struct ul_drv *udrv, int subdevidx, int newadr)
{
 #ifdef CONFIG_OC_UL_DRV_WITH_MULTI_DEV
  if((subdevidx<0)||(subdevidx>=UL_SUBDEVNUM_MAX))
    return -1;
  udrv->my_adr_arr[subdevidx]=newadr;
 #else /*CONFIG_OC_UL_DRV_WITH_MULTI_DEV*/
  udrv->my_adr_arr[0]=newadr;
 #endif /*CONFIG_OC_UL_DRV_WITH_MULTI_DEV*/
 #ifdef UL_WITH_MULTI_NET
  udrv->my_net_base=newadr&UL_ADR_NET_MASK;
 #endif /*UL_WITH_MULTI_NET*/
  return 0;
}

int ul_drv_common_fnc_setpromode(struct ul_drv *udrv, int pro_mode)
{
  udrv->pro_mode=pro_mode;
  return 0;
}

int ul_drv_start(ul_drv* udrv,int options)
{
  int ret=0;
  /* ports */
  if ((options&1)==0) {
    if(udrv->chip_ops->fnc_pinit!=NULL) {
      ret=udrv->chip_ops->fnc_pinit(udrv);
      UL_PRINTF(KERN_INFO "uLan_pinit ret %d\n",ret);
      if (ret>=0) udrv->chip_start_status|=UL_CSS_PINIT;
      else udrv->chip_start_status&=~UL_CSS_PINIT;
    } else {
      udrv->chip_start_status|=UL_CSS_PINIT;
    }
    if (ret<0) {
      UL_PRINTF(KERN_ERR "uLan ul_drv_new : port 0x%lx init error\n",
                ul_physaddr2uint(udrv->physbase));
      return -1;
    }
  }
  /* request irq */
  if ((options&2)==0) {
    if(udrv->chip_ops->fnc_rqirq!=NULL) {
      ret=udrv->chip_ops->fnc_rqirq(udrv);
      UL_PRINTF(KERN_INFO "uLan_rqirq ret %d\n",ret);
      if (ret>=0) udrv->chip_start_status|=UL_CSS_RQIRQ;
      else udrv->chip_start_status&=~UL_CSS_RQIRQ;
    } else {
      udrv->chip_start_status|=UL_CSS_RQIRQ;
    }
    if (ret<0) {
      UL_PRINTF(KERN_ERR "uLan ul_drv_new : cannot request irq %d\n",udrv->irq);
      return -1;
    }
  }
  /* sucessful initialization */
  if ((udrv->chip_start_status&(UL_CSS_PINIT|UL_CSS_RQIRQ))==(UL_CSS_PINIT|UL_CSS_RQIRQ)) {
    INIT_UDRV_WDTIM(udrv);
    INIT_UDRV_BH(udrv);
    uld_set_dfl(udrv,CHIPOK);
    /* activate */
    if(udrv->chip_ops->fnc_activate!=NULL) {
      udrv->chip_ops->fnc_activate(udrv);
      UL_PRINTF(KERN_INFO "uLan_activate\n");
    }
  }
  return ret;
}


int ul_drv_stop(ul_drv *udrv)
{
  int ret=0;
  int old_CHIPOK;
  if(!udrv) return -1;
  old_CHIPOK=uld_atomic_test_dfl(udrv,CHIPOK);
  if(old_CHIPOK && (udrv->chip_ops->fnc_freeirq!=NULL))
  { if(udrv->chip_ops->fnc_freeirq(udrv)<0) {
      UL_PRINTF(KERN_ERR "ul_drv_stop : failed to free irq\n");
      ret=-1;
    }
  }
  uld_atomic_clear_dfl(udrv,CHIPOK);
  KILL_UDRV_BH(udrv);
  DONE_UDRV_WDTIM(udrv);

 #ifdef UL_WITH_CHIP_TIMER
  uld_chiptimer_cancel_sync(udrv);
 #endif /*UL_WITH_CHIP_TIMER*/

 #if !defined(_WIN32) && !defined(FOR_NUTTX_KERNEL)
  schedule();
 #endif /* _WIN32 */
  if(old_CHIPOK)
  { if(udrv->chip_ops->fnc_pdone!=NULL)
      if(udrv->chip_ops->fnc_pdone(udrv)<0)
        ret=-1;
  }
 #ifndef _WIN32
  if(udrv->irq>0) ul_synchronize_irq(udrv->irq);
 #endif /* _WIN32 */
  KILL_UDRV_BH(udrv);
  udrv->chip_start_status=0;
  return ret;
}

/*******************************************************************/
/* initialize and free of one driver */

/* set initial state */
void ul_drv_new_init_state(ul_drv* udrv, int my_adr)
{
  /* init sequencer */
  udrv->fnc_sp=&udrv->fnc_stack[0];
 #ifdef UL_WITH_FRAME_FSM
  udrv->fnc_act=&uld_drvloop;
 #else /*UL_WITH_FRAME_FSM*/
  udrv->fnc_act=&uld_null_fnc;
 #endif /*UL_WITH_FRAME_FSM*/
  ul_drv_reset_myadr(udrv, my_adr);
 #ifdef uld_kwt_wq_init
  uld_kwt_wq_init(udrv);
 #endif
  udrv->fnc_timeout=NULL;
 #ifdef UL_WITH_CHIP_TIMER
  uld_chiptimer_init(udrv);
 #ifdef UL_WITH_FRAME_FSM
  udrv->chip_timer.function = uld_chip_timer_callback;
 #endif /*UL_WITH_FRAME_FSM*/
 #endif /*UL_WITH_CHIP_TIMER*/
  /* udrv->flags=0; */
  udrv->magic=UL_DRV_MAGIC;
  udrv->operators=NULL;
  udrv->pro_mode=0;
 #ifdef UL_WITH_MULTI_NET
  udrv->gw_adr=1;
 #endif /*UL_WITH_MULTI_NET*/
}

/* setups buffers, ports and irq for sucesfully detected device */
int ul_drv_new_start(ul_drv* udrv, unsigned buffer_size)
{ int ret=0;
  ret=ul_mem_init(udrv,buffer_size);
  UL_PRINTF(KERN_INFO "ul_mem_init ret %d\n",ret);
  if(ret>=0)
  {
    ret=ul_drv_start(udrv,0);
    if(ret>=0)
      return 0;
    ul_mem_done(udrv);
  } else UL_PRINTF(KERN_ERR "uLan ul_drv_new : no memory for buffers\n");

  return -1;
}

#ifndef _WIN32

/* initialize new driver from given parameters - internal version with ul_physaddr_t */
ul_drv *ul_drv_new_paddr(ul_physaddr_t physbase, int irq, int baud, int my_adr, const char *chip_name, long baudbase)
{
  ul_drv* udrv;
  int ret;
  int chipidx=0;
  int chipprobe=1;
  /* covert chip name to index */
  if(chip_name&&strcmp(chip_name,"auto")) {
    for(chipidx=0;1;chipidx++){
      if(!ul_chip_types[chipidx].chip_name){
	UL_PRINTF(KERN_ERR "uLan ul_drv_new : unknown chip type %s\n",chip_name);
	return NULL;
      }
      if(!strcmp(ul_chip_types[chipidx].chip_name,chip_name)){
	chipprobe=0; break;
      }
    }
  }
  UL_PRINTF(KERN_INFO "uLan ul_drv_new : chip %s index %d probe %d\n",
 		 chip_name?chip_name:"auto",chipidx,chipprobe);
  /* mem for ul_drv */
  if(!(udrv=MALLOC(sizeof(ul_drv)))) return NULL;
  /* clear memory */
  memset(udrv,0,sizeof(ul_drv));
  /* set initial state */
  ul_drv_new_init_state(udrv, my_adr);
  /* init chip driver */
  ret=-1;
  do{
    ul_chip_init_fnc *chip_init;
    int chip_flags=ul_chip_types[chipidx].chip_flags;
    int chip_options=ul_chip_types[chipidx].chip_options;
    chip_init=ul_chip_types[chipidx].chip_init;
    if(chipprobe&&(ul_chip_types[chipidx].chip_flags&UL_CHIPT_NOPROBE))
      continue;
    /* if(ret=u510_init(udrv, port, irq, baud, baudbase))>=0) */
    if((chip_flags&UL_CHIPT_RQPORT)&&!ul_physaddr2uint(physbase)) continue;
    if((chip_flags&UL_CHIPT_RQIRQ)&&!irq) continue;
    if((ret=(*chip_init)(udrv, physbase, irq, baud, baudbase, chip_options))>=0){
      UL_PRINTF(KERN_INFO "uLan ul_drv_new : %s found at 0x%lx\n",
                ul_chip_types[chipidx].chip_name,ul_physaddr2uint(udrv->physbase));
      break;
    }
  }while(chipprobe&&ul_chip_types[++chipidx].chip_name);
  if(ret>=0)
  { /* setups buffers, ports and irq for sucesfully detected device */
    if(ul_drv_new_start(udrv,ulbuffer)>=0)
      return udrv;
  }
  UL_PRINTF(KERN_ERR "uLan ul_drv_new : failed to init\n");
  FREE(udrv);
  return NULL;
};

/* destroy driver */
void ul_drv_free(ul_drv *udrv)
{
  if(!udrv) return;
  ul_drv_stop(udrv);

  { /* first not complete sanity check of client list */
    ul_opdata *opptr;
    UL_DRV_LOCK_FINI
    UL_DRV_LOCK;
    while((opptr=udrv->operators)!=NULL){
      udrv->operators=opptr->opnext; 
      if(udrv->operators!=NULL) udrv->operators->opprew=NULL;
      opptr->udrv=NULL;
     #ifdef FOR_LINUX_KERNEL
      wake_up_interruptible(&opptr->wqrec);
     #endif /*FOR_LINUX_KERNEL*/
    }
    UL_DRV_UNLOCK;
  };
  ul_mem_done(udrv);
  FREE(udrv);
};

#endif /* _WIN32 */
