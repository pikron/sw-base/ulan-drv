/*******************************************************************
  uLan Communication - uL_DRV - multiplatform uLan driver

  ul_c450.c	- chip driver for 16450

  (C) Copyright 1996-2004 by Pavel Pisa - project originator
        http://cmp.felk.cvut.cz/~pisa
  (C) Copyright 1996-2004 PiKRON Ltd.
        http://www.pikron.com
  (C) Copyright 2002-2004 Petr Smolik
  

  The uLan driver project can be used and distributed 
  in compliance with any of next licenses
   - GPL - GNU Public License
     See file COPYING for details.
   - LGPL - Lesser GNU Public License
   - MPL - Mozilla Public License
   - and other licenses added by project originator

  Code can be modified and re-distributed under any combination
  of the above listed licenses. If contributor does not agree with
  some of the licenses, he/she can delete appropriate line.
  WARNING: if you delete all lines, you are not allowed to
  distribute code or sources in any form.
 *******************************************************************/

/*******************************************************************/
/* Chip driver for 16450 */

#include "serial_reg.h"

#define u450_inb(base,port)	 ul_inb(ul_ioaddr_add(base,port*U450_BYTES_PER_REGS))
#define u450_outb(base,port,val) ul_outb(ul_ioaddr_add(base,port*U450_BYTES_PER_REGS),val)

#define U450_MCR_IE_C	0x0D	/* receive */
#define U450_MCR_OE_C	0x0E	/* transmit */
#define U450_MCR_OEQ_C	0x02	/* transmit ? */

#define U450_MSR_RxD_C	UART_MSR_CTS /* RxD line readback */
#define U450_MSR_TxD_C	UART_MSR_DSR /* TxD line readback */
#define U450_MSR_DTxD_C	UART_MSR_DDSR /* TxD line changed */

#define U450_LCR_UL	0x3B	/* receive/transmit */
#define U450_LCR_ULC	0x2B	/* transmit control character */
#define U450_LCR_ULB	0x7B	/* transmit line break */

#define U450_LSR_C	0x04	/* controll character received */
#define U450_LSR_ERR	0x1A	/* error in receiver */

#ifdef CONFIG_OC_UL_DRV_SYSLESS
 #define U450_FIX_RDI_GARBAGE(udrv,ret_code) \
 do { if(ret_code==5) u450_inb(udrv->iobase,UART_RX); } while(0)
#else /* CONFIG_OC_UL_DRV_SYSLESS */
#define U450_FIX_RDI_GARBAGE(udrv,ret_code) do{;}while(0)
#endif /* CONFIG_OC_UL_DRV_SYSLESS */

/*Definitions to allow support converters with different pins routing*/
#ifdef CONFIG_OC_UL_DRV_U450_VARPINS
#define U450_CHOPT_MSRSWAP	0x2000
#define U450_CHOPT_DIRNEG	0x4000
#define U450_MCR_CODES chip_buff[4]
#define U450_MSR_CODES chip_buff[5]
#define U450_MCR_IE(udrv)	((uchar)(udrv->U450_MCR_CODES>>0))
#define U450_MCR_OE(udrv)	((uchar)(udrv->U450_MCR_CODES>>8))
#define U450_MCR_OEQ(udrv)	((uchar)(udrv->U450_MCR_CODES>>16))
#define U450_MSR_RxD(udrv)	((uchar)(udrv->U450_MSR_CODES>>0))
#define U450_MSR_TxD(udrv)	((uchar)(udrv->U450_MSR_CODES>>8))
#define U450_MSR_DTxD(udrv)	((uchar)(udrv->U450_MSR_CODES>>16))
#define U450_SET_MCR_CODES(udrv,ie,oe,oeq) \
	(udrv->U450_MCR_CODES=(ie)<<0|(oe)<<8|(oeq)<<16)
#define U450_SET_MSR_CODES(udrv,rxd,txd,dtxd) \
	(udrv->U450_MSR_CODES=(rxd)<<0|(txd)<<8|(dtxd)<<16)
#else /*CONFIG_OC_UL_DRV_U450_VARPINS*/
#define U450_MCR_IE(udrv)	U450_MCR_IE_C
#define U450_MCR_OE(udrv)	U450_MCR_OE_C
#define U450_MCR_OEQ(udrv)	U450_MCR_OEQ_C
#define U450_MSR_RxD(udrv)	U450_MSR_RxD_C
#define U450_MSR_TxD(udrv)	U450_MSR_TxD_C
#define U450_MSR_DTxD(udrv)	U450_MSR_DTxD_C
#endif /*CONFIG_OC_UL_DRV_U450_VARPINS*/

#define U450_CHOPT_HSPDOSC	1

#define u450_chip_state_flags   chip_buff[3]

#ifdef CONFIG_OC_UL_DRV_U450_PE_LOST_ERRATA
#define U450_PE_LOST_STATE 1
#endif /*CONFIG_OC_UL_DRV_U450_PE_LOST_ERRATA*/

/*** Test interrupt request state ***/
static int u450_pool(ul_drv *udrv)
{
  unsigned u;
  u=u450_inb(udrv->iobase,UART_IIR);
  return (u&UART_IIR_NO_INT)?0:((u&UART_IIR_ID)|1);
};

/*** Wait end of transmit ***/
static int u450_weot(ul_drv *udrv, int ret_code)
{
  int ret;
  unsigned u;
  uchar lsr_val;
  uchar uc;
  if(!udrv->chip_buff[0]||
     !(u450_inb(udrv->iobase,UART_MCR)&U450_MCR_OEQ(udrv)))
  {
    UL_FRET;
    u450_outb(udrv->iobase,UART_LCR,U450_LCR_UL);
    return UL_RC_PROC;
  };
  u450_outb(udrv->iobase,UART_IER,UART_IER_RDI);
  lsr_val=u450_inb(udrv->iobase,UART_LSR);
  ret=UL_RC_WIRQ;
  if(!(~lsr_val&(UART_LSR_THRE|UART_LSR_TEMT)))
  {
    LOG_CHIO(" TMI");
    ret=UL_RC_PROC; /* has been commented out for LPC in past */
  }
  if(lsr_val&U450_LSR_ERR) 
    ret=UL_RC_EFRAME;	/* frame error */
  if(lsr_val&UART_LSR_DR)
  {
    if((u=udrv->chip_buff[1])) udrv->chip_buff[1]=0;
     else if((u=udrv->chip_buff[0])) udrv->chip_buff[0]=0;
    uc=u450_inb(udrv->iobase,UART_RX);
    if((u^uc)&0xff||!u)
    {
      LOG_CHIO(" TE%c!",u?'0':'1');
      ret=UL_RC_EBADCHR;
    }else{
      LOG_CHIO(".");
      if(ret>=0&&!udrv->chip_buff[0])
	ret=UL_RC_PROC;
    }; 
  } else {
    U450_FIX_RDI_GARBAGE(udrv,ret_code);
  }
  if(ret!=UL_RC_WIRQ)
  {
    u450_outb(udrv->iobase,UART_LCR,U450_LCR_UL);
    UL_FRET;
  };
  LOG_CHIO(" weot ret %d ",ret);
  return ret;
};


#ifndef CONFIG_OC_UL_DRV_U450_LOOPBACK

/*** Switch from receive to transmit ***/
  static int u450_sw_R2T_1(ul_drv *udrv, int ret_code);
  static int u450_sw_R2T_2(ul_drv *udrv, int ret_code);
  static int u450_sw_R2T_3(ul_drv *udrv, int ret_code);

static int u450_sw_R2T(ul_drv *udrv, int ret_code)
{
  udrv->chip_buff[0]=udrv->chip_buff[1]=0;
 /* A_wait */
  UL_FCALL2(u450_weot,u450_sw_R2T_1);
  return UL_RC_PROC;
};

static int u450_sw_R2T_1(ul_drv *udrv, int ret_code)
{
  uchar uc;
  if(ret_code<0) {UL_FRET;return ret_code;};
  u450_outb(udrv->iobase,UART_MCR,U450_MCR_IE(udrv));
  u450_outb(udrv->iobase,UART_IER,UART_IER_THRI);
  uc=u450_inb(udrv->iobase,UART_LSR);
  if(uc&UART_LSR_THRE)
    u450_outb(udrv->iobase,UART_TX,0);
  UL_FNEXT(u450_sw_R2T_2);
  uc=u450_inb(udrv->iobase,UART_LSR);
  if(uc&UART_LSR_THRE) return UL_RC_PROC;
    else return UL_RC_WIRQ;
}; 

static int u450_sw_R2T_2(ul_drv *udrv, int ret_code)
{
  uchar uc, uc1;

 /* A_wait1: */
  uc=u450_inb(udrv->iobase,UART_MSR);
  if(uc&U450_MSR_TxD(udrv))
    udrv->chip_temp&=~0x400;
  else
    udrv->chip_temp|=0x400;
  u450_outb(udrv->iobase,UART_IER,UART_IER_MSI);
  uc1=u450_inb(udrv->iobase,UART_LSR);
  if (!(~uc1&(UART_LSR_TEMT|UART_LSR_THRE))) {
    UL_FRET;
    return UL_RC_PROC;
  }
 #ifdef CONFIG_OC_UL_DRV_U450_TMELATE
  /* The next code seems to be required for LPC2148 UART */
  if((!(uc&U450_MSR_TxD(udrv)))&&(uc1&UART_LSR_THRE)) {
    UL_FRET;
    return UL_RC_PROC;
  }
 #endif /* CONFIG_OC_UL_DRV_U450_TMELATE */
  UL_FNEXT(u450_sw_R2T_3);
  return UL_RC_WIRQ;
}; 

static int u450_sw_R2T_3(ul_drv *udrv, int ret_code)
{
  uchar uc;
 #if 0
  static int reported_bad_chip=0;
 #endif
 /* A_wait2: */
  uc=u450_inb(udrv->iobase,UART_MSR);
  if(uc&U450_MSR_DTxD(udrv))
    udrv->chip_temp&=~0x400;
  if((uc&U450_MSR_TxD(udrv))&&(udrv->chip_temp&0x400))
    return UL_RC_WIRQ;
 #if 0
  while(~u450_inb(udrv->iobase,UART_LSR)&
	(UART_LSR_TEMT|UART_LSR_THRE))
   if(!reported_bad_chip++)
     LOG_FATAL(KERN_CRIT "uLan u450_sw_R2T_3 : problematic IC => looping in ISR\n");
 #endif
  UL_FRET;
  return UL_RC_PROC;
}; 
#else /* CONFIG_OC_UL_DRV_U450_LOOPBACK */

/*** Switch from receive to transmit ***/
  static int u450_sw_R2T_1(ul_drv *udrv, int ret_code);
  static int u450_sw_R2T_2(ul_drv *udrv, int ret_code);

static int u450_sw_R2T(ul_drv *udrv, int ret_code)
{
  udrv->chip_buff[0]=udrv->chip_buff[1]=0;
 /* A_wait */
  UL_FCALL2(u450_weot,u450_sw_R2T_1);
  return UL_RC_PROC;
};

static int u450_sw_R2T_1(ul_drv *udrv, int ret_code)
{
  if(ret_code<0) {UL_FRET;return ret_code;};
  u450_outb(udrv->iobase,UART_MCR,U450_MCR_IE(udrv)|UART_MCR_LOOP);
  u450_outb(udrv->iobase,UART_IER,UART_IER_RDI);
  u450_outb(udrv->iobase,UART_TX,0);
  UL_FNEXT(u450_sw_R2T_2);
  return UL_RC_WIRQ;
}; 

static int u450_sw_R2T_2(ul_drv *udrv, int ret_code)
{
  uchar uc;
  uc=u450_inb(udrv->iobase,UART_LSR);
  u450_inb(udrv->iobase,UART_RX);
  if (!(~uc&(UART_LSR_TEMT|UART_LSR_THRE))) {
    u450_outb(udrv->iobase,UART_MCR,U450_MCR_IE(udrv));
    UL_FRET;
    return UL_RC_PROC;
  }
  return UL_RC_WIRQ;
}; 

#endif /* CONFIG_OC_UL_DRV_U450_LOOPBACK */


/*** Receive character into char_buff ***/
  static int u450_recch_1(ul_drv *udrv, int ret_code);
  static int u450_recch_2(ul_drv *udrv, int ret_code);

static int u450_recch(ul_drv *udrv, int ret_code)
{
  udrv->char_buff=0;
  if(u450_inb(udrv->iobase,UART_MCR)&U450_MCR_OEQ(udrv))
  {
    UL_FCALL2(u450_weot,u450_recch_1);
    return UL_RC_PROC;
  };
  UL_FNEXT(u450_recch_2);
  return UL_RC_PROC;
};

static int u450_recch_1(ul_drv *udrv, int ret_code)
{
  u450_outb(udrv->iobase,UART_MCR,U450_MCR_IE(udrv));
  if(ret_code<0) {UL_FRET;return ret_code;};
  UL_FNEXT(u450_recch_2);
  return UL_RC_PROC;
};

INLINE int u450_recch_sub(ul_drv *udrv,int lsr_val,int rec_char)
{ /* helper function to process received character */
  if(lsr_val&U450_LSR_C) udrv->last_ctrl=rec_char|=0x100; 
   else udrv->last_ctrl&=0x7F; 
  udrv->char_buff=rec_char;
  udrv->xor_sum^=rec_char;udrv->xor_sum++;
  if(lsr_val&U450_LSR_ERR)
  {
    LOG_CHIO(" ER:%03X",udrv->char_buff);
    return UL_RC_EFRAME; /* frame error */
  };
  LOG_CHIO(" R:%03X",udrv->char_buff);
  return UL_RC_PROC;
};
 
static int u450_recch_2(ul_drv *udrv, int ret_code)
{
  uchar lsr_val;
  u450_outb(udrv->iobase,UART_IER,UART_IER_RDI);
  lsr_val=u450_inb(udrv->iobase,UART_LSR);
  if(!(lsr_val&UART_LSR_DR)) {
    U450_FIX_RDI_GARBAGE(udrv,ret_code);
    return UL_RC_WIRQ;
  }
 
  UL_FRET;
  return u450_recch_sub(udrv,lsr_val,u450_inb(udrv->iobase,UART_RX));
};

/*** Readback check for send character ***/

INLINE int u450_sndch_readback(ul_drv *udrv) {
  unsigned u;
  uchar uc;
  if((u=udrv->chip_buff[1])) udrv->chip_buff[1]=0;
    else if((u=udrv->chip_buff[0])) udrv->chip_buff[0]=0;
      else u=0;
  uc=u450_inb(udrv->iobase,UART_RX);
  if((u^uc)&0xff||!u)
  { /* readback character error */
    LOG_CHIO(" TE%c!",u?'2':'3');
    return UL_RC_EBADCHR;
  };
  return 0;
}

/*** Send character from char_buff ***/
  static int u450_sndch_1(ul_drv *udrv, int ret_code);
  static int u450_sndch_2(ul_drv *udrv, int ret_code);

static int u450_sndch(ul_drv *udrv, int ret_code)
{
  uchar uc;
  uc=(udrv->char_buff&0x100)?U450_LCR_ULC:U450_LCR_UL;
  if(u450_inb(udrv->iobase,UART_LCR)!=uc)
  { /* change between control and data character format */
   if((u450_inb(udrv->iobase,UART_MCR)&U450_MCR_OEQ(udrv)))
   { /* wait to end of transmit */
     UL_FCALL2(u450_weot,u450_sndch_1);
     return UL_RC_PROC;
   }else{ /* direction switch necessary */
     udrv->chip_buff[0]=udrv->chip_buff[1]=0;
     UL_FCALL2(u450_sw_R2T_1,u450_sndch_1);
     UL_FCALL2(u450_weot,u450_sw_R2T_1);
     return UL_RC_PROC;
   };
  };
  if(!(u450_inb(udrv->iobase,UART_MCR)&U450_MCR_OEQ(udrv)))
  { /* direction switch necessary */
    udrv->chip_buff[0]=udrv->chip_buff[1]=0;
    UL_FCALL2(u450_sw_R2T_1,u450_sndch_1);
    UL_FCALL2(u450_weot,u450_sw_R2T_1);
  } else {
    uc=u450_inb(udrv->iobase,UART_LSR);
    if(uc&U450_LSR_ERR)
      return UL_RC_EFRAME;	/* frame error */
    if(uc&UART_LSR_DR) {
      if(u450_sndch_readback(udrv))
        return UL_RC_EBADCHR;
    } else {
      U450_FIX_RDI_GARBAGE(udrv,ret_code);
    }
    if(!(uc&UART_LSR_THRE)) {
      /* No space for next character, wait  for it */
      u450_outb(udrv->iobase,UART_IER,UART_IER_THRI|UART_IER_RDI);
      return UL_RC_WIRQ;
    }
    UL_FNEXT(u450_sndch_1);
  }
  return UL_RC_PROC;
};

static int u450_sndch_1(ul_drv *udrv, int ret_code)
{
  uchar uc;
  if(ret_code<0) {UL_FRET;return ret_code;};
  u450_outb(udrv->iobase,UART_MCR,U450_MCR_OE(udrv));
  if(udrv->char_buff&0x100)
  { /* store last ctrl for connect line busy/ready */
    udrv->last_ctrl=udrv->char_buff;
    uc=U450_LCR_ULC;
  }else{ 
    uc=U450_LCR_UL;
  };
  udrv->chip_buff[1]=udrv->chip_buff[0];
  udrv->chip_buff[0]=udrv->char_buff|0x1000; 
  udrv->xor_sum^=udrv->char_buff;udrv->xor_sum++;
  u450_outb(udrv->iobase,UART_IER,UART_IER_THRI|UART_IER_RDI);
  u450_outb(udrv->iobase,UART_LCR,uc);  /* control/data character */
  u450_outb(udrv->iobase,UART_TX,(uchar)udrv->char_buff);
  LOG_CHIO(" T:%03X",udrv->char_buff);
  UL_FNEXT(u450_sndch_2);
  return UL_RC_WIRQ;
}; 

static int u450_sndch_2(ul_drv *udrv, int ret_code)
{
  uchar uc;
  uc=u450_inb(udrv->iobase,UART_LSR);
  UL_FRET;
  if(uc&U450_LSR_ERR)
      return UL_RC_EFRAME;	/* frame error */
  if(!(uc&UART_LSR_DR)) {
      U450_FIX_RDI_GARBAGE(udrv,ret_code);
      return UL_RC_PROC;		/* no char */ 
  }
  if(u450_sndch_readback(udrv))
    return UL_RC_EBADCHR;

  LOG_CHIO(".");
  return UL_RC_PROC;
};
 
/*** Wait for time or received character ***/
  static int u450_wait_1(ul_drv *udrv, int ret_code);
  static int u450_wait_2(ul_drv *udrv, int ret_code);

static int u450_wait(ul_drv *udrv, int ret_code)
{
  udrv->char_buff=0;
 #ifdef CONFIG_OC_UL_DRV_U450_PE_LOST_ERRATA
  udrv->u450_chip_state_flags &= ~U450_PE_LOST_STATE;
 #endif /*CONFIG_OC_UL_DRV_U450_PE_LOST_ERRATA*/
  if(u450_inb(udrv->iobase,UART_MCR)&U450_MCR_OEQ(udrv))
  {
    UL_FCALL2(u450_weot,u450_wait_1);
  } else {
    udrv->chip_buff[1]=udrv->chip_buff[0]=0;
    UL_FNEXT(u450_wait_1);
  }
  return UL_RC_PROC;
};

static int u450_wait_1(ul_drv *udrv, int ret_code)
{
 uchar lsr_val, u;
 if(ret_code<0) {UL_FRET;return ret_code;};
 lsr_val=u450_inb(udrv->iobase,UART_LSR);
 u450_outb(udrv->iobase,UART_MCR,U450_MCR_IE(udrv));
 u450_outb(udrv->iobase,UART_IER,UART_IER_THRI|UART_IER_RDI);
 if(lsr_val&UART_LSR_DR)
 {
   if((u=udrv->chip_buff[0])!=0)
   {
     uchar uc;
     udrv->chip_buff[0]=0;
     uc=u450_inb(udrv->iobase,UART_RX);
     if((u^uc)&0xff)
     {
        LOG_CHIO(" TE4!");
        return UL_RC_EBADCHR;
     }
   } else {
     UL_FRET;
    #ifdef CONFIG_OC_UL_DRV_U450_PE_LOST_ERRATA
     if(udrv->u450_chip_state_flags & U450_PE_LOST_STATE)
       lsr_val|=UART_LSR_PE;
    #endif /*CONFIG_OC_UL_DRV_U450_PE_LOST_ERRATA*/
     return u450_recch_sub(udrv,lsr_val,u450_inb(udrv->iobase,UART_RX));
   }
 } else {
  #ifdef CONFIG_OC_UL_DRV_U450_PE_LOST_ERRATA
   if(lsr_val & UART_LSR_PE)
     udrv->u450_chip_state_flags |= U450_PE_LOST_STATE;
  #endif /*CONFIG_OC_UL_DRV_U450_PE_LOST_ERRATA*/
   U450_FIX_RDI_GARBAGE(udrv,ret_code);
 }
 if(!(lsr_val&UART_LSR_THRE)) return UL_RC_WIRQ;
 if(!udrv->wait_time)
 {
   UL_FRET;
   LOG_CHIO(" Timeout!");
   return UL_RC_ETIMEOUT;
 };
 UL_FNEXT(u450_wait_2);
 u450_outb(udrv->iobase,UART_TX,0);
 return UL_RC_WIRQ;
};

static int u450_wait_2(ul_drv *udrv, int ret_code)
{
  uchar lsr_val,u;
  lsr_val=u450_inb(udrv->iobase,UART_LSR);
  if(lsr_val&UART_LSR_DR)
  {
    if((u=udrv->chip_buff[0])!=0)
    {
      uchar uc;
      udrv->chip_buff[0]=0;
      uc=u450_inb(udrv->iobase,UART_RX);
      if((u^uc)&0xff)
      {
        LOG_CHIO(" TE5!");
        return UL_RC_EBADCHR;
      }
    } else {
      UL_FRET;
     #ifdef CONFIG_OC_UL_DRV_U450_PE_LOST_ERRATA
      if(udrv->u450_chip_state_flags & U450_PE_LOST_STATE)
        lsr_val|=UART_LSR_PE;
     #endif /*CONFIG_OC_UL_DRV_U450_PE_LOST_ERRATA*/
      return u450_recch_sub(udrv,lsr_val,u450_inb(udrv->iobase,UART_RX));
    }
  } else {
  #ifdef CONFIG_OC_UL_DRV_U450_PE_LOST_ERRATA
    if(lsr_val & UART_LSR_PE)
      udrv->u450_chip_state_flags |= U450_PE_LOST_STATE;
  #endif /*CONFIG_OC_UL_DRV_U450_PE_LOST_ERRATA*/
    U450_FIX_RDI_GARBAGE(udrv,ret_code);
  }
  if(!(lsr_val&UART_LSR_THRE)) return UL_RC_WIRQ;
  if(!udrv->wait_time--)
  {
    UL_FRET;
    LOG_CHIO(" Timeout!");
    return UL_RC_ETIMEOUT;
  };
  u450_outb(udrv->iobase,UART_TX,0);
  return UL_RC_WIRQ;
};

/*** Connect to RS485 bus ***/
  static int u450_connect_1(ul_drv *udrv, int ret_code);
  static int u450_connect_2(ul_drv *udrv, int ret_code);

static int u450_connect(ul_drv *udrv, int ret_code)
{
  unsigned u;
  LOG_CHIO(" C_0 ");
  u=udrv->last_ctrl;
  udrv->chip_temp=uld_get_arb_addr(udrv);
  udrv->wait_time=((udrv->chip_temp-u-1)&0xF)+4; 
  udrv->chip_temp=(udrv->chip_temp&0x3F)|0x40;
  if(((u&0x180)!=0x180)||
     (u==0x1FF)) udrv->wait_time+=0x10;
  udrv->last_ctrl=0;
  udrv->char_buff=0;
  u450_inb(udrv->iobase,UART_MSR);
  UL_FCALL2(*udrv->chip_ops->fnc_wait,u450_connect_1);
  return UL_RC_PROC;
};

static int u450_connect_1(ul_drv *udrv, int ret_code)
{
  LOG_CHIO(" C_1 ");

  if(ret_code!=UL_RC_ETIMEOUT)
  { UL_FRET;
    LOG_CHIO(" EARBIT1!");
    return UL_RC_EARBIT;
  };
  /* check for RxD low */
  if(u450_inb(udrv->iobase,UART_MSR)&U450_MSR_RxD(udrv))
  { UL_FRET;
    LOG_CHIO(" EARBIT2!");
    return UL_RC_EARBIT;
  };
  u450_outb(udrv->iobase,UART_LCR,U450_LCR_ULB);
  u450_inb(udrv->iobase,UART_LSR);
  udrv->chip_temp|=0x300;
  u450_outb(udrv->iobase,UART_IER, (UART_IER_RLSI|UART_IER_RDI));
  u450_outb(udrv->iobase,UART_MCR,U450_MCR_OE(udrv));
  UL_FNEXT(u450_connect_2);
  /* wait for break condition now */
  return UL_RC_WIRQ;
};

static int u450_connect_2(ul_drv *udrv, int ret_code)
{
  uchar lsr_val;
  LOG_CHIO(" C_2 ");
  lsr_val=u450_inb(udrv->iobase,UART_LSR);
  if(lsr_val&UART_LSR_BI) udrv->chip_temp&=~0x100;
  if(lsr_val&UART_LSR_DR) {
    udrv->chip_temp&=~0x200;
    u450_inb(udrv->iobase,UART_RX);
  }
  if (udrv->chip_temp&0x300) {
    return UL_RC_WIRQ;
  }
  u450_outb(udrv->iobase,UART_LCR,U450_LCR_ULC);
  u450_outb(udrv->iobase,UART_MCR,U450_MCR_IE(udrv));
  u450_inb(udrv->iobase,UART_LSR);
  u450_inb(udrv->iobase,UART_RX);
  if(udrv->chip_temp==1)
  {LOG_CHIO(" Connected");
    UL_FRET;
  }else{ 
    udrv->wait_time=(udrv->chip_temp&3)+1; 
    udrv->chip_temp>>=2;
    UL_FCALL2(*udrv->chip_ops->fnc_wait,u450_connect_1);
  };
  return UL_RC_PROC;
};

/*** Finish tx ***/
  static int u450_finishtx_1(ul_drv *udrv, int ret_code);

static int u450_finishtx(ul_drv *udrv, int ret_code)
{
  UL_FCALL2(u450_weot,u450_finishtx_1);
  return UL_RC_PROC;
}

static int u450_finishtx_1(ul_drv *udrv, int ret_code)
{
  u450_outb(udrv->iobase,UART_MCR, U450_MCR_IE(udrv)); 
  UL_FRET;
  return UL_RC_PROC;
}

static const char *u450_port_name="ulan_u450";

/*** 16450 initialize ports ***/
static int u450_pinit(ul_drv *udrv)
{
  unsigned u;
  int baud=udrv->baud_val;

  if (ul_io_map(udrv->physbase,8*U450_BYTES_PER_REGS,u450_port_name, &udrv->iobase)<0)
  { LOG_FATAL(KERN_CRIT "uLan u450_pinit : cannot reguest ports !\n");
    return UL_RC_EPORT;
  };
 
  if (!baud) baud=19200;
  u=(udrv->baud_base+baud/2)/baud;
  if (u>0xFFFF) u=0xFFFF;
  udrv->baud_div=u;

  udrv->chip_buff[0]=udrv->chip_buff[1]=0;
  u450_outb(udrv->iobase,UART_MCR,UART_MCR_OUT1|UART_MCR_DTR);
  u450_outb(udrv->iobase,UART_LCR,UART_LCR_DLAB);
  u450_outb(udrv->iobase,(uchar)UART_DLL,(uchar)udrv->baud_div);
  u450_outb(udrv->iobase,(uchar)UART_DLM,(uchar)(udrv->baud_div>>8));
  u450_outb(udrv->iobase,UART_LCR,U450_LCR_UL);
  u450_outb(udrv->iobase,UART_IER,0);
  u450_outb(udrv->iobase,UART_FCR,0);	/* disable FIFOs */
  u450_inb(udrv->iobase,UART_LSR);		/* reset errors */
  u450_inb(udrv->iobase,UART_RX);		/* and other irq */
  return UL_RC_PROC;
};

/*** 16450 deinitialize ports ***/
static int u450_pdone(ul_drv *udrv)
{
  u450_outb(udrv->iobase,UART_IER, 0);	/* disable interrupts */
  u450_outb(udrv->iobase,UART_MCR, U450_MCR_IE(udrv)&3);  /* transmitter off */
  u450_inb(udrv->iobase,UART_IIR);		/* flush irq */
  u450_inb(udrv->iobase,UART_LSR);		/* flush irq */

  ul_io_unmap(udrv->physbase,8*U450_BYTES_PER_REGS,udrv->iobase);
  return UL_RC_PROC;
};

/*** 16450 activate ***/
static void u450_activate(ul_drv *udrv)
{
  u450_outb(udrv->iobase,UART_MCR,U450_MCR_IE(udrv)); /* connect irq */
}

/*** 16450 generate irq for irq_probe */
static int u450_genirq(ul_drv *udrv,int param)
{
  if(param) 
  {
    u450_outb(udrv->iobase,UART_MCR, U450_MCR_IE(udrv));  /* transmitter off */
    u450_outb(udrv->iobase,UART_IER, UART_IER_MSI);	/* enable interrupts */
    u450_outb(udrv->iobase,UART_LCR, U450_LCR_ULB);	/* trig break */
  }else{
    u450_outb(udrv->iobase,UART_IER, 0);		/* disable interrupts */
    u450_outb(udrv->iobase,UART_MCR, U450_MCR_IE(udrv));	/* transmitter off */
    u450_inb(udrv->iobase,UART_MSR);		/* flush irq */
  };
  return UL_RC_PROC;
};

/* support for hardware tests */
static int u450_hwtest(ul_drv *udrv,int param)
{
  unsigned u;
  switch(param)
  {
   case 0x10:
   case 0x11:
	u450_outb(udrv->iobase,UART_IER, 0); /* disable interrupts */
	u450_outb(udrv->iobase,UART_MCR, U450_MCR_OE(udrv)); /* transmitter on */
	u450_outb(udrv->iobase,UART_LCR, param&1?
	             U450_LCR_UL:U450_LCR_ULB); /* set TD lines */
   case 0x12:
        u=u450_inb(udrv->iobase,UART_MSR);
	return u450_inb(udrv->iobase,UART_LSR)|(u<<8)|
	                (u&(U450_MSR_TxD(udrv))?0:0x100000)|
			(u&(U450_MSR_RxD(udrv))?0:0x10000);
   case 0x13:
	u450_outb(udrv->iobase,UART_MCR, U450_MCR_IE(udrv)); /* transmitter off */
        u=u450_inb(udrv->iobase,UART_MSR);
	return u450_inb(udrv->iobase,UART_LSR)|(u<<8)|
	                (u&(U450_MSR_TxD(udrv))?0:0x100000)|
			(u&(U450_MSR_RxD(udrv))?0:0x10000);
  }
  return UL_RC_ENOFNC;
};

/*** Control functions of chip driver  ***/
static int u450_txoe(struct ul_drv *udrv, int enable)
{ /* switch on/off line transmitter */
  if(!enable)
  { /* switch off line transmitter */
    u450_outb(udrv->iobase,UART_MCR, U450_MCR_IE(udrv)); 
    u450_outb(udrv->iobase,UART_LCR, U450_LCR_UL); 
  } else {
    /* switch on line transmitter */
    u450_outb(udrv->iobase,UART_MCR, U450_MCR_OE(udrv));
    u450_outb(udrv->iobase,UART_LCR, U450_LCR_UL); 
  }
  return UL_RC_PROC;
}

const ul_drv_chip_ops u450_chip_ops = {
  "16450",	/* chip_type - text type identification*/
  u450_recch,	/* fnc_recch */
  u450_sndch,	/* fnc_sndch */
  u450_wait,	/* fnc_wait */
  u450_connect,	/* fnc_connect */
  u450_finishtx,	/* fnc_finishtx */
  u450_pool,	/* fnc_pool */
  NULL,		/* fnc_cctrl */
  NULL,		/* fnc_stroke */

  u450_txoe,	/* fnc_txoe */
  u450_pinit,	/* fnc_pinit */
  u450_pdone,	/* fnc_pdone */
  u450_activate,/* fnc_activate */
  u450_genirq,	/* fnc_genirq */
  ul_drv_common_fnc_rqirq_probe,	/* fnc_rqirq */
  ul_drv_common_fnc_freeirq,	/* fnc_freeirq */
  u450_hwtest,	/* fnc_hwtest */
  ul_drv_common_fnc_setmyadr,	/* fnc_setmyadr */
  ul_drv_common_fnc_setpromode,	/* fnc_setpromode */
};

/*** 16450 chip driver initialize ***/
int u450_init(ul_drv *udrv, ul_physaddr_t physbase, int irq, int baud, long baudbase, int options)
{
  unsigned u;
  unsigned old_mcr;
  unsigned old_lcr;

  if (ul_io_map(physbase,8*U450_BYTES_PER_REGS,u450_port_name, &udrv->iobase)<0)
  { LOG_FATAL(KERN_CRIT "uLan u450_init : cannot reguest ports !\n");
    return UL_RC_EPORT;
  };

 #ifdef CONFIG_OC_UL_DRV_U450_VARPINS
  if(!(options&U450_CHOPT_DIRNEG))
    U450_SET_MCR_CODES(udrv,U450_MCR_IE_C,U450_MCR_OE_C,U450_MCR_OEQ_C);
  else
    U450_SET_MCR_CODES(udrv,U450_MCR_OE_C,U450_MCR_IE_C,U450_MCR_OEQ_C>>1);
  if(!(options&U450_CHOPT_MSRSWAP))
    U450_SET_MSR_CODES(udrv,U450_MSR_RxD_C,U450_MSR_TxD_C,U450_MSR_DTxD_C);
  else
    U450_SET_MSR_CODES(udrv,UART_MSR_DSR,UART_MSR_CTS,UART_MSR_DCTS);
 #endif /*CONFIG_OC_UL_DRV_U450_VARPINS*/

  LOG_PORTS(KERN_INFO "uLan u450_init : probe MCR\n");
  old_mcr=u450_inb(udrv->iobase,UART_MCR);
  LOG_PORTS(KERN_INFO "uLan u450_init : MCR = 0x%02X\n",old_mcr);

  u450_outb(udrv->iobase,UART_MCR,UART_MCR_OUT1|UART_MCR_DTR);
  #ifdef FOR_LINUX_KERNEL
   set_current_state(TASK_INTERRUPTIBLE);
   schedule_timeout(ULD_HZ/10);
  #elif defined(_WIN32)
   /* !!!!!!!!!!!!!! */
   /* KeStallExecutionProcessor(20000); */
   { LARGE_INTEGER delay;
     delay=RtlConvertLongToLargeInteger(-30*10000);
     KeDelayExecutionThread(KernelMode,FALSE,&delay);
   }
  #else
   sleep(1);
  #endif
  old_lcr=u450_inb(udrv->iobase,UART_LCR);
  u450_outb(udrv->iobase,UART_LCR,UART_LCR_SBC);
  #ifdef FOR_LINUX_KERNEL
   set_current_state(TASK_INTERRUPTIBLE);
   schedule_timeout(ULD_HZ/10);
  #elif defined(_WIN32)
   /* !!!!!!!!!!!!!! */
   /* KeStallExecutionProcessor(2000); */
   { LARGE_INTEGER delay;
     delay=RtlConvertLongToLargeInteger(-10*10000);
     KeDelayExecutionThread(KernelMode,FALSE,&delay);
   }
  #else
   sleep(1);
  #endif
#ifndef CONFIG_OC_UL_DRV_U450_LOOPBACK
  if(u450_inb(udrv->iobase,UART_MSR)&U450_MSR_TxD(udrv))
  { u450_outb(udrv->iobase,UART_LCR,U450_LCR_UL);
    LOG_PORTS(KERN_INFO "uLan u450_init : MCR test 1 OK\n");
    #ifdef FOR_LINUX_KERNEL
     set_current_state(TASK_INTERRUPTIBLE);
     schedule_timeout(ULD_HZ/10);
    #elif defined(_WIN32)
     /* !!!!!!!!!!!!!! */
     /* KeStallExecutionProcessor(2000); */
     { LARGE_INTEGER delay;
       delay=RtlConvertLongToLargeInteger(-10*10000);
       KeDelayExecutionThread(KernelMode,FALSE,&delay);
     }
    #else
     sleep(1);
    #endif
    if(!(u450_inb(udrv->iobase,UART_MSR)&U450_MSR_TxD(udrv)))
    { LOG_PORTS(KERN_INFO "uLan u450_init : MCR test 2 OK\n");
#endif /*CONFIG_OC_UL_DRV_U450_LOOPBACK*/
      udrv->chip_options=options;
      udrv->baud_base=baudbase;
     /* check and setup baud base oscilator frequency */
      if(!udrv->baud_base){
       if(udrv->chip_options&U450_CHOPT_HSPDOSC)
         udrv->baud_base=U450_BAUD_BASE_DEFAULT*8;
       else
        udrv->baud_base=U450_BAUD_BASE_DEFAULT;
      }
      LOG_PORTS(KERN_INFO "uLan u450_init : baud_base 0x%X\n",(unsigned int)udrv->baud_base);
      if(!baud) baud=19200;
      udrv->baud_val=baud;
      u=(udrv->baud_base+baud/2)/baud;
      if (u>0xFFFF) u=0xFFFF;
      udrv->baud_div=u;
      udrv->physbase=physbase;
      udrv->irq=irq; 
      udrv->chip_ops=&u450_chip_ops;
      ul_io_unmap(udrv->physbase,8,udrv->iobase);
      return 0;
#ifndef CONFIG_OC_UL_DRV_U450_LOOPBACK
    };
    LOG_PORTS(KERN_ERR "uLan u450_init : MCR test 2 Error\n");
  };
  u450_outb(udrv->iobase,UART_LCR,(uchar)old_lcr);
  u450_outb(udrv->iobase,UART_MCR,(uchar)old_mcr);
  LOG_PORTS(KERN_ERR "uLan u450_init : MCR test Error\n");
  ul_io_unmap(physbase,8*U450_BYTES_PER_REGS,udrv->iobase);
  return UL_RC_EPORT;
#endif /*CONFIG_OC_UL_DRV_U450_LOOPBACK*/
};

