/*******************************************************************
  uLan Communication - uL_DRV - multiplatform uLan driver

  ul_linpci.c	- Linux kernel PCI device support

  (C) Copyright 1996-2004 by Pavel Pisa - project originator
        http://cmp.felk.cvut.cz/~pisa
  (C) Copyright 1996-2004 PiKRON Ltd.
        http://www.pikron.com
  (C) Copyright 2002-2004 Petr Smolik
  

  The uLan driver project can be used and distributed 
  in compliance with any of next licenses
   - GPL - GNU Public License
     See file COPYING for details.
   - LGPL - Lesser GNU Public License
   - MPL - Mozilla Public License
   - and other licenses added by project originator

  Code can be modified and re-distributed under any combination
  of the above listed licenses. If contributor does not agree with
  some of the licenses, he/she can delete appropriate line.
  WARNING: if you delete all lines, you are not allowed to
  distribute code or sources in any form.
 *******************************************************************/

/*******************************************************************/
/* Linux kernel PCI device support */ 

/* config space access (byte|word|dword) return to text pcibios_strerror */
/* pci_read_config_byte(struct pci_dev *dev, int where, u8 *val) */
/* pci_write_config_byte(struct pci_dev *dev, int where, u8 *val) */
/* request_region(), request_mem_region() */
/* void *pci_get_drvdata (struct pci_dev *pdev) */
/* void pci_set_drvdata (struct pci_dev *pdev, void *data) */

/* default invocation of chip_init for PCI devices */
static int /*__devinit*/ 
  ulan_init_chan(struct pci_dev *dev,char *subdev, 
  		 char *chip_name, ul_drv **pudrv,
                 ul_chip_init_fnc *chip_init,
                 ul_physaddr_t physaddr, int irq, int options)
{
  ul_drv *udrv;
  int amy_adr=0;
  int abaud=0;
  long abaudbase=0;
  int match;
  int minor;
  int ret;
  int i;
#ifdef UL_WITH_DEVFS
  kc_devfs_handle_t devfs_handle;
  char dev_name[32];
#endif /* UL_WITH_DEVFS */
  
  *pudrv=NULL;
  /* try to find best minor and parameters */
  match=ulan_init_find_minor("pci",kc_pci_name(dev),subdev,&minor,&i);
  if(i>=0){
    abaud=baud[i];amy_adr=my_adr[i]; abaudbase=baudbase[i];
  }

  /* mem for ul_drv */
  if(!(udrv=MALLOC(sizeof(ul_drv)))) return -ENOMEM;
  /* clear memory */
  memset(udrv,0,sizeof(ul_drv));
  /* set initial state */
  ul_drv_new_init_state(udrv, amy_adr);
  udrv->dev=(struct ul_phys_dev_ptr *)dev;
  /* init chip driver */
  if((ret=(*chip_init)(udrv, physaddr, irq, abaud, abaudbase, options))<0){
    printk(KERN_CRIT "ulan_init_chan: ERROR - chip_init returned %d\n",ret);
    FREE(udrv);
    return -EIO;
  }
  /* setups buffers, ports and irq for sucesfully detected device */
  if((ret=ul_drv_new_start(udrv,ulbuffer))<0){
    printk(KERN_CRIT "ulan_init_chan: ERROR - ul_drv_new_start returned %d\n",ret);
    FREE(udrv);
    return -EIO;
  }
  #ifdef UL_WITH_DEVFS
  sprintf (dev_name, "ulan%d", minor);
  devfs_handle=kc_devfs_new_cdev(NULL, MKDEV(ulan_major_dev, minor), 
			S_IFCHR | S_IRUSR | S_IWUSR | S_IRGRP | S_IWGRP, 
			&ulan_fops, udrv, dev_name);
  udrv->devfs_handle=devfs_handle;
  #endif /* UL_WITH_DEVFS */

  printk(KERN_INFO "ulan_init_chan: chip=%s minor=%d baud=%d my_adr=%d ready\n",
	 chip_name,/*dev_name*/minor,udrv->baud_val,udrv->my_adr_arr[0]);
  
  if(minor>=0) ul_drv_arr[minor]=udrv;
  *pudrv=udrv;
  udrv->next_chan=pci_get_drvdata(dev);
  pci_set_drvdata(dev, udrv);
  kc_class_device_create(ulan_class, NULL, MKDEV(ulan_major_dev, minor),
			kc_pci_dev_to_dev(dev), "ulan%d", minor);
  return 0;
}


static int /*__devinit*/ ulan_init_one_pci (struct pci_dev *dev,
				   const struct pci_device_id *ent,
                                   char *chip_name, ul_chip_init_fnc *chip_init)
{
  int chip_options=ent->driver_data;
  int ret, retall=-ENODEV;
  char subdev[2];
  ul_physaddr_t physbase;
  resource_size_t barbase, chanbase;
  int subno, irq;
  ul_drv *udrv;
  unsigned int chanspan=8;

  if (pci_enable_device (dev)){
    printk(KERN_CRIT "ulan_init_one_pci: Cannot enable device\n");
    return -EIO;
  }
  barbase=pci_resource_start(dev,0);
  irq=dev->irq;
  if(!irq||!barbase){
    LOG_FATAL(KERN_CRIT "uLan pci_init : bad/zero PCI port or irq !\n");
    return -ENODEV;
  }
  if (pci_resource_flags(dev,0)&IORESOURCE_IO) {
    if ((~UL_PHYSADDR_PIO_MASK&barbase)!=0) {
      LOG_FATAL(KERN_CRIT "uLan pci_init : PIO base out of range!\n");
      return -ENODEV;
    }
  } else if (pci_resource_flags(dev,0)&IORESOURCE_MEM) {
    if ((~UL_PHYSADDR_PIO_MASK&barbase)==0) {
      LOG_FATAL(KERN_CRIT "uLan pci_init : Mem base overlaps PIO range!\n");
      return -ENODEV;
    }
  }

  chanbase=barbase;
  if (chip_options&U950PCI_CHOPT_SP512F4K) {
    chanbase+=0x1000;
    chanspan=0x200;
  }

  for(subno=0;subno<2;subno++,chanbase+=chanspan){
    subdev[0]='0'+subno;subdev[1]=0;
    physbase=ul_uint2physaddr(chanbase);
    /* (dev,subdev,chip_name,pudrv,chip_init,port,irq,options) */
    ret=ulan_init_chan(dev,subdev,chip_name,&udrv,chip_init,physbase,irq,chip_options);
    if(ret>=0){
      retall=0;
    } else {
      if(retall<0) retall=ret;
    }
  }
  return retall;
}

static int /*__devinit*/ ulan_pci_init_one (struct pci_dev *dev,
				   const struct pci_device_id *ent)
{
  unsigned long driver_data=ent->driver_data;
  printk(KERN_INFO "ulan_init_one: PCI device found at slot : %s\n",
         kc_pci_name(dev)?kc_pci_name(dev):"unknown");
  pci_set_drvdata(dev, NULL);
  switch(driver_data&~0xff){
    case 0x16450000 :
      return ulan_init_one_pci(dev,ent,"16450",&u450_init);
    case 0x16954000 :
      return ulan_init_one_pci(dev,ent,"16950-pci",&u950pci_init);
  }
  printk(KERN_CRIT "ulan_init_one: No device of specified driver_data type\n");
  return -ENODEV;
}

static void /*__devexit*/ ulan_pci_remove_one (struct pci_dev *pdev)
{
  ul_drv *udrv, *next_udrv;
  int i;
  udrv=(ul_drv *)pci_get_drvdata(pdev);
  if(!udrv){
    printk(KERN_CRIT "ulan_remove_one: no uLan drvdata\n");
    return;
  }
  for(;udrv;udrv=next_udrv){
    if(udrv->magic!=UL_DRV_MAGIC){
      printk(KERN_CRIT "ulan_remove_one: Wrong uLan MAGIC number!!!\n");
      return;
    }
    next_udrv=udrv->next_chan;
    if((struct ul_phys_dev_ptr *)pdev!=udrv->dev){
      printk(KERN_CRIT "ulan_remove_one: BAD - cross PCI device remove\n");
    }
    for(i=0;i<UL_MINORS;i++){
      if (udrv==ul_drv_arr[i]){
        kc_class_device_destroy(ulan_class, MKDEV(ulan_major_dev, i));
        ul_drv_arr[i]=NULL;
      }
    }

    #ifdef UL_WITH_DEVFS
    if(udrv->devfs_handle) kc_devfs_delete(udrv->devfs_handle);
    #endif /* UL_WITH_DEVFS */
    ul_drv_free(udrv);
  }
  pci_disable_device(pdev);
  pci_set_drvdata(pdev, NULL);
  printk(KERN_INFO "ulan_remove_one: PCI device removed\n");
}

#if defined(CONFIG_PM) && (LINUX_VERSION_CODE > VERSION(2,6,18))
static int ulan_pci_suspend_one(struct pci_dev *pdev, pm_message_t state)
{
  ul_drv *udrv, *udrv4pdev;
  int ret;
  udrv4pdev=(ul_drv *)pci_get_drvdata(pdev);
  if(!udrv4pdev){
    printk(KERN_CRIT "ulan_pci_suspend_one: no uLan drvdata\n");
    return 0;
  }
  for(udrv=udrv4pdev;udrv;udrv=(udrv->next_chan!=udrv4pdev)?udrv->next_chan:NULL){
    if(udrv->magic!=UL_DRV_MAGIC){
      printk(KERN_CRIT "ulan_pci_suspend_one: Wrong uLan MAGIC number!!!\n");
      return 0;
    }
    if((struct ul_phys_dev_ptr *)pdev!=udrv->dev){
      printk(KERN_CRIT "ulan_pci_suspend_one: BAD - cross PCI device remove\n");
      return 0;
    }
    ret=ul_drv_stop(udrv);
    if(ret<0)
    { UL_PRINTF(KERN_CRIT "ulan_pci_suspend_one : ul_drv_stop failed\n");
    }
  }
  pci_save_state(pdev);
  pci_disable_device(pdev);
  pci_set_power_state(pdev, pci_choose_state(pdev, state));
  return 0;
}

static int ulan_pci_resume_one(struct pci_dev *pdev)
{
  ul_drv *udrv, *udrv4pdev;
  int ret;
  udrv4pdev=(ul_drv *)pci_get_drvdata(pdev);
  if(!udrv4pdev){
    printk(KERN_CRIT "ulan_pci_resume_one: no uLan drvdata\n");
    return 0;
  }

  pci_set_power_state(pdev, 0);
  ret = pci_enable_device(pdev);
  if (ret)
    return ret;
  pci_restore_state(pdev);

  for(udrv=udrv4pdev;udrv;udrv=(udrv->next_chan!=udrv4pdev)?udrv->next_chan:NULL){
    if(udrv->magic!=UL_DRV_MAGIC){
      printk(KERN_CRIT "ulan_pci_resume_one: Wrong uLan MAGIC number!!!\n");
      return -ENODEV;
    }
    if((struct ul_phys_dev_ptr *)pdev!=udrv->dev){
      printk(KERN_CRIT "ulan_pci_resume_one: BAD - cross PCI device remove\n");
      return -ENODEV;
    }
    if(!uld_atomic_test_dfl(udrv,CHIPOK)){
      ret=ul_drv_start(udrv,0);
      if(ret<0){
        UL_PRINTF(KERN_CRIT "ulan_pci_resume_one : ul_drv_start failed\n");
        return -EIO;
      }
    }
  }
  return 0;
}
#endif /* CONFIG_PM */

