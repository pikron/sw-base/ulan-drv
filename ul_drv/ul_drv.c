/*******************************************************************
  uLan Communication - uL_DRV - multiplatform uLan driver

  ul_drv.c	- uLan driver main source file

  (C) Copyright 1996-2004 by Pavel Pisa - project originator
        http://cmp.felk.cvut.cz/~pisa
  (C) Copyright 1996-2004 PiKRON Ltd.
        http://www.pikron.com
  (C) Copyright 2002-2004 Petr Smolik
  

  The uLan driver project can be used and distributed 
  in compliance with any of next licenses
   - GPL - GNU Public License
     See file COPYING for details.
   - LGPL - Lesser GNU Public License
   - MPL - Mozilla Public License
   - and other licenses added by project originator

  Code can be modified and re-distributed under any combination
  of the above listed licenses. If contributor does not agree with
  some of the licenses, he/she can delete appropriate line.
  WARNING: if you delete all lines, you are not allowed to
  distribute code or sources in any form.
 *******************************************************************/

#if defined(OMK_FOR_KERNEL) || defined(CONFIG_OC_UL_DRV_SYSLESS) || \
    defined(CONFIG_OC_UL_DRV_NUTTX)
  #include "ul_drv_config.h"
#endif /* CONFIG_OC_UL_DRV_SYSLESS */

#ifdef CONFIG_OC_UL_DRV_WITH_TRACER
  #define UL_WITH_TRACER
#endif /*CONFIG_OC_UL_DRV_WITH_TRACER*/

#ifdef CONFIG_OC_UL_DRV_WITH_IAC
  #define UL_WITH_IAC
#endif /*CONFIG_OC_UL_DRV_WITH_IAC*/

#ifdef CONFIG_OC_UL_DRV_WITH_MULTI_NET
  #define UL_WITH_MULTI_NET
#endif /*CONFIG_OC_UL_DRV_WITH_MULTI_NET*/

#define UL_WITH_UART_450
#define UL_WITH_UART_510

#ifdef CONFIG_OC_UL_DRV_USLIB
  #define  UL_DRV_IN_LIB
  #include <stdio.h>
  #include <unistd.h>
  #include <time.h>
  #include <errno.h>
  #include <asm/io.h>
  #include <asm/bitops.h>
  #include <asm/atomic.h>
  #include <sys/perm.h>
  /*#include <sys/io.h>*/
  #include <string.h>
  #include "emu_irq.h"
#elif defined(_WIN32)
  #ifdef FOR_WIN_KMD
    #pragma warning(disable:4242)
    #ifndef FOR_WIN_KMD
      #define FOR_WIN_KMD
    #endif
    #define UL_WITH_PCI
    #include "ntddk.h"
  #else
    #ifndef FOR_WIN_WDM
      #define FOR_WIN_WDM
    #endif
    #pragma warning(disable:4242)
    #define DBG 1
    #define DBG_USB 0
    #define ULD_DEBUG_DEFAULT (0x01|0x08|0x10|0x40|0x80)
    #define UL_WITH_PCI
    #define UL_WITH_USB
    #include "wdm.h"
    //#include "ntddk.h"	/*KeReadStateEvent is missing in old WDM*/
    //NTKERNELAPI LONG KeReadStateEvent (IN PRKEVENT Event);

    #include "ul_wdbase.h"
    #include "ul_wdinc.h"
    #ifdef UL_WITH_USB
      //#include "usb.h"
      #include "usbdi.h"
      //#define DECLSPEC_EXPORT  /*Hack needed for REACTOS build*/
      #include "usbdlib.h"
    #endif
    #define UL_WITH_WIN_PWR
  #endif
#elif defined(__DJGPP__)
  int uld_djgpp_data_beg;
  void uld_djgpp_code_beg(void) {;};
  #include <unistd.h>
  #include <errno.h>
  #include <stdio.h>
  #include <time.h>
  #include <string.h>
#elif defined(CONFIG_OC_UL_DRV_SYSLESS) || defined(CONFIG_OC_UL_DRV_NUTTX)
  #include <unistd.h>
  #include <errno.h>
  #include <stdio.h>
  #include <stdlib.h>
  #include <string.h>
  #include <sys/time.h>
  #include <time.h>
  #ifdef CONFIG_OC_UL_DRV_NUTTX
    #include <stdatomic.h>
    #include <nuttx/wdog.h>
    #ifndef FOR_NUTTX_KERNEL
      #define FOR_NUTTX_KERNEL
    #endif
  #else /*CONFIG_OC_UL_DRV_NUTTX*/
    #include <cpu_def.h>
    #include <hal_machperiph.h>
  #endif /*CONFIG_OC_UL_DRV_NUTTX*/
  #undef UL_WITH_UART_510
  #define UL_WITH_IAC
#else
  #include <linux/version.h>
  #if (LINUX_VERSION_CODE <= KERNEL_VERSION(2,6,17))
    #include <linux/config.h>
  #endif
  #include <linux/module.h>
  #include <linux/errno.h>
  #include <linux/kernel.h>
  #include <linux/sched.h>
  #include <linux/ioport.h>
  #include <linux/sys.h>
  #include <linux/fs.h>
  #include <linux/interrupt.h>
  #include <linux/poll.h>
  #include <linux/delay.h>
  #include <asm/bitops.h>
  #if (LINUX_VERSION_CODE < KERNEL_VERSION(3,4,0))
    #include <asm/system.h>
  #endif
  #include <asm/io.h>
  #include <asm/irq.h>

  #ifdef __KERNEL__
    #ifndef KERNEL
      #define KERNEL
    #endif
  #endif

  #include "k_compat.h"

  #if (LINUX_VERSION_CODE < VERSION(2,5,50))
    #include <linux/modversions.h>
  #endif /* 2.5.50 */

  #if (LINUX_VERSION_CODE < VERSION(2,4,10))
    #include <linux/malloc.h>
  #else
    #include <linux/slab.h>
  #endif /* 2.4.10 */

  #if (LINUX_VERSION_CODE < VERSION(2,3,20))
    #include <asm/spinlock.h>
  #else
    #include <linux/spinlock.h>
  #endif

  #if (LINUX_VERSION_CODE >= VERSION(2,4,0))
    #if (LINUX_VERSION_CODE <= VERSION(2,6,17))
      #define UL_WITH_DEVFS
    #endif /*2.6.17*/
    #if !defined(OMK_FOR_KERNEL) || defined(CONFIG_OC_UL_DRV_WITH_PCI)
      #define UL_WITH_PCI
    #endif
    #if !defined(OMK_FOR_KERNEL) || defined(CONFIG_OC_UL_DRV_WITH_USB)
      #define UL_WITH_USB
    #endif
    #if defined(CONFIG_OC_UL_DRV_WITH_OF)
      #define UL_WITH_OF
      #define UL_WITH_UART_PL011
    #endif
    #if defined(CONFIG_OC_UL_DRV_WITH_AMBA)
      #define UL_WITH_AMBA
      #define UL_WITH_UART_PL011
    #endif
    #include <linux/miscdevice.h>
    #include <linux/init.h>
  #endif

  #if (LINUX_VERSION_CODE >= VERSION(2,6,16))
    #include <linux/clk.h>
  #endif

  #if (LINUX_VERSION_CODE >= VERSION(2,6,0))
    #include <linux/kthread.h>
  #endif /* 2.6.0 */

  #if (LINUX_VERSION_CODE < VERSION(2,5,40))
    #include <linux/tqueue.h>
  #else /* 2.5.40 */
    #ifdef UL_WITH_DEVFS
      #include <linux/devfs_fs_kernel.h>
    #endif /*UL_WITH_DEVFS*/
  #endif /* 2.5.40 */

  #ifdef UL_WITH_PCI
    #include <linux/pci.h>
  #endif

  #ifdef UL_WITH_OF
    #include <linux/of.h>
    #include <linux/of_platform.h>
    #include <linux/platform_device.h>
  #endif

  #ifdef UL_WITH_AMBA
    #include <linux/amba/bus.h>
  #endif

  #ifdef UL_WITH_USB
    #include <linux/usb.h>
    #if (LINUX_VERSION_CODE < VERSION(2,6,39))
      #include <linux/smp_lock.h>
    #endif
  #endif

  #ifdef UL_WITH_OF
     #ifdef CONFIG_PPC_MPC52xx
       #define UL_WITH_UART_MPC52xx_PSC
     #endif /*PPC_MPC52xx*/
  #endif /*UL_WITH_OF*/

  #if (LINUX_VERSION_CODE >= KERNEL_VERSION(4,0,0))
    #define UL_WITH_CHIP_TIMER
    #include <linux/hrtimer.h>
  #endif

#endif

#if defined(UL_WITH_UART_450) || defined(UL_WITH_UART_PL011) || defined(UL_WITH_UART_510)
  #define UL_WITH_UARTS
#endif

#if defined(UL_WITH_UARTS) || defined(UL_WITH_PCI) || defined(UL_WITH_OF) || defined(UL_WITH_AMBA)
  #define UL_WITH_FRAME_FSM
  #define ENABLE_UL_IRQ_STALE_WDG
#endif /*defined(UL_WITH_UARTS) || defined(UL_WITH_PCI)*/

#define ULD_DEFAULT_BUFFER_SIZE 0x8000

#include "ul_drv.h"
#include "ul_hdep.h"

#if defined(CONFIG_OC_UL_DRV_SYSLESS) || defined(CONFIG_OC_UL_DRV_NUTTX)
  /* for check prototypes */
  #include "ul_drv_init.h"
#endif /* CONFIG_OC_UL_DRV_SYSLESS */

#if defined(_WIN32) && defined(UL_WITH_USB)
  #include "ul_wdusb.h"
#endif /*defined(_WIN32) && defined(UL_WITH_USB)*/

#ifndef UL_DRV_IN_LIB
unsigned uld_debug_flg=ULD_DEBUG_DEFAULT;
#else
unsigned uld_debug_flg; /* Left application  set defaults */
#endif
int ulbuffer=ULD_DEFAULT_BUFFER_SIZE;  /* default communication buffer size */

#ifdef UL_IRQ_LOCK_GINI
 UL_IRQ_LOCK_GINI	/* Inicialize global uLan spinlock */
#endif

#ifdef UL_GLOBAL_IRQ_LOCK_GINI
 UL_GLOBAL_IRQ_LOCK_GINI /* Initialize global IRQ lock,
 			    very bad if needed */
#endif

#ifdef ENABLE_UL_MEM_CHECK
atomic_t ul_mem_check_counter=ATOMIC_INIT(0);
#endif /* ENABLE_UL_MEM_CHECK */

int uld_null_fnc(ul_drv *udrv, int ret_code)
{
  return 0;
}

#ifndef UL_WITH_FRAME_FSM
  /* empty watchdog timer function */
  #ifdef _WIN32
  VOID NTAPI ulan_wd_dpc(IN PKDPC Dpc,IN PVOID data,
                    IN PVOID arg1,IN PVOID arg2) {;}
 #elif defined(FOR_LINUX_KERNEL)
  void ulan_do_wd_timer(KC_TIMER_HANDLER_ARGS)
 #else
  void ulan_do_wd_timer(unsigned long data)
 #endif /* _WIN32 */
#endif /*UL_WITH_FRAME_FSM*/

/* uLan state automata tracer code */
#ifdef UL_WITH_TRACER
#include "ul_tracer.c"
#else /*UL_WITH_TRACER*/
#define uld_tracer_log_fsm_poll(ret)
#define uld_tracer_log_fsm_fnc(fnc, ret)
#define uld_tracer_log_fsm_timeout(ret)
#define uld_tracer_log_mark(val)
#endif /*UL_WITH_TRACER*/

/* Data iterator */
#include "ul_di.c"

/* uLan memory management and bidirection linked lists */
#include "ul_mem.c"

#ifdef UL_WITH_IAC
int uld_iac_immediate(ul_drv *udrv, ul_mem_blk *mes);
#endif /*UL_WITH_IAC*/

#ifdef CONFIG_OC_UL_DRV_WITH_VIRTUAL
  /* Virtual/loopback schip driver */
  #include "ul_cvirtual.c"
#endif /*CONFIG_OC_UL_DRV_WITH_VIRTUAL*/

#ifdef UL_WITH_UART_450
  /* Chip driver for 16450 */
  #include "ul_c450.c"
#endif /*UL_WITH_UART_450*/

#ifdef UL_WITH_UART_PL011
  /* Chip driver for PL011 */
  #include "ul_cpl011.c"
#endif /*UL_WITH_UART_PL010*/

#ifdef UL_WITH_UART_510
  /* Chip driver for 82510 */
  #include "ul_c510.c"
#endif /*UL_WITH_UART_510*/

#ifdef UL_WITH_PCI
  #include "ul_c950pci.c"
#endif

#ifdef UL_WITH_UART_MPC52xx_PSC
  #include "ul_cmpc52xx.c"
#endif /*UL_WITH_UART_MPC52xx_PSC*/

#ifdef UL_WITH_USB
  /* Petr Smolik 1 */ 
  #include "ul_cps1.c"
#endif /*UL_WITH_USB*/

#ifdef UL_WITH_FRAME_FSM
  /* Frame begin/end */
  #include "ul_frame.c"
#endif /*UL_WITH_FRAME_FSM*/

#ifdef UL_WITH_IAC
  #include "ul_iac.c"
#endif /*UL_WITH_IAC*/

#ifdef UL_WITH_FRAME_FSM
  /* uLan main driver automata loop */
  #include "ul_ufsm.c"
#endif /*UL_WITH_FRAME_FSM*/

/* Supported devices table */ 
#include "ul_devtab.c"

/* initialize and free of one driver */ 
#include "ul_tors.c"

#undef UL_INT_TST

#ifdef UL_INT_TST
  /* Some internal test routines */
  #include "ul_tst.c"
#endif /* UL_INT_TST */

#include "ul_debug.c"

#include "ul_base.c"

/*******************************************************************/
/* Linux kernel and module specific part */ 

#ifdef FOR_LINUX_KERNEL
  #include "ul_linux.c"

  #ifdef UL_WITH_PCI
    /* Linux kernel PCI device support */ 
    #include "ul_linpci.c"
  #endif /*UL_WITH_PCI*/

  #ifdef UL_WITH_OF
    /* Linux kernel OpenFirmware device support */ 
    #include "ul_linof.c"
  #endif /*UL_WITH_OF*/

  #ifdef UL_WITH_AMBA
    /* Linux kernel ARM AMBA bus support */
    #include "ul_linamba.c"
  #endif /*UL_WITH_AMBA*/

  #ifdef UL_WITH_USB
    /* Linux kernel PCI device support */
    #include "ul_linusb.c"
  #endif /*UL_WITH_USB*/

#endif /* FOR_LINUX_KERNEL */

#ifdef UL_DRV_IN_LIB
  /* Interface for driver in library */
  #include "ul_inlib.c"
#endif /* UL_DRV_IN_LIB */

#ifdef __DJGPP__
  /* Interface for driver in library for DJGPP environment */
  #include "ul_djgpp.c"
#endif /* __DJGPP__ */

#if defined(CONFIG_OC_UL_DRV_SYSLESS)
  /* Interface for driver in library for SYSLESS environment */
  #include "ul_sysless.c"
#endif /* CONFIG_OC_UL_DRV_SYSLESS */

#if defined(CONFIG_OC_UL_DRV_NUTTX)
  /* Interface for driver in library for SYSLESS environment */
  #include "ul_nuttx.c"
#endif /* CONFIG_OC_UL_DRV_SYSLESS */

/*******************************************************************/
/* NT Kernel Mode Driver specific code */ 

#ifdef _WIN32

#include "ul_wdbase.h"

#include "ul_wdbase.c"

#ifndef FOR_WIN_WDM
  #include "ul_kdmnt.c"
#endif /* !FOR_WIN_WDM */


#ifdef FOR_WIN_WDM

#ifdef UL_WITH_USB
  #include "ul_wdusb.c"
#endif /*UL_WITH_USB*/

#include "ul_wdent.c"

#include "ul_wdpnp.c"

#ifdef UL_WITH_WIN_PWR
  #include "ul_wdpwr.c"
#endif /* UL_WITH_WIN_PWR */

#endif /* FOR_WIN_WDM */


#endif /* _WIN32 */
