/*******************************************************************
  uLan Communication - user visible definitions

  ul_fddir.h	- defines for actual access to the local driver

  (C) Copyright 1996-2016 by Pavel Pisa - project originator
        http://cmp.felk.cvut.cz/~pisa
  (C) Copyright 1996-2016 PiKRON Ltd.
        http://www.pikron.com
  (C) Copyright 2002-2016 Petr Smolik


  The uLan driver project can be used and distributed
  in compliance with any of next licenses
   - GPL - GNU Public License
     See file COPYING for details.
   - LGPL - Lesser GNU Public License
   - MPL - Mozilla Public License
   - and other licenses added by project originator

  Code can be modified and re-distributed under any combination
  of the above listed licenses. If contributor does not agree with
  some of the licenses, he/she can delete appropriate line.
  WARNING: if you delete all lines, you are not allowed to
  distribute code or sources in any form.
 *******************************************************************/

#ifndef _UL_FDDIR_H
#define _UL_FDDIR_H

#ifdef __cplusplus
extern "C" {
#endif

#ifdef _WIN32

 #define UL_DEV_NAME "\\\\.\\UL_DRV"

 typedef HANDLE ul_fd_direct_t;

 #define UL_FD_DIRECT_INVALID INVALID_HANDLE_VALUE
 #if !defined(_SSIZE_T_) && !defined(_SSIZE_T_DEFINED)
 typedef long ssize_t;
 #endif

 #define WITHOUT_SYS_SELECT
 #define HAS_GETOPT_LONG

 #ifdef WITH_UL_FD_INDIRECT
 #define WIN32_FILE_OVERLAPPED
 #endif /* WITH_UL_FD_INDIRECT */
 
 #if defined(WIN32_FILE_OVERLAPPED)&&!defined(WITH_UL_FD_INDIRECT)
   #error to enable WIN32_FILE_OVERLAPPED operation is neccessary compile sources with flag WITH_UL_FD_INDIRECT
 #endif

#elif defined(__DJGPP__) || defined(CONFIG_OC_UL_DRV_SYSLESS) || defined(CONFIG_OC_UL_DRV_USLIB)

 #ifdef __DJGPP__
   #define UL_DEV_NAME "1"
 #else /* !__DJGPP__ */
   #define UL_DEV_NAME "/dev/ulan"
 #endif  /* !__DJGPP__ */

 struct ul_opdata;
 typedef struct ul_opdata *ul_fd_direct_t;
 #define UL_FD_DIRECT_INVALID (NULL)

 #define WITHOUT_SYS_SELECT
 #define UL_DRV_IN_LIB

#else /* !_WIN32 && !__DJGPP__ */

 #define UL_DEV_NAME "/dev/ulan"

 typedef int ul_fd_direct_t;

 #define UL_FD_DIRECT_INVALID (-1)

 #define HAS_GETDELIM
 #ifndef CONFIG_OC_UL_DRV_NUTTX
   #define HAS_GETOPT_LONG
#endif

#endif /* _WIN32 */

#ifdef __cplusplus
} /* extern "C"*/
#endif

#endif /*_UL_FDDIR_H*/
