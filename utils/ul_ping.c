/*******************************************************************
  uLan Communication - simple test communication

  ul_ping.c	- 

  (C) Copyright 1996,1999 by Pavel Pisa 

  The uLan driver is distributed under the Gnu General Public Licence. 
  See file COPYING for details.
 *******************************************************************/

#include <unistd.h>
#include <stdlib.h>
#include <fcntl.h>
#include <stdio.h>
#include <getopt.h>
#include <errno.h>
#include <string.h>
#include <sys/types.h>
#include <sys/time.h>
#include <sys/stat.h>
#include <math.h>
#include <ul_lib/ulan.h>
#include <signal.h>

#ifdef CONFIG_OC_UL_DRV_NUTTX
#ifndef SIGTERM
#define SIGTERM SIGUSR1
#endif
#ifndef SIGINT
#define SIGINT SIGUSR1
#endif
#endif /*CONFIG_OC_UL_DRV_NUTTX*/

/*******************************************************************/

char *ul_dev_name = UL_DEV_NAME;
int module= 3;
int interval = 10;
int prt_modules= 0;
int debugk     = 0;
int debugk_flg = 0;
int terminate_flg = 0;


int timeval_subtract (struct timeval *result,
                      struct timeval *x,
                      struct timeval *y)
{
  /* Perform the carry for the later subtraction by updating y. */
  if (x->tv_usec < y->tv_usec) {
    int nsec = (y->tv_usec - x->tv_usec) / 1000000 + 1;
    y->tv_usec -= 1000000 * nsec;
    y->tv_sec += nsec;
  }
  if (x->tv_usec - y->tv_usec > 1000000) {
    int nsec = (y->tv_usec - x->tv_usec) / 1000000;
    y->tv_usec += 1000000 * nsec;
    y->tv_sec -= nsec;
  }

  /* Compute the time remaining to wait.
     tv_usec is certainly positive. */
  result->tv_sec = x->tv_sec - y->tv_sec;
  result->tv_usec = x->tv_usec - y->tv_usec;

  /* Return 1 if result is negative. */
  return x->tv_sec < y->tv_sec;
}

int print_modules(int max_addr)
{ int ret;
  int i;
  ul_fd_t ul_fd;
  uchar *p;
  void *buf=NULL;
  int buf_len;
  int count=0; 
  ul_fd=ul_open(ul_dev_name, NULL);
  if(ul_fd==UL_FD_INVALID)
    { perror("print_modules : uLan open failed");return -1;};
  for(i=1;i<=max_addr;i++)
  {
    ret=ul_send_query_wait(ul_fd,i,UL_CMD_SID,
  			UL_BFL_NORE|UL_BFL_PRQ,NULL,0,&buf,&buf_len);
    if(ret>=0)
    { count++;
      p=(uchar*)buf;
      for(ret=0;(ret<buf_len)&&*p;ret++,p++);
      printf("%2d:  ",i);
      fwrite(buf,ret,1,stdout);
      printf("\n");
    };
    if(buf) free(buf);
    buf=NULL;
  };
  ul_close(ul_fd);
  return count;
};

int debug_kernel(int debug_msk)
{ int ret;
  ul_fd_t ul_fd;
  ul_fd=ul_open(ul_dev_name, NULL);
  if(ul_fd==UL_FD_INVALID) { perror("send_cmd_go : uLan open failed");return -1;};
  ret=ul_drv_debflg(ul_fd,debug_msk);
  ul_close(ul_fd);
  return ret;
};


void exec_ping(void)
{ int ret;
  ul_fd_t ul_fd;
  ul_msginfo msginfo;
  struct timeval tv_diff,tv_start,tv_end;
  struct timeval tv_start_ping,tv_end_ping;
  uchar buf_out[10];
  uchar *buf=NULL;
  int msg_flg;
  int rec_ok;
  long int tdiff_ms;
  int stat_cnt;
  int stat_rec;
  int stat_lost;
  long int stat_tmin_ms;
  long int stat_tmax_ms;

  printf("UL_PING %d.\n",module);

  ul_fd=ul_open(ul_dev_name, NULL);
  if(ul_fd==UL_FD_INVALID) { perror("exec_ping : uLan open failed");return;};

  memset(&msginfo,0,sizeof(ul_msginfo));
  msginfo.cmd=UL_CMD_NCS;
  ul_addfilt(ul_fd,&msginfo);  

  stat_cnt=0;
  stat_rec=0;
  stat_tmin_ms=0;
  stat_tmax_ms=0;

  gettimeofday(&tv_start_ping, NULL);

  buf_out[0]=ULNCS_SID_RQ;
  while(!terminate_flg) {
    gettimeofday(&tv_start, NULL);

    msg_flg=UL_BFL_NORE;
    ret=ul_send_command(ul_fd,module,UL_CMD_NCS,
                        msg_flg,buf_out,1);
    if (ret<0) break;

    stat_cnt++;
    printf("1 byte to %d, seq %d",module, stat_cnt);

    rec_ok=0;
    while (!terminate_flg) {
      ret=ul_fd_wait(ul_fd,5);
      if(ret<0) break;
      ret=ul_acceptmsg(ul_fd,&msginfo);
      if(ret<0) break;

      if ((msginfo.cmd!=UL_CMD_NCS) || (msginfo.len<3)) { 
        ul_freemsg(ul_fd);
        continue;
      }
      buf=malloc(msginfo.len);
      ret=ul_read(ul_fd,buf,msginfo.len);
      ul_freemsg(ul_fd);
      if ((buf[0]==ULNCS_SID_RPLY) && (msginfo.len>=5)) {
        stat_rec++;
        rec_ok=1;
      }
      free(buf);
      buf=NULL;
      break;
    }

    if (terminate_flg) {
      if(rec_ok)
        stat_rec--;
      stat_cnt--;
      printf("\n");
      break;
    } else {
      if (rec_ok) {
        printf(", sid_rply len %d",msginfo.len-5);
      } else {
        printf(", no sid_rply");
      }
    }

    gettimeofday(&tv_end, NULL);
    timeval_subtract(&tv_diff,&tv_end,&tv_start);
    tdiff_ms=tv_diff.tv_sec*1000+tv_diff.tv_usec/1000;

    if ((tdiff_ms>0) && (rec_ok)) {
      /* min */
      if (stat_tmin_ms!=0) {
        if (tdiff_ms<stat_tmin_ms)
          stat_tmin_ms=tdiff_ms;
      } else 
        stat_tmin_ms=tdiff_ms;
      /* max */
      if (tdiff_ms>stat_tmax_ms)
        stat_tmax_ms=tdiff_ms;
    }

    printf(", time=%ld miliseconds",tdiff_ms);
    printf("\n");

    if (!terminate_flg) {
     #ifdef _WIN32
      Sleep(100*interval);
     #else
      usleep(100000*interval);
     #endif
    }
  }

  gettimeofday(&tv_end_ping, NULL);

  ul_close(ul_fd);

  /* show statistics */
  printf("\n");
  printf("--- %d ul_ping statistics ---\n",module);

  stat_lost=0;
  if (stat_cnt>0) {
    stat_lost=(stat_cnt-stat_rec)*100/stat_cnt;
  }

  timeval_subtract(&tv_diff,&tv_end_ping,&tv_start_ping);
  tdiff_ms=tv_diff.tv_sec*1000+tv_diff.tv_usec/1000;

  printf("%d packets transmitted, %d received, %d%% packet loss, time %ldms\n",stat_cnt,stat_rec,stat_lost,tdiff_ms);
  printf("rtt min/max = %ld/%ld ms\n",stat_tmin_ms,stat_tmax_ms);
}

void sig_usr(int signo) {
  if ((signo==SIGTERM) || (signo==SIGINT)) {
    terminate_flg=1;
  }
}

static void
usage(void)
{
  printf("Usage: ul_ping <parameters> module\n");
  printf("  -d, --uldev <name>       name of uLan device [%s]\n", ul_dev_name);
  printf("  -m, --module <num>       target uLan module\n");
  printf("  -i, --interval <num>     wait interval in 0.1 seconds between sending each packet [%d].\n",interval);
  printf("  -p, --print <max>        print modules to max address\n");
  printf("      --debug-kernel <m>   flags to debug kernel\n");
  printf("  -V, --version            show version\n");
  printf("  -h, --help               this usage screen\n");
}

int main(int argc,char *argv[])
{
  static struct option long_opts[] = {
    { "uldev", 1, 0, 'd' },
    { "module",1, 0, 'm' },
    { "interval",1, 0, 'i' },
    { "print", 1, 0, 'p' },
    { "version",0,0, 'V' },
    { "help",  0, 0, 'h' },
    { "debug-kernel", 1, 0, 'D' },
    { 0, 0, 0, 0}
  };
  int opt;

 #ifndef HAS_GETOPT_LONG
  while ((opt = getopt(argc, argv, "d:m:i:p:DVh")) != EOF)
 #else
  while ((opt = getopt_long(argc, argv, "d:m:i:p:DVh",
			    &long_opts[0], NULL)) != EOF)
 #endif
    switch (opt) {
    case 'd':
      ul_dev_name=optarg;
      break;
    case 'm':
      module = strtol(optarg,NULL,0);
      break;
    case 'i':
      interval = strtol(optarg,NULL,0);
      break;
    case 'p':
      prt_modules = strtol(optarg,NULL,0);
      break;
    case 'D':
      debugk = strtol(optarg,NULL,0);
      debugk_flg=1;
      break;
    case 'V':
      fputs("uLan ping v1.1\n", stdout);
      exit(0);
    case 'h':
    default:
      usage();
      exit(opt == 'h' ? 0 : 1);
    }

  /* init signal handling */
  signal(SIGINT,  sig_usr);

  if(debugk_flg) debug_kernel(debugk);

  if(prt_modules) print_modules(prt_modules);

  exec_ping();

  return 0;
}
