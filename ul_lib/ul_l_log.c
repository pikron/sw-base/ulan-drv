/*******************************************************************
  uLan Communication - low level C and driver interface library

  ul_l_log.c	- simple logging support

  (C) Copyright 1996-2016 by Pavel Pisa - project originator
        http://cmp.felk.cvut.cz/~pisa
  (C) Copyright 1996-2016 PiKRON Ltd.
        http://www.pikron.com
  (C) Copyright 2002-2016 Petr Smolik


  The uLan driver project can be used and distributed
  in compliance with any of next licenses
   - GPL - GNU Public License
     See file COPYING for details.
   - LGPL - Lesser GNU Public License
   - MPL - Mozilla Public License
   - and other licenses added by project originator

  Code can be modified and re-distributed under any combination
  of the above listed licenses. If contributor does not agree with
  some of the licenses, he/she can delete appropriate line.
  WARNING: if you delete all lines, you are not allowed to
  distribute code or sources in any form.
 *******************************************************************/

//#ifndef _MSC_VER
//#include <unistd.h>
//#endif /*_MSC_VER*/
#include <stdlib.h>
//#include <fcntl.h>
#include <stdio.h>
#include <sys/types.h>
//#include <sys/time.h>
//#include <sys/stat.h>
//#include <string.h>
#include <stdarg.h>
#include <ul_lib/ulan.h>
#include <ul_lib/ul_l_log.h>

void
ul_log_fnc_default(struct ul_log_domain *domain, int level,
	const char *format, va_list args);

ul_log_fnc_t *ul_log_output;
FILE *ul_log_default_file;

void
ul_log(struct ul_log_domain *domain, int level,
       const char *format, ...)
{
  va_list ap;
  if(ul_log_output==NULL) {
    char *log_fname;
    ul_log_output=ul_log_fnc_default;
    if((log_fname=getenv("UL_LOG_FILENAME"))!=NULL){
      ul_log_default_file=fopen(log_fname,"a");
    }
    if(ul_log_default_file==NULL)
      ul_log_default_file=stderr;
  }
  va_start(ap, format);
  (*ul_log_output)(domain,level,format,ap);
  va_end(ap);
}

void
ul_log_redir(ul_log_fnc_t *log_fnc, int add_flags)
{
  if(log_fnc==NULL) log_fnc=ul_log_fnc_default;
  ul_log_output=log_fnc;
}

void
ul_log_fnc_default(struct ul_log_domain *domain, int level,
	const char *format, va_list ap)
{
  if(!(level&UL_LOGL_CONT)) {
    level&=UL_LOGL_MASK;
    fprintf(ul_log_default_file,"<%d>: ",level);
  }
  vfprintf(ul_log_default_file,format, ap);
  fflush(ul_log_default_file);
}

