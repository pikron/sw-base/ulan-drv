/*******************************************************************
  uLan Communication - uL_DRV - multiplatform uLan driver

  ul_cvirtual.c	- virtual/lopback chip driver

  (C) Copyright 1996-2009 by Pavel Pisa - project originator
        http://cmp.felk.cvut.cz/~pisa
  (C) Copyright 1996-2009 PiKRON Ltd.
        http://www.pikron.com
  (C) Copyright 2002-2009 Petr Smolik


  The uLan driver project can be used and distributed
  in compliance with any of next licenses
   - GPL - GNU Public License
     See file COPYING for details.
   - LGPL - Lesser GNU Public License
   - MPL - Mozilla Public License
   - and other licenses added by project originator

  Code can be modified and re-distributed under any combination
  of the above listed licenses. If contributor does not agree with
  some of the licenses, he/she can delete appropriate line.
  WARNING: if you delete all lines, you are not allowed to
  distribute code or sources in any form.
 *******************************************************************/

/* Definition of chip_buff usage */
#define ul_virtual_daemon_st chip_buff[0] /* completion status of  ?_complete_in */

enum ul_virtual_daemon_st_cases {
  UL_VIRTUAL_ST_NONE=0,
  UL_VIRTUAL_ST_WAITING,
  UL_VIRTUAL_ST_READY,
  UL_VIRTUAL_ST_ERROR_RETRY,
  UL_VIRTUAL_ST_ERROR_FATAL,
  UL_VIRTUAL_ST_CANCEL_STARTED,
  UL_VIRTUAL_ST_CANCEL_COMPLETE,
};

INLINE int ul_virtual_daemon_st_get(const ul_drv *udrv)
{
  int daemon_st;
  UL_MB();
  daemon_st=*(volatile unsigned *)&udrv->ul_virtual_daemon_st;
  UL_MB();
  return daemon_st;
}

INLINE void ul_virtual_daemon_st_set(const ul_drv *udrv, int daemon_st)
{
  UL_MB();
  *(volatile unsigned *)&udrv->ul_virtual_daemon_st=daemon_st;
  UL_MB();
}

INLINE int ul_virtual_daemon_st_xchg(ul_drv *udrv, int daemon_st)
{
 #ifndef _WIN32
  UL_MB();
  daemon_st=xchg(&udrv->ul_virtual_daemon_st,daemon_st);
  UL_MB();
 #else
  daemon_st=InterlockedExchange(&udrv->ul_virtual_daemon_st,daemon_st);
 #endif
  return daemon_st;
}

/*** Worker thread for virtual/loopback device support ***/
static int ul_virtual_thread(void *ptr)
{
  ul_drv *udrv=(ul_drv *)ptr;
  int ret;
  int fatal_err=0;
  ul_mem_blk *act_mes=NULL;
  /*ul_mem_blk *frame;*/
  int async_op_scheduled=0;
  volatile unsigned daemon_st;
 #ifdef _WIN32
  PIRP in_irp=NULL;
 #endif /*_WIN32*/

  if(udrv->magic!=UL_DRV_MAGIC)
  {
    LOG_FATAL(KERN_CRIT "ul_virtual_thread: Wrong uLan MAGIC number!!!\n");
    uld_kwt_set_stopped(udrv);
    return -ENODEV;
  }

#ifdef FOR_LINUX_KERNEL
  /*daemonize("kulvirtuald");*/
  /*allow_signal(SIGKILL);*/
#endif /*FOR_LINUX_KERNEL*/

  ul_virtual_daemon_st_set(udrv,UL_VIRTUAL_ST_NONE);

  while(!uld_kwt_should_stop(udrv) && !fatal_err){

    uld_kwt_confirm(udrv);

    while(1) {
      int addr_match;
      int check_dest=0;

      /* Initiate sending of message from the prep_bll queue */
      if(!(act_mes=udrv->con_message=udrv->prep_bll.first)) break;

      /* printudrvbll(udrv); */
      ul_bll_move_mes(&udrv->work_bll,act_mes);
      LOG_MESSAGES(KERN_INFO "ul_virtual: KWT processing message\n");

      addr_match=uld_adr_match(udrv,UL_BLK_HEAD(act_mes).dadr);

      if(UL_BLK_HEAD(act_mes).flg&UL_BFL_ARQ) {
        check_dest=1;
      } else if((UL_BLK_HEAD(act_mes).flg&UL_BFL_TAIL)&&
                (UL_BLK_HEAD(act_mes).next!=NULL)) {
        if(UL_BLK_HEAD(UL_BLK_HEAD(act_mes).next).flg&UL_BFL_REC)
          check_dest=1;
      }

      if(check_dest &&
         (addr_match!=UL_ADR_MATCH_BROADCAST) &&
         (addr_match!=UL_ADR_MATCH_MINE)) {
        /* mark message as filed */
        UL_BLK_HEAD(act_mes).flg|=UL_BFL_FAIL;
        ul_bll_move_mes(&udrv->proc_bll,act_mes);
        SCHEDULE_BH(udrv);
        /* ulan_do_bh(udrv); */
	act_mes=udrv->con_message=NULL;
        continue;
      }

     #ifdef UL_WITH_IAC
      if((UL_BLK_HEAD(act_mes).flg&UL_BFL_PRQ)&&
         ((addr_match==UL_ADR_MATCH_MINE)||(addr_match==UL_ADR_MATCH_BROADCAST))) {
        int ret_code;
        ret_code=uld_iac_immediate(udrv, act_mes);
        if((ret_code<0) && check_dest)
          UL_BLK_HEAD(act_mes).flg|=UL_BFL_FAIL;
      }
     #endif /*UL_WITH_IAC*/

      if(UL_BLK_HEAD(act_mes).flg&UL_BFL_M2IN || UL_BLK_HEAD(act_mes).flg&UL_BFL_FAIL ||
	       (uld_adr_match(udrv,UL_BLK_HEAD(act_mes).dadr)!=UL_ADR_MATCH_NONE)){
        ul_bll_move_mes(&udrv->proc_bll,act_mes);
        SCHEDULE_BH(udrv);
      }else{
        ul_bll_free_mes(act_mes);
      }
      act_mes=NULL;
    }

    UL_MB();
    daemon_st=ul_virtual_daemon_st_get(udrv);
    UL_MB();

    if(daemon_st==UL_VIRTUAL_ST_READY) {
      UL_MB();
    }

    if(daemon_st!=UL_VIRTUAL_ST_WAITING)
    {
      if(daemon_st==UL_VIRTUAL_ST_ERROR_FATAL){
        LOG_FATAL(KERN_INFO "ul_virtual: problem detected\n");
        fatal_err=1;
        continue;
      }
      UL_MB();
      if(uld_kwt_should_stop(udrv)) {
        ul_virtual_daemon_st_set(udrv,UL_VIRTUAL_ST_NONE);
        break;
      }
      LOG_IRQ(KERN_INFO "ul_virtual: initiate wait for data\n");
      ul_virtual_daemon_st_set(udrv,UL_VIRTUAL_ST_WAITING);
      /*async_op_scheduled=1;*/
      if(0) {
        LOG_FATAL(KERN_INFO "ul_virtual: KWT fatal error %d\n",ret);
        fatal_err=1;
        ul_virtual_daemon_st_set(udrv,UL_VIRTUAL_ST_NONE);
      }
    } else {
      LOG_IRQ(KERN_INFO "ul_virtual: KWT sleeping\n");
      if(uld_kwt_wait(udrv)!=ULD_KWT_WAIT_SUCCESS) {
         LOG_FATAL(KERN_INFO "ul_virtual: KWT wait for event interrupted/killed, stopping\n");
         break;
      }
      LOG_IRQ(KERN_INFO "ul_virtual: KWT wakeup\n");
    }
  }

  UL_MB();
  if(async_op_scheduled) {
    #ifdef _WIN32
      daemon_st=ul_virtual_daemon_st_xchg(udrv,UL_VIRTUAL_ST_CANCEL_STARTED);
      if (in_irp && (daemon_st==UL_VIRTUAL_ST_WAITING)) {
        IoCancelIrp(in_irp);
        daemon_st=ul_virtual_daemon_st_xchg(udrv,UL_VIRTUAL_ST_CANCEL_COMPLETE);
	if (daemon_st!=UL_VIRTUAL_ST_CANCEL_STARTED) {
          IoCompleteRequest(in_irp, IO_NO_INCREMENT);
        }
        do {
          UL_MB();
          daemon_st=ul_virtual_daemon_st_get(udrv);
          if(daemon_st!=UL_VIRTUAL_ST_CANCEL_COMPLETE)
            break;
          LOG_IRQ(KERN_INFO "ul_virtual: finalize KWT sleeping\n");
          uld_kwt_wait(udrv);
          uld_kwt_confirm(udrv);
          LOG_IRQ(KERN_INFO "ul_virtual: finalize KWT wakeup\n");
        } while(1);
      }
    #endif /*_WIN32*/
  }

  LOG_IRQ(KERN_INFO "ul_virtual: KWT finished\n");
  uld_kwt_finished(udrv);
  return 0;
}

/*** Wake up worker thread  ***/
static int ul_virtual_stroke(ul_drv *udrv)
{
  uld_kwt_wake(udrv);
  return 0;
}

/*** Control functions of chip driver  ***/
static int ul_virtual_rqirq(ul_drv *udrv)
{ /* request IRQ handling - kernel worker thread*/
  if(uld_kwt_test_and_set_prepared(udrv)) {
    LOG_IRQ("ul_virtual_cctrl: KWT already running\n");
    return 0;
  }
  uld_atomic_clear_dfl(udrv,KWTKILL);

  if(uld_kwt_run(udrv, ul_virtual_thread, "kulvirtuald")<0) {
    LOG_FATAL(KERN_CRIT "ul_virtual_cctrl: Cannot fork KWT thread!!!\n");
    /*uld_atomic_clear_dfl(udrv,KWTRUN);*/
    uld_kwt_set_stopped(udrv);
    return UL_RC_EKWT;
  }
  LOG_IRQ("ul_virtual_cctrl: KWT forked\n");
  return 0;
}

static int ul_virtual_freeirq(struct ul_drv *udrv)
{ /* release IRQ handling - kernel worker thread*/
  if(!uld_kwt_is_running(udrv)) {
    LOG_IRQ("ul_virtual_cctrl: KWT already stopped\n");
    return 0;
  }
  uld_kwt_kill(udrv);
  uld_kwt_wait_stopped(udrv);
  LOG_IRQ("ul_virtual_cctrl: KWT stopped\n");
  return 0;
}

const ul_drv_chip_ops ul_virtual_chip_ops = {
  "virtual",		/* chip_type - text type identification*/
  NULL,			/* fnc_recch */
  NULL,			/* fnc_sndch */
  NULL,			/* fnc_wait */
  NULL,			/* fnc_connect */
  NULL,			/* fnc_finishtx */
  NULL,			/* fnc_pool */
  NULL,			/* fnc_cctrl */
  ul_virtual_stroke,	/* fnc_stroke */

  NULL,			/* fnc_txoe */
  NULL,			/* fnc_pinit */
  NULL,			/* fnc_pdone */
  NULL,			/* fnc_activate */
  NULL,			/* fnc_genirq */
  ul_virtual_rqirq,	/* fnc_rqirq */
  ul_virtual_freeirq,	/* fnc_freeirq */
  NULL,			/* fnc_hwtest */
  ul_drv_common_fnc_setmyadr,	/* fnc_setmyadr */
  ul_drv_common_fnc_setpromode,	/* fnc_setpromode */
};

/*** Petr Smolik's USB2ULAN version 1 initialize ***/
int ul_virtual_init(ul_drv *udrv, ul_physaddr_t physbase, int irq, int baud, long baudbase, int options)
{
  udrv->chip_options=options;
  if(!baud) baud=19200;
  udrv->baud_val=baud;
  if(!baudbase)
    baudbase=57600;
  udrv->baud_base=baudbase;
  udrv->baud_div=(udrv->baud_base+baud/2)/baud;
  udrv->physbase=physbase;
  udrv->irq=0;
  udrv->chip_ops=&ul_virtual_chip_ops;

  return 0;
};


