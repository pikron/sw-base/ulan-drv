/*******************************************************************
  uLan Communication - simple test client

  ul_lcabsp.c	- pasive receive and show absorbancy 
 		  send by LCD5000

  (C) Copyright 1996,1999 by Pavel Pisa 

  The uLan driver is distributed under the Gnu General Public Licence. 
  See file COPYING for details.
 *******************************************************************/

#include <unistd.h>
#include <stdlib.h>
#include <fcntl.h>
#include <stdio.h>
#include <getopt.h>
#include <errno.h>
#include <string.h>
#include <sys/types.h>
#include <sys/time.h>
#include <sys/stat.h>
#include <math.h>
#include <ul_lib/ulan.h>

#ifndef WITHOUT_SYS_SELECT
#include <sys/select.h>
#endif

#define UL_CMD_LCDABS	0x4f	/* Absorbance data block */
#define UL_CMD_LCDMRK	0x4e	/* Mark */

/*******************************************************************/

char *ul_dev_name = UL_DEV_NAME;
int module= 0;
int prt_modules= 0;
int debugk     = 0;
int debugk_flg = 0;
int samples_count = 0;
int hires_flg = 0;

int print_modules(int max_addr)
{ int ret;
  int i;
  ul_fd_t ul_fd;
  uchar *p;
  void *buf=NULL;
  int buf_len;
  int count=0; 
  ul_fd=ul_open(ul_dev_name, NULL);
  if(ul_fd==UL_FD_INVALID)
    { perror("print_modules : uLan open failed");return -1;};
  for(i=1;i<=max_addr;i++)
  {
    ret=ul_send_query_wait(ul_fd,i,UL_CMD_SID,
  			UL_BFL_NORE|UL_BFL_PRQ,NULL,0,&buf,&buf_len);
    if(ret>=0)
    { count++;
      p=(uchar*)buf;
      for(ret=0;(ret<buf_len)&&*p;ret++,p++);
      printf("%2d:  ",i);
      fwrite(buf,ret,1,stdout);
      printf("\n");
    };
    if(buf) free(buf);
    buf=NULL;
  };
  ul_close(ul_fd);
  return count;
};

int debug_kernel(int debug_msk)
{ int ret;
  ul_fd_t ul_fd;
  ul_fd=ul_open(ul_dev_name, NULL);
  if(ul_fd==UL_FD_INVALID) { perror("send_cmd_go : uLan open failed");return -1;};
  ret=ul_drv_debflg(ul_fd,debug_msk);
  ul_close(ul_fd);
  return ret;
};

float IEEE4tofloat(void *p)
{
 uchar *up=p;
 long mantisa;
 int exponent;
 mantisa=up[0]|((unsigned)up[1]<<8)|
         ((unsigned long)(up[2] & 0x7f)<<16);
 exponent=(up[2]>>7)|(((int)up[3]&0x7f)<<1);
 if(exponent) {
   mantisa|=0x800000;
 } else {
   if(mantisa)
     exponent++;
 }
 if(up[3]&0x80) mantisa=-mantisa;
 return ldexp((float)mantisa,exponent-0x7f-23);
};

int receive_abs(void)
{
  int ret;
  ul_fd_t ul_fd;
  ul_msginfo msginfo;
  uchar  buf[10];
  struct timeval timeout;
  fd_set set;
  float  abs;

  ul_fd=ul_open(ul_dev_name, NULL);
  if(ul_fd==UL_FD_INVALID) { perror("receive_abs : uLan open failed");return -1;};
//  ioctl(ul_fd2sys_fd(ul_fd),UL_STROKE);  /* !!! will be removed */
  memset(&msginfo,0,sizeof(msginfo));
  msginfo.sadr=module;
  msginfo.cmd=UL_CMD_LCDABS;
  ret=ul_addfilt(ul_fd,&msginfo);
  if(ret<0) { printf("receive_abs : add filter failed\n");return ret;};
  msginfo.cmd=UL_CMD_LCDMRK;
  ret=ul_addfilt(ul_fd,&msginfo);
  if(ret<0) { printf("receive_abs : add filter failed\n");return ret;};

  for(;;)
  {
  #ifndef WITHOUT_SYS_SELECT
    FD_ZERO (&set);
    FD_SET (ul_fd2sys_fd(ul_fd), &set);
    FD_SET (0, &set);
    timeout.tv_sec = 10;
    timeout.tv_usec = 0;
    while ((ret=select(FD_SETSIZE,&set, NULL, NULL,&timeout))==-1
            &&errno==-EINTR);
    if(FD_ISSET(0,&set))
      {printf("\nreceive_abs : break by user\n");break;};
    if(ret<0) return ret;
    if(!ret) continue;
  #else /* WITHOUT_SYS_SELECT */
    while(1) {
      if(ul_inepoll(ul_fd)) break;
      Sleep(100);
    };
  #endif /* WITHOUT_SYS_SELECT */
    ret=ul_acceptmsg(ul_fd,&msginfo);
    if(ret<0) 
    { printf("receive_abs : problem to accept message\n");
      return ret;
    };
    if(msginfo.flg&UL_BFL_FAIL) 
      {ul_freemsg(ul_fd);continue;};
    if(msginfo.cmd==UL_CMD_LCDABS)
    { while((ret=ul_read(ul_fd,buf,4))==4)
      {
        abs=IEEE4tofloat(buf);
        if (!hires_flg)
          printf("%9.6f\n",abs);
        else
          printf("%11.8f\n",abs);
	if(samples_count) {
	  samples_count--;
	  if(!samples_count)
	    return 0;
	}
      };
    }else 
    if(msginfo.cmd==UL_CMD_LCDMRK)
    {
      memset(buf,0,sizeof(buf));
      ul_read(ul_fd,buf,4);
      printf("MARK %d %d\n",buf[0],buf[1]);
    };
    ul_freemsg(ul_fd);
  };
  ul_close(ul_fd);
  return 0;
};

static void
usage(void)
{
  printf("Usage: ul_lcabsp <parameters> <hex_file>\n");
  printf("  -d, --uldev <name>       name of uLan device [%s]\n", ul_dev_name);
  printf("  -m, --module <num>       messages from/to module\n");
  printf("  -c, --samples-count <n>  stop after given number of samples received\n");
  printf("  -r, --high-resolution    higher resolution of printed value\n");
  printf("  -p, --print <max>        print modules to max address\n");
  printf("      --debug-kernel <m>   flags to debug kernel\n");
  printf("  -V, --version            show version\n");
  printf("  -h, --help               this usage screen\n");
}

int main(int argc,char *argv[])
{
  static struct option long_opts[] = {
    { "uldev", 1, 0, 'd' },
    { "module",1, 0, 'm' },
    { "samples-count",1, 0, 'c' },
    { "high-resolution",0, 0, 'r' },
    { "print", 1, 0, 'p' },
    { "version",0,0, 'V' },
    { "help",  0, 0, 'h' },
    { "debug-kernel", 1, 0, 'D' },
    { 0, 0, 0, 0}
  };
  int opt;

 #ifndef HAS_GETOPT_LONG
  while ((opt = getopt(argc, argv, "d:m:c:rp:Vh")) != EOF)
 #else
  while ((opt = getopt_long(argc, argv, "d:m:c:rp:Vh",
			    &long_opts[0], NULL)) != EOF)
 #endif
    switch (opt) {
    case 'd':
      ul_dev_name=optarg;
      break;
    case 'm':
      module = strtol(optarg,NULL,0);
      break;
    case 'c':
      samples_count = strtol(optarg,NULL,0);
      break;
    case 'r':
      hires_flg=1;
      break;
    case 'p':
      prt_modules = strtol(optarg,NULL,0);
      break;
    case 'D':
      debugk = strtol(optarg,NULL,0);
      debugk_flg=1;
      break;
    case 'V':
      fputs("uLan lcabs v1.1\n", stdout);
      exit(0);
    case 'h':
    default:
      usage();
      exit(opt == 'h' ? 0 : 1);
    }

  if(debugk_flg) debug_kernel(debugk);

  if(prt_modules) print_modules(prt_modules);

  receive_abs();

  return 0;
}
