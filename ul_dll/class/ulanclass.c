#include <windows.h>
#include <tchar.h>
#include <setupapi.h>
/*++
Routine Description:

    Entry-point a class installer.  This routine is only here
 so that we can use this dll to get the icon for the device
 class.

Arguments:

    InstallFunction - identifies the installation request to perform
    DeviceInfoSet - handle to the device information set
    DeviceInfoData - pointer to an SP_DEVINFO_DATA structure
       that contains information about the device to install.

Return Value:

    DWORD:  Always returns ERROR_DI_DO_DEFAULT to indicate using the
         default installer.
--*/
DWORD WINAPI ClassInstall(IN DI_FUNCTION InstallFunction,
     IN HDEVINFO DeviceInfoSet,
     IN PSP_DEVINFO_DATA DeviceInfoData)
{
 return ERROR_DI_DO_DEFAULT;
}