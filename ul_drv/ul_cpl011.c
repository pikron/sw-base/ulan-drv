/*******************************************************************
  uLan Communication - uL_DRV - multiplatform uLan driver

  ul_cpl011.c	- chip driver for pl011

  (C) Copyright 1996-2004 by Pavel Pisa - project originator
        http://cmp.felk.cvut.cz/~pisa
  (C) Copyright 1996-2004 PiKRON Ltd.
        http://www.pikron.com
  (C) Copyright 2002-2004 Petr Smolik


  The uLan driver project can be used and distributed
  in compliance with any of next licenses
   - GPL - GNU Public License
     See file COPYING for details.
   - LGPL - Lesser GNU Public License
   - MPL - Mozilla Public License
   - and other licenses added by project originator

  Code can be modified and re-distributed under any combination
  of the above listed licenses. If contributor does not agree with
  some of the licenses, he/she can delete appropriate line.
  WARNING: if you delete all lines, you are not allowed to
  distribute code or sources in any form.
 *******************************************************************/

/*******************************************************************/
/* Chip driver for pl011 */

#include <linux/amba/serial.h>

#define UART011_ITIP_CTS	0x08
#define UART011_ITIP_RXD	0x01

#define upl011_inw(base,port)	 ul_inw(ul_ioaddr_add(base,port))
#define upl011_outw(base,port,val) ul_outw(ul_ioaddr_add(base,port),val)

/*** Test interrupt request state ***/
static int upl011_pool(ul_drv *udrv)
{
  unsigned int u;
  if(uld_test_dfl(udrv,CHIPTIMER))
  {
    uld_clear_dfl(udrv,CHIPTIMER);
    return UL_RC_CHIPTIMER;
  }
  u=upl011_inw(udrv->iobase,UART011_MIS);
  LOG_CHIO(" pool:%d",u);
  upl011_outw(udrv->iobase,UART011_ICR,u&0xfff);  /* clear interrupts */
  return u;
}

/*** Wait end of transmit ***/
static int upl011_weot(ul_drv *udrv, int ret_code)
{
  int ret;
  unsigned fr,u,dr,uc;

  if(!udrv->chip_buff[0]||
     ((upl011_inw(udrv->iobase,UART011_CR)&UART011_CR_RTS)!=0))
  {
    UL_FRET;
    /* 9bit, even parity (space,low) */
    upl011_outw(udrv->iobase,UART011_LCRH,
                UART011_LCRH_SPS|UART01x_LCRH_WLEN_8|UART01x_LCRH_EPS|UART01x_LCRH_PEN);
    return UL_RC_PROC;
  }
  /* enable interrupts */
  upl011_outw(udrv->iobase,UART011_IMSC,UART011_RXIM);
  fr=upl011_inw(udrv->iobase,UART01x_FR);
  LOG_CHIO(" upl011_weot: 0x%x",fr);
  ret=UL_RC_WIRQ;

  if((fr&UART01x_FR_TMSK)==0)
  {
    LOG_CHIO(" TMI");
    ret=UL_RC_PROC;
  }
  if((fr&UART011_FR_RXFF)!=0) {
    if((u=udrv->chip_buff[1])) udrv->chip_buff[1]=0;
    else if((u=udrv->chip_buff[0])) udrv->chip_buff[0]=0;
    dr=upl011_inw(udrv->iobase,UART01x_DR);
    uc=dr&0xff;
    if((dr&(UART011_DR_OE|UART011_DR_BE|UART011_DR_FE))!=0)
      ret=UL_RC_EFRAME;
    if((u^uc)&0xff||!u)
    {
      LOG_CHIO(" TE%c!",u?'0':'1');
      ret=UL_RC_EBADCHR;
    }else{
      LOG_CHIO(".");
      if(ret>=0&&!udrv->chip_buff[0])
        ret=UL_RC_PROC;
    }
  }
  if(ret!=UL_RC_WIRQ)
  {
    /* 9bit, even parity (space,low) */
    upl011_outw(udrv->iobase,UART011_LCRH,
                UART011_LCRH_SPS|UART01x_LCRH_WLEN_8|UART01x_LCRH_EPS|UART01x_LCRH_PEN);
    UL_FRET;
  }
  LOG_CHIO(" weot ret %d ",ret);
  return ret;
}

/*** Receive character into char_buff ***/
static int upl011_recch_1(ul_drv *udrv, int ret_code);
static int upl011_recch_2(ul_drv *udrv, int ret_code);

static int upl011_recch(ul_drv *udrv, int ret_code)
{
  LOG_CHIO(" upl011_recch");
  udrv->char_buff=0;
  if((upl011_inw(udrv->iobase,UART011_CR)&UART011_CR_RTS)==0)
  {
    UL_FCALL2(upl011_weot,upl011_recch_1);
    return UL_RC_PROC;
  }
  UL_FNEXT(upl011_recch_2);
  return UL_RC_PROC;
}

static int upl011_recch_1(ul_drv *udrv, int ret_code)
{
  /* transmitter off, rx & tx on, uart on */
  upl011_outw(udrv->iobase,UART011_CR,UART011_CR_RTS|UART011_CR_RXE|UART011_CR_TXE|UART01x_CR_UARTEN);
  if(ret_code<0) {UL_FRET;return ret_code;}
  UL_FNEXT(upl011_recch_2);
  return UL_RC_PROC;
}

INLINE int upl011_recch_sub(ul_drv *udrv,int rec_char)
{ /* helper function to process received character */
  int rec_char_orig=rec_char;
  rec_char&=0xff;
  if(rec_char_orig&UART011_DR_PE) udrv->last_ctrl=rec_char|=0x100;
  else udrv->last_ctrl&=0x7F;
  udrv->char_buff=rec_char;
  udrv->xor_sum^=rec_char;udrv->xor_sum++;
  if(rec_char_orig&(UART011_DR_OE|UART011_DR_BE|UART011_DR_FE))
  {
    LOG_CHIO(" ER:%03X",udrv->char_buff);
    return UL_RC_EFRAME;
  }
  LOG_CHIO(" R:%03X",rec_char);
  return UL_RC_PROC;
}

static int upl011_recch_2(ul_drv *udrv, int ret_code)
{
  LOG_CHIO(" upl011_recch_2");
  upl011_outw(udrv->iobase,UART011_IMSC,UART011_RXIM);  /* enable interrupts */
  if((upl011_inw(udrv->iobase,UART01x_FR)&UART011_FR_RXFF)==0) {
    return UL_RC_WIRQ;
  }
  UL_FRET;
  return upl011_recch_sub(udrv,upl011_inw(udrv->iobase,UART01x_DR));
}

/*-- Helper functions for sndchar --*/

INLINE int upl011_sndch_readback(ul_drv *udrv,unsigned c) {
  unsigned u;
  uchar uc;
  if((u=udrv->chip_buff[1])) udrv->chip_buff[1]=0;
  else if((u=udrv->chip_buff[0])) udrv->chip_buff[0]=0;
  else u=0;
  uc=c&0xff;
  if((u^uc)&0xff||!u)
  { /* readback character error */
    LOG_CHIO(" TE%c expected: %04x received: %04x!",u?'2':'3',u,uc);
    return UL_RC_EBADCHR;
  }
  return 0;
}

/*** Send character from char_buff ***/
static int upl011_sndch_1(ul_drv *udrv, int ret_code);
static int upl011_sndch_2(ul_drv *udrv, int ret_code);

/*** Send character from char_buff ***/
static int upl011_sndch(ul_drv *udrv, int ret_code)
{
  unsigned fr,dr,uc;
  u64 ns_wt;
  int rt_flg=0;

  /* if in receive mode then wait one character transfer time before switch to tx */
  LOG_CHIO(" upl011_sndch 0x%x",udrv->char_buff);
  if((upl011_inw(udrv->iobase,UART011_CR)&UART011_CR_RTS)!=0)
  { /* direction switch necessary */
    LOG_CHIO("/ - call R2T");
    if(ret_code==UL_RC_CHIPTIMER)
    {
      /* wait finished, switch transmitter on, rx & tx on, uart on */
      upl011_outw(udrv->iobase,UART011_CR,UART011_CR_RXE|UART011_CR_TXE|UART01x_CR_UARTEN);
      udrv->chip_buff[0]=udrv->chip_buff[1]=0;
      rt_flg=1;
    } else {
      ns_wt=1000000000/udrv->baud_val*11;
      uld_chiptimer_start(udrv, ns_wt);
      return UL_RC_WIRQ;
    }
  }
  uc=(udrv->char_buff&0x100)?0:UART01x_LCRH_EPS;
  if((upl011_inw(udrv->iobase,UART011_LCRH)&UART01x_LCRH_EPS)!=uc)
  { /* change between control and data character format */
    if(!rt_flg)
    { /* wait to end of transmit */
      UL_FCALL2(upl011_weot,upl011_sndch_1);
      return UL_RC_PROC;
    }
  }
  fr=upl011_inw(udrv->iobase,UART01x_FR);
  if((fr&UART011_FR_RXFF)!=0)
  {
    dr=upl011_inw(udrv->iobase,UART01x_DR);
    LOG_CHIO("tx - processing 0x%x",dr);
    if((dr&(UART011_DR_OE|UART011_DR_BE|UART011_DR_FE))!=0)
      return UL_RC_EFRAME;
    if(upl011_sndch_readback(udrv,dr))
      return UL_RC_EBADCHR;
  }
  if ((fr&UART011_FR_TXFE)==0)
  {
    /* No space for next character in Tx buffer, wait  for it */
    upl011_outw(udrv->iobase,UART011_IMSC,UART011_TXIM|UART011_RXIM);  /* enable interrupts */
    return UL_RC_WIRQ;
  }
  if (udrv->chip_buff[1])
  {
    /* No space to store character for looback check */
    upl011_outw(udrv->iobase,UART011_IMSC,UART011_RXIM);  /* enable interrupts */
    return UL_RC_WIRQ;
  }
  UL_FNEXT(upl011_sndch_1);
  return UL_RC_PROC;
}

static int upl011_sndch_1(ul_drv *udrv, int ret_code)
{
  unsigned uc;
  if(ret_code<0) {UL_FRET;return ret_code;}
  /* transmitter on, rx & tx on, uart on */
  upl011_outw(udrv->iobase,UART011_CR,UART011_CR_RXE|UART011_CR_TXE|UART01x_CR_UARTEN);
  if(udrv->char_buff&0x100)
  { /* store last ctrl for connect line busy/ready */
    udrv->last_ctrl=udrv->char_buff;
    uc=0;
  }else{
    uc=UART01x_LCRH_EPS;
  }
  udrv->chip_buff[1]=udrv->chip_buff[0];
  udrv->chip_buff[0]=udrv->char_buff|0x1000;
  udrv->xor_sum^=udrv->char_buff;udrv->xor_sum++;
  upl011_outw(udrv->iobase,UART011_IMSC,UART011_TXIM|UART011_RXIM);  /* enable interrupts */
  upl011_outw(udrv->iobase,UART011_LCRH, UART011_LCRH_SPS|UART01x_LCRH_WLEN_8|uc|UART01x_LCRH_PEN); /* control/data character */
  upl011_outw(udrv->iobase,UART01x_DR,(unsigned int)udrv->char_buff&0xff);
  LOG_CHIO(" T:%03X",udrv->char_buff);
  UL_FNEXT(upl011_sndch_2);
  return UL_RC_WIRQ;
}

static int upl011_sndch_2(ul_drv *udrv, int ret_code)
{
  unsigned fr,dr;
  fr=upl011_inw(udrv->iobase,UART01x_FR);
  UL_FRET;
  if((fr&UART011_FR_RXFF)!=0)
  {
    dr=upl011_inw(udrv->iobase,UART01x_DR);
    if((dr&(UART011_DR_OE|UART011_DR_BE|UART011_DR_FE))!=0)
      return UL_RC_EFRAME;
    if(upl011_sndch_readback(udrv,dr))
      return UL_RC_EBADCHR;
  }
  LOG_CHIO(".");
  return UL_RC_PROC;
}

/*** Wait for time or received character ***/
static int upl011_wait_1(ul_drv *udrv, int ret_code);
static int upl011_wait_2(ul_drv *udrv, int ret_code);

static int upl011_wait(ul_drv *udrv, int ret_code)
{
  LOG_CHIO(" upl011_wait");
  uld_chiptimer_cancel(udrv);

  udrv->char_buff=0;
  if((upl011_inw(udrv->iobase,UART011_CR)&UART011_CR_RTS)==0)
  {
    UL_FCALL2(upl011_weot,upl011_wait_1);
  } else {
    udrv->chip_buff[1]=udrv->chip_buff[0]=0;
    UL_FNEXT(upl011_wait_1);
  }
  return UL_RC_PROC;
}

static int upl011_wait_1(ul_drv *udrv, int ret_code)
{
  u64 ns_wt;
  udrv->char_buff=0;

  LOG_CHIO(" upl011_wait1");

  if((upl011_inw(udrv->iobase,UART011_CR)&UART011_CR_RTS)==0)
  { /* direction switch necessary - in TX? -> do RX */
    /* transmitter off, rx & tx on, uart on */
    upl011_outw(udrv->iobase,UART011_CR,UART011_CR_RTS|UART011_CR_RXE|UART011_CR_TXE|UART01x_CR_UARTEN);
    /* enable interrupts - receive */
    upl011_outw(udrv->iobase,UART011_IMSC,UART011_RXIM);
  }
  ns_wt=1000000000/udrv->baud_val*11*udrv->wait_time;
  uld_chiptimer_start(udrv, ns_wt);
  UL_FNEXT(upl011_wait_2);
  return UL_RC_WIRQ;
}

static int upl011_wait_2(ul_drv *udrv, int ret_code)
{
  unsigned fr,u;
  fr=upl011_inw(udrv->iobase,UART01x_FR);
  LOG_CHIO(" upl011_wait_2 fr 0x%x",fr);
  if((fr&UART011_FR_RXFF)!=0)
  {
    uld_chiptimer_cancel(udrv);

    if((u=udrv->chip_buff[0])!=0)
    {
      unsigned uc;
      udrv->chip_buff[0]=0;
      uc=upl011_inw(udrv->iobase,UART01x_DR);
      if((u^uc)&0xff)
      {
        LOG_CHIO(" TE5!");
        return UL_RC_EBADCHR;
      }
    } else {
      UL_FRET;
      return upl011_recch_sub(udrv,upl011_inw(udrv->iobase,UART01x_DR));
    }
  }
  if(ret_code==UL_RC_CHIPTIMER)
  {
    LOG_CHIO(" Timeout!");
    UL_FRET;
    return UL_RC_ETIMEOUT;
  }
  return UL_RC_WIRQ;
}

/*** Connect to RS485 bus ***/
static int upl011_connect_1(ul_drv *udrv, int ret_code);
static int upl011_connect_2(ul_drv *udrv, int ret_code);

/*** Connect to RS485 bus ***/
static int upl011_connect(ul_drv *udrv, int ret_code)
{
  unsigned u;
  LOG_CHIO(" C_0 ");
  u=udrv->last_ctrl;
  udrv->chip_temp=uld_get_arb_addr(udrv);
  udrv->wait_time=((udrv->chip_temp-u-1)&0xF)+4;
  udrv->chip_temp=(udrv->chip_temp&0x3F)|0x40;
  if(((u&0x180)!=0x180)||
    (u==0x1FF)) udrv->wait_time+=0x10;
  udrv->last_ctrl=0;
  udrv->char_buff=0;
  UL_FCALL2(*udrv->chip_ops->fnc_wait,upl011_connect_1);
  return UL_RC_PROC;
}

static int upl011_connect_1(ul_drv *udrv, int ret_code)
{
  LOG_CHIO(" C_1 ");
  if(ret_code!=UL_RC_ETIMEOUT)
  { UL_FRET;
    LOG_CHIO(" EARBIT1!");
    return UL_RC_EARBIT;
  }
  /* check for RxD low */
  if((upl011_inw(udrv->iobase,ST_UART011_ITIP)&UART011_ITIP_RXD)==0)
  { UL_FRET;
    LOG_CHIO(" EARBIT2!");
    return UL_RC_EARBIT;
  }

  udrv->chip_temp|=0x300;
  /* enable interrupts - break|receive */
  upl011_outw(udrv->iobase,UART011_IMSC,UART011_BEIM|UART011_RXIM);
  /* transmitter on, rx & tx on, uart on */
  upl011_outw(udrv->iobase,UART011_CR,UART011_CR_RXE|UART011_CR_TXE|UART01x_CR_UARTEN);
  /* 9bit,parita even(space,low),break */
  upl011_outw(udrv->iobase,UART011_LCRH, UART011_LCRH_SPS|UART01x_LCRH_WLEN_8|UART01x_LCRH_EPS|UART01x_LCRH_PEN|UART01x_LCRH_BRK);
  UL_FNEXT(upl011_connect_2);
  /* wait for break condition now */
  return UL_RC_WIRQ;
}

static int upl011_connect_2(ul_drv *udrv, int ret_code)
{
  unsigned fr,uc=0;
  LOG_CHIO(" C_2 ");
  fr=upl011_inw(udrv->iobase,UART01x_FR);
  if((fr&UART011_FR_RXFF)!=0) {
    udrv->chip_temp&=~0x200;
    uc=upl011_inw(udrv->iobase,UART01x_DR);
    if (uc&UART011_DR_BE) udrv->chip_temp&=~0x100;
  }
  LOG_CHIO(" C_2 chiptemp 0x%x, fr 0x%x, uc 0x%x",udrv->chip_temp,fr,uc);
  if (udrv->chip_temp&0x300) {
    return UL_RC_WIRQ;
  }
  /* 9bit,parita odd (mark,high) */
  upl011_outw(udrv->iobase,UART011_LCRH,UART011_LCRH_SPS|UART01x_LCRH_WLEN_8|UART01x_LCRH_PEN);
  /* transmitter off, rx & tx on, uart on */
  upl011_outw(udrv->iobase,UART011_CR,UART011_CR_RTS|UART011_CR_RXE|UART011_CR_TXE|UART01x_CR_UARTEN);
  /* enable interrupts - receive */
  upl011_outw(udrv->iobase,UART011_IMSC,UART011_RXIM);
  if(udrv->chip_temp==1)
  {
    LOG_CHIO(" Connected");
    UL_FRET;
  }else{
    udrv->wait_time=(udrv->chip_temp&3)+1;
    udrv->chip_temp>>=2;
    UL_FCALL2(*udrv->chip_ops->fnc_wait,upl011_connect_1);
  }
  return UL_RC_PROC;
}

/*** Finish tx ***/
static int upl011_finishtx_1(ul_drv *udrv, int ret_code);

static int upl011_finishtx(ul_drv *udrv, int ret_code)
{
  UL_FCALL2(upl011_weot,upl011_finishtx_1);
  return UL_RC_PROC;
}

static int upl011_finishtx_1(ul_drv *udrv, int ret_code)
{
  /* transmitter off, rx & tx on, uart on */
  upl011_outw(udrv->iobase,UART011_CR,UART011_CR_RTS|UART011_CR_RXE|UART011_CR_TXE|UART01x_CR_UARTEN);
  UL_FRET;
  return UL_RC_PROC;
}

static const char *pl011_port_name="ulan_pl011";

/*** pl011 initialize ports ***/
static int upl011_pinit(ul_drv *udrv)
{
  if (ul_io_map(udrv->physbase,0x90,pl011_port_name, &udrv->iobase)<0)
  { LOG_FATAL(KERN_CRIT "uLan pl011_pinit : cannot reguest ports !\n");
    return UL_RC_EPORT;
  }

  udrv->chip_buff[0]=udrv->chip_buff[1]=0;

  /* Set baud rate */
  upl011_outw(udrv->iobase,UART011_FBRD,udrv->baud_div & 0x3f);
  upl011_outw(udrv->iobase,UART011_IBRD,udrv->baud_div >> 6);
  /* 9bit, even parity (space,low) */
  upl011_outw(udrv->iobase,UART011_LCRH,
  UART011_LCRH_SPS|UART01x_LCRH_WLEN_8|UART01x_LCRH_EPS|UART01x_LCRH_PEN);
  /* transmitter off, rx & tx on, uart on */
  upl011_outw(udrv->iobase,UART011_CR,UART011_CR_RTS|UART011_CR_RXE|UART011_CR_TXE|UART01x_CR_UARTEN);
  /* disable interrupts */
  upl011_outw(udrv->iobase,UART011_IMSC,0);
  /* clear interrupts */
  upl011_outw(udrv->iobase,UART011_ICR,0xfff);

  LOG_PORTS(KERN_INFO "uLan pl011_init : init done\n");

  return UL_RC_PROC;
}

/*** pl011 deinitialize ports ***/
static int upl011_pdone(ul_drv *udrv)
{
  upl011_outw(udrv->iobase,UART011_IMSC,0);  /* disable interrupts */
  upl011_outw(udrv->iobase,UART011_CR,UART011_CR_RTS); /* transmitter off */

  ul_io_unmap(udrv->physbase,0x90,udrv->iobase);
  return UL_RC_PROC;
}

/*** pl011 activate ***/
static void upl011_activate(ul_drv *udrv)
{
  upl011_outw(udrv->iobase,UART011_IMSC,UART011_RXIM);  /* enable recevive interrupts */
}

/*** pl011 generate irq for irq_probe */
static int upl011_genirq(ul_drv *udrv,int param)
{
  if(param)
  {
    upl011_outw(udrv->iobase,UART011_CR,UART011_CR_RTS|UART011_CR_RXE|UART011_CR_TXE|UART01x_CR_UARTEN); /* transmitter off, rx & tx on, uart on */
    upl011_outw(udrv->iobase,UART011_IMSC,UART011_BEIM|UART011_RXIM); /* enable interrupts - break|receive */
    upl011_outw(udrv->iobase,UART011_LCRH, UART011_LCRH_SPS|UART01x_LCRH_WLEN_8|UART01x_LCRH_EPS|UART01x_LCRH_PEN|UART01x_LCRH_BRK); /* 9bit,parita even(space,low),break */
  }else{
    upl011_outw(udrv->iobase,UART011_IMSC,0);  /* disable interrupts */
    upl011_outw(udrv->iobase,UART011_ICR,0xfff); /* clear interrupts */
    upl011_outw(udrv->iobase,UART011_CR,UART011_CR_RTS|UART011_CR_RXE|UART011_CR_TXE|UART01x_CR_UARTEN); /* transmitter off, rx & tx on, uart on */
  }
  return UL_RC_PROC;
}

/* support for hardware tests */
static int upl011_hwtest(ul_drv *udrv,int param)
{
  switch(param)
  {
   case 0x10:
   case 0x11:
//	upl011_outw(udrv->iobase,UART_IER, 0); /* disable interrupts */
//	upl011_outw(udrv->iobase,UART_MCR, U450_MCR_OE(udrv)); /* transmitter on */
//	upl011_outw(udrv->iobase,UART_LCR, param&1?
//	             U450_LCR_UL:U450_LCR_ULB); /* set TD lines */
   case 0x12:
//        u=upl011_inw(udrv->iobase,UART_MSR);
//	return upl011_inw(udrv->iobase,UART_LSR)|(u<<8)|
//	                (u&(U450_MSR_TxD(udrv))?0:0x100000)|
//			(u&(U450_MSR_RxD(udrv))?0:0x10000);
	return 0;
   case 0x13:
//	upl011_outw(udrv->iobase,UART_MCR, U450_MCR_IE(udrv)); /* transmitter off */
//        u=upl011_inw(udrv->iobase,UART_MSR);
//	return upl011_inw(udrv->iobase,UART_LSR)|(u<<8)|
//	                (u&(U450_MSR_TxD(udrv))?0:0x100000)|
//			(u&(U450_MSR_RxD(udrv))?0:0x10000);
	return 0;
  }
  return UL_RC_ENOFNC;
}

/*** Control functions of chip driver  ***/
static int upl011_txoe(struct ul_drv *udrv, int enable)
{ /* switch on/off line transmitter */
  if(!enable)
  { /* switch off line transmitter */
    /* transmitter off, rx & tx on, uart on */
      upl011_outw(udrv->iobase,UART011_CR,UART011_CR_RTS|UART011_CR_RXE|UART011_CR_TXE|UART01x_CR_UARTEN);
  } else {
    /* switch on line transmitter */
    /* transmitter on, rx & tx on, uart on */
      upl011_outw(udrv->iobase,UART011_CR,UART011_CR_RXE|UART011_CR_TXE|UART01x_CR_UARTEN);
  }
  return UL_RC_PROC;
}

const ul_drv_chip_ops upl011_chip_ops = {
  "pl011",	/* chip_type - text type identification*/
  upl011_recch,	/* fnc_recch */
  upl011_sndch,	/* fnc_sndch */
  upl011_wait,	/* fnc_wait */
  upl011_connect,/* fnc_connect */
  upl011_finishtx,/* fnc_finishtx */
  upl011_pool,	/* fnc_pool */
  NULL,		/* fnc_cctrl */
  NULL,		/* fnc_stroke */

  upl011_txoe,	/* fnc_txoe */
  upl011_pinit,	/* fnc_pinit */
  upl011_pdone,	/* fnc_pdone */
  upl011_activate,/* fnc_activate */
  upl011_genirq,	/* fnc_genirq */
  ul_drv_common_fnc_rqirq_probe,	/* fnc_rqirq */
  ul_drv_common_fnc_freeirq,	/* fnc_freeirq */
  upl011_hwtest,	/* fnc_hwtest */
  ul_drv_common_fnc_setmyadr,	/* fnc_setmyadr */
  ul_drv_common_fnc_setpromode,	/* fnc_setpromode */
};

/*** pl011 chip driver initialize ***/
int upl011_init(ul_drv *udrv, ul_physaddr_t physbase, int irq, int baud, long baudbase, int options)
{
  if (ul_io_map(physbase,0x90,pl011_port_name, &udrv->iobase)<0)
  { LOG_FATAL(KERN_CRIT "uLan pl011_init : cannot reguest ports !\n");
    return UL_RC_EPORT;
  }

  udrv->chip_options=options;
  udrv->baud_base=baudbase;
 /* check and setup baud base oscilator frequency */
  if(!udrv->baud_base){
    udrv->baud_base=U450_BAUD_BASE_DEFAULT;
  }
  LOG_PORTS(KERN_INFO "uLan pl011_init : baud_base 0x%X\n",(unsigned int)udrv->baud_base);
  if(!baud) baud=19200;
  udrv->baud_val=baud;
  udrv->baud_div=DIV_ROUND_CLOSEST(baudbase*4,baud);
  udrv->physbase=physbase;
  udrv->irq=irq;
  udrv->chip_ops=&upl011_chip_ops;
  ul_io_unmap(udrv->physbase,0x90,udrv->iobase);
  return 0;
}
