/*******************************************************************
  uLan Communication - low level C and driver interface library

  ulan.h      - basic uLan network access operations

  (C) Copyright 1996-2016 by Pavel Pisa - project originator
        http://cmp.felk.cvut.cz/~pisa
  (C) Copyright 1996-2016 PiKRON Ltd.
        http://www.pikron.com
  (C) Copyright 2002-2016 Petr Smolik


  The uLan driver project can be used and distributed
  in compliance with any of next licenses
   - GPL - GNU Public License
     See file COPYING for details.
   - LGPL - Lesser GNU Public License
   - MPL - Mozilla Public License
   - and other licenses added by project originator

  Code can be modified and re-distributed under any combination
  of the above listed licenses. If contributor does not agree with
  some of the licenses, he/she can delete appropriate line.
  WARNING: if you delete all lines, you are not allowed to
  distribute code or sources in any form.
 *******************************************************************/

#ifndef _ULAN_H
#define _ULAN_H

#ifdef __cplusplus
extern "C" {
#endif

/* dummy definition of macro used in SDCC build */
#define UL_FNC_REENTRANT

#include <stdarg.h>
#include <sys/types.h>
#include <ul_lib/ul_drvdef.h>
#include <ul_lib/ul_fd.h>

#ifndef CONFIG_OC_ULUT
#include <ul_lib/ul_utsubst.h>
#else /*CONFIG_OC_ULUT*/
#include <ul_utdefs.h>
#endif /*CONFIG_OC_ULUT*/
/* driver interface */

ul_fd_t	ul_open(const char *dev_name, const char *options);
int	ul_close(ul_fd_t ul_fd);
int	ul_drv_version(ul_fd_t ul_fd);
ssize_t	ul_read(ul_fd_t ul_fd, void *buffer, size_t size);
ssize_t	ul_write(ul_fd_t ul_fd, const void *buffer, size_t size);
int	ul_newmsg(ul_fd_t ul_fd,const ul_msginfo *msginfo);
int	ul_tailmsg(ul_fd_t ul_fd,const ul_msginfo *msginfo);
int	ul_freemsg(ul_fd_t ul_fd);
int	ul_acceptmsg(ul_fd_t ul_fd,ul_msginfo *msginfo);
int	ul_actailmsg(ul_fd_t ul_fd,ul_msginfo *msginfo);
int	ul_addfilt(ul_fd_t ul_fd,const ul_msginfo *msginfo);
int	ul_abortmsg(ul_fd_t ul_fd);
int	ul_rewmsg(ul_fd_t ul_fd);
int	ul_inepoll(ul_fd_t ul_fd);
int	ul_drv_debflg(ul_fd_t ul_fd,int debug_msk);
int	ul_fd_wait(ul_fd_t ul_fd, int wait_sec);
int	ul_setmyadr(ul_fd_t ul_fd, int newadr);
int	ul_setidstr(ul_fd_t ul_fd, const char *idstr);
int	ul_setbaudrate(ul_fd_t ul_fd, int baudrate);
int	ul_setpromode(ul_fd_t ul_fd, int pro_mode);
int	ul_setsubdev(ul_fd_t ul_fd, int subdevidx);
int	ul_queryparam(ul_fd_t ul_fd, unsigned long query, unsigned long *pvalue);
int	ul_route(ul_fd_t ul_fd, ul_route_range_t *rr);
int	ul_stroke(ul_fd_t ul_fd);

int	ul_fd_is_valid(ul_fd_t ul_fd);

/* simple message operations */

int ul_send_command(ul_fd_t ul_fd,int dadr,int cmd,int flg,
                    const void *buf,int len);
int ul_send_command_wait(ul_fd_t ul_fd,int dadr,int cmd,int flg,
                         const void *buf,int len);
int ul_send_query(ul_fd_t ul_fd,int dadr,int cmd,int flg,
                  const void *buf,int len);
int ul_send_query_wait(ul_fd_t ul_fd,int dadr,int cmd,int flg,
		       const void *bufin,int lenin,void **bufout,int *lenout);

/* basic uLan commands/services */

#define UL_CMD_OISV	0x10	/* Object Interface Service */
#define UL_CMD_LCDABS	0x4f	/* Absorbance data block */
#define UL_CMD_LCDMRK	0x4e	/* Mark */
#define UL_CMD_PDO	0x50	/* Asynchronous service */
#define UL_CMD_NCS	0x7f	/* Network Control Service */
#define UL_CMD_GST	0xc1	/* Fast module get status */

/* UL_CMD_NCS	Network Control Service */

#define ULNCS_ADR_RQ	        0xC0	/* SN0 SN1 SN2 SN3 */
#define ULNCS_SET_ADDR	        0xC1	/* SN0 SN1 SN2 SN3 NEW_ADR */
#define ULNCS_SID_RQ	        0xC2	/* send serial num and ID string request */
#define ULNCS_SID_RPLY	        0xC3	/* SN0 SN1 SN2 SN3 ID ... */
#define ULNCS_ADDR_NVSV	        0xC4	/* SN0 SN1 SN2 SN3 - save addres to EEPROM */
#define ULNCS_BOOT_ACT          0xC5    /* SN0 SN1 SN2 SN3 */
#define ULNCS_BOOT_ACK          0xC6    /* SN0 SN1 SN2 SN3 */
#define ULNCS_NDATA_RQ          0xC7    /*  */
#define ULNCS_NDATA_RPLY        0xC8    /* ((type|len) data ) x n */
#define ULNCS_SET_SN            0xE0    /* SN0 SN1 SN2 SN3 */

#define ULNCS_NDATA_TAG_NETWORK_BASE      0x10  /* NBASE0 NBASE1 */
#define ULNCS_NDATA_TAG_ROUTE_DEFAULT_GW  0x20  /* GW */
#define ULNCS_NDATA_TAG_ROUTE_RANGE_FIRST 0x40  /* ADDR */
#define ULNCS_NDATA_TAG_ROUTE_RANGE_LAST  0x50  /* ADDR */
#define ULNCS_NDATA_TAG_ROUTE_RANGE_GW    0x60  /* ADDR */

/* UL_CMD_RES	Reinitialize RS485 or connected module */

#define ULRES_LINK      0x10
#define ULRES_BAUD      0x12
#define ULRES_CPU       0x21    /* password - default 0x55 0xAA */

#if 0
  #define  read		error_to_use_read
  #define  write	error_to_use_write
  #define  open		error_to_use_open
  #define  close	error_to_use_close
  #define  ioctl	error_to_use_ioctl
#endif

#ifdef __cplusplus
} /* extern "C"*/
#endif

#endif /* _ULAN_H */

