/*******************************************************************
  uLan Communication - user visible definitions

  ul_fd4indir.h	- net access through interface kind multiplexor

  (C) Copyright 1996-2016 by Pavel Pisa - project originator
        http://cmp.felk.cvut.cz/~pisa
  (C) Copyright 1996-2016 PiKRON Ltd.
        http://www.pikron.com
  (C) Copyright 2002-2016 Petr Smolik


  The uLan driver project can be used and distributed
  in compliance with any of next licenses
   - GPL - GNU Public License
     See file COPYING for details.
   - LGPL - Lesser GNU Public License
   - MPL - Mozilla Public License
   - and other licenses added by project originator

  Code can be modified and re-distributed under any combination
  of the above listed licenses. If contributor does not agree with
  some of the licenses, he/she can delete appropriate line.
  WARNING: if you delete all lines, you are not allowed to
  distribute code or sources in any form.
 *******************************************************************/

#ifndef _UL_FD_H
#define _UL_FD_H

#define WITH_UL_FD_INDIRECT

#include <stdint.h>
#include <ul_lib/ul_fddir.h>

#ifdef __cplusplus
extern "C" {
#endif

typedef struct ul_fd_context_t *ul_fd_t;
#define UL_FD_INVALID (NULL)

typedef struct ul_fd_ops_t {
  const char *ops_name;
  int (*ulop_namematch)(const char *dev_name);
  ul_fd_t (*ulop_open)(const char *dev_name, const char *options);
  int (*ulop_close)(ul_fd_t ul_fd);
  int (*ulop_drv_version)(ul_fd_t ul_fd);
  ssize_t (*ulop_read)(ul_fd_t ul_fd, void *buffer, size_t size);
  ssize_t (*ulop_write)(ul_fd_t ul_fd, const void *buffer, size_t size);
  int (*ulop_newmsg)(ul_fd_t ul_fd,const ul_msginfo *msginfo);
  int (*ulop_tailmsg)(ul_fd_t ul_fd,const ul_msginfo *msginfo);
  int (*ulop_freemsg)(ul_fd_t ul_fd);
  int (*ulop_acceptmsg)(ul_fd_t ul_fd,ul_msginfo *msginfo);
  int (*ulop_actailmsg)(ul_fd_t ul_fd,ul_msginfo *msginfo);
  int (*ulop_addfilt)(ul_fd_t ul_fd,const ul_msginfo *msginfo);
  int (*ulop_abortmsg)(ul_fd_t ul_fd);
  int (*ulop_rewmsg)(ul_fd_t ul_fd);
  int (*ulop_inepoll)(ul_fd_t ul_fd);
  int (*ulop_drv_debflg)(ul_fd_t ul_fd,int debug_msk);
  int (*ulop_fd_wait)(ul_fd_t ul_fd, int wait_sec);
  ul_fd_direct_t (*ulop_fd2sys_fd)(ul_fd_t ul_fd);
  int (*ulop_setmyadr)(ul_fd_t ul_fd, int newmyadr);
  int (*ulop_setidstr)(ul_fd_t ul_fd, const char *idstr);
  int (*ulop_setbaudrate)(ul_fd_t ul_fd, int baudrate);
  int (*ulop_setpromode)(ul_fd_t ul_fd, int pro_mode);
  int (*ulop_setsubdev)(ul_fd_t ul_fd, int subdevidx);
  int (*ulop_queryparam)(ul_fd_t ul_fd, unsigned long query, unsigned long *pvalue);
  int (*ulop_route)(ul_fd_t ul_fd, ul_route_range_t *rr);
} ul_fd_ops_t;

typedef struct ul_fd_context_t {
  ul_fd_ops_t *fd_ops;
} ul_fd_context_t;

int ul_fd_ops_register(ul_fd_ops_t *fd_ops);
ul_fd_direct_t ul_fd2sys_fd(ul_fd_t fd);

#ifdef __cplusplus
} /* extern "C"*/
#endif

#endif /*_UL_FD_H*/
