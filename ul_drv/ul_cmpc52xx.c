/*******************************************************************
  uLan Communication - uL_DRV - multiplatform uLan driver

  ul_cmpc52xx.c	- chip driver for MPC52xx integrated PSC UART

  (C) Copyright 1996-2004 by Pavel Pisa - project originator
        http://cmp.felk.cvut.cz/~pisa
  (C) Copyright 1996-2004 PiKRON Ltd.
        http://www.pikron.com
  (C) Copyright 2002-2004 Petr Smolik
  (C) Copyright 2009 Martin Samek

  The uLan driver project can be used and distributed
  in compliance with any of next licenses
   - GPL - GNU Public License
     See file COPYING for details.
   - LGPL - Lesser GNU Public License
   - MPL - Mozilla Public License
   - and other licenses added by project originator

  Code can be modified and re-distributed under any combination
  of the above listed licenses. If contributor does not agree with
  some of the licenses, he/she can delete appropriate line.
  WARNING: if you delete all lines, you are not allowed to
  distribute code or sources in any form.
 *******************************************************************/

/*******************************************************************/
/* Chip driver for MPC52xx integrated PSC UART */

#include <asm/mpc52xx_psc.h>

/* chip configurations constants */

#define UMPC52xx_DEF_IPB_FREQ	132000000
#define UMPC52xx_DEF_BAUDRATE	19200

/* flags of chip options used in umpc52xx_init */

#define UMPC52xx_FLG_TXOE	0x01
#define UMPC52xx_FLG_WI_TXEMP	0x02
#define UMPC52xx_FLG_WI_TXRDY	0x04
#define UMPC52xx_FLG_WI_RXRDY	0x08
#define UMPC52xx_FLG_TXB8	0x10

#define umpc52xx_flg           chip_buff[1]
#define umpc52xx_psc_base      chip_buff[2]
#define umpc52xx_psc_fifo_base chip_buff[3]

#define umpc52xx_psc_reg(M_udrv, M_reg) \
  (&(((struct mpc52xx_psc __iomem *)(udrv)->umpc52xx_psc_base)->M_reg))
#define umpc52xx_psc_fifo_reg(M_udrv, M_reg) \
  (&(((struct mpc52xx_psc_fifo __iomem *)(udrv)->umpc52xx_psc_fifo_base)->M_reg))

#ifndef MPC52xx_PSC_MODE_PARMDROP
#define MPC52xx_PSC_MODE_PARMDROP	0x18
#endif /*MPC52xx_PSC_MODE_PARMDROP*/

#define UMPC52xx_PSC_MODE (MPC52xx_PSC_MODE_8_BITS|MPC52xx_PSC_MODE_PARMDROP)

static inline
void umpc52xx_set_mode_txb8(ul_drv *udrv, int val)
{
  /* switch to access MR1 */
  out_8(umpc52xx_psc_reg(udrv, command), MPC52xx_PSC_SEL_MODE_REG_1);
  /* multidrop, address char, 8bit */
  if(val){
    out_8(umpc52xx_psc_reg(udrv, mode), UMPC52xx_PSC_MODE|MPC52xx_PSC_MODE_PARODD);
    udrv->umpc52xx_flg |= UMPC52xx_FLG_TXB8;
  } else {
    out_8(umpc52xx_psc_reg(udrv, mode), UMPC52xx_PSC_MODE);
    udrv->umpc52xx_flg &= ~UMPC52xx_FLG_TXB8;
  }
}

static inline
void umpc52xx_set_txoe(ul_drv *udrv, int val)
{
  if(val) {
    udrv->umpc52xx_flg |= UMPC52xx_FLG_TXOE;
    out_8(umpc52xx_psc_reg(udrv, op0), 0x1);	/* set RTS = 1 */
  } else {
    udrv->umpc52xx_flg &= ~UMPC52xx_FLG_TXOE;
    out_8(umpc52xx_psc_reg(udrv, op1), 0x1);	/* set RTS = 0 */
  }
}

static int umpc52xx_txoe(ul_drv *udrv, int enable)
{
  umpc52xx_set_txoe(udrv, enable);
  return 0;
}

/* Help subroutine for reading 9-bit character */
static int umpc52xx_recch_sub_rec9bit(ul_drv *udrv, int sr)
{
  udrv->char_buff=in_8(umpc52xx_psc_reg(udrv, mpc52xx_psc_buffer_8));

  if(sr & MPC52xx_PSC_SR_PE){
    udrv->last_ctrl=udrv->char_buff|=0x100;	/* last char for ready/busy */
  } else udrv->last_ctrl&=0x7f;			/* somebody other has line */

  udrv->xor_sum^=udrv->char_buff;
  udrv->xor_sum++;

  if(sr & (MPC52xx_PSC_SR_OE|MPC52xx_PSC_SR_FE|MPC52xx_PSC_SR_RB|MPC52xx_PSC_SR_PE))
    out_8(umpc52xx_psc_reg(udrv, command), MPC52xx_PSC_RST_ERR_STAT);	/* reset error */

  if(sr & (MPC52xx_PSC_SR_OE|MPC52xx_PSC_SR_FE|MPC52xx_PSC_SR_RB)){
    LOG_CHIO(" ER:%03X",udrv->char_buff);
    return UL_RC_EFRAME; /* frame error */
  }else{
    LOG_CHIO(" R:%03X",udrv->char_buff);
    return UL_RC_PROC;
  }
}

static
void umpc52xx_set_wait_mode(ul_drv *udrv, int mode)
{
  int imr=0;
  const int wi_mask = UMPC52xx_FLG_WI_RXRDY|UMPC52xx_FLG_WI_TXRDY|UMPC52xx_FLG_WI_TXEMP;

  if(!((udrv->umpc52xx_flg ^ mode) & wi_mask))
    return;

  udrv->umpc52xx_flg &= ~wi_mask;
  udrv->umpc52xx_flg |= mode & wi_mask;

  if(mode & UMPC52xx_FLG_WI_RXRDY)
    imr |= MPC52xx_PSC_IMR_RXRDY;

  if(mode & UMPC52xx_FLG_WI_TXRDY)
    imr |= MPC52xx_PSC_IMR_TXRDY;

  if(mode & UMPC52xx_FLG_WI_TXEMP)
    imr |= MPC52xx_PSC_IMR_TXEMP;

  LOG_CHIO("imr=0x%03x\n", imr);

  out_be16(umpc52xx_psc_reg(udrv, mpc52xx_psc_imr), imr);
}

/*** Test interrupt request state ***/
static int umpc52xx_pool(ul_drv *udrv)
{
  int sr;
  int ret=0;

  sr=in_be16(umpc52xx_psc_reg(udrv, mpc52xx_psc_status));

  if(sr & (MPC52xx_PSC_SR_RXRDY|MPC52xx_PSC_SR_OE|
        MPC52xx_PSC_SR_PE|MPC52xx_PSC_SR_FE|MPC52xx_PSC_SR_RB))
    ret |= UMPC52xx_FLG_WI_RXRDY;

  if(sr & MPC52xx_PSC_SR_TXRDY)
    ret |= UMPC52xx_FLG_WI_TXRDY;

  if(sr & MPC52xx_PSC_SR_TXEMP)
    ret |= UMPC52xx_FLG_WI_TXEMP;

  LOG_CHIO("poll ret=0x%02x flg=0x%02x\n", ret, udrv->umpc52xx_flg);

  return ret & udrv->umpc52xx_flg;
}

/*** Wait end of transmit ***/
static int umpc52xx_weot(ul_drv *udrv, int ret_code)
{
  int sr;

  sr=in_be16(umpc52xx_psc_reg(udrv, mpc52xx_psc_status));

  if(sr & MPC52xx_PSC_SR_TXEMP) {
    umpc52xx_set_txoe(udrv, 0);
    LOG_CHIO("T2R");
    UL_FRET;
    return UL_RC_PROC;
  }

  umpc52xx_set_wait_mode(udrv,UMPC52xx_FLG_WI_TXEMP);
  LOG_CHIO(".weot");
  return UL_RC_WIRQ;
}

/*** Receive character into char_buff ***/
static int umpc52xx_recch(ul_drv *udrv, int ret_code)
{
  int sr;

  if(udrv->umpc52xx_flg & UMPC52xx_FLG_TXOE) {
    UL_FCALL(umpc52xx_weot);
    return UL_RC_PROC;
  }

  umpc52xx_set_wait_mode(udrv,UMPC52xx_FLG_WI_RXRDY);
  sr=in_be16(umpc52xx_psc_reg(udrv, mpc52xx_psc_status));

  if(sr & (MPC52xx_PSC_SR_RXRDY|MPC52xx_PSC_SR_OE|
           MPC52xx_PSC_SR_PE|MPC52xx_PSC_SR_FE|MPC52xx_PSC_SR_RB)) {
    UL_FRET;
    return umpc52xx_recch_sub_rec9bit(udrv, sr);
  }
  LOG_CHIO("-");
  return UL_RC_WIRQ;			/**/
}

/*** Send character from char_buff ***/
static int umpc52xx_sndch_1(ul_drv *udrv, int ret_code);

static int umpc52xx_sndch(ul_drv *udrv, int ret_code)
{
  unsigned u;
  int sr;
  int txb8_ch;

  u=udrv->char_buff;
  sr=in_be16(umpc52xx_psc_reg(udrv, mpc52xx_psc_status));

  if(!(udrv->umpc52xx_flg & UMPC52xx_FLG_TXOE)) {
    out_8(umpc52xx_psc_reg(udrv, command), MPC52xx_PSC_RST_TX);
    out_8(umpc52xx_psc_reg(udrv, command), MPC52xx_PSC_TX_ENABLE);
    out_8(umpc52xx_psc_reg(udrv, mpc52xx_psc_buffer_8), 0x55);
    UL_FNEXT(umpc52xx_sndch_1);
    umpc52xx_set_wait_mode(udrv,UMPC52xx_FLG_WI_TXEMP);
    LOG_CHIO("R2T");
    return UL_RC_WIRQ;
  }

  txb8_ch=(u&0x100) ^ (udrv->umpc52xx_flg & UMPC52xx_FLG_TXB8?0x100:0);

  if(txb8_ch) {
    if(!(sr & MPC52xx_PSC_SR_TXEMP)) {
      umpc52xx_set_wait_mode(udrv,UMPC52xx_FLG_WI_TXEMP);
      return UL_RC_WIRQ;
    }
    umpc52xx_set_mode_txb8(udrv, u&0x100?1:0);
  }

  umpc52xx_set_wait_mode(udrv,UMPC52xx_FLG_WI_TXRDY);

  if(!(sr & MPC52xx_PSC_SR_TXRDY)) {
   LOG_CHIO("+");
   return UL_RC_WIRQ;
  }

  out_8(umpc52xx_psc_reg(udrv, mpc52xx_psc_buffer_8), u&0xff);

  udrv->xor_sum^=u;
  udrv->xor_sum++;

  UL_FRET;
  LOG_CHIO(" T:%03X",u);
  return UL_RC_PROC;
}

static int umpc52xx_sndch_1(ul_drv *udrv, int ret_code)
{
  umpc52xx_set_txoe(udrv, 1);
  UL_FNEXT(umpc52xx_sndch);
  return UL_RC_PROC;
}

/*** Wait for time or received character ***/
static int umpc52xx_wait(ul_drv *udrv, int ret_code)
{
  int sr;

  if(udrv->umpc52xx_flg & UMPC52xx_FLG_TXOE) {
    UL_FCALL(umpc52xx_weot);
    return UL_RC_PROC;
  }

  sr=in_be16(umpc52xx_psc_reg(udrv, mpc52xx_psc_status));

  if(sr & (MPC52xx_PSC_SR_RXRDY|MPC52xx_PSC_SR_OE|
           MPC52xx_PSC_SR_PE|MPC52xx_PSC_SR_FE|MPC52xx_PSC_SR_RB)) {
    out_8(umpc52xx_psc_reg(udrv, command), MPC52xx_PSC_RST_TX);
    out_8(umpc52xx_psc_reg(udrv, command), MPC52xx_PSC_TX_ENABLE);
    UL_FRET;
    return umpc52xx_recch_sub_rec9bit(udrv, sr);
  }

  while((udrv->wait_time>0) && (sr & MPC52xx_PSC_SR_TXRDY)) {
    /* wait timed by dummy TX */
    udrv->wait_time--;
    out_8(umpc52xx_psc_reg(udrv, mpc52xx_psc_buffer_8), 0);
    sr=in_be16(umpc52xx_psc_reg(udrv, mpc52xx_psc_status));
    LOG_CHIO("W");
  }

  if((udrv->wait_time==0) && (sr & MPC52xx_PSC_SR_TXEMP) && (sr & MPC52xx_PSC_SR_TXRDY)) {
    LOG_CHIO(" Timeout!");
    UL_FRET;
    return UL_RC_ETIMEOUT;
  }

  umpc52xx_set_wait_mode(udrv,UMPC52xx_FLG_WI_RXRDY|UMPC52xx_FLG_WI_TXEMP);
  return UL_RC_WIRQ;			/**/
}


/*** Connect to RS485 bus ***/
static int umpc52xx_connect_1(ul_drv *udrv, int ret_code);
int umpc52xx_connect_2(ul_drv *udrv, int ret_code);

static int umpc52xx_connect(ul_drv *udrv, int ret_code)
{
  unsigned u;

  u=udrv->last_ctrl;
  udrv->chip_temp=uld_get_arb_addr(udrv);
  udrv->wait_time=((udrv->chip_temp-u-1)&0xF)+4;
  udrv->chip_temp=(udrv->chip_temp&0x3F)|0x40;

  if(((u&0x180)!=0x180)||
    (u==0x1FF)) udrv->wait_time+=0x10;

  udrv->last_ctrl=0;
  udrv->char_buff=0;
  UL_FCALL2(*udrv->chip_ops->fnc_wait,umpc52xx_connect_1);

  return UL_RC_PROC;
}

static int umpc52xx_connect_1(ul_drv *udrv, int ret_code)
{
  unsigned ip;
  if(ret_code!=UL_RC_ETIMEOUT)
    { UL_FRET; return UL_RC_EARBIT;}
  ip=in_8(umpc52xx_psc_reg(udrv, ip));
  if(ip & 1) /* check for possible collision */
  { UL_FRET;
    LOG_CHIO(" EARBIT!");
    return UL_RC_EARBIT;
  }
  UL_FNEXT(umpc52xx_connect_2);
  /* generate dominant pulse */
  out_8(umpc52xx_psc_reg(udrv, mpc52xx_psc_buffer_8), 0x00);
  umpc52xx_set_txoe(udrv, 1);
  /* wait for TEMT */
  umpc52xx_set_wait_mode(udrv,UMPC52xx_FLG_WI_TXEMP);
  LOG_CHIO("R2T");
  return UL_RC_WIRQ;
}

static int umpc52xx_connect_2(ul_drv *udrv, int ret_code)
{
  umpc52xx_set_txoe(udrv, 0);
  if(0)
  { UL_FRET;
    LOG_CHIO(" EARBIT1!");
    return UL_RC_EARBIT;
  }
  if(udrv->chip_temp==1)
  { LOG_CHIO(" Connected");
    UL_FRET;
  }else{
    udrv->wait_time=(udrv->chip_temp&3)+1;
    udrv->chip_temp>>=2;
    UL_FCALL2(*udrv->chip_ops->fnc_wait,umpc52xx_connect_1);
  }
  return UL_RC_PROC;
}


static char *umpc52xx_port_name="ulan_umpc52xx";

#ifndef MPC52xx_PSC_AND_FIFO_SIZE
  #define MPC52xx_PSC_AND_FIFO_SIZE \
    (sizeof(struct mpc52xx_psc)+sizeof(struct mpc52xx_psc_fifo))
#endif

/*** MPC52xx integrated PSC UART initialize ports ***/
static int umpc52xx_pinit(ul_drv *udrv)
{
  static struct resource *psc_res;
  void __iomem *psc_remap_base;

  psc_res = request_mem_region(udrv->port, MPC52xx_PSC_AND_FIFO_SIZE, umpc52xx_port_name);
  if (psc_res == NULL) {
    LOG_FATAL(KERN_CRIT "uLan umpc52xx_pinit : cannot reguest ports !\n");
    return UL_RC_EPORT;
  }

  psc_remap_base = ioremap_nocache(udrv->port, MPC52xx_PSC_AND_FIFO_SIZE);
  if(psc_remap_base == NULL) {
    LOG_FATAL(KERN_CRIT "uLan umpc52xx_pinit : cannot remap base !\n");
    release_mem_region(udrv->port, MPC52xx_PSC_AND_FIFO_SIZE);
    return UL_RC_EPORT;
  }

  udrv->umpc52xx_psc_base=(long)psc_remap_base;
  udrv->umpc52xx_psc_fifo_base=(long)psc_remap_base+sizeof(struct mpc52xx_psc);

  udrv->umpc52xx_flg=0;

  /* Disable transmitter output  */
  umpc52xx_set_txoe(udrv, 0);

  out_8(umpc52xx_psc_reg(udrv, command), MPC52xx_PSC_RST_RX);		/* reset receiver */
  out_8(umpc52xx_psc_reg(udrv, command), MPC52xx_PSC_RST_TX);		/* reset transmitter */

  out_8(umpc52xx_psc_reg(udrv, command), MPC52xx_PSC_SEL_MODE_REG_1);	/* switch to access MR1 */
  // printk(KERN_INFO "MR1: %x, MR2: %x\n", in_8(umpc52xx_psc_reg(udrv, mode)), in_8(umpc52xx_psc_reg(udrv, mode)));

  /* 9-bit mode enable */
  umpc52xx_set_mode_txb8(udrv, 0);

  out_8(umpc52xx_psc_reg(udrv, command), MPC52xx_PSC_SEL_MODE_REG_1);	/* switch to access MR1 */
  // printk(KERN_INFO "MR1: %x, MR2: %x\n", in_8(umpc52xx_psc_reg(udrv, mode)), in_8(umpc52xx_psc_reg(udrv, mode)));

  out_be16(umpc52xx_psc_reg(udrv, mpc52xx_psc_clock_select), 0xdd00);	/* set 32bit prescaller */
  out_8(umpc52xx_psc_reg(udrv, ctlr), udrv->baud_div & 0xff);
  out_8(umpc52xx_psc_reg(udrv, ctur), (udrv->baud_div>>8) & 0xff);

  /* setup initial FIFO levels */
  out_8(umpc52xx_psc_fifo_reg(udrv, rfcntl), 0x00);
  out_be16(umpc52xx_psc_fifo_reg(udrv, rfalarm), 0x1ff);
  out_8(umpc52xx_psc_fifo_reg(udrv, tfcntl), 0x07);
  out_be16(umpc52xx_psc_fifo_reg(udrv, tfalarm), 0x80);

  out_8(umpc52xx_psc_reg(udrv, command), MPC52xx_PSC_TX_ENABLE);
  out_8(umpc52xx_psc_reg(udrv, command), MPC52xx_PSC_RX_ENABLE);

  in_be16(umpc52xx_psc_reg(udrv, mpc52xx_psc_status));
  in_be16(umpc52xx_psc_fifo_reg(udrv, rfnum));

  /* disable interrupt sources */
  /* enable and clear FIFOs */
  /* transmittion FIFO treshold */
  /* receive FIFO treshold */
  /* reset errors */
  /* and other irq */
  /* Enable interrupts */

  return UL_RC_PROC;
}

/*** MPC52xx integrated PSC UART deinitialize ports ***/
static int umpc52xx_pdone(ul_drv *udrv)
{
  /* disable interrupts */
  out_be16(umpc52xx_psc_reg(udrv, mpc52xx_psc_imr), 0);
  /* disable interrupts */
  out_be16(umpc52xx_psc_reg(udrv, mpc52xx_psc_imr), 0);
  /* transmitter off */
  umpc52xx_set_txoe(udrv, 0);
  out_8(umpc52xx_psc_reg(udrv, command), MPC52xx_PSC_RST_RX);		/* reset receiver */
  out_8(umpc52xx_psc_reg(udrv, command), MPC52xx_PSC_RST_TX);		/* reset transmitter */
  /* flush irq */
  /* flush irq */
  iounmap((void __iomem *)(udrv->umpc52xx_psc_base));
  release_mem_region(udrv->port, MPC52xx_PSC_AND_FIFO_SIZE);
  return UL_RC_PROC;
}

/*** MPC52xx integrated PSC UART generate irq for irq_probe */
static int umpc52xx_genirq(ul_drv *udrv,int param)
{
  if(param){
    /* transmitter off */
    umpc52xx_set_txoe(udrv, 0);
    /* enable interrupts */
    /* trig break */
  }else{
    /* disable interrupts */
    umpc52xx_set_wait_mode(udrv, 0);
    /* transmitter off */
    umpc52xx_set_txoe(udrv, 0);
    /* no break */
    /* flush irq */
  }
  return UL_RC_PROC;
}

/* support for hardware tests */
static int umpc52xx_hwtest(ul_drv *udrv,int param)
{
  return UL_RC_ENOFNC;
}

const ul_drv_chip_ops umpc52xx_chip_ops = {
  "mpc5200-psc",	/* chip_type - text type identification*/
  umpc52xx_recch,	/* fnc_recch */
  umpc52xx_sndch,	/* fnc_sndch */
  umpc52xx_wait,	/* fnc_wait */
  umpc52xx_connect,	/* fnc_connect */
  NULL,			/* fnc_finishtx */
  umpc52xx_pool,	/* fnc_pool */
  NULL,			/* fnc_cctrl */
  NULL,			/* fnc_stroke */

  umpc52xx_txoe,	/* fnc_txoe */
  umpc52xx_pinit,	/* fnc_pinit */
  umpc52xx_pdone,	/* fnc_pdone */
  umpc52xx_genirq,	/* fnc_genirq */
  ul_drv_common_fnc_rqirq_noprobe,	/* fnc_rqirq */
  ul_drv_common_fnc_freeirq,	/* fnc_freeirq */
  umpc52xx_hwtest,	/* fnc_hwtest */
  ul_drv_common_fnc_setmyadr,	/* fnc_setmyadr */
  ul_drv_common_fnc_setpromode,	/* fnc_setpromode */
};

/*** MPC52xx chip driver initialize ***/
int umpc52xx_init(ul_drv *udrv, int port, int irq, int baud, long baudbase, int options)
{
  if(!irq||!port){
    LOG_FATAL(KERN_CRIT "uLan umpc52xx_init : port or irq unknown\n");
    return UL_RC_EPORT;
  }
  UL_PRINTF(KERN_INFO "uLan umpc52xx_init : irq %d port 0x%x\n",irq,port);

  if (UL_REQ_IOS(port,8,umpc52xx_port_name)<0)
  { LOG_FATAL(KERN_CRIT "uLan umpc52xx_init : cannot reguest ports !\n");
    return UL_RC_EPORT;
  }

  { LOG_PORTS(KERN_INFO "uLan umpc52xx_init : init OK\n");
    udrv->chip_options=options;

    /* setup configurations for RX/TX */
    /* check and setup baud base oscilator frequency */
    if(!baudbase)
    {
      LOG_PORTS(KERN_ERR "uLan umpc52xx_init : using default baud base !!!\n");
      udrv->baud_base=UMPC52xx_DEF_IPB_FREQ;
    }
    else
      udrv->baud_base=baudbase;

    /* check and setup baud rate */
    if(!baud)
    {
      LOG_PORTS(KERN_ERR "uLan umpc52xx_init : using default baud rate !!!\n");
      udrv->baud_val=UMPC52xx_DEF_BAUDRATE;
    }
    else
      udrv->baud_val=baud;

    udrv->baud_div = (udrv->baud_base + udrv->baud_val*32/2)/(udrv->baud_val*32);
    if (udrv->baud_div>0xFFFFFFF)
      udrv->baud_div=0xFFFFFFF;

    /* setup driver main structure instance */
    udrv->port=port;
    udrv->irq=irq;
    udrv->chip_ops=&umpc52xx_chip_ops;
    UL_REL_IOS(port,8);
    return 0;
  }
  LOG_PORTS(KERN_ERR "uLan umpc52xx_init : MCR test Error\n");
  UL_REL_IOS(port,8);
  return UL_RC_EPORT;
}
