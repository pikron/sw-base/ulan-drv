/*******************************************************************
  uLan Communication - uL_DRV - multiplatform uLan driver

  ul_linusb.c	- Linux USB specific support routines

  (C) Copyright 1996-2004 by Pavel Pisa - project originator
        http://cmp.felk.cvut.cz/~pisa
  (C) Copyright 1996-2004 PiKRON Ltd.
        http://www.pikron.com
  (C) Copyright 2002-2004 Petr Smolik
  

  The uLan driver project can be used and distributed 
  in compliance with any of next licenses
   - GPL - GNU Public License
     See file COPYING for details.
   - LGPL - Lesser GNU Public License
   - MPL - Mozilla Public License
   - and other licenses added by project originator

  Code can be modified and re-distributed under any combination
  of the above listed licenses. If contributor does not agree with
  some of the licenses, he/she can delete appropriate line.
  WARNING: if you delete all lines, you are not allowed to
  distribute code or sources in any form.
 *******************************************************************/

/*******************************************************************/
/* USB Devices Support */

int ul_usb_ps1_init(ul_drv *udrv, ul_physaddr_t physbase, int irq, int baud, long baudbase, int options);

static int /*__devinit*/ 
  ul_usb_init_chan(struct usb_device *dev, unsigned int ifnum,
                 const struct usb_device_id *ent, ul_drv **pudrv,
                 ul_chip_init_fnc *chip_init, int options)
{
  ul_drv *udrv;
  int amy_adr=0;
  int abaud=0;
  long abaudbase=0;
  int match;
  int minor;
  int ret;
  int i;
#ifdef UL_WITH_DEVFS
  kc_devfs_handle_t devfs_handle;
  char dev_name[32];
#endif /* UL_WITH_DEVFS */
  
  *pudrv=NULL;
  /* try to find best minor and parameters */
  match=ulan_init_find_minor("usb","unknown","unknown",&minor,&i);
  if(i>=0){
    abaud=baud[i];amy_adr=my_adr[i]; abaudbase=baudbase[i];
  }

  /* mem for ul_drv */
  if(!(udrv=MALLOC(sizeof(ul_drv)))) return -ENOMEM;
  /* clear memory */
  memset(udrv,0,sizeof(ul_drv));
  /* set initial state */
  ul_drv_new_init_state(udrv, amy_adr);
  udrv->dev=(struct ul_phys_dev_ptr *)dev;
  /* init chip driver */
  if((ret=(*chip_init)(udrv, ul_uint2physaddr(ifnum), 0, abaud, abaudbase, options))<0){
    printk(KERN_CRIT "ul_usb_init_chan: ERROR - chip_init returned %d\n",ret);
    FREE(udrv);
    return -EIO;
  }
  /* setups buffers, ports and irq for sucesfully detected device */
  if((ret=ul_drv_new_start(udrv,ulbuffer))<0){
    printk(KERN_CRIT "ulan_init_chan: ERROR - ul_drv_new_start returned %d\n",ret);
    FREE(udrv);
    return -EIO;
  }
  #ifdef UL_WITH_DEVFS
  sprintf (dev_name, "ulan%d", minor);
  devfs_handle=kc_devfs_new_cdev(NULL, MKDEV(ulan_major_dev, minor), 
			S_IFCHR | S_IRUSR | S_IWUSR | S_IRGRP | S_IWGRP, 
			&ulan_fops, udrv, dev_name);
  udrv->devfs_handle=devfs_handle;
  #endif /* UL_WITH_DEVFS */

  printk(KERN_INFO "ul_usb_init_chan: minor=%d baud=%d my_adr=%d ready\n",
	 minor,udrv->baud_val,udrv->my_adr_arr[0]);
  
  if(minor>=0){
    ul_drv_arr[minor]=udrv;
    kc_class_device_create(ulan_class, NULL, MKDEV(ulan_major_dev, minor),
			kc_usb_dev_to_dev(dev), "ulan%d", minor);
  }

  *pudrv=udrv;
  return 0;
}

#if (LINUX_VERSION_CODE < VERSION(2,5,41))
static void *ul_usb_probe(struct usb_device *dev, unsigned int ifnum, const struct usb_device_id *id)
{
  unsigned long driver_info=id->driver_info;
  ul_drv *udrv;
  printk(KERN_INFO "ulan_init_one: USB device found devnum=%d, ifnum=%d\n",
         dev->devnum, ifnum);
  switch(driver_info&~0xff){
    case  UL_USB_HW_PS1:
      if(ul_usb_init_chan(dev,ifnum,id,&udrv,ul_usb_ps1_init,driver_info)<0)
        return NULL;
      printk(KERN_INFO "ul_usb_probe: data type\n");
      return udrv;
  }
  printk(KERN_CRIT "ul_usb_probe: No device of specified driver_data type\n");
  return NULL;
}
#else /* 2.5.41 */
static int ul_usb_probe(struct usb_interface *intf, const struct usb_device_id *id)
{
  struct usb_device *dev = interface_to_usbdev(intf);
  int ifnum = intf->altsetting->desc.bInterfaceNumber;
  unsigned long driver_info=id->driver_info;
  ul_drv *udrv;
  dev_set_drvdata(&intf->dev,NULL);
  printk(KERN_INFO "ulan_init_one: USB device found devnum=%d, ifnum=%d\n",
         dev->devnum, ifnum);
  switch(driver_info&~0xff){
    case  UL_USB_HW_PS1:
      if(ul_usb_init_chan(dev,ifnum,id,&udrv,ul_usb_ps1_init,driver_info)<0)
        return -EIO;
      printk(KERN_INFO "ul_usb_probe: data type\n");
      dev_set_drvdata(&intf->dev,udrv);
      return 0;
  }
  printk(KERN_CRIT "ul_usb_probe: No device of specified driver_data type\n");
  return -EIO;
}
#endif /* 2.5.41 */

#if (LINUX_VERSION_CODE < VERSION(2,5,41))
static void ul_usb_disconnect(struct usb_device *dev, void *ptr)
{
  ul_drv *udrv, *next_udrv;
  int i;
  udrv=(ul_drv *)ptr;
  if(!udrv){
    printk(KERN_CRIT "ulan_remove_one: no uLan drvdata\n");
    return;
  }
  for(;udrv;udrv=next_udrv){
    if(udrv->magic!=UL_DRV_MAGIC){
      printk(KERN_CRIT "ulan_remove_one: Wrong uLan MAGIC number!!!\n");
      return;
    }
    next_udrv=udrv->next_chan;
    if(dev!=(struct usb_device *)udrv->dev){
      printk(KERN_CRIT "ul_usb_disconnect: BAD - cross USB device remove\n");
    }
    for(i=0;i<UL_MINORS;i++){
      if (udrv==ul_drv_arr[i]) ul_drv_arr[i]=NULL;
    }
    #ifdef UL_WITH_DEVFS
    if(udrv->devfs_handle) kc_devfs_delete(udrv->devfs_handle);
    #endif /* UL_WITH_DEVFS */
    ul_drv_free(udrv);
  }
  printk(KERN_INFO "ul_usb_disconnect: USB device removed\n");
}
#else /* 2.5.41 */
static void ul_usb_disconnect(struct usb_interface *intf)
{
  struct usb_device *dev = interface_to_usbdev(intf);
  ul_drv *udrv, *next_udrv;
  int i;
  udrv=(ul_drv *)dev_get_drvdata(&intf->dev);
  dev_set_drvdata(&intf->dev,NULL);
  if(!udrv){
    printk(KERN_CRIT "ulan_remove_one: no uLan drvdata\n");
    return;
  }
  for(;udrv;udrv=next_udrv){
    if(udrv->magic!=UL_DRV_MAGIC){
      printk(KERN_CRIT "ulan_remove_one: Wrong uLan MAGIC number!!!\n");
      return;
    }
    next_udrv=udrv->next_chan;
    if(dev!=(struct usb_device *)udrv->dev){
      printk(KERN_CRIT "ul_usb_disconnect: BAD - cross USB device remove\n");
    }
    for(i=0;i<UL_MINORS;i++){
      if (udrv==ul_drv_arr[i]){
        ul_drv_arr[i]=NULL;
        kc_class_device_destroy(ulan_class, MKDEV(ulan_major_dev, i));
      }
    }
    #ifdef UL_WITH_DEVFS
    if(udrv->devfs_handle) kc_devfs_delete(udrv->devfs_handle);
    #endif /* UL_WITH_DEVFS */
    ul_drv_free(udrv);
  }
  printk(KERN_INFO "ul_usb_disconnect: USB device removed\n");
}
#endif /* 2.5.41 */

#if defined(CONFIG_PM) && (LINUX_VERSION_CODE > VERSION(2,6,18))
static int ul_usb_suspend(struct usb_interface *intf, pm_message_t message)
{
  ul_drv *udrv, *udrv4pdev;
  int ret;
  struct usb_device *usbdev = interface_to_usbdev(intf);

  /*udrv4pdev=(ul_drv *) = usb_get_intfdata(intf);*/
  udrv4pdev=(ul_drv *)dev_get_drvdata(&intf->dev);
  if(!udrv4pdev){
    printk(KERN_CRIT "ul_usb_suspend: no uLan drvdata\n");
    return 0;
  }
  for(udrv=udrv4pdev;udrv;udrv=(udrv->next_chan!=udrv4pdev)?udrv->next_chan:NULL){
    if(udrv->magic!=UL_DRV_MAGIC){
      printk(KERN_CRIT "ul_usb_suspend: Wrong uLan MAGIC number!!!\n");
      return 0;
    }
    if((struct ul_phys_dev_ptr *)usbdev!=udrv->dev){
      printk(KERN_CRIT "ul_usb_suspend: BAD - cross USB device remove\n");
      return 0;
    }
    ret=ul_drv_stop(udrv);
    if(ret<0)
    { UL_PRINTF(KERN_CRIT "ul_usb_suspend : ul_drv_stop failed\n");
    }
  }
  return 0;
}

static int ul_usb_resume(struct usb_interface *intf)
{
  ul_drv *udrv, *udrv4pdev;
  int ret;
  struct usb_device *usbdev = interface_to_usbdev(intf);

  /*udrv4pdev=(ul_drv *) = usb_get_intfdata(intf);*/
  udrv4pdev=(ul_drv *)dev_get_drvdata(&intf->dev);
  if(!udrv4pdev){
    printk(KERN_CRIT "ul_usb_resume: no uLan drvdata\n");
    return 0;
  }
  for(udrv=udrv4pdev;udrv;udrv=(udrv->next_chan!=udrv4pdev)?udrv->next_chan:NULL){
    if(udrv->magic!=UL_DRV_MAGIC){
      printk(KERN_CRIT "ul_usb_resume: Wrong uLan MAGIC number!!!\n");
      return 0;
    }
    if((struct ul_phys_dev_ptr *)usbdev!=udrv->dev){
      printk(KERN_CRIT "ul_usb_resume: BAD - cross USB device remove\n");
      return 0;
    }
    if(!uld_atomic_test_dfl(udrv,CHIPOK)){
      ret=ul_drv_start(udrv,0);
      if(ret<0){
        UL_PRINTF(KERN_CRIT "ul_usb_resume : ul_drv_start failed\n");
        return -EIO;
      }
    }
  }
  return 0;
}

#endif /*CONFIG_PM*/