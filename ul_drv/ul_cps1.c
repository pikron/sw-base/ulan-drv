  /*******************************************************************
  uLan Communication - uL_DRV - multiplatform uLan driver

  ul_cps1.c	- chip driver for USB2uLan device developed
                  by Petr Smolik

  (C) Copyright 1996-2004 by Pavel Pisa - project originator
        http://cmp.felk.cvut.cz/~pisa
  (C) Copyright 1996-2004 PiKRON Ltd.
        http://www.pikron.com
  (C) Copyright 2002-2004 Petr Smolik
  

  The uLan driver project can be used and distributed 
  in compliance with any of next licenses
   - GPL - GNU Public License
     See file COPYING for details.
   - LGPL - Lesser GNU Public License
   - MPL - Mozilla Public License
   - and other licenses added by project originator

  Code can be modified and re-distributed under any combination
  of the above listed licenses. If contributor does not agree with
  some of the licenses, he/she can delete appropriate line.
  WARNING: if you delete all lines, you are not allowed to
  distribute code or sources in any form.
 *******************************************************************/

/*******************************************************************/
/* usb2ulan device requests */

#define U2U_PKT_BUF_SIZE       32     /* driver maximal supported packed size */
#define U2U_MAX_MSG_INPR        4     /* maximal number of messages in progress */

#define VENDOR_START_ULAN       0     /* Inform converter about uLan driver ready */
#define VENDOR_STOP_ULAN        1     /* Inform about uLan driver stopping  */
#define VENDOR_IS_RUNNING_ULAN  2
#define VENDOR_SETPROMODE_ULAN  3     /* Set promiscuous monitoring mode */

#define U2UBFL_NORE     0x040         /* Do not try to repeat if error occurs */ 
#define U2UBFL_TAIL     0x020         /* Message has tail frame */
#define U2UBFL_REC      0x010         /* Request receiption of block */
#define U2UBFL_FAIL     0x008         /* Message cannot be send - error */
#define U2UBFL_PROC	0x004         /* Message succesfull send */
#define U2UBFL_AAP	0x003         /* Request imediate proccessing of frame by receiver station with acknowledge */
#define U2UBFL_PRQ	0x002         /* Request imediate proccessing of frame by receiver station */
#define U2UBFL_ARQ	0x001         /* Request imediate acknowledge by receiving station */

/* Definition of chip_buff usage */
#define ul_usb_in_urb_st chip_buff[0] /* completion status of  ?_complete_in */
#define ul_usb_max_pkt_size chip_buff[1] /* maximal USB packet size */
#define ul_usb_max_msg_inpr chip_buff[2] /* maximal number of messages in progress */
#define ul_usb_outpipe_nr   chip_buff[3] /* OUT pipe number */
#define ul_usb_inpipe_nr    chip_buff[4] /* IN pipe number */
#define ul_usb_cfg_updatefl chip_buff[5] /* flags for configuration update - promode, addr, baurate */

#define UL_USB_CFGUEV_ADDRBAUD  0
#define UL_USB_CFGUEV_PROMODE   1

#ifdef FOR_LINUX_KERNEL
#if (LINUX_VERSION_CODE <= VERSION(2,4,10))
  #define usb_fill_bulk_urb FILL_BULK_URB
#endif

#if (LINUX_VERSION_CODE < VERSION(2,5,0))
  #define kc_usb_alloc_urb(urb, gfp) usb_alloc_urb(urb) 
  #define kc_usb_submit_urb(urb, gfp) usb_submit_urb(urb) 
#else
  #define kc_usb_alloc_urb usb_alloc_urb
  #define kc_usb_submit_urb usb_submit_urb
#endif
#endif /*FOR_LINUX_KERNEL*/


#ifdef _WIN32
int ul_usb_msg_inpr=0;	/* number of concurrent messages sent over USB */
#else
extern int ul_usb_msg_inpr;	/* number of concurrent messages sent over USB */
#endif /* _WIN32 */


/* Support for multiple submitted messages */

typedef struct ul_usb_tx_fifo_slot{
  ul_mem_blk    *msg;
  unsigned char ustamp;
}ul_usb_tx_fifo_slot;

typedef struct ul_usb_tx_fifo{
  ul_usb_tx_fifo_slot slot[U2U_MAX_MSG_INPR];
  int size;
  int used;
  int idxin;
  int idxout;
}ul_usb_tx_fifo;

static int ul_usb_tx_fifo_empty(const ul_usb_tx_fifo *fifo)
{
  return fifo->slot[fifo->idxout].msg==NULL;
}

static int ul_usb_tx_fifo_full(const ul_usb_tx_fifo *fifo)
{
  return fifo->slot[fifo->idxin].msg!=NULL;
}

static int ul_usb_tx_fifo_incidx(const ul_usb_tx_fifo *fifo, int idx)
{
  if(++idx>=fifo->size) idx=0;
  return idx;
}

static void ul_usb_tx_fifo_print(const ul_usb_tx_fifo *fifo)
{
  int i;
  LOG_MESSAGES("ul_usb: tx_fifo: idxin=%d idxout=%d [",
            fifo->idxin,fifo->idxout);
  for(i=0;i<fifo->size;i++){
    if(i) LOG_MESSAGES(",");
    if(!fifo->slot[i].msg){
      LOG_MESSAGES("Empty");
    }else{
      LOG_MESSAGES("%d",fifo->slot[i].ustamp);
    }
  }
  LOG_MESSAGES("]\n");
}

enum ul_usb_ps1_in_urb_st{
  UL_USB_IUST_NONE=0,
  UL_USB_IUST_WAITING,
  UL_USB_IUST_READY,
  UL_USB_IUST_ERROR_RETRY,
  UL_USB_IUST_ERROR_FATAL,
  UL_USB_IUST_CANCEL_STARTED,
  UL_USB_IUST_CANCEL_COMPLETE,
};

INLINE int ul_usb_ps1_in_urb_st_get(const ul_drv *udrv)
{
  int in_urb_st;
  UL_MB();
  in_urb_st=*(volatile unsigned *)&udrv->ul_usb_in_urb_st;
  UL_MB();
  return in_urb_st;
}

INLINE void ul_usb_ps1_in_urb_st_set(const ul_drv *udrv, int in_urb_st)
{
  UL_MB();
  *(volatile unsigned *)&udrv->ul_usb_in_urb_st=in_urb_st;
  UL_MB();
}

INLINE int ul_usb_ps1_in_urb_st_xchg(ul_drv *udrv, int in_urb_st)
{
 #ifndef _WIN32
  UL_MB();
  in_urb_st=xchg(&udrv->ul_usb_in_urb_st,in_urb_st);
  UL_MB();
 #else
  in_urb_st=InterlockedExchange(&udrv->ul_usb_in_urb_st,in_urb_st);
 #endif
  return in_urb_st;
}

static int ul_usb_ps1_cfg_updatefl_set(ul_drv *udrv, int cfg_event)
{
  if(cfg_event>=sizeof(udrv->ul_usb_cfg_updatefl))
    return -1;
  ((char*)&(udrv->ul_usb_cfg_updatefl))[cfg_event]=1;
  return 0;
}

static int ul_usb_ps1_cfg_updatefl_empty(ul_drv *udrv)
{
  return udrv->ul_usb_cfg_updatefl==0;
}

static int ul_usb_ps1_cfg_updatefl_test_and_clear(ul_drv *udrv, int cfg_event)
{
  int ret;
  if(cfg_event>=sizeof(udrv->ul_usb_cfg_updatefl))
    return -1;
  ret=((char*)&(udrv->ul_usb_cfg_updatefl))[cfg_event];
  ((char*)&(udrv->ul_usb_cfg_updatefl))[cfg_event]=0;
  return ret;
}

static int ul_usb_ps1_update_addrbaud(ul_drv *udrv)
{
  struct usb_device *dev;
  unsigned char *u2u_ret;
  int ret;
  dev=(struct usb_device *)udrv->dev;
  /* Set converter parameters */
  LOG_IRQ("ul_usb_ps1_set_addrbaud: baudrate:%d\n",udrv->baud_val);

  u2u_ret = MALLOC(4);
  if (u2u_ret == NULL)
    return UL_RC_ENOMEM;
  u2u_ret[0] = 0;
  ret=usb_control_msg(dev, usb_rcvctrlpipe(dev,0),
              VENDOR_START_ULAN, USB_TYPE_VENDOR | USB_DIR_IN,
              /*value*/ (short)udrv->baud_val, /*index*/ (short)udrv->my_adr_arr[0], u2u_ret, 1, 2*ULD_HZ);
  if(ret<0) {
    LOG_FATAL(KERN_CRIT "ul_usb_ps1_set_addrbaud: KWT usb_control_msg error %d returned\n",ret);
    FREE(u2u_ret);
    return UL_RC_EUSB;
  }
  if(!u2u_ret[0]) {
    LOG_FATAL(KERN_CRIT "ul_usb_ps1_set_addrbaud: not supported baudrate (%d)\n",udrv->baud_val);
    FREE(u2u_ret);
    return UL_RC_EBAUD;
  }
  FREE(u2u_ret);
  return UL_RC_PROC;
};

static int ul_usb_ps1_update_promode(ul_drv *udrv)
{
  struct usb_device *dev;
  int ret;
  dev=(struct usb_device *)udrv->dev;
  /* set pro_mode */
  LOG_IRQ("ul_usb_ps1_set_promode: %d\n",udrv->pro_mode);
  ret=usb_control_msg(dev, usb_rcvctrlpipe(dev,0), 
              VENDOR_SETPROMODE_ULAN, USB_TYPE_VENDOR,
              /*value*/ (short)(udrv->pro_mode&UL_PRO_MODE_CAPTURE_MASK),
              /*index*/ (short)0, NULL, 0, ULD_HZ);
  if(ret<0) {
    LOG_FATAL(KERN_CRIT "ul_usb_ps1_setpromode: KWT usb_control_msg error %d returned\n",ret);
    return UL_RC_EUSB;
  }
  return UL_RC_PROC;
}

static int ul_usb_ps1_start(ul_drv *udrv)
{
   return ul_usb_ps1_update_addrbaud(udrv); 
}

static int ul_usb_ps1_stop(ul_drv *udrv)
{
  struct usb_device *dev;
  int ret;
  dev=(struct usb_device *)udrv->dev;
  /* Set converter parameters */
  LOG_IRQ("ul_usb_ps1_stop:\n");
  ret=usb_control_msg(dev, usb_rcvctrlpipe(dev,0),
                      VENDOR_STOP_ULAN, USB_TYPE_VENDOR,
                      /*value*/ 0, /*index*/ 0, NULL, 0, ULD_HZ);
  if(ret<0) {
    LOG_FATAL(KERN_CRIT "ul_usb_ps1_thread: KWT usb_control_msg error %d returned\n",ret);
    return UL_RC_EUSB;
  }
  return UL_RC_PROC;
}

/*** Input completion for Petr Smolik's USB2uLan converter ***/
#ifdef FOR_LINUX_KERNEL
#if (LINUX_VERSION_CODE >= VERSION(2,6,0)) && (LINUX_VERSION_CODE <= VERSION(2,6,18))
  static void ul_usb_ps1_complete_in(struct urb *urb,struct pt_regs *regs)
#else
  static void ul_usb_ps1_complete_in(struct urb *urb)
#endif
#else /*FOR_LINUX_KERNEL*/
  NTSTATUS ul_usb_ps1_complete_in( IN PDEVICE_OBJECT DeviceObject, IN PIRP Irp, IN PVOID Context)
#endif /*FOR_LINUX_KERNEL*/
{
  ul_drv *udrv;
  int st;

  #ifdef FOR_LINUX_KERNEL
    udrv=urb->context;
  #else /*FOR_LINUX_KERNEL*/
    udrv=(ul_drv*)Context;
  #endif /*FOR_LINUX_KERNEL*/
  if(udrv->magic!=UL_DRV_MAGIC){
    LOG_FATAL(KERN_CRIT "ul_usb_ps1_complete_in: Wrong uLan MAGIC number!!!\n");
    #ifdef _WIN32
      return STATUS_INVALID_PARAMETER;
    #else /* _WIN32 */
      return;
    #endif /* _WIN32 */
  }
  #ifdef FOR_LINUX_KERNEL
    st=urb->status;
    if(st>=0) 
      st=UL_USB_IUST_READY;
    else if ((st==-ENODEV)||(st==-EILSEQ)||(st==-EPROTO))
      st=UL_USB_IUST_ERROR_FATAL;
    else
      st=UL_USB_IUST_ERROR_RETRY;
  #else
    switch (Irp->IoStatus.Status) {
      case STATUS_SUCCESS:
        st=UL_USB_IUST_READY;
        break;
      case STATUS_DEVICE_NOT_CONNECTED:
        st=UL_USB_IUST_ERROR_FATAL;
        LOG_FATAL(KERN_CRIT "ul_usb_ps1_complete_in STATUS_DEVICE_NOT_CONNECTED\n");
        break;
      case STATUS_PENDING:
        st=UL_USB_IUST_ERROR_FATAL;
        LOG_FATAL(KERN_CRIT "ul_usb_ps1_complete_in STATUS_PENDING\n");
        break;
      case STATUS_DEVICE_DATA_ERROR:
        st=UL_USB_IUST_ERROR_RETRY;
        LOG_FATAL(KERN_CRIT "ul_usb_ps1_complete_in STATUS_DEVICE_DATA_ERROR\n");
        break;
      case STATUS_TIMEOUT:
        st=UL_USB_IUST_ERROR_RETRY;
        LOG_FATAL(KERN_CRIT "ul_usb_ps1_complete_in STATUS_TIMEOUT\n");
        break;
      case STATUS_INSUFFICIENT_RESOURCES:
        st=UL_USB_IUST_ERROR_RETRY;
        LOG_FATAL(KERN_CRIT "ul_usb_ps1_complete_in STATUS_INSUFFICIENT_RESOURCES\n");
        break;
      default:
        st=UL_USB_IUST_ERROR_RETRY;
        LOG_FATAL(KERN_CRIT "ul_usb_ps1_complete_in URB IoStatus 0x%08lx\n",
                          (unsigned long)Irp->IoStatus.Status);
    }
  #endif

  LOG_IRQ(KERN_CRIT "ul_usb_ps1_complete_in\n");
  st=ul_usb_ps1_in_urb_st_xchg(udrv, st);
  uld_kwt_wake(udrv);

  #ifdef _WIN32
  if(st==UL_USB_IUST_CANCEL_STARTED)
      return STATUS_MORE_PROCESSING_REQUIRED;
  return STATUS_CONTINUE_COMPLETION;
  #endif /* _WIN32 */
}


/*** Send one frame to USB2uLan converter ***/
static int ul_usb_ps1_send_frame(ul_drv *udrv, ul_mem_blk *frame,
             unsigned char ustamp, unsigned char *work_buf)
{
  /*unsigned char buf[U2U_PKT_BUF_SIZE];*/
  unsigned char *buf=work_buf;
  int i, ret;
  ul_data_it di;
  struct usb_device *dev=(struct usb_device *)udrv->dev;
  int max_pkt=udrv->ul_usb_max_pkt_size;
  ul_nadr_t dadr=UL_BLK_HEAD(frame).dadr;
  ul_nadr_t sadr=UL_BLK_HEAD(frame).sadr;
  uchar cmd=UL_BLK_HEAD(frame).cmd;
  unsigned flg=UL_BLK_HEAD(frame).flg;
  unsigned len=UL_BLK_HEAD(frame).len;
 #ifdef UL_WITH_MULTI_NET
  uchar ext_header=0;
  uchar ext_data[11];
  uchar *ext_ptr=&ext_data[1];
  int ext_len=0;
  int dadr_lg2l;
  int sadr_lg2l;
  int dadr_local;
  int sadr_local;

  dadr_local=ul_dadr2dadr_local(udrv,dadr,&dadr_lg2l);
  sadr_local=ul_sadr2sadr_local(udrv,sadr,&sadr_lg2l);
  if (dadr_lg2l) {
    ext_ptr=ul_nadr2buff(ext_ptr,dadr,dadr_lg2l);
    ext_header|=dadr_lg2l;
  }
  if (sadr_lg2l) {
    ext_ptr=ul_nadr2buff(ext_ptr,sadr,sadr_lg2l);
    ext_header|=sadr_lg2l<<3;
  }
  if(ext_header) {
    ext_data[0]=ext_header;
    *(ext_ptr++)=UL_BLK_HEAD(frame).hops;
    *(ext_ptr++)=cmd;
    cmd=UL_CMD_EADR;
    ext_len=ext_ptr-ext_data;
    dadr=dadr_local;
    sadr=sadr_local;
    len+=ext_len;
    ext_ptr=ext_data;
  }
 #endif /*UL_WITH_MULTI_NET*/
  buf[0]=dadr;
  buf[1]=sadr;
  buf[2]=cmd;
  buf[3]=0;
  if(flg&UL_BFL_ARQ) buf[3]|=U2UBFL_ARQ;
  if(flg&UL_BFL_PRQ) buf[3]|=U2UBFL_PRQ;
  if(flg&UL_BFL_NORE) buf[3]|=U2UBFL_NORE;
  if(flg&UL_BFL_TAIL) buf[3]|=U2UBFL_TAIL;
  if(flg&UL_BFL_REC) buf[3]|=U2UBFL_REC;
  buf[4]=ustamp;
  buf[5]=0;

  if(flg&UL_BFL_REC){	/* Placeholder message for receive does not */
    len=0;		/* need data, but should check UL_BFL_LNMM !!! */
  }
  buf[6]=(unsigned char)len; buf[7]=(unsigned char)(len/0x100);

  LOG_IRQ(KERN_CRIT "ul_usb : sending head URB (d:%d,s:%d,c:%X,f:%X,l:%d) stmp %d\n",
               buf[0],buf[1],buf[2],buf[3],
               buf[6]+0x100*buf[7],buf[4]);

  ret=usb_bulk_msg(dev, usb_sndbulkpipe(dev, udrv->ul_usb_outpipe_nr), buf, 8, &i, ULD_HZ);
  if(ret<0) {
    LOG_FATAL(KERN_CRIT "ul_usb_ps1_send_frame: usb_bulk_write head error %d returned\n",ret);
    return ret;
  }

  ul_di_init(&di,frame);
  while(len>0) {
    int chunk_len=(int)len<max_pkt?len:max_pkt;
   #ifdef UL_WITH_MULTI_NET
    int data_len=chunk_len;
    uchar *data_ptr=buf;
    if(ext_len) {
      int ext_chunk;
      ext_chunk=ext_len<chunk_len?ext_len:chunk_len;
      ext_len-=ext_chunk;
      data_len-=ext_chunk;
      data_ptr+=ext_chunk;
      memcpy(buf,ext_ptr,ext_chunk);
      ext_ptr+=ext_chunk;
    }
    if(data_len)
      ul_di_read(&di, data_ptr, data_len);
   #else /*UL_WITH_MULTI_NET*/
    ul_di_read(&di, buf, chunk_len);
   #endif /*UL_WITH_MULTI_NET*/
    ret=usb_bulk_msg(dev, usb_sndbulkpipe(dev, udrv->ul_usb_outpipe_nr), buf,
                     chunk_len, &chunk_len, ULD_HZ);
    if(ret<0) {
      LOG_FATAL(KERN_CRIT "ul_usb_ps1_send_frame: usb_bulk_write data error %d returned\n",ret);
      return ret;
    }
    len-=chunk_len;
  }
  return di.pos;
}

/*** Receive data of one frame from USB2uLan converter ***/
static int ul_usb_ps1_receive_data(ul_drv *udrv, ul_mem_blk *frame, int len,
                            int discard_fl, unsigned char *work_buf)
{
  /*unsigned char buf[U2U_PKT_BUF_SIZE];*/
  unsigned char *buf=work_buf;
  unsigned char *pbuf;
  int i, ret, ext_st;
  ul_data_it di;
  struct usb_device *dev=(struct usb_device *)udrv->dev;
  int max_pkt=udrv->ul_usb_max_pkt_size;

  ext_st=-1;
 #ifdef UL_WITH_MULTI_NET
  if (UL_BLK_HEAD(frame).cmd==UL_CMD_EADR) ext_st=0;
 #endif /* UL_WITH_MULTI_NET */
  
  if (!discard_fl)
    ul_di_init(&di,frame);
  while(len>0) {
    i=len<max_pkt?len:max_pkt; len-=i;
    ret=usb_bulk_msg(dev, usb_rcvbulkpipe(dev, udrv->ul_usb_inpipe_nr), buf, i, &i, ULD_HZ);
    if(ret<0) {
      LOG_FATAL(KERN_CRIT "ul_usb_ps1_thread: usb_bulk_read data error %d returned\n",ret);
      return ret;
    } else {
      if ((discard_fl) || (i<=0))
	continue;
      pbuf=buf;
     #ifdef UL_WITH_MULTI_NET
      if (ext_st==0) {
        int ext_ln;
        udrv->con_ext_header=*(pbuf++); i--;
        ext_ln=ul_ext_header2len(udrv->con_ext_header);
        if((ext_ln<0)||(udrv->con_ext_header&0xC0)) {
          LOG_FATAL(KERN_CRIT "ul_usb_ps1_thread: invalid ExtHeader\n");
          discard_fl = 1;
          continue;
        }
        udrv->con_ext_len=ext_ln;
        udrv->con_pos=0;
        if (ext_ln>0) ext_st=1;
        else ext_st=2;
        UL_BLK_HEAD(frame).len-=(ext_ln+2);
      }
      if (ext_st==1) {
        while(i>0) {
          if (udrv->con_pos>=udrv->con_ext_len) {
            ext_st=2;
            break;
          }
          udrv->con_ext_data[udrv->con_pos++]=*(pbuf++); i--;
        }
      }
      if (ext_st==2) {
        if (i>0) {
          int dadr_lg2l=udrv->con_ext_header&7;
          int sadr_lg2l=(udrv->con_ext_header>>3)&7;
          uchar *ptr=udrv->con_ext_data;
          UL_BLK_HEAD(frame).cmd=*(pbuf++); i--;
          if(dadr_lg2l)
            ptr=ul_buff2nadr(ptr,&UL_BLK_HEAD(frame).dadr,dadr_lg2l);
          if(sadr_lg2l)
            ptr=ul_buff2nadr(ptr,&UL_BLK_HEAD(frame).sadr,sadr_lg2l);
          udrv->con_hops=*(ptr++);
          ext_st=-1;
        }
      }
     #endif /* UL_WITH_MULTI_NET */
      if (ext_st==-1) {
        if(i!=ul_di_write(&di, pbuf, i)){
          LOG_FATAL(KERN_CRIT "ul_usb_ps1_thread: usb_bulk_read data error %d returned\n",ret);
          discard_fl = 1;
        }
      }
    }
  }
  if (discard_fl) {
   #ifdef FOR_LINUX_KERNEL
    return -EIO;
   #else
    return -1;
   #endif /* FOR_LINUX_KERNEL */
  }
  return di.pos;
}

/*** Worker thread for Petr Smolik's USB2uLan converter ***/
static int ul_usb_ps1_thread(void *ptr)
{
  ul_drv *udrv=(ul_drv *)ptr;
  struct usb_device *dev;
  int ret;
  int i, len;
  int fatal_err=0;
  unsigned char *in_buf=NULL;
  unsigned char *work_buf=NULL;
  unsigned char ustamp=0;
  ul_mem_blk *tx_mes=NULL;
  ul_mem_blk *rx_mes=NULL;
  ul_mem_blk *frame;
  ul_mem_blk *tail_frame=NULL;
  long tx_sta_time;
  ul_usb_tx_fifo tx_fifo;
  struct urb *in_urb;
  volatile unsigned in_urb_st;

  if(udrv->magic!=UL_DRV_MAGIC)
  {
    LOG_FATAL(KERN_CRIT "ul_usb_ps1_thread: Wrong uLan MAGIC number!!!\n");
    uld_kwt_set_stopped(udrv);
    return -ENODEV;
  }
  dev=(struct usb_device *)udrv->dev;
  if(udrv->ul_usb_max_pkt_size>U2U_PKT_BUF_SIZE)
    udrv->ul_usb_max_pkt_size=U2U_PKT_BUF_SIZE;

  if(udrv->ul_usb_max_msg_inpr>U2U_MAX_MSG_INPR)
    udrv->ul_usb_max_msg_inpr=U2U_MAX_MSG_INPR;
  memset(&tx_fifo,0,sizeof(tx_fifo));
  tx_fifo.size=udrv->ul_usb_max_msg_inpr;

#ifdef FOR_LINUX_KERNEL
#if (LINUX_VERSION_CODE >= VERSION(2,6,0))
  /*daemonize("kulusbps1d");*/
  /*allow_signal(SIGKILL);*/
#else /* <2.6.0 */
  lock_kernel();
  daemonize();

 #if (LINUX_VERSION_CODE >= VERSION(2,4,10))
  exit_files(current);
  current->files = init_task.files;
  atomic_inc(&current->files->count);
  daemonize();
  reparent_to_init();
 #endif /* >=2.4.10 */

  /* avoid getting signals */
  spin_lock_irq(&current->sigmask_lock);
  flush_signals(current);
  sigfillset(&current->blocked);
  recalc_sigpending(current);
  spin_unlock_irq(&current->sigmask_lock);

  set_fs(KERNEL_DS);
  strcpy(current->comm, "kulusbps1d");
  unlock_kernel();
#endif /* <2.6.0 */
#endif /*FOR_LINUX_KERNEL*/

  /* wait till active */
  while(!uld_kwt_should_stop(udrv)){
    uld_kwt_confirm(udrv);
    if (uld_atomic_test_dfl(udrv,KWTACTIVE)) {
      LOG_FATAL(KERN_INFO "ul_usb: KWT activated\n");
      break;
    }
    if(uld_kwt_wait(udrv)!=ULD_KWT_WAIT_SUCCESS) {
      LOG_FATAL(KERN_INFO "ul_usb: KWT wait for event interrupted/killed, stopping\n");
      break;
    }
  }
  if (!uld_atomic_test_dfl(udrv,KWTACTIVE)) {
    LOG_IRQ(KERN_INFO "ul_usb: KWT finished - not activated\n");
    uld_kwt_finished(udrv);
    return 0;
  }

  in_buf = MALLOC(U2U_PKT_BUF_SIZE);
  if (in_buf == NULL) {
    LOG_FATAL(KERN_CRIT "ul_usb_ps1_thread: KWT cannot alloc memory for in_buf\n");
    fatal_err=1;
  }

  work_buf = MALLOC(U2U_PKT_BUF_SIZE);
  if (work_buf == NULL) {
    LOG_FATAL(KERN_CRIT "ul_usb_ps1_thread: KWT cannot alloc memory for work_buf\n");
    fatal_err=1;
  }

 /* start converter with preset parameters */
 ret = ul_usb_ps1_start(udrv);
 if (ret<0) {
   LOG_FATAL(KERN_CRIT "ul_usb_ps1_start: return with ret_code (%d)\n", ret);
   fatal_err=1;
 }

 #ifdef FOR_LINUX_KERNEL
  if((in_urb=kc_usb_alloc_urb(0, GFP_KERNEL))) {
    /* URB allocated OK */
  }else{
    LOG_FATAL(KERN_CRIT "ul_usb_ps1_thread: KWT cannot alloc in_urb\n");
    fatal_err=1;
  }
 #else
  in_urb = usb_alloc_urb(sizeof( struct _URB_BULK_OR_INTERRUPT_TRANSFER),0);
  if ( !in_urb) {
    LOG_FATAL(KERN_CRIT "ul_usb_ps1_thread: KWT cannot alloc in_urb\n");
    fatal_err=1;
  }
 #endif /* FOR_LINUX_KERNEL */

  usb_clear_halt(dev, usb_sndbulkpipe(dev, udrv->ul_usb_outpipe_nr));
  usb_clear_halt(dev, usb_rcvbulkpipe(dev, udrv->ul_usb_inpipe_nr));
  ul_usb_ps1_in_urb_st_set(udrv,UL_USB_IUST_NONE);

  while(!uld_kwt_should_stop(udrv) && !fatal_err){

    uld_kwt_confirm(udrv);

    /* check update flags */
    if (!ul_usb_ps1_cfg_updatefl_empty(udrv)) {
      if (ul_usb_ps1_cfg_updatefl_test_and_clear(udrv,UL_USB_CFGUEV_ADDRBAUD)) {
        ul_usb_ps1_update_addrbaud(udrv);
      }
      if (ul_usb_ps1_cfg_updatefl_test_and_clear(udrv,UL_USB_CFGUEV_PROMODE)) {
        ul_usb_ps1_update_promode(udrv);
      }
    }

    while(!ul_usb_tx_fifo_full(&tx_fifo)){ /* when free Tx slot */

      /* Initiate sending of message from the prep_bll queue */
      if(!(tx_mes=udrv->con_message=udrv->prep_bll.first)) break;

      /* printudrvbll(udrv); */
      ul_bll_move_mes(&udrv->work_bll,tx_mes);
      LOG_MESSAGES(KERN_INFO "ul_usb: KWT processing message\n");

      if(!++ustamp) ustamp=1;

      /* send frames of message */
      for(frame=tx_mes;frame;frame=UL_BLK_HEAD(frame).next){
	ret=ul_usb_ps1_send_frame(udrv, frame, ustamp, work_buf);
	if(ret<0) {
          usb_clear_halt(dev, usb_sndbulkpipe(dev, udrv->ul_usb_outpipe_nr));
          UL_BLK_HEAD(tx_mes).flg|=UL_BFL_FAIL;
          ul_bll_move_mes(&udrv->proc_bll,tx_mes);
          SCHEDULE_BH(udrv);
          /* ulan_do_bh(udrv); */
	  tx_mes=udrv->con_message=NULL;
          break;
	}
	/* Message has no more frames */
	if(!(UL_BLK_HEAD(frame).flg&UL_BFL_TAIL)||
	   (UL_BLK_HEAD(frame).next==NULL)){
	  tx_fifo.slot[tx_fifo.idxin].ustamp=ustamp;
	  tx_fifo.slot[tx_fifo.idxin].msg=tx_mes;
          tx_fifo.idxin=ul_usb_tx_fifo_incidx(&tx_fifo,tx_fifo.idxin);
          #ifdef FOR_LINUX_KERNEL
            tx_sta_time=jiffies;
          #endif /* FOR_LINUX_KERNEL */
          ul_usb_tx_fifo_print(&tx_fifo);
	}
      }
    }
    
    UL_MB();
    in_urb_st=ul_usb_ps1_in_urb_st_get(udrv);
    UL_MB();
    if(in_urb_st==UL_USB_IUST_READY) {
      UL_MB();
      LOG_IRQ(KERN_CRIT "ul_usb : received head URB (d:%d,s:%d,c:%X,f:%X,l:%d) stmp %d\n",
                   in_buf[0],in_buf[1],in_buf[2],in_buf[3],
                   in_buf[6]+0x100*in_buf[7],in_buf[4]);
      if(in_buf[3]&(U2UBFL_PROC|U2UBFL_FAIL)){
        tx_mes=NULL;
        ul_usb_tx_fifo_print(&tx_fifo);
        if(!ul_usb_tx_fifo_empty(&tx_fifo)){
          if(in_buf[4]==tx_fifo.slot[tx_fifo.idxout].ustamp){
	    tx_mes=tx_fifo.slot[tx_fifo.idxout].msg;
	  }else{
            i=ul_usb_tx_fifo_incidx(&tx_fifo,tx_fifo.idxout);
	    while((i!=tx_fifo.idxout)&&(tx_fifo.slot[i].msg!=NULL)){
	      if(in_buf[4]==tx_fifo.slot[i].ustamp){
	        do{
                  tx_mes=tx_fifo.slot[tx_fifo.idxout].msg;
	          UL_BLK_HEAD(tx_mes).flg|=UL_BFL_FAIL;
                  ul_bll_move_mes(&udrv->proc_bll,tx_mes);
		  tx_fifo.slot[tx_fifo.idxout].msg=NULL;
		  tx_fifo.idxout=ul_usb_tx_fifo_incidx(&tx_fifo,tx_fifo.idxout);
                }while(tx_fifo.idxout!=i);
                SCHEDULE_BH(udrv);
	        tx_mes=tx_fifo.slot[tx_fifo.idxout].msg;
              }
              i=ul_usb_tx_fifo_incidx(&tx_fifo,i);
            }
	    if(!tx_mes){
              LOG_FATAL(KERN_CRIT "ul_usb_ps1_thread: incorrect ustamp of returned message\n");
	    }else{
              LOG_FATAL(KERN_CRIT "ul_usb_ps1_thread: some lost ustamp messages\n");
	    }
          }
	}else{
	  LOG_FATAL(KERN_CRIT "ul_usb_ps1_thread: tx_mes for returned message\n");
	}

	if(!tx_mes){
	  LOG_FATAL(KERN_CRIT "ul_usb_ps1_thread: no message found for usamp\n");
	  usb_clear_halt(dev, usb_rcvbulkpipe(dev, udrv->ul_usb_inpipe_nr));
	}else{
	 #ifdef UL_WITH_MULTI_NET
	  int rx_dadr_local=-1;
	 #endif /*UL_WITH_MULTI_NET*/
	  if(tail_frame&&(UL_BLK_HEAD(tx_mes).flg&UL_BFL_TAIL)){
	    frame=tail_frame;
	    LOG_MESSAGES("ul_usb_ps1_thread: tail receiption\n");
	  }else{
	    frame=tx_mes;
	  }

          if(in_buf[3]&U2UBFL_FAIL){
	    UL_BLK_HEAD(tx_mes).flg|=UL_BFL_FAIL;
	  }else if(in_buf[3]&U2UBFL_REC) {
	    if(!(UL_BLK_HEAD(frame).flg&UL_BFL_REC)){
	      UL_BLK_HEAD(tx_mes).flg|=UL_BFL_FAIL;
              LOG_FATAL(KERN_CRIT "ul_usb_ps1_thread: tail rec mitchmatch\n");
	    }else{
	      len=in_buf[6]+in_buf[7]*0x100;
	      UL_BLK_HEAD(frame).dadr=in_buf[0];
	      UL_BLK_HEAD(frame).sadr=in_buf[1];
	      UL_BLK_HEAD(frame).cmd=in_buf[2];
	      UL_BLK_HEAD(frame).len=len;
	     #ifdef UL_WITH_MULTI_NET
	      rx_dadr_local=UL_BLK_HEAD(frame).dadr;
	      UL_BLK_HEAD(frame).sadr_local=UL_BLK_HEAD(frame).sadr;
	     #endif /*UL_WITH_MULTI_NET*/
              ret=ul_usb_ps1_receive_data(udrv, frame, len, 0, work_buf);
	      if(ret<0) {
		UL_BLK_HEAD(tx_mes).flg|=UL_BFL_FAIL;
        	LOG_FATAL(KERN_CRIT "ul_usb_ps1_thread: tail receive problem\n");
	      }
	    }
	  }

	  if((in_buf[3]&U2UBFL_TAIL)&&(UL_BLK_HEAD(frame).flg&UL_BFL_TAIL)
	      &&!(UL_BLK_HEAD(tx_mes).flg&UL_BFL_FAIL)){
	    tail_frame=UL_BLK_HEAD(frame).next;
	    LOG_MESSAGES("ul_usb_ps1_thread: preparing for tail receive\n");
          }else{
	    int m2in=UL_BLK_HEAD(tx_mes).flg&UL_BFL_M2IN || UL_BLK_HEAD(tx_mes).flg&UL_BFL_FAIL;
	   #ifdef UL_WITH_MULTI_NET
	    if(!m2in&&(rx_dadr_local>=0)) /* Check for gateway address match */
	      m2in=uld_adr_match(udrv,rx_dadr_local)!=UL_ADR_MATCH_NONE;
	   #endif /*UL_WITH_MULTI_NET*/
	    if(!m2in)
	      m2in=uld_adr_match(udrv,UL_BLK_HEAD(tx_mes).dadr)!=UL_ADR_MATCH_NONE;

	    if(m2in){
              ul_bll_move_mes(&udrv->proc_bll,tx_mes);
              SCHEDULE_BH(udrv);
              /* ulan_do_bh(udrv); */
	    }else{
              ul_bll_free_mes(tx_mes);
	    }
	    tx_mes=NULL;
	    tail_frame=NULL;
	    tx_fifo.slot[tx_fifo.idxout].msg=NULL;
	    tx_fifo.idxout=ul_usb_tx_fifo_incidx(&tx_fifo,tx_fifo.idxout);
	    ul_usb_tx_fifo_print(&tx_fifo);
	  }
	}
      } else {
	LOG_MESSAGES("uLan : begin of receive message\n");
	i=0;
        /* if(in_buf[3]&U2UBFL_TAIL ) i|=UL_BFL_TAIL; */
        /* if(in_buf[3]&U2UBFL_REC) i|=UL_BFL_REC; */
        rx_mes=ul_new_frame_head(udrv,in_buf[0]/*dadr*/,in_buf[1]/*sadr*/, 
             	                 in_buf[2]/*cmd*/, i/*flg*/);
        len=in_buf[6]+in_buf[7]*0x100;
	if(rx_mes==NULL){ 
	  // usb_clear_halt(dev, usb_rcvbulkpipe(dev, udrv->ul_usb_inpipe_nr));
	  LOG_FAILS(KERN_ERR "uLan : NO memory for receive\n");
	  
          ret=ul_usb_ps1_receive_data(udrv, NULL, len, 1, work_buf);
	  if(ret<0) {
            LOG_FATAL(KERN_CRIT "ul_usb_ps1_thread: message receive problem\n");
	  }

	}else{
	  ul_bll_ins(&udrv->work_bll,rx_mes);

          UL_BLK_HEAD(rx_mes).len=len;
          ret=ul_usb_ps1_receive_data(udrv, rx_mes, len, 0, work_buf);
	  if(ret<0) {
            LOG_FATAL(KERN_CRIT "ul_usb_ps1_thread: message receive problem\n");
            ul_bll_free_mes(rx_mes);
	  }else{
            UL_BLK_HEAD(rx_mes).stamp=ul_gen_stamp();
            ul_bll_move_mes(&udrv->proc_bll,rx_mes);
	  }

          rx_mes=NULL;
          SCHEDULE_BH(udrv);
          /* ulan_do_bh(udrv); */
          /* printudrvbll(udrv); */
	}
      }
    }

    if(in_urb_st!=UL_USB_IUST_WAITING)
    {
      if(in_urb_st==UL_USB_IUST_ERROR_FATAL){
        LOG_FATAL(KERN_INFO "ul_usb: usb2ulan device problem detected\n");
        fatal_err=1;
        continue;
      }
      UL_MB();
      if(uld_kwt_should_stop(udrv)) {
        ul_usb_ps1_in_urb_st_set(udrv,UL_USB_IUST_NONE);
        break;
      }
      LOG_IRQ(KERN_INFO "ul_usb: submitting URB\n");
      ul_usb_ps1_in_urb_st_set(udrv,UL_USB_IUST_WAITING);
      usb_fill_bulk_urb(in_urb, dev, usb_rcvbulkpipe(dev, udrv->ul_usb_inpipe_nr),
            (void*)in_buf, 8, ul_usb_ps1_complete_in, udrv);
      #ifdef FOR_LINUX_KERNEL
        ret=kc_usb_submit_urb(in_urb, GFP_KERNEL);
        if (ret==-EPIPE) {
          usb_clear_halt(dev, usb_rcvbulkpipe(dev, udrv->ul_usb_inpipe_nr));
          ret=kc_usb_submit_urb(in_urb, GFP_KERNEL);
        }
        if(ret<0) {
          LOG_FATAL(KERN_INFO "ul_usb: KWT usb_submit_urb fatal error %d\n",ret);
          fatal_err=1;
          ul_usb_ps1_in_urb_st_set(udrv,UL_USB_IUST_NONE);
        }
      #else
        ret=usb_submit_urb(in_urb, 0 );
        if ( !NT_SUCCESS(ret)) {
          LOG_FATAL(KERN_INFO "ul_usb: KWT usb_submit_urb error %d retrying\n",ret);
          usb_clear_halt(dev, usb_rcvbulkpipe(dev, udrv->ul_usb_inpipe_nr));
          ret=usb_submit_urb(in_urb, 0 );
        }
        if ( !NT_SUCCESS(ret)) {
          LOG_FATAL(KERN_INFO "ul_usb: KWT usb_submit_urb fatal error %d\n",ret);
          fatal_err=1;
          ul_usb_ps1_in_urb_st_set(udrv,UL_USB_IUST_NONE);
        }
      #endif /* FOR_LINUX_KERNEL */

    } else {
      LOG_IRQ(KERN_INFO "ul_usb: KWT sleeping\n");
      if(uld_kwt_wait(udrv)!=ULD_KWT_WAIT_SUCCESS) {
         LOG_FATAL(KERN_INFO "ul_usb: KWT wait for event interrupted/killed, stopping\n");
         break;
      }
      LOG_IRQ(KERN_INFO "ul_usb: KWT wakeup\n");
    }
  }

  ul_usb_tx_fifo_print(&tx_fifo);
  while(!ul_usb_tx_fifo_empty(&tx_fifo)){
//    ul_bll_free_mes(tx_fifo.slot[tx_fifo.idxout].msg);
    tx_mes=tx_fifo.slot[tx_fifo.idxout].msg;
    UL_BLK_HEAD(tx_mes).flg|=UL_BFL_FAIL;
    ul_bll_move_mes(&udrv->proc_bll,tx_mes);
    tx_fifo.slot[tx_fifo.idxout].msg=NULL;
    tx_fifo.idxout=ul_usb_tx_fifo_incidx(&tx_fifo,tx_fifo.idxout);
  }
  
  UL_MB();
  if(in_urb) {
    #ifdef FOR_LINUX_KERNEL
     #if (LINUX_VERSION_CODE >= VERSION(2,6,9))
      usb_kill_urb(in_urb);
     #else
      usb_unlink_urb(in_urb);
     #endif
      usb_free_urb(in_urb);
    #else
      in_urb_st=ul_usb_ps1_in_urb_st_xchg(udrv,UL_USB_IUST_CANCEL_STARTED);
      if (in_urb->irp && (in_urb_st==UL_USB_IUST_WAITING)) { 
        IoCancelIrp(in_urb->irp);
        in_urb_st=ul_usb_ps1_in_urb_st_xchg(udrv,UL_USB_IUST_CANCEL_COMPLETE);
	if (in_urb_st!=UL_USB_IUST_CANCEL_STARTED) {
          IoCompleteRequest(in_urb->irp, IO_NO_INCREMENT);
        }
        do {
          UL_MB();
          in_urb_st=ul_usb_ps1_in_urb_st_get(udrv);
          if(in_urb_st!=UL_USB_IUST_CANCEL_COMPLETE)
            break;
          LOG_IRQ(KERN_INFO "ul_usb: finalize KWT sleeping\n");
          uld_kwt_wait(udrv);
          uld_kwt_confirm(udrv);
          LOG_IRQ(KERN_INFO "ul_usb: finalize KWT wakeup\n");
        } while(1);
      }
      usb_free_urb(in_urb);
    #endif /* FOR_LINUX_KERNEL */  
  }
  
   /* stop converter */
  ret = ul_usb_ps1_stop(udrv);
  if (ret<0) {
    LOG_FATAL(KERN_CRIT "ul_usb_ps1_stop: return with ret_code (%d)\n", ret);
  }

  if (in_buf != NULL) {
    FREE(in_buf);
  }

  if (work_buf != NULL) {
    FREE(work_buf);
  }

  LOG_IRQ(KERN_INFO "ul_usb: KWT finished\n");
  uld_kwt_finished(udrv);
  return 0;
}

/*** Wake up worker thread  ***/
static int ul_usb_ps1_stroke(ul_drv *udrv)
{
  uld_kwt_wake(udrv);
  return 0;
}

/*** Control functions of chip driver  ***/

static void ul_usb_ps1_active(struct ul_drv *udrv)
{
  uld_atomic_set_dfl(udrv,KWTACTIVE);
  uld_kwt_wake(udrv);
}

static int ul_usb_ps1_rqirq(struct ul_drv *udrv)
{ /* request IRQ handling - kernel worker thread*/
  if(uld_kwt_test_and_set_prepared(udrv)) {
    LOG_IRQ("ul_usb_ps1_cctrl: KWT already running\n");
    return 0;
  }
  uld_atomic_clear_dfl(udrv,KWTKILL);

  if(uld_kwt_run(udrv, ul_usb_ps1_thread, "kulusbps1d")<0)
  {
    LOG_FATAL(KERN_CRIT "ul_usb_ps1_cctrl: Cannot fork KWT thread!!!\n");
    /*uld_atomic_clear_dfl(udrv,KWTRUN);*/
    uld_kwt_set_stopped(udrv);
    return UL_RC_EKWT;
  }

  LOG_IRQ("ul_usb_ps1_cctrl: KWT forked\n");
  return 0;
}

static int ul_usb_ps1_freeirq(struct ul_drv *udrv)
{ /* release IRQ handling - kernel worker thread*/
  uld_atomic_clear_dfl(udrv,KWTACTIVE);
  if(!uld_kwt_is_running(udrv)) { 
    LOG_IRQ("ul_usb_ps1_cctrl: KWT already stopped\n");
    return 0;
  }
  uld_kwt_kill(udrv);
  uld_kwt_wait_stopped(udrv);
  LOG_IRQ("ul_usb_ps1_cctrl: KWT stopped\n");
  return 0;
}

static int ul_usb_ps1_setmyadr(struct ul_drv *udrv, int subdevidx, int newadr)
{ /* station address changed */
  #if defined(CONFIG_OC_UL_DRV_WITH_MULTI_DEV) && !defined(_WIN32)
    #warning multiple addresses and subdevices unsupported for CPS1
  #endif /*CONFIG_OC_UL_DRV_WITH_MULTI_DEV*/

  if(ul_drv_common_fnc_setmyadr(udrv, subdevidx, newadr)<0)
    return -1;
  ul_usb_ps1_cfg_updatefl_set(udrv,UL_USB_CFGUEV_ADDRBAUD);
  uld_kwt_wake(udrv);
  return 0;
}

static int ul_usb_ps1_setpromode(struct ul_drv *udrv, int pro_mode)
{ /* promode changed */
  if(ul_drv_common_fnc_setpromode(udrv, pro_mode)<0)
    return -1;
  ul_usb_ps1_cfg_updatefl_set(udrv,UL_USB_CFGUEV_PROMODE);        
  uld_kwt_wake(udrv);
  return 0;
}

const ul_drv_chip_ops ul_usb_ps1_chip_ops = {
  "usb_ps1",		/* chip_type - text type identification*/
  NULL,			/* fnc_recch */
  NULL,			/* fnc_sndch */
  NULL,			/* fnc_wait */
  NULL,			/* fnc_connect */
  NULL,			/* fnc_finishtx */
  NULL,			/* fnc_pool */
  NULL,			/* fnc_cctrl */
  ul_usb_ps1_stroke,	/* fnc_stroke */

  NULL,			/* fnc_txoe */
  NULL,			/* fnc_pinit */
  NULL,			/* fnc_pdone */
  ul_usb_ps1_active,	/* fnc_activate */
  NULL,			/* fnc_genirq */
  ul_usb_ps1_rqirq,	/* fnc_rqirq */
  ul_usb_ps1_freeirq,	/* fnc_freeirq */
  NULL,			/* fnc_hwtest */
  ul_usb_ps1_setmyadr,	/* fnc_setmyadr */
  ul_usb_ps1_setpromode,	/* fnc_setpromode */
};

/*** Petr Smolik's USB2ULAN version 1 initialize ***/
int ul_usb_ps1_init(ul_drv *udrv, ul_physaddr_t physbase, int irq, int baud, long baudbase, int options)
{
  udrv->chip_options=options;
  if(!baud) baud=19200;
  udrv->baud_val=baud;
  if(!baudbase)
    baudbase=57600;
  udrv->baud_base=baudbase;
  udrv->baud_div=(udrv->baud_base+baud/2)/baud;
  udrv->physbase=physbase;
  udrv->irq=0;
  udrv->chip_ops=&ul_usb_ps1_chip_ops;
  udrv->ul_usb_max_pkt_size=8; /* <= U2U_PKT_BUF_SIZE */
  udrv->ul_usb_max_msg_inpr=ul_usb_msg_inpr;
  if(!udrv->ul_usb_max_msg_inpr)
    udrv->ul_usb_max_msg_inpr=2; /* <= U2U_MAX_MSG_INPR */
  udrv->ul_usb_outpipe_nr=1;     /* OUT pipe number */
  udrv->ul_usb_inpipe_nr=1;      /* IN pipe number */
  return 0;
};


