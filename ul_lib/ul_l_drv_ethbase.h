/*******************************************************************
  uLan Communication - low level C and driver interface library

  ul_l_drv_eth.h   - ethernet based indirect driver access

  (C) Copyright 1996-2016 by Pavel Pisa - project originator
        http://cmp.felk.cvut.cz/~pisa
  (C) Copyright 1996-2016 PiKRON Ltd.
        http://www.pikron.com
  (C) Copyright 2002-2016 Petr Smolik


  The uLan driver project can be used and distributed
  in compliance with any of next licenses
   - GPL - GNU Public License
     See file COPYING for details.
   - LGPL - Lesser GNU Public License
   - MPL - Mozilla Public License
   - and other licenses added by project originator

  Code can be modified and re-distributed under any combination
  of the above listed licenses. If contributor does not agree with
  some of the licenses, he/she can delete appropriate line.
  WARNING: if you delete all lines, you are not allowed to
  distribute code or sources in any form.
 *******************************************************************/

#ifndef _UL_L_DRV_ETHBASE_H
#define _UL_L_DRV_ETHBASE_H

#ifdef __cplusplus
extern "C" {
#endif

#include <ul_dbuff.h>
#include <ul_msg_buf.h>

int ul_eth_recv_msg(int fd,ul_dbuff_t *rcvbuff,int timeout, int *terminated);
int ul_eth_send_msg(FILE *fd,ul_msg_buf_t *sndbuff);
int ul_eth_demarshal_stream(ul_msg_buf_t *msg_buf,char *stream,int stream_len);

#ifdef __cplusplus
} /* extern "C"*/
#endif

#endif /*_UL_L_DRV_ETHBASE_H*/
