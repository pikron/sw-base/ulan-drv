/*******************************************************************
  uLan Communication - uL_DRV - multiplatform uLan driver

  ul_c950pci.c	- chip driver for Oxford Semiconductor's 
                  OX16950 based pCI acards

  (C) Copyright 1996-2004 by Pavel Pisa - project originator
        http://cmp.felk.cvut.cz/~pisa
  (C) Copyright 1996-2004 PiKRON Ltd.
        http://www.pikron.com
  (C) Copyright 2002-2004 Petr Smolik
  

  The uLan driver project can be used and distributed 
  in compliance with any of next licenses
   - GPL - GNU Public License
     See file COPYING for details.
   - LGPL - Lesser GNU Public License
   - MPL - Mozilla Public License
   - and other licenses added by project originator

  Code can be modified and re-distributed under any combination
  of the above listed licenses. If contributor does not agree with
  some of the licenses, he/she can delete appropriate line.
  WARNING: if you delete all lines, you are not allowed to
  distribute code or sources in any form.
 *******************************************************************/

/*******************************************************************/
/* Chip driver for OX16950-pci */

#include "serial_reg.h"

#define u950pci_inb(base,port)	 ul_inb(ul_ioaddr_add(base,port))
#define u950pci_outb(base,port,val) ul_outb(ul_ioaddr_add(base,port),val)

#define U950PCI_MCR_IE	0x08	/* receive  0x09 for Advantech */
#define U950PCI_MCR_OE	0x09	/* transmit 0x08 for Advantech */
#define U950PCI_MCR_TXDTRPOL 0x01

#define U950PCI_LCR_UL	0x03	/* receive/transmit */
#define U950PCI_LCR_ULB	0x43	/* transmit line break */
#define U950PCI_LCR_EFR	0xBF	/* enable access to EFR, XONx */

#define U950PCI_LSR_C	0x04	/* controll character received */
#define U950PCI_LSR_ERR	0x1A	/* error in receiver */

#define U950PCI_NMR_EN	0x03	/* 9-bit communication settings */

#define U950PCI_IIR_ID	0x3f	/* more IRQ levels */

#define U950pci_MSR_RxD UART_MSR_RI /* Pin for RxD level sense */

/* flags of chip options used in u950pci_init */
/* unfortunate random DTR polarity over different vendors  !!! */
#define U950PCI_CHOPT_TXDTRNEG 1	/* negated polarity (Advantech) */
#define U950PCI_CHOPT_HSPDOSC  2	/* oscilator 14.7456MHz/1.8432MHz */
#define U950PCI_CHOPT_RXDONRI  4	/* RxD signal connected to RI */
#define U950PCI_CHOPT_SP512F4K 8	/* Channel bases span 4k */
#define U950PCI_CHOPT_OSC62_5  16	/* oscilator 62_5MHz*/

#define U950PCI_ACR_VAL chip_buff[2] /* shaddow buffer of ACR */
/* default ACR value : DTR=1 when Tx, 950 trigger levels enable  */
#define U950PCI_ACR_UL	(UART_ACR_TLENB)
#define U950PCI_ACR_ULTX (U950PCI_ACR_UL|0x10|UART_ACR_RXDIS)/**/
#define U950PCI_ACR_TXDTRPOL 0x08	/* ^- 0x18 for Advantech */

#define U950PCI_STATE chip_buff[3] /* chip state information */
#define U950PCI_STATE_TX    1
#define U950PCI_STATE_FLUSH 2

#define U950PCI_RX_ACRMCR chip_buff[4] /* set this for RX - LSB=ACR, MSB=MCR */
  #define U950PCI_RX_ACR(udrv) ((uchar)udrv->U950PCI_RX_ACRMCR)
  #define U950PCI_RX_MCR(udrv) ((uchar)(udrv->U950PCI_RX_ACRMCR>>8))
#define U950PCI_TX_ACRMCR chip_buff[5] /* set this for TX - LSB=ACR, MSB=MCR */
  #define U950PCI_TX_ACR(udrv) ((uchar)udrv->U950PCI_TX_ACRMCR)
  #define U950PCI_TX_MCR(udrv) ((uchar)(udrv->U950PCI_TX_ACRMCR>>8))

INLINE void u950pci_icr_write(ul_drv *udrv, int index, int val)
{
  u950pci_outb(udrv->iobase,UART_SCR,(uchar)index);
  u950pci_outb(udrv->iobase,UART_ICR,(uchar)val);
}

static unsigned u950pci_icr_read(ul_drv *udrv, int index)
{
  unsigned val;
  u950pci_icr_write(udrv,UART_ACR,udrv->U950PCI_ACR_VAL|UART_ACR_ICRRD);
  u950pci_outb(udrv->iobase,UART_SCR,(uchar)index);
  val=u950pci_inb(udrv->iobase,UART_ICR);
  u950pci_icr_write(udrv,UART_ACR,udrv->U950PCI_ACR_VAL);
  return val;
}

static unsigned u950pci_xfl_read(ul_drv *udrv, int reg)
{
  unsigned val;
  u950pci_icr_write(udrv,UART_ACR,udrv->U950PCI_ACR_VAL|UART_ACR_ASREN);
  val=u950pci_inb(udrv->iobase,reg);
  /* no need to set ACR index - SCR not changed */
  u950pci_outb(udrv->iobase,UART_ICR,(uchar)(udrv->U950PCI_ACR_VAL));
  return val;
}

INLINE void u950pci_acr_write(ul_drv *udrv, int val)
{
  udrv->U950PCI_ACR_VAL=val;
  u950pci_icr_write(udrv,UART_ACR,val); 
}

static int u950pci_rxd_cs(ul_drv *udrv)
{ /* returns 1 if active level detected */

  if(!(udrv->chip_options&U950PCI_CHOPT_RXDONRI))
    return 0;
  if(!(u950pci_inb(udrv->iobase,UART_MSR)&U950pci_MSR_RxD))
    return 0;
  LOG_CHIO(" CSCD");
  return 1;
}

/*** Test interrupt request state ***/
static int u950pci_pool(ul_drv *udrv)
{
  unsigned u;
  u=u950pci_inb(udrv->iobase,UART_IIR);
  return (u&UART_IIR_NO_INT)?0:((u&U950PCI_IIR_ID)|1);
};

/*** Wait end of transmit ***/
static int u950pci_weot(ul_drv *udrv, int ret_code)
{
  uchar lsr_val;

  lsr_val=u950pci_inb(udrv->iobase,UART_LSR);
  if(lsr_val&UART_LSR_TEMT){
    UL_FRET;
    return UL_RC_PROC;
  }
  u950pci_icr_write(udrv,UART_TTL,0);	/* transmittion FIFO treshold */
  u950pci_outb(udrv->iobase,UART_IER,UART_IER_THRI);
  LOG_CHIO(".weot");
  return UL_RC_WIRQ;
};

/* Help subroutine for switch from transmit to receive */
static int u950pci_recch_sub_tx2rx(ul_drv *udrv, int lsr_val)
{
  if(lsr_val&UART_LSR_TEMT){
    udrv->U950PCI_STATE&=~(U950PCI_STATE_TX|U950PCI_STATE_FLUSH);
    u950pci_outb(udrv->iobase,UART_FCR,UART_FCR_ENABLE_FIFO|
        	 UART_FCR_CLEAR_RCVR); /**/
    u950pci_acr_write(udrv,U950PCI_RX_ACR(udrv));
    return UL_RC_PROC;
  }
  u950pci_icr_write(udrv,UART_TTL,0);	/* wait for TEMT */
  u950pci_outb(udrv->iobase,UART_IER,UART_IER_THRI);
  LOG_CHIO(".t2r");
  return UL_RC_WIRQ;
}

/* Help subroutine for reading 9-bit character */
static int u950pci_recch_sub_rec9bit(ul_drv *udrv, int lsr_val)
{
  udrv->char_buff=u950pci_inb(udrv->iobase,UART_RX);
  if(lsr_val&U950PCI_LSR_C){
    udrv->last_ctrl=udrv->char_buff|=0x100; /* last char for ready/busy */
  } else udrv->last_ctrl&=0x7f; /* somebody other has line */
  udrv->xor_sum^=udrv->char_buff;udrv->xor_sum++;
  if(lsr_val&U950PCI_LSR_ERR){
    LOG_CHIO(" ER:%03X",udrv->char_buff);
    return UL_RC_EFRAME; /* frame error */
  }else{
    LOG_CHIO(" R:%03X",udrv->char_buff);
    return UL_RC_PROC;
  }
}

/*** Receive character into char_buff ***/
static int u950pci_recch(ul_drv *udrv, int ret_code)
{
  uchar lsr_val;
  uchar rfl_val;
  udrv->char_buff=0;
  lsr_val=u950pci_inb(udrv->iobase,UART_LSR);
  LOG_CHIO(" lsr%02X",lsr_val);
  if(udrv->U950PCI_STATE&
     (U950PCI_STATE_TX|U950PCI_STATE_FLUSH)){
    /* Manage all necessary for TX/RX switch */
    return u950pci_recch_sub_tx2rx(udrv,lsr_val);
  }
  rfl_val=(uchar)u950pci_xfl_read(udrv,UART_RFL);
  LOG_CHIO(" rfl%02X",rfl_val);
  /*if(lsr_val&UART_LSR_DR){*/
  if(rfl_val>0){
    /* Construct and return 9-bit character */
    UL_FRET;
    return u950pci_recch_sub_rec9bit(udrv,lsr_val);
  }
  LOG_CHIO("-");
  u950pci_outb(udrv->iobase,UART_IER,UART_IER_RDI|UART_IER_RLSI);
  u950pci_icr_write(udrv,UART_RTL,6);	/* receive FIFO treshold */
  return UL_RC_WIRQ;			/**/
}

/*** Send character from char_buff ***/
  static int u950pci_sndch_1(ul_drv *udrv, int ret_code);

static int u950pci_sndch(ul_drv *udrv, int ret_code)
{
  unsigned u;
  uchar lsr_val;
  uchar tfl_val;
  lsr_val=u950pci_inb(udrv->iobase,UART_LSR);
  if(udrv->U950PCI_STATE&U950PCI_STATE_TX){
    LOG_CHIO(" lsr%02X",lsr_val);
    /*if(lsr_val&UART_LSR_THRE){*/
    tfl_val=(uchar)u950pci_xfl_read(udrv,UART_TFL);
    LOG_CHIO(" tfl%02X",tfl_val);
    if(tfl_val<0x08){
      u=udrv->char_buff;
      udrv->xor_sum^=udrv->char_buff;udrv->xor_sum++;
      if(u&0x100){
	udrv->last_ctrl=u; /* store last control char */
	u950pci_outb(udrv->iobase,UART_SCR,1);
     } else u950pci_outb(udrv->iobase,UART_SCR,0);
     u950pci_outb(udrv->iobase,UART_TX,(uchar)u);
     UL_FRET;
     LOG_CHIO(" T:%03X",u);
     return UL_RC_PROC;
   }
   LOG_CHIO("-");
   u950pci_outb(udrv->iobase,UART_IER,UART_IER_THRI);
   return UL_RC_WIRQ;
 }

 /* flush timing dummy transmit chars and manage 1 char switch delay*/
 u950pci_outb(udrv->iobase,UART_FCR,UART_FCR_ENABLE_FIFO|
              UART_FCR_CLEAR_RCVR|UART_FCR_CLEAR_XMIT);
 u950pci_acr_write(udrv,U950PCI_RX_ACR(udrv));
 u950pci_icr_write(udrv,UART_TTL,0);	/* wait for TEMT */
 u950pci_outb(udrv->iobase,UART_TX,0);
 u950pci_outb(udrv->iobase,UART_IER,UART_IER_THRI);
 UL_FNEXT(u950pci_sndch_1); 
 LOG_CHIO(".r2t");
 return UL_RC_WIRQ;
}

static int u950pci_sndch_1(ul_drv *udrv, int ret_code)
{
  uchar lsr_val;
  lsr_val=u950pci_inb(udrv->iobase,UART_LSR);
  if(~lsr_val&UART_LSR_TEMT) return UL_RC_WIRQ;

  /* finally setup for transmit */
  udrv->U950PCI_STATE|=U950PCI_STATE_TX;
  u950pci_icr_write(udrv,UART_TTL,4);	/* min 4 bytes in Tx FIFO */
  u950pci_acr_write(udrv,U950PCI_TX_ACR(udrv));
  UL_FNEXT(u950pci_sndch); 
  return UL_RC_PROC;
}

/*** Wait for time or received character ***/
  static int u950pci_wait_1(ul_drv *udrv, int ret_code);

static int u950pci_wait(ul_drv *udrv, int ret_code)
{
  int i;
  uchar lsr_val;
  uchar rfl_val;
  udrv->char_buff=0;
  lsr_val=u950pci_inb(udrv->iobase,UART_LSR);
  if(udrv->U950PCI_STATE&
     (U950PCI_STATE_TX|U950PCI_STATE_FLUSH)){
    /* Manage all necessary for TX/RX switch */
    return u950pci_recch_sub_tx2rx(udrv,lsr_val);
  }
  rfl_val=(uchar)u950pci_xfl_read(udrv,UART_RFL);
  LOG_CHIO(" w1rfl%02X",rfl_val);
  /*if(lsr_val&UART_LSR_DR){*/
  if(rfl_val>0){
    /* Construct and return 9-bit character */
    UL_FRET;
    return u950pci_recch_sub_rec9bit(udrv,lsr_val);
  }
  /* flush Tx FIFO */
  u950pci_outb(udrv->iobase,UART_FCR,UART_FCR_ENABLE_FIFO|
               UART_FCR_CLEAR_XMIT);
  u950pci_icr_write(udrv,UART_RTL,1);	/* receive FIFO treshold */
  u950pci_icr_write(udrv,UART_TTL,0);	/* wait TEMT */
  for(i=udrv->wait_time;i--;){
    u950pci_outb(udrv->iobase,UART_TX,0);		/* wait timed by dummy TX */
  }
  u950pci_outb(udrv->iobase,UART_IER,UART_IER_RDI|
               UART_IER_RLSI|UART_IER_THRI);
  UL_FNEXT(u950pci_wait_1);
  LOG_CHIO("=");
  return UL_RC_WIRQ;
}

static int u950pci_wait_1(ul_drv *udrv, int ret_code)
{
  uchar lsr_val;
  uchar rfl_val;
  uchar tfl_val;
  lsr_val=u950pci_inb(udrv->iobase,UART_LSR);
  rfl_val=(uchar)u950pci_xfl_read(udrv,UART_RFL);
  LOG_CHIO(" w2rfl%02X",rfl_val);
  /*if(lsr_val&UART_LSR_DR){*/
  if(rfl_val>0){
    /* Construct and return 9-bit character */
    UL_FRET;
    return u950pci_recch_sub_rec9bit(udrv,lsr_val);
  }
  tfl_val=(uchar)u950pci_xfl_read(udrv,UART_TFL);
  LOG_CHIO(" w3tfl%02X",tfl_val);
  if(tfl_val>0)
    return UL_RC_WIRQ;
  u950pci_outb(udrv->iobase,UART_FCR,UART_FCR_ENABLE_FIFO|
               UART_FCR_CLEAR_XMIT);
  UL_FRET;
  LOG_CHIO(" Timeout!");
  return UL_RC_ETIMEOUT;
};

/*** Connect to RS485 bus ***/
  static int u950pci_connect_1(ul_drv *udrv, int ret_code);
  static int u950pci_connect_2(ul_drv *udrv, int ret_code);

static int u950pci_connect(ul_drv *udrv, int ret_code)
{
 unsigned u;
 u=udrv->last_ctrl;
 udrv->chip_temp=uld_get_arb_addr(udrv);
 udrv->wait_time=((udrv->chip_temp-u-1)&0xF)+4; 
 udrv->chip_temp=(udrv->chip_temp&0x3F)|0x40;
 if(((u&0x180)!=0x180)||
    (u==0x1FF)) udrv->wait_time+=0x10;
 udrv->last_ctrl=0;
 udrv->char_buff=0;
 UL_FCALL2(*udrv->chip_ops->fnc_wait,u950pci_connect_1);
 return UL_RC_PROC;
};

static int u950pci_connect_1(ul_drv *udrv, int ret_code)
{
  if(ret_code!=UL_RC_ETIMEOUT)
    { UL_FRET; return UL_RC_EARBIT;}
  if(u950pci_rxd_cs(udrv)|| /* check for possible collision */
     (u950pci_inb(udrv->iobase,UART_LSR)&UART_LSR_DR))
  { UL_FRET;
    LOG_CHIO(" EARBIT!");
    return UL_RC_EARBIT;
  }
  udrv->U950PCI_STATE|=U950PCI_STATE_TX;
  u950pci_acr_write(udrv,U950PCI_TX_ACR(udrv));
  u950pci_outb(udrv->iobase,UART_LCR,U950PCI_LCR_ULB);
  u950pci_outb(udrv->iobase,UART_MCR,U950PCI_TX_MCR(udrv));
  UL_FNEXT(u950pci_connect_2);
  /* wait to end of transfer */
  u950pci_icr_write(udrv,UART_TTL,0);	/* wait for TEMT */
  u950pci_outb(udrv->iobase,UART_TX,0);
  u950pci_outb(udrv->iobase,UART_IER,UART_IER_THRI|UART_IER_RDI);
  return UL_RC_WIRQ;
};

static int u950pci_connect_2(ul_drv *udrv, int ret_code)
{
  u950pci_outb(udrv->iobase,UART_LCR,U950PCI_LCR_UL);
  u950pci_outb(udrv->iobase,UART_MCR,U950PCI_RX_MCR(udrv));
  udrv->U950PCI_STATE&=~U950PCI_STATE_TX;
  u950pci_acr_write(udrv,U950PCI_RX_ACR(udrv));
  if(u950pci_xfl_read(udrv,UART_RFL)>0)
  { UL_FRET;
    LOG_CHIO(" EARBIT1!");
    return UL_RC_EARBIT;
  }
  if(udrv->chip_temp==1)
  { LOG_CHIO(" Connected");
    UL_FRET;
  }else{ 
    udrv->wait_time=(udrv->chip_temp&3)+1;
    udrv->chip_temp>>=2;
    UL_FCALL2(*udrv->chip_ops->fnc_wait,u950pci_connect_1);
  }
  return UL_RC_PROC;
};

/*
 UL_FNEXT(u950pci_recch);
 UL_FCALL(u950pci_recch);
 UL_FCALL2(u950pci_recch,u950pci_recch);
 UL_FRET;
*/

static char *u950pci_port_name="ulan_u950pci";

/*** 16950pci initialize ports ***/
static int u950pci_pinit(ul_drv *udrv)
{
  unsigned u;
  int baud=udrv->baud_val;
  int cpr, tcr, dl;
  if (ul_io_map(udrv->physbase,8,u950pci_port_name, &udrv->iobase)<0)
  { LOG_FATAL(KERN_CRIT "uLan u950pci_pinit : cannot reguest ports !\n");
    return UL_RC_EPORT;
  }

  /* Switch to enhanced mode */
  u950pci_outb(udrv->iobase,UART_LCR,U950PCI_LCR_EFR);
  u950pci_outb(udrv->iobase,UART_EFR,UART_EFR_ECB);
  u950pci_outb(udrv->iobase,UART_LCR,U950PCI_LCR_UL);
  /* Disable transmitter output  */
  u950pci_outb(udrv->iobase,UART_MCR,U950PCI_RX_MCR(udrv)&3);
  /* Set up ACR and state */
  u950pci_acr_write(udrv,U950PCI_RX_ACR(udrv));
  udrv->U950PCI_STATE=U950PCI_STATE_FLUSH;
  #if 0
  /* Read chip ID */
  chip_id=u950pci_icr_read(udrv,UART_ID1)<<16;
  chip_id|=u950pci_icr_read(udrv,UART_ID2)<<8;
  chip_id|=u950pci_icr_read(udrv,UART_ID3);
  chip_rev=u950pci_icr_read(udrv,UART_REV);
  LOG_PORTS(KERN_INFO "uLan u950pci : chip ID %06X. rev %02x\n",chip_id,chip_rev);
  #endif

  udrv->chip_buff[0]=0;

  /* setup baud rate */
  if(!baud) baud=19200;
  u=(udrv->baud_base*8+baud/2)/baud;
  if (u>0xFFFFFFF) u=0xFFFFFFF;
  udrv->baud_div=u;
  /*{long e, tcr_err=0xFFFFFFF;
    int i;
    tcr=-1;
    for(i=16;i>=4;i--){
      if(i*0xFFFFFF<u) break;
      e=u%i;
      if(e<tcr_err){tcr=i;tcr_err=e;}
    }
  }*/
  tcr=16; cpr=8;
  dl=(udrv->baud_div+(cpr*tcr)/2)/(cpr*tcr);
  /*LOG_PORTS*/
  UL_PRINTF(KERN_INFO "uLan u950pci : baud setup TCR=%d CPR=%d DL=%d baud=%ld\n",
            tcr,cpr,dl,udrv->baud_base*8/(tcr*cpr*dl));
  UL_PRINTF(KERN_INFO "uLan u950pci : baud=%d baud_base=%ld baud_div=%d\n",
            baud,udrv->baud_base,udrv->baud_div);
  u950pci_icr_write(udrv,UART_CPR,cpr);	/* prescaller 1 to 31.875 step 0.125*/
  u950pci_icr_write(udrv,UART_TCR,tcr);	/* 4 to 16 times per clock */
  u950pci_outb(udrv->iobase,UART_LCR,UART_LCR_DLAB);
  u950pci_outb(udrv->iobase,(uchar)UART_DLL,(uchar)dl);
  u950pci_outb(udrv->iobase,(uchar)UART_DLM,(uchar)(dl>>8));
  u950pci_outb(udrv->iobase,UART_LCR,U950PCI_LCR_UL);

  /* setup initial FIFO levels */
  u950pci_icr_write(udrv,UART_NMR,U950PCI_NMR_EN); /* 9-bit mode enable */
  u950pci_outb(udrv->iobase,UART_IER,0);	/* disable interrupt sources */
  u950pci_outb(udrv->iobase,UART_FCR,UART_FCR_ENABLE_FIFO|
	 UART_FCR_CLEAR_RCVR|UART_FCR_CLEAR_XMIT); /* enable and clear FIFOs */
  u950pci_icr_write(udrv,UART_TTL,1);	/* transmittion FIFO treshold */
  u950pci_icr_write(udrv,UART_RTL,1);	/* receive FIFO treshold */
  u950pci_inb(udrv->iobase,UART_LSR);	/* reset errors */
  u950pci_inb(udrv->iobase,UART_RX);	/* and other irq */
  return UL_RC_PROC;
};

/*** 16950pci deinitialize ports ***/
static int u950pci_pdone(ul_drv *udrv)
{
  u950pci_outb(udrv->iobase,UART_IER, 0);	/* disable interrupts */
  /* Switch enhanced mode off */
  u950pci_outb(udrv->iobase,UART_LCR,U950PCI_LCR_EFR);
  u950pci_outb(udrv->iobase,UART_EFR,0);
  u950pci_outb(udrv->iobase,UART_LCR,U950PCI_LCR_UL);
  /* clean up */
  u950pci_outb(udrv->iobase,UART_IER,0);	/* disable interrupts */
  u950pci_outb(udrv->iobase,UART_MCR,U950PCI_RX_MCR(udrv)&3); /* transmitter off */
  u950pci_inb(udrv->iobase,UART_IIR);	/* flush irq */
  u950pci_inb(udrv->iobase,UART_LSR);	/* flush irq */
  ul_io_unmap(udrv->physbase,8,udrv->iobase);
  return UL_RC_PROC;
};

/*** 16950pci activate ***/
static void u950pci_activate(ul_drv *udrv)
{
  u950pci_outb(udrv->iobase,UART_MCR,U950PCI_RX_MCR(udrv)); /* Enable interrupts */
}

/*** 16950pci generate irq for irq_probe */
static int u950pci_genirq(ul_drv *udrv,int param)
{
  if(param){
    u950pci_outb(udrv->iobase,UART_MCR, U950PCI_RX_MCR(udrv)); /* transmitter off */
    u950pci_outb(udrv->iobase,UART_IER, UART_IER_MSI);	/* enable interrupts */
    u950pci_outb(udrv->iobase,UART_LCR, U950PCI_LCR_ULB);	/* trig break */
  }else{
    u950pci_outb(udrv->iobase,UART_IER, 0);		/* disable interrupts */
    u950pci_outb(udrv->iobase,UART_MCR, U950PCI_RX_MCR(udrv)&3); /* transmitter off */
    u950pci_outb(udrv->iobase,UART_LCR, U950PCI_LCR_UL); /* no break */
    u950pci_inb(udrv->iobase,UART_MSR);		/* flush irq */
  }
  return UL_RC_PROC;
};

#if 0
/*** 16950pci request IRQ handling */
int u950pci_rqirq(ul_drv *udrv,int param)
{
  return UL_RC_PROC;
};

/*** 16950pci release IRQ handling */
int u950pci_freeirq(ul_drv *udrv,int param)
{
  if(uldrv->irq)
    free_irq(udrv->irq, udrv);
  return UL_RC_PROC;
};
#endif

/* support for hardware tests */
static int u950pci_hwtest(ul_drv *udrv,int param)
{
  unsigned char uc;
  switch(param)
  {
    case 0x10:
    case 0x11:
	  u950pci_outb(udrv->iobase,UART_IER, 0); /* disable interrupts */
	  u950pci_outb(udrv->iobase,UART_MCR, U950PCI_TX_MCR(udrv)); /* transmitter on */
	  u950pci_outb(udrv->iobase,UART_LCR, param&1?
	               U950PCI_LCR_UL:U950PCI_LCR_ULB); /* set TD lines */
    case 0x12:
	  uc=u950pci_inb(udrv->iobase,UART_MSR);
	  return u950pci_inb(udrv->iobase,UART_LSR)|(uc<<8)|
		 (uc&(U950pci_MSR_RxD)?0:0x10000);

    case 0x13:
	  u950pci_outb(udrv->iobase,UART_MCR, U950PCI_RX_MCR(udrv)); /* transmitter off */
	  uc=u950pci_inb(udrv->iobase,UART_MSR);
	  return u950pci_inb(udrv->iobase,UART_LSR)|(uc<<8)|
		 (uc&(U950pci_MSR_RxD)?0:0x10000);
  }
  return UL_RC_ENOFNC;
};

/*** Control functions of chip driver  ***/
static int u950pci_txoe(struct ul_drv *udrv, int enable)
{ /* switch on/off line transmitter */
  if(!enable)
  { /* switch off line transmitter */
    u950pci_outb(udrv->iobase,UART_MCR, U950PCI_RX_MCR(udrv));
    u950pci_outb(udrv->iobase,UART_LCR, U950PCI_LCR_UL);
    u950pci_acr_write(udrv,U950PCI_RX_ACR(udrv));
  } else {
    /* switch on line transmitter */
    u950pci_outb(udrv->iobase,UART_MCR, U950PCI_TX_MCR(udrv));
    u950pci_outb(udrv->iobase,UART_LCR, U950PCI_LCR_UL);
    u950pci_acr_write(udrv,U950PCI_RX_ACR(udrv));
  }
  return UL_RC_PROC;
}

const ul_drv_chip_ops u950_chip_ops = {
  "16950-pci",		/* chip_type - text type identification*/
  u950pci_recch,	/* fnc_recch */
  u950pci_sndch,	/* fnc_sndch */
  u950pci_wait,		/* fnc_wait */
  u950pci_connect,	/* fnc_connect */
  NULL,			/* fnc_finishtx */
  u950pci_pool,		/* fnc_pool */
  NULL,			/* fnc_cctrl */
  NULL,			/* fnc_stroke */

  u950pci_txoe,		/* fnc_txoe */
  u950pci_pinit,	/* fnc_pinit */
  u950pci_pdone,	/* fnc_pdone */
  u950pci_activate,	/* fnc_activate */
  u950pci_genirq,	/* fnc_genirq */
  ul_drv_common_fnc_rqirq_noprobe,	/* fnc_rqirq */
  ul_drv_common_fnc_freeirq,	/* fnc_freeirq */
  u950pci_hwtest,	/* fnc_hwtest */
  ul_drv_common_fnc_setmyadr,	/* fnc_setmyadr */
  ul_drv_common_fnc_setpromode,	/* fnc_setpromode */
};

/*** 16950pci chip driver initialize ***/
int u950pci_init(ul_drv *udrv, ul_physaddr_t physbase, int irq, int baud, long baudbase, int options)
{
  unsigned u;
  unsigned old_mcr, mcr_ie;
  unsigned chip_id, chip_rev;
  if(!irq||!ul_physaddr2uint(physbase)){
    LOG_FATAL(KERN_CRIT "uLan u950pci_init : port or irq unknown\n");
    return UL_RC_EPORT;
  }
  UL_PRINTF(KERN_INFO "uLan u950pci_init : irq %d port 0x%lx\n",irq,ul_physaddr2uint(physbase));

  if (ul_io_map(physbase,8,u950pci_port_name,&udrv->iobase)<0)
  { LOG_FATAL(KERN_CRIT "uLan u950pci_init : cannot reguest ports !\n");
    return UL_RC_EPORT;
  }

  old_mcr=u950pci_inb(udrv->iobase,UART_MCR);
  /* find right value for mcr_ie */
  mcr_ie=U950PCI_MCR_IE;
  if(options&U950PCI_CHOPT_TXDTRNEG)
    mcr_ie^=U950PCI_MCR_TXDTRPOL;
  /* Disable output buffer */
  u950pci_outb(udrv->iobase,UART_MCR,mcr_ie&3);
  /* Switch to enhanced mode */
  u950pci_outb(udrv->iobase,UART_LCR,U950PCI_LCR_EFR);
  u950pci_outb(udrv->iobase,UART_EFR,UART_EFR_ECB);
  u950pci_outb(udrv->iobase,UART_LCR,U950PCI_LCR_UL);
  /* Disable transmitter output  */
  u950pci_outb(udrv->iobase,UART_MCR,mcr_ie&3);
  /* Set up ACR and state */
  u950pci_outb(udrv->iobase,UART_SCR,UART_ACR);
  u950pci_outb(udrv->iobase,UART_ICR,U950PCI_ACR_UL|UART_ACR_ICRRD);
  /* Read chip ID */
  u950pci_outb(udrv->iobase,UART_SCR,UART_ID1);
  chip_id=u950pci_inb(udrv->iobase,UART_ICR)<<16;
  u950pci_outb(udrv->iobase,UART_SCR,UART_ID2);
  chip_id|=u950pci_inb(udrv->iobase,UART_ICR)<<8;
  u950pci_outb(udrv->iobase,UART_SCR,UART_ID3);
  chip_id|=u950pci_inb(udrv->iobase,UART_ICR);
  u950pci_outb(udrv->iobase,UART_SCR,UART_REV);
  chip_rev=u950pci_inb(udrv->iobase,UART_ICR);
  LOG_PORTS(KERN_INFO "uLan u950pci : chip ID %06X. rev %02x\n",chip_id,chip_rev);
  /* Switch enhancements off */
  u950pci_outb(udrv->iobase,UART_LCR,U950PCI_LCR_EFR);
  u950pci_outb(udrv->iobase,UART_EFR,0);
  u950pci_outb(udrv->iobase,UART_LCR,U950PCI_LCR_UL);

  { LOG_PORTS(KERN_INFO "uLan u950pci_init : init OK\n");
    udrv->chip_options=options;
    udrv->baud_base=baudbase;
    /* setup configurations for RX/TX */
    udrv->U950PCI_RX_ACRMCR=U950PCI_ACR_UL|(U950PCI_MCR_IE<<8);
    udrv->U950PCI_TX_ACRMCR=U950PCI_ACR_ULTX|(U950PCI_MCR_OE<<8);
    if(udrv->chip_options&U950PCI_CHOPT_TXDTRNEG){
      udrv->U950PCI_RX_ACRMCR^=U950PCI_MCR_TXDTRPOL<<8;
      udrv->U950PCI_TX_ACRMCR^=U950PCI_ACR_TXDTRPOL|(U950PCI_MCR_TXDTRPOL<<8);
    }
    /* check and setup baud base oscilator frequency */
    if(!udrv->baud_base){
     if (udrv->chip_options&U950PCI_CHOPT_OSC62_5) {
         udrv->baud_base=62500000;
     } else {
       if(udrv->chip_options&U950PCI_CHOPT_HSPDOSC)
         udrv->baud_base=14745600;
       else
         udrv->baud_base=1843200;
     }
    }
    if(!baud) baud=19200;
    udrv->baud_val=baud;
    u=(udrv->baud_base*8+baud/2)/baud;
    if (u>0xFFFFFFF) u=0xFFFFFFF;
    udrv->baud_div=u;
    udrv->physbase=physbase;
    udrv->irq=irq; 
    udrv->chip_ops=&u950_chip_ops;
    ul_io_unmap(udrv->physbase,8,udrv->iobase);
    return 0;
  }
  LOG_PORTS(KERN_ERR "uLan u950pci_init : MCR test Error\n");
  ul_io_unmap(physbase,8,udrv->iobase);
  return UL_RC_EPORT;
};

