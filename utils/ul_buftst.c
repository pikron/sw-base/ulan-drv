/*******************************************************************
  uLan Communication - simple test client

  ul_buftst.c	- check of driver interface

  (C) Copyright 1996,1999 by Pavel Pisa 

  The uLan driver is distributed under the Gnu General Public Licence. 
  See file COPYING for details.
 *******************************************************************/

#include <unistd.h>
#include <stdlib.h>
#include <fcntl.h>
#include <stdio.h>
#include <string.h>
#include <sys/types.h>
#include <sys/time.h>
#include <sys/stat.h>
#include <errno.h>
#include <ul_lib/ulan.h>

#ifdef _WIN32
#define sleep Sleep
#endif

#if !defined(_WIN32)

int ul_bt_klogbll(ul_fd_t ul_fd)
{
  return  ioctl(ul_fd,UL_KLOGBLL);
}

int ul_bt_stroke(ul_fd_t ul_fd)
{
  return  ioctl(ul_fd,UL_STROKE);
}

int ul_bt_hwtest(ul_fd_t ul_fd, long arg)
{
  return  ioctl(ul_fd,UL_HWTEST,arg);
}

#else /* _WIN32 */

int ul_bt_klogbll(ul_fd_t ul_fd)
{
  DWORD bytes_ret;
  if(!DeviceIoControl(ul_fd,UL_KLOGBLL,
		  NULL,0,NULL,0,
		  &bytes_ret,NULL)) return -1;
  return 0;
}

int ul_bt_stroke(ul_fd_t ul_fd)
{
  DWORD bytes_ret;
  if(!DeviceIoControl(ul_fd,UL_STROKE,
		  NULL,0,NULL,0,
		  &bytes_ret,NULL)) return -1;
  return 0;
}

ul_bt_hwtest(ul_fd_t ul_fd, long arg)
{
  DWORD bytes_ret;
  DWORD ret;
  if(!DeviceIoControl(ul_fd,UL_HWTEST,
		  &arg,sizeof(arg),
		  &ret,sizeof(ret),
		  &bytes_ret,NULL)) return -1;
  if(!bytes_ret) ret=0;
  return ret;
}

#endif /* _WIN32 */


char *ul_dev_name = UL_DEV_NAME;

int test1(void)
{
  uchar ch;
  ul_fd_t ul_fd;
  int i;
  ul_msginfo msginfo;
  uchar buf1[40]={1,2,3,4,5,6,7,8,9,10};
  uchar buf2[40]={11,12,13,14,15,16,17,18,19,20};

  printf("\n\n*** Simple message send test with read, write and lseek ***\n");

  ul_fd=ul_open(ul_dev_name, NULL);
  if(ul_fd==UL_FD_INVALID){ 
    perror("test1 : uLan open failed");return -1;
  };

  ul_write(ul_fd,buf2,4);
  perror("test1 : ul_write ");

  memset(&msginfo,0,sizeof(msginfo));
  msginfo.dadr=3; msginfo.cmd=10;
  ul_newmsg(ul_fd,&msginfo);

  ul_write(ul_fd,buf1,10);
  lseek(ul_fd,100,SEEK_SET);
  ul_write(ul_fd,buf2,10);
 
  memset(buf1,0,sizeof(buf1));
  memset(buf2,0,sizeof(buf1));
 
  lseek(ul_fd,4,SEEK_SET);
  printf("data from pos 4\n");
  for(i=0;i<20;i++) {ul_read(ul_fd,&ch,1); printf(" %d",ch);}
  printf("\n");

  lseek(ul_fd,100,SEEK_SET);
  printf("data from pos 100\n");
  for(i=0;i<20;i++) {if(ul_read(ul_fd,&ch,1)<=0) break; printf(" %d",ch);}
  printf("\n");

  ul_freemsg(ul_fd);

  ul_bt_stroke(ul_fd);

  sleep(5);

  ul_bt_klogbll(ul_fd);

  ul_close(ul_fd);
  return 0;
}

int test2(void)
{
  ul_fd_t ul_fd;
  int ret;
  ul_msginfo msginfo;
  uchar buf[0x40];
 
  printf("\n\n*** Tailed processed mesage test ***\n");
 
  ul_fd=ul_open(ul_dev_name, NULL);
  if(ul_fd==UL_FD_INVALID){ 
    perror("test2 : uLan open failed");return -1;
  };

  printf("preparing first part\n");
  msginfo.dadr=3;
  msginfo.cmd=UL_CMD_RDM;
  msginfo.flg=UL_BFL_ARQ|UL_BFL_PRQ|UL_BFL_M2IN;
  ul_newmsg(ul_fd,&msginfo);
  buf[0]=(uchar)2;	buf[1]=2>>8;
  buf[2]=(uchar)0x8800;	buf[3]=0x8800>>8;
  buf[4]=(uchar)0x40;	buf[5]=0x40>>8;
  if(ul_write(ul_fd,buf,6)!=6)
    perror("test2 : ul_write ");

  if(0)
  {
    int i;
    uchar ch;
    lseek(ul_fd,0,SEEK_SET);
    printf("data from pos 0\n");
    for(i=0;i<20;i++) {if(ul_read(ul_fd,&ch,1)<=0) break; printf(" %d",ch);}
    printf("\n");
  };

  printf("preparing second part\n");
  msginfo.dadr=3;
  msginfo.cmd=UL_CMD_RDM&0x7F;
  msginfo.flg=UL_BFL_REC|UL_BFL_LNMM|UL_BFL_M2IN;
  msginfo.len=0x40;
  ul_tailmsg(ul_fd,&msginfo);

  ul_freemsg(ul_fd);

  ul_bt_klogbll(ul_fd);

  ul_bt_stroke(ul_fd);


  printf("Sending tailed message an waiting for input\n");

  ret=ul_fd_wait(ul_fd, 10);

  printf("ul_fd_wait returned %d\n",ret);

  memset(&msginfo,0,sizeof(msginfo));
  ret=ul_acceptmsg(ul_fd,&msginfo);

  printf("accept message returned %d\n",ret);
  printf("  flg=0x%X, dadr=%d, sadr=%d, cmd=%d, len=%d, stamp=%d\n",
           msginfo.flg,msginfo.dadr,msginfo.sadr,msginfo.cmd,
           msginfo.len,msginfo.stamp);
 
  memset(&msginfo,0,sizeof(msginfo));
  ret=ul_actailmsg(ul_fd,&msginfo);
  printf("accept tail returned %d\n",ret);
  printf("  flg=0x%X, dadr=%d, sadr=%d, cmd=%d, len=%d, stamp=%d\n",
           msginfo.flg,msginfo.dadr,msginfo.sadr,msginfo.cmd,
           msginfo.len,msginfo.stamp);

  ul_close(ul_fd);

  return 0;
}

int test3(void)
{
  ul_fd_t ul_fd;
  int ret;
  ul_fd=ul_open(ul_dev_name, NULL);
  if(ul_fd==UL_FD_INVALID){ 
    perror("test3 : uLan open failed");return -1;
  };

  ret=ul_bt_hwtest(ul_fd, 0x12);
  printf("get msr/lsr %06X\n",ret);
  ret=ul_bt_hwtest(ul_fd, 0x10);
  printf("set break   %06X\n",ret);
  sleep(2);
  ret=ul_bt_hwtest(ul_fd, 0x12);
  printf("get msr/lsr %06X\n",ret);
  ret=ul_bt_hwtest(ul_fd, 0x13);
  printf("set tx off  %06X\n",ret);
  sleep(2);
  ret=ul_bt_hwtest(ul_fd, 0x12);
  printf("get msr/lsr %06X\n",ret);
  ret=ul_bt_hwtest(ul_fd, 0x11);
  printf("set one     %06X\n",ret);
  sleep(2);
  ret=ul_bt_hwtest(ul_fd, 0x12);
  printf("get msr/lsr %06X\n",ret);
  sleep(2);
  ret=ul_bt_hwtest(ul_fd, 0x13);
  printf("set tx off  %06X\n",ret);
  sleep(2);
  ret=ul_bt_hwtest(ul_fd, 0x12);
  printf("get msr/lsr %06X\n",ret);

  ul_close(ul_fd);
  return 0;
}


int main(int argc,char *argv[])
{

  if(argc>=2)
    ul_dev_name=argv[1];
  /* test1(); */
  /* test2(); */
  test3();

  return 0;
}
