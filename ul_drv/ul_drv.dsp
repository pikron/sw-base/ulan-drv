# Microsoft Developer Studio Project File - Name="ul_drv" - Package Owner=<4>
# Microsoft Developer Studio Generated Build File, Format Version 6.00
# ** DO NOT EDIT **

# TARGTYPE "Win32 (x86) External Target" 0x0106

CFG=ul_drv - Win32 Debug
!MESSAGE This is not a valid makefile. To build this project using NMAKE,
!MESSAGE use the Export Makefile command and run
!MESSAGE 
!MESSAGE NMAKE /f "ul_drv.mak".
!MESSAGE 
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE 
!MESSAGE NMAKE /f "ul_drv.mak" CFG="ul_drv - Win32 Debug"
!MESSAGE 
!MESSAGE Possible choices for configuration are:
!MESSAGE 
!MESSAGE "ul_drv - Win32 Release" (based on "Win32 (x86) External Target")
!MESSAGE "ul_drv - Win32 Debug" (based on "Win32 (x86) External Target")
!MESSAGE 

# Begin Project
# PROP AllowPerConfigDependencies 0
# PROP Scc_ProjName ""
# PROP Scc_LocalPath ""

!IF  "$(CFG)" == "ul_drv - Win32 Release"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 0
# PROP BASE Output_Dir "Release"
# PROP BASE Intermediate_Dir "Release"
# PROP BASE Cmd_Line "NMAKE /f ul_drv.mak"
# PROP BASE Rebuild_Opt "/a"
# PROP BASE Target_File "ul_drv.exe"
# PROP BASE Bsc_Name "ul_drv.bsc"
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 0
# PROP Output_Dir "Release"
# PROP Intermediate_Dir "Release"
# PROP Cmd_Line "nmake /f "ul_drv.mak""
# PROP Rebuild_Opt "/a"
# PROP Target_File "ul_drv.sys"
# PROP Bsc_Name ""
# PROP Target_Dir ""

!ELSEIF  "$(CFG)" == "ul_drv - Win32 Debug"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 1
# PROP BASE Output_Dir "Debug"
# PROP BASE Intermediate_Dir "Debug"
# PROP BASE Cmd_Line "NMAKE /f ul_drv.mak"
# PROP BASE Rebuild_Opt "/a"
# PROP BASE Target_File "ul_drv.exe"
# PROP BASE Bsc_Name "ul_drv.bsc"
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 1
# PROP Output_Dir "Debug"
# PROP Intermediate_Dir "Debug"
# PROP Cmd_Line "nmake /f "ul_drv.mak""
# PROP Rebuild_Opt "/a"
# PROP Target_File "ul_drv.sys"
# PROP Bsc_Name ""
# PROP Target_Dir ""

!ENDIF 

# Begin Target

# Name "ul_drv - Win32 Release"
# Name "ul_drv - Win32 Debug"

!IF  "$(CFG)" == "ul_drv - Win32 Release"

!ELSEIF  "$(CFG)" == "ul_drv - Win32 Debug"

!ENDIF 

# Begin Group "Source Files"

# PROP Default_Filter "cpp;c;cxx;rc;def;r;odl;idl;hpj;bat"
# Begin Source File

SOURCE=.\ul_16950pci.c
# End Source File
# Begin Source File

SOURCE=.\ul_drv.c
# End Source File
# Begin Source File

SOURCE=.\ul_wdent.c
# End Source File
# Begin Source File

SOURCE=.\ul_wdpnp.c
# End Source File
# End Group
# Begin Group "Header Files"

# PROP Default_Filter "h;hpp;hxx;hm;inl"
# Begin Source File

SOURCE=.\serial_reg.h
# End Source File
# Begin Source File

SOURCE=.\ul_82510.h
# End Source File
# Begin Source File

SOURCE=.\ul_drv.h
# End Source File
# Begin Source File

SOURCE=.\ul_hdep.h
# End Source File
# End Group
# Begin Group "Resource Files"

# PROP Default_Filter "ico;cur;bmp;dlg;rc2;rct;bin;rgs;gif;jpg;jpeg;jpe"
# End Group
# Begin Group "Additional Files"

# PROP Default_Filter ""
# Begin Source File

SOURCE=.\ul_drv.err
# End Source File
# Begin Source File

SOURCE=.\ul_drv.mak
# End Source File
# End Group
# End Target
# End Project
