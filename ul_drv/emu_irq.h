#ifndef EMU_IRQ_H
#define EMU_IRQ_H

/*******************************************************************
  User space IRQ handling emulator for i386 Linux based PCs
  It uses VM86_PLUS services developed for DOSEMU

  emu_irq.h

  (C) Copyright 1998 by Pavel Pisa 

  This code is distributed under the Gnu General Public Licence. 
  See file COPYING for details.
 *******************************************************************/

#include <signal.h>

#define USER_SPACE_EMU_IRQ

#define EMU_IRQ_SIG SIGIO

static inline void emu_irq_cli(void)
{sigset_t my_set;
 sigemptyset(&my_set);
 sigaddset(&my_set,EMU_IRQ_SIG);
 sigprocmask(SIG_BLOCK,&my_set,NULL);
};

static inline void emu_irq_sti(void)
{sigset_t my_set;
 sigemptyset(&my_set);
 sigaddset(&my_set,EMU_IRQ_SIG);
 sigprocmask(SIG_UNBLOCK,&my_set,NULL);
};

typedef  void (emu_irq_handler)(int intno, void *dev_id, void *regs);

int  request_emu_irq(unsigned int intno, 
                     emu_irq_handler *handler,
                     unsigned long flags, 
                     const char *device,
                     void *dev_id);
int  release_emu_irq(int intno, void *dev_id);

#endif /* EMU_IRQ_H */
