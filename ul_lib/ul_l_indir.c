/*******************************************************************
  uLan Communication - low level C and driver interface library

  ul_l_indir.c	- multiple driver interface kinds selector

  (C) Copyright 1996-2016 by Pavel Pisa - project originator
        http://cmp.felk.cvut.cz/~pisa
  (C) Copyright 1996-2016 PiKRON Ltd.
        http://www.pikron.com
  (C) Copyright 2002-2016 Petr Smolik


  The uLan driver project can be used and distributed
  in compliance with any of next licenses
   - GPL - GNU Public License
     See file COPYING for details.
   - LGPL - Lesser GNU Public License
   - MPL - Mozilla Public License
   - and other licenses added by project originator

  Code can be modified and re-distributed under any combination
  of the above listed licenses. If contributor does not agree with
  some of the licenses, he/she can delete appropriate line.
  WARNING: if you delete all lines, you are not allowed to
  distribute code or sources in any form.
 *******************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ul_lib/ulan.h>

#define UL_LIB_INDIR_FNC_SPEC

typedef struct {
  int count;
  int capacity;
  ul_fd_ops_t **fd_ops;
} ul_fd_ops_list_t;

extern ul_fd_ops_t ul_fd_direct_ops;
#ifdef CONFIG_UL_FD_ETH
extern ul_fd_ops_t ul_fd_eth_ops;
#endif

ul_fd_ops_t *ul_fd_ops_defaults[]={
  &ul_fd_direct_ops,
 #ifdef CONFIG_UL_FD_ETH
  &ul_fd_eth_ops,
 #endif
};

ul_fd_ops_list_t ul_fd_ops_list={
  sizeof(ul_fd_ops_defaults)/sizeof(ul_fd_ops_defaults[0]),
  -1,
  ul_fd_ops_defaults
};

int ul_fd_ops_register(ul_fd_ops_t *fd_ops)
{
  ul_fd_ops_list_t *list=&ul_fd_ops_list;

  if(list->capacity<list->count+1){
    ul_fd_ops_t **data;
    int capacity=list->count+10;
    data=malloc(capacity);
    if(!data) return -1;
    if(list->fd_ops){
      memcpy(data,list->fd_ops,list->count*sizeof(ul_fd_ops_t *));
      if(list->capacity>=0)
        free(list->fd_ops);
    }
    list->fd_ops=data;
    list->capacity=capacity;
  }

  list->fd_ops[list->count]=fd_ops;
  list->count++;
  return 0;
}

ul_fd_t ul_open(const char *dev_name, const char *options) {
  ul_fd_t ul_fd;
  int idx, val, match_idx=-1, match_val=0;
  
  for(idx=0;idx<ul_fd_ops_list.count;idx++){
    if(ul_fd_ops_list.fd_ops[idx]->ulop_namematch){
      val=ul_fd_ops_list.fd_ops[idx]->ulop_namematch(dev_name);
      if(val>match_val){
        match_val=val;
	match_idx=idx;
      }
    }
  }

  if(match_idx<0)
    return UL_FD_INVALID;

  ul_fd=ul_fd_ops_list.fd_ops[match_idx]->ulop_open(dev_name, options);

  return ul_fd;
}

UL_LIB_INDIR_FNC_SPEC
int ul_close(ul_fd_t ul_fd) {
  return ul_fd->fd_ops->ulop_close(ul_fd);
}

UL_LIB_INDIR_FNC_SPEC
int ul_drv_version(ul_fd_t ul_fd) {
  return ul_fd->fd_ops->ulop_drv_version(ul_fd);
}

UL_LIB_INDIR_FNC_SPEC
ssize_t ul_read(ul_fd_t ul_fd, void *buffer, size_t size) {
  return ul_fd->fd_ops->ulop_read(ul_fd,buffer,size);
}

UL_LIB_INDIR_FNC_SPEC
ssize_t ul_write(ul_fd_t ul_fd, const void *buffer, size_t size) {
  return ul_fd->fd_ops->ulop_write(ul_fd,buffer,size);
}

UL_LIB_INDIR_FNC_SPEC
int ul_newmsg(ul_fd_t ul_fd,const ul_msginfo *msginfo) {
  return ul_fd->fd_ops->ulop_newmsg(ul_fd,msginfo);
}

UL_LIB_INDIR_FNC_SPEC
int ul_tailmsg(ul_fd_t ul_fd,const ul_msginfo *msginfo) {
  return ul_fd->fd_ops->ulop_tailmsg(ul_fd,msginfo);
}

UL_LIB_INDIR_FNC_SPEC
int ul_freemsg(ul_fd_t ul_fd) {
  return ul_fd->fd_ops->ulop_freemsg(ul_fd);
}

UL_LIB_INDIR_FNC_SPEC
int ul_acceptmsg(ul_fd_t ul_fd,ul_msginfo *msginfo) {
  return ul_fd->fd_ops->ulop_acceptmsg(ul_fd,msginfo);
}

UL_LIB_INDIR_FNC_SPEC
int ul_actailmsg(ul_fd_t ul_fd,ul_msginfo *msginfo) {
  return ul_fd->fd_ops->ulop_actailmsg(ul_fd,msginfo);
}

UL_LIB_INDIR_FNC_SPEC
int ul_addfilt(ul_fd_t ul_fd,const ul_msginfo *msginfo) {
  return ul_fd->fd_ops->ulop_addfilt(ul_fd,msginfo);
}

UL_LIB_INDIR_FNC_SPEC
int ul_abortmsg(ul_fd_t ul_fd) {
  return ul_fd->fd_ops->ulop_abortmsg(ul_fd);
}

UL_LIB_INDIR_FNC_SPEC
int ul_rewmsg(ul_fd_t ul_fd) {
  return ul_fd->fd_ops->ulop_rewmsg(ul_fd);
}

UL_LIB_INDIR_FNC_SPEC
int ul_inepoll(ul_fd_t ul_fd) {
  return ul_fd->fd_ops->ulop_inepoll(ul_fd);
}

UL_LIB_INDIR_FNC_SPEC
int ul_drv_debflg(ul_fd_t ul_fd,int debug_msk) {
  return ul_fd->fd_ops->ulop_drv_debflg(ul_fd,debug_msk);
}

UL_LIB_INDIR_FNC_SPEC
int ul_fd_wait(ul_fd_t ul_fd, int wait_sec) {
  return ul_fd->fd_ops->ulop_fd_wait(ul_fd,wait_sec);
}

UL_LIB_INDIR_FNC_SPEC
ul_fd_direct_t ul_fd2sys_fd(ul_fd_t fd)
{
  if(!fd->fd_ops->ulop_fd2sys_fd)
    return UL_FD_DIRECT_INVALID;
  else
    return fd->fd_ops->ulop_fd2sys_fd(fd);
}

UL_LIB_INDIR_FNC_SPEC
int ul_setmyadr(ul_fd_t ul_fd, int newadr)
{
  return ul_fd->fd_ops->ulop_setmyadr(ul_fd,newadr);
}

UL_LIB_INDIR_FNC_SPEC
int ul_setidstr(ul_fd_t ul_fd, const char *idstr)
{
  return ul_fd->fd_ops->ulop_setidstr(ul_fd,idstr);
}

UL_LIB_INDIR_FNC_SPEC
int ul_setbaudrate(ul_fd_t ul_fd, int baudrate)
{
  return ul_fd->fd_ops->ulop_setbaudrate(ul_fd,baudrate);
}

UL_LIB_INDIR_FNC_SPEC
int ul_setpromode(ul_fd_t ul_fd, int pro_mode)
{
  return ul_fd->fd_ops->ulop_setpromode(ul_fd,pro_mode);
}

UL_LIB_INDIR_FNC_SPEC
int ul_setsubdev(ul_fd_t ul_fd, int subdevidx)
{
  return ul_fd->fd_ops->ulop_setsubdev(ul_fd,subdevidx);
}

UL_LIB_INDIR_FNC_SPEC
int ul_queryparam(ul_fd_t ul_fd, unsigned long query, unsigned long *pvalue)
{
  return ul_fd->fd_ops->ulop_queryparam(ul_fd,query,pvalue);
}

UL_LIB_INDIR_FNC_SPEC
int ul_route(ul_fd_t ul_fd, ul_route_range_t *rr)
{
  return ul_fd->fd_ops->ulop_route(ul_fd,rr);
}

UL_LIB_INDIR_FNC_SPEC
int ul_fd_is_valid(ul_fd_t ul_fd)
{
  return (ul_fd != UL_FD_INVALID);
}
