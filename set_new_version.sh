#!/bin/bash

if [ $# -lt 3 ] ; then
  echo "$0: missing parameters" 1>&2
  echo "Usage: $0 <major> <minor> <patchlevel>" 1>&2
  exit 1
fi


V_MAJOR="$1"
V_MINOR="$2"
V_PATCH="$3"

V_TXT="$V_MAJOR.$V_MINOR.$V_PATCH"
V_CODE="$(printf '0x%02X%02X%02X' "$V_MAJOR" "$V_MINOR" "$V_PATCH")"

V_DATE_MDY="$( date '+%m/%d/%Y' )"
V_DATE_YMD="$( date '+%Y-%m-%d' )"
V_DATE_RFC="$( date -R )"

V_USER_NAME="$(git config user.name)"
V_USER_EMAIL="$(git config user.email)"

for f in config/winnt/*.in[fe] ; do
  sed --in-place -e 's#^DriverVer=.*$#DriverVer='"$V_DATE_MDY, $V_TXT.0\r#" "$f"
done

sed --in-place \
  -e 's#^Version:.*$#Version:\t'"$V_TXT#" \
  -e 's#^Entered-date:.*$#Entered-date:\t'"$V_DATE_YMD#" \
  -e 's#ul_drv-[0-9.]*.tar.gz#ul_drv-'"$V_TXT"'.tar.gz#' \
  ul_drv.lsm

sed --in-place \
  -e 's/^#define UL_DRV_VERSION .*$/#define UL_DRV_VERSION "'"$V_TXT"'"/' \
  -e 's/^#define UL_DRV_VERCODE .*$/#define UL_DRV_VERCODE '"$V_CODE"'/' \
  ul_drv/ul_drv.h

sed --in-place \
  -e 's#^Driver version:.*$#Driver version: '"$V_TXT#" \
  -e 's#^Release date:.*$#Release date: '"$V_DATE_YMD#" \
  README

sed --in-place \
  -e 's#^\([\t ]*\)uLan Driver for NT v.*$#\1uLan Driver for NT v. '"$V_TXT#" \
  ul_iss/readme.txt

sed --in-place \
  -e 's#^AppVerName=uLan driver for NT v.*$#AppVerName=uLan driver for NT v '"$V_TXT#" \
  ul_iss/ul_drvnt.iss

C_APP_FILES="utils/ul_lcabsp.c utils/ul_ping.c utils/ul_sendhex.c utils/ul_sendmsg.c utils/ul_spy.c"

for f in $C_APP_FILES ; do
  sed --in-place \
    -e 's#^\([\t ]*fputs("uLan [[:alnum:] ]* v\)[0-9.]*\([^0-9"]*", *stdout);\)#\1'"$V_MAJOR.$V_MINOR"'\2#' \
    "$f"
done

sed --in-place \
  -e 's#PACKAGE_VERSION=.*$#PACKAGE_VERSION="'"$V_TXT"'"#' \
  dkms.conf

if grep -q "ulan-drv ($V_TXT)" debian/changelog ; then
sed --in-place \
  -e '1,/^ -- .*$/s/^ -- .*$/'" -- $V_USER_NAME <$V_USER_EMAIL> $V_DATE_RFC/" \
  debian/changelog
else
cat >debian/changelog.tmp <<EOF
ulan-drv ($V_TXT) unstable; urgency=medium

  * Debian package updated to version $V_TXT.

 -- $V_USER_NAME <$V_USER_EMAIL> $V_DATE_RFC

EOF
cat debian/changelog >>debian/changelog.tmp
mv debian/changelog.tmp debian/changelog
fi


echo -e "\nRemove possible old local tag mark for this release\n"
echo -e "  git tag -d ul_drv-$V_TXT-release \n"
echo -e "Run next command to add tag into git repository\n"
echo -e "  git tag -a ul_drv-$V_TXT-release \n"
echo -e "Push tag to remote repository after testing\n"

