/*******************************************************************
  uLan Communication - user visible definitions

  ul_fd4dir.h	- net access through direct system handle

  (C) Copyright 1996-2016 by Pavel Pisa - project originator
        http://cmp.felk.cvut.cz/~pisa
  (C) Copyright 1996-2016 PiKRON Ltd.
        http://www.pikron.com
  (C) Copyright 2002-2016 Petr Smolik


  The uLan driver project can be used and distributed
  in compliance with any of next licenses
   - GPL - GNU Public License
     See file COPYING for details.
   - LGPL - Lesser GNU Public License
   - MPL - Mozilla Public License
   - and other licenses added by project originator

  Code can be modified and re-distributed under any combination
  of the above listed licenses. If contributor does not agree with
  some of the licenses, he/she can delete appropriate line.
  WARNING: if you delete all lines, you are not allowed to
  distribute code or sources in any form.
 *******************************************************************/

#ifndef _UL_FD_H
#define _UL_FD_H

/* The applications calls directly functions based above uLan driver API */

#include <ul_lib/ul_fddir.h>

#undef WITH_UL_FD_INDIRECT

#ifdef __cplusplus
extern "C" {
#endif

typedef ul_fd_direct_t ul_fd_t;
#define UL_FD_INVALID UL_FD_DIRECT_INVALID

static inline ul_fd_direct_t ul_fd2sys_fd(ul_fd_t fd)
{
  return fd;
}

#ifdef __cplusplus
} /* extern "C"*/
#endif

#endif /*_UL_FD_H*/
