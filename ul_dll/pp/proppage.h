/**************************************************/
/* uLan property page - Roman Bartosinski (C)2003 */
/**************************************************/
#ifndef PROPERTY_PAGE_H
  #define PROPERTY_PAGE_H

  #include <windows.h>
  #include <tchar.h>
  #include <setupapi.h>

  #include "resource.h"

/********************************************************************/

  #define ERROR_MEMORY_STRING       TEXT("Insufficient memory for this operation;\r\rclose one or more Windows applications to increase available memory.")

  #if DBG
    #define debugPrint(_x_) MessageBox(NULL,TEXT(_x_),TEXT("PROPPAGE CI - DebugInfo"),MB_OK | MB_ICONINFORMATION)
    #define debugPrintP(_x_) MessageBox(NULL,_x_,TEXT("PROPPAGE CI - DebugInfo"),MB_OK | MB_ICONINFORMATION)
    //DbgPrint _x_
  #else
    #define debugPrint(_x_)
    #define debugPrintP(_x_)
  #endif

/********************************************************************/

  #define ULANBAUDRATE  TEXT("uLanBaudrate")
  #define ULANMYADDRESS TEXT("uLanMyAddress")
  #define ULANGWADDRESS TEXT("uLanGWAddress")
  #define ULANDEBUG     TEXT("uLanDebugLevel")
  #define ULANPROMODE   TEXT("uLanProMode")

  #define ULANADDR_MIN  1
  #define ULANADDR_MAX  100

/********************************************************************/

  typedef struct {
    DWORD  DefBaudRate;
    DWORD  DefAddress;
    DWORD  DefGW;
    DWORD  DefDebug;
    DWORD  DefProMode;
    DWORD  BaudRate;
    DWORD  Address;
    DWORD  GW;
    DWORD  Debug;
    DWORD  ProMode;
  } PP_PORTSETTINGS, *PPP_PORTSETTINGS;

  typedef struct _PORT_PARAMS {
    PP_PORTSETTINGS              PortSettings;
    HDEVINFO                     DeviceInfoSet;
    PSP_DEVINFO_DATA             DeviceInfoData;
    BOOL                         ChangesEnabled;
    BOOL                         SomethingWasChanged;
  } PORT_PARAMS, *PPORT_PARAMS;


/********************************************************************/

  HPROPSHEETPAGE InitSettingsPage( PROPSHEETPAGE *Psp, OUT PPORT_PARAMS Params);
  UINT CALLBACK PortSettingsDlgCallback( HWND hwnd, UINT uMsg, LPPROPSHEETPAGE ppsp);
  INT_PTR APIENTRY PortSettingsDlgProc( IN HWND   hDlg, IN UINT   uMessage, IN WPARAM wParam, IN LPARAM lParam);
  BOOL FillSettingCommDlg( IN HWND hDlg, IN PPORT_PARAMS params);
  BOOL SetPortSettings( IN HWND hDlg, IN PPORT_PARAMS params);
  void RestorePortSettings( HWND hDlg, PPORT_PARAMS Params);
  VOID SetVisibleAndEnable( HWND hDlg, BOOL chng, BOOL ok);

  int myatoi(LPTSTR pszInt);

#endif

