/*******************************************************************
  uLan Communication - uL_DRV - multiplatform uLan driver

  ul_wdbase.c	- common Windows NT KDM and Win2k WDM driver code

  (C) Copyright 1996-2004 by Pavel Pisa - project originator
        http://cmp.felk.cvut.cz/~pisa
  (C) Copyright 1996-2004 PiKRON Ltd.
        http://www.pikron.com
  (C) Copyright 2002-2004 Petr Smolik
  

  The uLan driver project can be used and distributed 
  in compliance with any of next licenses
   - GPL - GNU Public License
     See file COPYING for details.
   - LGPL - Lesser GNU Public License
   - MPL - Mozilla Public License
   - and other licenses added by project originator

  Code can be modified and re-distributed under any combination
  of the above listed licenses. If contributor does not agree with
  some of the licenses, he/she can delete appropriate line.
  WARNING: if you delete all lines, you are not allowed to
  distribute code or sources in any form.
 *******************************************************************/

// This variable has to be initialized by RtlConvertLongToLargeInteger(0) in DriverEntry
LARGE_INTEGER ULD_LARGE_INTEGER_0;

//-------------------------------------------------------------------
// Memory check

#ifdef ENABLE_UL_MEM_CHECK
void * ul_mem_check_malloc(size_t size)
{ void *ptr;
  ptr=ExAllocatePool(NonPagedPool,size);
  if(ptr) InterlockedIncrement(&ul_mem_check_counter);
  return ptr;
}

void ul_mem_check_free(void *ptr)
{
  if(ptr) InterlockedDecrement(&ul_mem_check_counter);
  ExFreePool(ptr);
}
#endif /* ENABLE_UL_MEM_CHECK */

//-------------------------------------------------------------------
//
//  Begin FUNCTIONS
//

NTSTATUS
ulan_GetRegistryDword(
    IN      PWCHAR    RegPath,
    IN      PWCHAR    ValueName,
    IN OUT  PULONG    Value
    )

/*++

Routine Description:

	Obtain a Dword value from the registry


Arguments:

    RegPath  -- supplies absolute registry path
    ValueName    - Supplies the Value Name.
    Value      - receives the REG_DWORD value.

Return Value:

    TRUE if successfull, FALSE on fail.

--*/

{
  UNICODE_STRING path;
  RTL_QUERY_REGISTRY_TABLE paramTable[2];  //zero'd second table terminates parms
  ULONG lDef = *Value;                     // default
  NTSTATUS status;
  WCHAR wbuf[ MAXIMUM_FILENAME_LENGTH ];

  path.Length = 0;
  path.MaximumLength = MAXIMUM_FILENAME_LENGTH * sizeof( WCHAR );  // MAXIMUM_FILENAME_LENGTH defined in wdm.h
  path.Buffer = wbuf;

  RtlZeroMemory(path.Buffer, path.MaximumLength);
  RtlMoveMemory(path.Buffer, RegPath, wcslen( RegPath) * sizeof( WCHAR ));

  RtlZeroMemory(paramTable, sizeof(paramTable));

  paramTable[0].Flags = RTL_QUERY_REGISTRY_DIRECT;
  paramTable[0].Name = ValueName;
  paramTable[0].EntryContext = Value;
  paramTable[0].DefaultType = REG_DWORD;
  paramTable[0].DefaultData = &lDef;
  paramTable[0].DefaultLength = sizeof(ULONG);


  status = RtlQueryRegistryValues( RTL_REGISTRY_ABSOLUTE | RTL_REGISTRY_OPTIONAL,
                                  path.Buffer, paramTable, NULL, NULL);

  return status;
}

//-------------------------------------------------------------------
// ScanForPCICard for WinNT KMD
// WDM uses different technique to query VENDOR_ID and DEVICE_ID
// for example PCI\VEN_1415&DEV_950A&SUBSYS_00001415&REV_00

#ifdef UL_WITH_PCI

#ifdef FOR_WIN_WDM

BOOLEAN FindPciHWID(PWCHAR HWID,
		    OUT pci_device_id_t **Ppci_device_id
	           )
{
  PWCHAR pwcH, pwcM;
  WCHAR  fldName;
  USHORT vendorID=0;
  USHORT deviceID=0;
  USHORT subvendorID=0;
  USHORT subdeviceID=0;
  ULONG  val, i, c;
  static WCHAR PciHWIDMask[]=L"PCI\\VEN_vvvv&DEV_xxxx&SUBSYS_sssswwww";
  pci_device_id_t *device_id;

  *Ppci_device_id=NULL;
  if(HWID==NULL) return FALSE;

  pwcH=HWID;
  pwcM=PciHWIDMask;
  while(*pwcH&&*pwcM) {
    if(*pwcH==*pwcM){
      pwcH++; pwcM++;
      continue;
    }
    fldName=*pwcM;
    if((fldName!='v')&&(fldName!='x')&&
       (fldName!='s')&&(fldName!='w')) break;
    val=0;
    for(i=4;i--;){
      c=*(pwcH++); pwcM++;
      if(c<='9') c-='0';
      else if(c<='F') c=c-'A'+10;
      else if(c<='f') c=c-'a'+10;
      if(c>15) {val=0;break;}
      val<<=4;
      val+=c;
    }
    switch (fldName) {
      case 'v':
        vendorID=(USHORT)val;
	break;
      case 'x':
        deviceID=(USHORT)val;
	break;
      case 'w':
        subvendorID=(USHORT)val;
	break;
      case 's':
        subdeviceID=(USHORT)val;
	break;
    }
  }
#if DBG
  uLan_DbgPrint("uLan: PCI - Ven %04X Dev %04X SubVen %04X SubDev %04X\n", 
                 vendorID,deviceID,subvendorID,subdeviceID);
#endif

  for(device_id=ulan_pci_tbl;device_id->vendor;device_id++){
    if((device_id->vendor!=PCI_ANY_ID)&&(device_id->vendor!=vendorID)) continue;
    if((device_id->device!=PCI_ANY_ID)&&(device_id->device!=deviceID)) continue;
    if((device_id->subvendor!=PCI_ANY_ID)&&(device_id->vendor!=subvendorID)) continue;
    if((device_id->subdevice!=PCI_ANY_ID)&&(device_id->device!=subdeviceID)) continue;
    *Ppci_device_id=device_id;
    return TRUE;
  }

  return FALSE;
}

#else /* !FOR_WIN_WDM */

BOOLEAN ScanForPCICard(IN OUT PULONG PBusNumber,
                       IN OUT PPCI_SLOT_NUMBER PSlotNumber,
                       IN BOOLEAN FindFirst,
		       OUT pci_device_id_t **Ppci_device_id
		      )
{
  ULONG BusNumber=*PBusNumber;
  PCI_SLOT_NUMBER SlotNumber=*PSlotNumber;
  UCHAR Buffer[2*sizeof(USHORT)];
  ULONG Readed=sizeof(Buffer);
  pci_device_id_t *device_id;

  while(1){
    if(FindFirst){
      BusNumber=0;
      SlotNumber.u.AsULONG=0;
      FindFirst=0;
    }else{
      if(Readed>2){
        SlotNumber.u.bits.FunctionNumber=
            (SlotNumber.u.bits.FunctionNumber+1)&7;
      }else{
        SlotNumber.u.bits.FunctionNumber=0;
      }
      if(!SlotNumber.u.bits.FunctionNumber){
        if(!Readed&&!SlotNumber.u.bits.DeviceNumber)
            break;
        SlotNumber.u.bits.DeviceNumber=
            (SlotNumber.u.bits.DeviceNumber+1)&0x1f;
        if(!SlotNumber.u.bits.DeviceNumber)
            BusNumber++;
      }
    }

    Readed=HalGetBusData(PCIConfiguration,BusNumber,SlotNumber.u.AsULONG,
                         Buffer,sizeof(Buffer));
    if(Readed==sizeof(Buffer)){
      for(device_id=&ulan_pci_tbl[0];device_id->vendor;device_id++){
        if((((PPCI_COMMON_CONFIG)Buffer)->VendorID==device_id->vendor)&&
           (((PPCI_COMMON_CONFIG)Buffer)->DeviceID==device_id->device)){
            *PBusNumber=BusNumber;
            *PSlotNumber=SlotNumber;
            if(Ppci_device_id)
                *Ppci_device_id=device_id;
            return TRUE;
        }
      }
    }
  }

  return FALSE;
}                    

#endif /* FOR_WIN_WDM */

#endif /*UL_WITH_PCI*/

//-------------------------------------------------------------------
// WDM uses different technique to query VENDOR_ID and DEVICE_ID
// for example USB\Vid_dead&Pid_1001&REV_0001

#ifdef UL_WITH_USB

BOOLEAN FindUsbHWID(PWCHAR HWID,
		    OUT usb_device_id_t **Pusb_device_id
	           )
{
  PWCHAR pwcH, pwcM;
  WCHAR  fldName;
  USHORT vendorID=0;
  USHORT productID=0;
  ULONG  val, i, c;
  static WCHAR UsbHWIDMask[]=L"USB\\VID_vvvv&PID_pppp";
  usb_device_id_t *device_id;

  *Pusb_device_id=NULL;
  if(HWID==NULL) return FALSE;

  pwcH=HWID;
  pwcM=UsbHWIDMask;
  while(*pwcH&&*pwcM) {
    if(RtlUpcaseUnicodeChar(*pwcH)==*pwcM){
      pwcH++; pwcM++;
      continue;
    }
    fldName=*pwcM;
    if((fldName!='v')&&(fldName!='p')) break;
    val=0;
    for(i=4;i--;){
      c=*(pwcH++); pwcM++;
      if(c<='9') c-='0';
      else if(c<='F') c=c-'A'+10;
      else if(c<='f') c=c-'a'+10;
      if(c>15) {val=0;break;}
      val<<=4;
      val+=c;
    }
    switch (fldName) {
      case 'v':
        vendorID=(USHORT)val;
	break;
      case 'p':
        productID=(USHORT)val;
	break;
    }
  }
#if DBG
  uLan_DbgPrint("uLan: USB - Ven %04X Dev %04X\n", 
                 vendorID,productID);
#endif

  for(device_id=ulan_usb_tbl;device_id->idVendor;device_id++){
    if((device_id->idVendor!=vendorID)) continue;
    if((device_id->idProduct!=productID)) continue;
    *Pusb_device_id=device_id;
    return TRUE;
  }

  return FALSE;
}

#endif /* UL_WITH_USB */


//-------------------------------------------------------------------
//
//  Initialize and free driver
//
/* initialize new driver from given parameters */

NTSTATUS ul_drv_init_ext(ul_drv *udrv, ul_physaddr_t physbase,
	 int irq, int baud, long baudbase, int chip_options, int buffer_size, int my_adr)
{
  int ret;
  int test_cnt;
  unsigned long probe_irqs;
  udrv->flag_IN_ISR=0;
  udrv->flag_ASK_ISR=0;
  udrv->flag_NACTIV=0;
  udrv->flag_CHIPOK=0;
  udrv->flag_IN_BOTTOM=0;
  udrv->flag_ASK_BOTTOM=0;
  udrv->flag_CHECK_FILT=0;
  udrv->flag_KWTKILL=0;
  if(buffer_size<0x2000) buffer_size=0x2000;
  /* init sequencer */
  ul_drv_new_init_state(udrv,my_adr);

  if (chip_options&U950PCI_CHOPT_SP512F4K) {
      physbase.physaddr.QuadPart+=0x1000;
  }

  /* init chip driver */
  #ifdef CONFIG_OC_UL_DRV_WITH_VIRTUAL
  if(chip_options == UL_VIRTUAL_HW) {
    if((ret=ul_virtual_init(udrv, physbase, irq, baud, baudbase, chip_options))>=0){
      UL_PRINTF("uLan ul_drv_new : virtual\n");
    }
  } else
  #endif /* CONFIG_OC_UL_DRV_WITH_VIRTUAL */
  #ifdef UL_WITH_USB
  if((chip_options&~0xff)==UL_USB_HW_PS1){
    if((ret=ul_usb_ps1_init(udrv, physbase, irq, baud, baudbase, chip_options))>=0){
      UL_PRINTF("uLan ul_drv_new : usb_ps1\n");
    }
  } else
  #endif /* UL_WITH_USB */
  {
  #ifdef UL_WITH_PCI
    if((chip_options&~0xff)==0x16954000){
      if((ret=u950pci_init(udrv, physbase, irq, baud, baudbase, chip_options))>=0){
        UL_PRINTF("uLan ul_drv_new : 16C954 at 0x%lx\n",ul_physaddr2uint(physbase));
      }
    }else
  #endif /* UL_WITH_PCI */
    {
      if((ret=u510_init(udrv, physbase, irq, baud, baudbase, chip_options))>=0){
        UL_PRINTF("uLan ul_drv_new : 82510 found at 0x%lx\n",ul_physaddr2uint(physbase));
      }else if((ret=u450_init(udrv, physbase, irq, baud, baudbase, chip_options))>=0){
        UL_PRINTF("uLan ul_drv_new : 16450 found at 0x%lx\n",ul_physaddr2uint(physbase));
      };
    }
  }
  if(ret>=0)
  { 
    return STATUS_SUCCESS;
  }
  UL_PRINTF("ul_drv_init_ext: failed to init\n");
  return STATUS_INSUFFICIENT_RESOURCES;
};

/* destroy driver */
void ul_drv_done_ext(ul_drv *udrv)
{
  if(!udrv) return;
  ul_drv_stop(udrv);
  ul_mem_done(udrv);
};


//-------------------------------------------------------------------
//
//  File operations
//

NTSTATUS ul_drv_open(ul_drv *udrv,ul_opdata *opdata)
{ 
  opdata->magic=ULOP_MAGIC;
  opdata->message=NULL;
  opdata->udrv=udrv;
  opdata->subdevidx=0;
  opdata->pro_mode=0;
  /* init_waitqueue_head(&opdata->wqrec); */
  opdata->opprew=NULL;
  opdata->opnext=NULL;
  opdata->recchain=NULL;
  opdata->filtchain=NULL;
  opdata->wait_irp=NULL;
  { /* add us onto list of clients of udrv */
    ul_opdata *opptr;
    UL_DRV_LOCK_FINI
    UL_DRV_LOCK;
    opptr=udrv->operators;
    if(opptr) {opptr->opprew=opdata;opdata->opnext=opptr;};
    UL_MB();
    udrv->operators=opdata;
    UL_DRV_UNLOCK;
  };

  return 0;
}

NTSTATUS ul_drv_close(ul_drv *udrv,ul_opdata *opdata)
{
  ul_opchain *opmember;
  KIRQL OldIrql;
  int rel_msg_cnt=0;

  if (opdata->message) ulan_freemsg(opdata);

  KeRaiseIrql(DISPATCH_LEVEL,&OldIrql);
  {while(InterlockedExchange(&udrv->flag_IN_BOTTOM,1))/* spinlock loop*/;}
  { 
    /* delete us from list of clients of udrv */
    ul_drv *udrv=opdata->udrv;
    ul_opdata *opptr;
    UL_DRV_LOCK_FINI
    UL_DRV_LOCK;
    if((opptr=opdata->opnext)) opptr->opprew=opdata->opprew;
    if((opptr=opdata->opprew)) opptr->opnext=opdata->opnext;
    else udrv->operators=opdata->opnext;
    UL_DRV_UNLOCK;
  };
  InterlockedExchange(&udrv->flag_IN_BOTTOM,0);
  ulan_bottom_dpc(NULL,udrv,NULL,NULL);
  KeLowerIrql(OldIrql);

  while((opmember=opdata->recchain))
  {
    del_from_opchain(&opdata->recchain,opmember);
    if(opmember->message) {
      ul_dec_ref_cnt(opmember->message);
      rel_msg_cnt++;
    }
    FREE(opmember);
  };
  while((opmember=opdata->filtchain))
  {
    del_from_opchain(&opdata->filtchain,opmember);
    FREE(opmember);
  }; 
  LOG_FILEIO("opan_bll : released messages = %d, frames = %d close\n",rel_msg_cnt, udrv->opan_bll.cnt);
  return 0;
};


NTSTATUS ulan_read(ul_drv *udrv,ul_opdata *opdata,
				   uchar *buf, ULONG count,
				   LARGE_INTEGER pos, ULONG_PTR *pcount)
{
  int cn;
  int len;

  if(!opdata->message) return -ENOMSG;

  if(opdata->data.pos+count>UL_BLK_HEAD(opdata->data.head_blk).len)
    count=UL_BLK_HEAD(opdata->data.head_blk).len-opdata->data.pos;

  cn=count;
  while(cn>0)
  {
    if(!ul_di_adjust(&opdata->data))
    {
      RtlZeroMemory(buf,cn);
      cn=0;
      break;
    };
    len=ul_di_atonce(&opdata->data);
    if(len>cn) len=cn;
    RtlCopyMemory(buf,ul_di_byte(&opdata->data),len);
    ul_di_add(&opdata->data,len);
    buf+=len;
    cn-=len;
  };

  *pcount=count-cn;

  return 0;
};


NTSTATUS ulan_write(ul_drv *udrv,ul_opdata *opdata,
				   const uchar *buf, ULONG count,
				   LARGE_INTEGER pos, ULONG_PTR *pcount)
{
  int cn;
  int len;
  ul_mem_blk *blk;

  if(!opdata->message) return -ENOMSG;

  cn=count;
  while(cn>0)
  {
    while(!ul_di_adjust(&opdata->data))
    {
      if(!(blk=ul_alloc_blk(opdata->udrv))) 
        {count-=cn;cn=0;break;};
      RtlZeroMemory(UL_BLK_NDATA(blk),UL_BLK_SIZE);
      opdata->data.blk->next=blk;
    };
    len=ul_di_atonce(&opdata->data);
    if(len>cn) len=cn;
    RtlCopyMemory(ul_di_byte(&opdata->data),buf,len);
    ul_di_add(&opdata->data,len);
    buf+=len;
    cn-=len;
  };

  if((unsigned)opdata->data.pos>UL_BLK_HEAD(opdata->data.head_blk).len)
   UL_BLK_HEAD(opdata->data.head_blk).len=opdata->data.pos;

  *pcount=count-cn;

  return 0;
};


//-------------------------------------------------------------------
//
//  deffered proccessing
//

/* notice about new message in udrv->proc_bll */
VOID NTAPI ulan_bottom_dpc(IN PKDPC Dpc,IN PVOID contex,
		     IN PVOID arg1,IN PVOID arg2)
{
  ul_mem_blk *message;
  ul_drv *udrv=(ul_drv *)contex;

  if(udrv->magic!=UL_DRV_MAGIC)
  { UL_PRINTF("ulan_bottom_dpc : BAD udrv magic !!!");
    return;
  }

  #ifdef ENABLE_UL_IRQ_STALE_WDG
  ul_irq_stale_wdg_cnt=0;
  #endif /* ENABLE_UL_IRQ_STALE_WDG */

  LOG_MESSAGES("ulan_bottom_dpc : announcing messages\n");
  while(udrv->proc_bll.first||uld_test_dfl(udrv,ASK_BOTTOM))
  { /*  more Dpc can be started simultaneously on NT SMP box */
    if(uld_test_and_set_dfl(udrv,IN_BOTTOM))
    { LOG_MESSAGES("ulan_bottom_dpc : else is already in");
	  return;
    }
    uld_clear_dfl(udrv,ASK_BOTTOM);
    if(uld_test_dfl(udrv,CHECK_FILT)){
      uld_clear_dfl(udrv,CHECK_FILT);
      check_for_filtnew(udrv);
    }
    while((message=udrv->proc_bll.first))
    {
      int i;
      ul_bll_move_mes(&udrv->opan_bll,message);
      i=ulan_proc_arrived(udrv,message);
      LOG_MESSAGES("ulan_bottom_dpc : message sent to %d recchains\n",i);
    };
    uld_atomic_clear_dfl(udrv,IN_BOTTOM);
  }
};

PIRP xch_pending_irp(PIRP *irp_ptr, PIRP new_irp)
{
  PIRP old_irp;
  UL_DRV_LOCK_FINI
  UL_DRV_LOCK;
  old_irp=*irp_ptr;
  *irp_ptr=new_irp;
  UL_DRV_UNLOCK;
  return old_irp;
}

void del_pending_irp(PIRP *irp_ptr, PIRP del_irp)
{
  PIRP old_irp;
  UL_DRV_LOCK_FINI
  UL_DRV_LOCK;
  if(del_irp==*irp_ptr) *irp_ptr=NULL;
  UL_DRV_UNLOCK;
}

void abort_pending_irp(PIRP *irp_ptr, NTSTATUS status)
{
  PIRP old_irp;
  KIRQL OldIrql;
  IoAcquireCancelSpinLock(&OldIrql);
  old_irp=xch_pending_irp(irp_ptr,NULL);
  if(old_irp!=NULL) 
    IoSetCancelRoutine(old_irp,NULL);
  IoReleaseCancelSpinLock(OldIrql);
  if(old_irp!=NULL){
    old_irp->IoStatus.Status=status;
    IoCompleteRequest(old_irp,IO_NO_INCREMENT);
  }
}

/*
  PDRIVER_CANCEL IoSetCancelRoutine(Irp,CancelRoutine);
*/
void ulan_cancel(IN PDEVICE_OBJECT DeviceObject, IN PIRP Irp)
{
  PIO_STACK_LOCATION  irpStack;
  ul_opdata	*opdata;
  PIRP old_irp;
  LOG_FILEIO("uLan: Irp Cancelled\n");
  irpStack = IoGetCurrentIrpStackLocation (Irp);
  opdata=irpStack->FileObject->FsContext;
  if(!opdata||(opdata->magic!=ULOP_MAGIC))
  { 
    uLan_DbgPrint ("uLan: corrupted opdata !!!!\n");
  }else {
    if(Irp==opdata->wait_irp)
      del_pending_irp(&opdata->wait_irp,Irp);
    IoReleaseCancelSpinLock(Irp->CancelIrql);
  }
  Irp->IoStatus.Information = 0;
  Irp->IoStatus.Status=STATUS_CANCELLED;
  IoCompleteRequest(Irp,IO_NO_INCREMENT);
}


//---------------------------------------------------------------------------
//
//
// Routine Description:
// 
//     Process the IRPs sent to this device.
// 
// Arguments:
// 
//     DeviceObject - pointer to a device object
// 
//     Irp          - pointer to an I/O Request Packet
// 
// Return Value:
// 
// 
NTSTATUS NTAPI DispatchRoutine (IN PDEVICE_OBJECT DeviceObject, IN PIRP Irp)
{
    PIO_STACK_LOCATION  irpStack;
    PULAN_DEVICE_EXTENSION   extension;
    PVOID               ioBuffer;
    ULONG               inputBufferLength;
    ULONG               outputBufferLength;
    ULONG               ioControlCode;
    NTSTATUS            ntStatus=0;
    KIRQL		OldIrql;

    LARGE_INTEGER       CurrentSystemTime;
    LARGE_INTEGER       ElapsedTime;
    ul_opdata			*opdata;
	ul_msginfo msginfo;

    
    Irp->IoStatus.Status      = STATUS_SUCCESS;
    Irp->IoStatus.Information = 0;

    //
    // Get a pointer to the current location in the Irp. This is where
    //     the function codes and parameters are located.
    //

    irpStack = IoGetCurrentIrpStackLocation (Irp);

    //
    // Get a pointer to the device extension
    //

    extension = DeviceObject->DeviceExtension;

  #ifdef FOR_WIN_WDM
    if(!extension||(extension->State<=STATE_ALL_BELOW_FAIL)||(!extension->flag_CHIPOK && !ulan_in_sleep_mode(extension))){
       uLan_DbgPrint ("uLan: DeviceObject initialization not finished\n");
	   Irp->IoStatus.Status=STATUS_UNSUCCESSFUL;
       IoCompleteRequest (Irp, IO_NO_INCREMENT);
	   return STATUS_UNSUCCESSFUL;
				
	}
  #endif /* FOR_WIN_WDM */

    //
    // Get the pointer to the input/output buffer and it's length
    //

    switch (irpStack->MajorFunction) {
        case IRP_MJ_CREATE:
        {    
            LOG_FILEIO("uLan: IRP_MJ_CREATE\n");
	    if(irpStack->FileObject==NULL)
	    {	uLan_DbgPrint ("uLan: Open called without FileObject\n");
		Irp->IoStatus.Status=STATUS_UNSUCCESSFUL;
		break;
	    }
	    if(irpStack->FileObject->FsContext)
	    {	uLan_DbgPrint ("uLan: FsContext already used\n");
		Irp->IoStatus.Status=STATUS_UNSUCCESSFUL;
		break;
	    }
            if(!(opdata=MALLOC(sizeof(ul_opdata))))
	    {	uLan_DbgPrint ("uLan: Cannot allocate op_data\n");
		Irp->IoStatus.Status=STATUS_NO_MEMORY;
		break;
	    }
	    irpStack->FileObject->FsContext=opdata;
            opdata->file=irpStack->FileObject;
	    if((ntStatus=ul_drv_open(extension,opdata)))
	    {	LOG_FILEIO ("uLan: ul_drv_open failed\n");
		Irp->IoStatus.Status=ntStatus;
		FREE(opdata);
	        irpStack->FileObject->FsContext=NULL;
		break;
	    }
            break;
        }

        case IRP_MJ_CLOSE:
        {
	    LOG_FILEIO("uLan: IRP_MJ_CLOSE\n");
	    opdata=irpStack->FileObject->FsContext;
	    if(!opdata||(opdata->magic!=ULOP_MAGIC))
	    {	uLan_DbgPrint ("uLan: corrupted opdata !!!!\n");
		Irp->IoStatus.Status=STATUS_UNSUCCESSFUL;
		break;
	    }
	    abort_pending_irp(&opdata->wait_irp,STATUS_UNSUCCESSFUL);
	    if((ntStatus=ul_drv_close(extension,opdata)))
	    {	LOG_FILEIO("uLan: ul_drv_close failed\n");
		Irp->IoStatus.Status=ntStatus;
	    }
	    irpStack->FileObject->FsContext=NULL;
	    FREE(opdata);
            break;
        }

        case IRP_MJ_READ:
        {
	    LOG_FILEIO("uLan: IRP_MJ_READ\n");
	    opdata=irpStack->FileObject->FsContext;
	    if(!opdata||(opdata->magic!=ULOP_MAGIC))
	    {	uLan_DbgPrint ("uLan: corrupted opdata !!!!\n");
		Irp->IoStatus.Status=STATUS_UNSUCCESSFUL;
		break;
	    }
	    ntStatus=ulan_read(extension,opdata,
				Irp->AssociatedIrp.SystemBuffer,
				irpStack->Parameters.Read.Length,
				irpStack->Parameters.Read.ByteOffset,
				&Irp->IoStatus.Information);
	    if(ntStatus)
	    {	LOG_FILEIO("uLan: ul_drv_read failed\n");
		Irp->IoStatus.Status=ntStatus;
	    }
            break;
        }

        case IRP_MJ_WRITE:
        {
	    LOG_FILEIO("uLan: IRP_MJ_WRITE\n");
	    opdata=irpStack->FileObject->FsContext;
	    if(!opdata||(opdata->magic!=ULOP_MAGIC))
	    {	uLan_DbgPrint ("uLan: corrupted opdata !!!!\n");
		Irp->IoStatus.Status=STATUS_UNSUCCESSFUL;
		break;
	    }
	    ntStatus=ulan_write(extension,opdata,
				Irp->AssociatedIrp.SystemBuffer,
				irpStack->Parameters.Read.Length,
				irpStack->Parameters.Read.ByteOffset,
				&Irp->IoStatus.Information);
	    if(ntStatus)
	    {	LOG_FILEIO("uLan: ul_drv_write failed\n");
		Irp->IoStatus.Status=ntStatus;
	    }
            break;
        }

        case IRP_MJ_CLEANUP:
        {
	    LOG_FILEIO("uLan: IRP_MJ_CLEANUP\n");
	    opdata=irpStack->FileObject->FsContext;
	    if(!opdata||(opdata->magic!=ULOP_MAGIC))
	    {	uLan_DbgPrint ("uLan: corrupted opdata !!!!\n");
		Irp->IoStatus.Status=STATUS_UNSUCCESSFUL;
		break;
	    }
	    abort_pending_irp(&opdata->wait_irp,STATUS_UNSUCCESSFUL);
            break;
        }

        case IRP_MJ_DEVICE_CONTROL:
        {
	    LOG_FILEIO("uLan: IRP_MJ_DEVICE_CONTROL - ");
	    opdata=irpStack->FileObject->FsContext;
	    if(!opdata||(opdata->magic!=ULOP_MAGIC))
	    {	uLan_DbgPrint ("uLan: corrupted opdata !!!!\n");
		Irp->IoStatus.Status=STATUS_UNSUCCESSFUL;
		break;
	    }
			    
            ioControlCode = irpStack->Parameters.DeviceIoControl.IoControlCode;
	    ioBuffer      = Irp->AssociatedIrp.SystemBuffer;
	    inputBufferLength  = irpStack->Parameters.DeviceIoControl.InputBufferLength;
	    outputBufferLength = irpStack->Parameters.DeviceIoControl.OutputBufferLength;

            switch (ioControlCode)
            {
		case UL_DRV_VER :
		    LOG_FILEIO("UL_DRV_VER\n");
                    if (outputBufferLength < sizeof(ULONG))
		    {
			Irp->IoStatus.Status = STATUS_UNSUCCESSFUL;
			break;
		    }
                    *(ULONG *)ioBuffer = UL_DRV_VERCODE;
                    Irp->IoStatus.Information = sizeof(ULONG);
		    break;
	    
		case UL_NEWMSG :
		    LOG_FILEIO("UL_NEWMSG\n");
                    if (inputBufferLength < sizeof(ul_msginfo)) 
		    {	LOG_FILEIO("uLan: newmsg shorten msginfo !!!!\n");
			Irp->IoStatus.Status = STATUS_UNSUCCESSFUL;
			break;
		    }
		    if((ntStatus=ulan_newmsg(opdata,ioBuffer)))
		    {	LOG_FILEIO("uLan: newmsg failed !!!!\n");
			Irp->IoStatus.Status = ntStatus;
		    }
		    break;
		case UL_TAILMSG :
		    LOG_FILEIO("UL_TAILMSG\n");
                    if (inputBufferLength < sizeof(ul_msginfo)) 
		    {	LOG_FILEIO("uLan: tailmsg shorten msginfo !!!!\n");
			Irp->IoStatus.Status = STATUS_UNSUCCESSFUL;
			break;
		    }
		    if((ntStatus=ulan_tailmsg(opdata,ioBuffer)))
		    {	LOG_FILEIO("uLan: tailmsg failed !!!!\n");
			Irp->IoStatus.Status = ntStatus;
		    }
		    break;
		case UL_FREEMSG :
		    LOG_FILEIO("UL_FREEMSG\n");
		    if((ntStatus=ulan_freemsg(opdata))&~0x7fffffff)
		    {	LOG_FILEIO("uLan: freemsg failed !!!!\n");
			Irp->IoStatus.Status = ntStatus;
		    }
                    if (outputBufferLength >= sizeof(ULONG)) {
                        *(ULONG *)ioBuffer = ntStatus;
                        Irp->IoStatus.Information = sizeof(ULONG);
                    }
					break;
		case UL_ACCEPTMSG :  	
		    LOG_FILEIO("UL_ACCEPTMSG\n");
                    if (outputBufferLength < sizeof(ul_msginfo)) 
		    {	LOG_FILEIO("uLan: acceptmsg shorten msginfo !!!!\n");
			Irp->IoStatus.Status = STATUS_UNSUCCESSFUL;
			break;
		    }
		    if((ntStatus=ulan_acceptmsg(opdata,ioBuffer)))
		    {	LOG_FILEIO("uLan: acceptmsg failed !!!!\n");
			Irp->IoStatus.Status = ntStatus;
		    }
		    else Irp->IoStatus.Information = sizeof(ul_msginfo);
		    break;
		case UL_ACTAILMSG :  	
		    LOG_FILEIO("UL_ACTAILMSG\n");
                    if (outputBufferLength < sizeof(ul_msginfo)) 
		    {	LOG_FILEIO("uLan: actailmsg shorten msginfo !!!!\n");
			Irp->IoStatus.Status = STATUS_UNSUCCESSFUL;
			break;
		    }
		    if((ntStatus=ulan_actailmsg(opdata,ioBuffer)))
		    {	LOG_FILEIO("uLan: actailmsg failed !!!!\n");
			Irp->IoStatus.Status = ntStatus;
		    }
		    else Irp->IoStatus.Information = sizeof(ul_msginfo);
		    break;
		case UL_ADDFILT :
		    LOG_FILEIO("UL_ADDFILT\n");
                    if (inputBufferLength < sizeof(ul_msginfo)) 
		    {	LOG_FILEIO("uLan: addfilt shorten msginfo !!!!\n");
			Irp->IoStatus.Status = STATUS_UNSUCCESSFUL;
			break;
		    }
		    if((ntStatus=ulan_addfilt(opdata,ioBuffer)))
		    {	LOG_FILEIO("uLan: addfilt failed !!!!\n");
			Irp->IoStatus.Status = ntStatus;
		    }
		    break;
		case UL_ABORTMSG :
		    LOG_FILEIO("UL_ABORTMSG\n");
		    if((ntStatus=ulan_abortmsg(opdata)))
		    {	LOG_FILEIO("uLan: abortmsg failed !!!!\n");
			Irp->IoStatus.Status = ntStatus;
		    }
		    break;
		case UL_REWMSG :
		    LOG_FILEIO("UL_REWMSG\n");
		    if((ntStatus=ulan_rewmsg(opdata)))
		    {	LOG_FILEIO("uLan: rewmsg failed !!!!\n");
			Irp->IoStatus.Status = ntStatus;
		    }
		    break;
		case UL_INEPOLL :
		    LOG_FILEIO("UL_INEPOLL\n");
                    if (outputBufferLength < sizeof(ULONG))
		    {
			Irp->IoStatus.Status = STATUS_UNSUCCESSFUL;
			break;
		    }
                    *(ULONG *)ioBuffer = ulan_inepoll(opdata);
                    Irp->IoStatus.Information = sizeof(ULONG);
		    break;
		case UL_WAITREC :
		    LOG_FILEIO("UL_WAITREC\n");
                    if(ulan_inepoll(opdata)) break;
		    {
			KIRQL OldIrql;
			IoAcquireCancelSpinLock(&OldIrql);
			if (Irp->Cancel) { 
			   Irp->IoStatus.Status = STATUS_CANCELLED; 
			  // IRP should be completed after releasing the 
			  // cancel spin lock.
			  IoReleaseCancelSpinLock(OldIrql);
			} else {
			  PIRP old_irp;
			  IoMarkIrpPending (Irp);  
			  IoSetCancelRoutine(Irp,&ulan_cancel);
	                  old_irp=xch_pending_irp(&opdata->wait_irp,Irp);
			  if(old_irp!=NULL){
			    IoSetCancelRoutine(old_irp,NULL);
			  }
			  IoReleaseCancelSpinLock(OldIrql);
			  if(old_irp!=NULL){
			    old_irp->IoStatus.Status=STATUS_UNSUCCESSFUL;
			    IoCompleteRequest(old_irp,IO_NO_INCREMENT);
			  }
			  return STATUS_PENDING;
		        }
                    }
		case UL_KLOGBLL :
		    LOG_FILEIO("UL_KLOGBLL\n");
		    printudrvbll(extension);
		    #ifdef ENABLE_UL_MEM_CHECK
		     printudrvoperators(extension);
		    #endif /* ENABLE_UL_MEM_CHECK */
		    break;
		case UL_STROKE :
		    LOG_FILEIO("UL_STROKE\n");
		    ulan_stroke(extension, 1);
		    break;
		case UL_DEBFLG :
		    LOG_FILEIO("UL_DEBFLG\n");
                    if (inputBufferLength >= sizeof(ULONG)) 
		    {	
			uld_debug_flg=*(ULONG *)ioBuffer;
			break;
		    }
		    break;
		case UL_HWTEST :
		    LOG_FILEIO("UL_HWTEST\n");
                    if ((outputBufferLength < sizeof(ULONG))||
                        (inputBufferLength < sizeof(ULONG)))
		    {	
			Irp->IoStatus.Status = STATUS_UNSUCCESSFUL;
			break;
		    }
                    *(LONG *)ioBuffer = ulan_hwtest(opdata,*(ULONG *)ioBuffer);
		    if(*(LONG *)ioBuffer<0)
		        Irp->IoStatus.Status = STATUS_UNSUCCESSFUL;
                    Irp->IoStatus.Information = sizeof(ULONG);
		    break;
		case UL_SETPROMODE :
		    LOG_FILEIO("UL_SETPROMODE\n");
                    if (inputBufferLength >= sizeof(ULONG)) 
		    {	
			ulan_setpromode(opdata,*(ULONG *)ioBuffer);
			break;
		    }
		    break;
		case UL_SETMYADR :
		    LOG_FILEIO("UL_SETMYADR\n");
                    if (inputBufferLength >= sizeof(ULONG)) 
		    {	
			if(ulan_setmyadr(opdata,*(ULONG *)ioBuffer) >= 0)
			    break;
		    }
		    Irp->IoStatus.Status = STATUS_UNSUCCESSFUL;
		    break;
	    #ifdef UL_WITH_IAC
		case UL_SETIDSTR :
		    LOG_FILEIO("UL_SETIDSTR\n");
		    if (inputBufferLength > 0) {
			int ln=0;
			char idstr[41];
			const char *p = (char*)ioBuffer;
			while(*p && (ln < sizeof(idstr)-1))
				idstr[ln++] = *(p++);
			idstr[ln] = 0;
			if(ulan_setidstr(opdata,idstr) >= 0)
			    break;
		    }
		    Irp->IoStatus.Status = STATUS_UNSUCCESSFUL;
		    break;
	    #endif /*UL_WITH_IAC*/
	    #ifdef CONFIG_OC_UL_DRV_WITH_MULTI_DEV
		case UL_SETSUBDEV :
		    LOG_FILEIO("UL_SETSUBDEV\n");
                    if (inputBufferLength >= sizeof(ULONG)) 
		    {	
			if(ulan_setsubdev(opdata,*(ULONG *)ioBuffer) >= 0)
			    break;
		    }
		    Irp->IoStatus.Status = STATUS_UNSUCCESSFUL;
		    break;
	    #endif /*CONFIG_OC_UL_DRV_WITH_MULTI_DEV*/
		case UL_QUERYPARAM :
		    LOG_FILEIO("UL_QUERYPARAM\n");
                    if ((outputBufferLength < sizeof(ULONG))||
                        (inputBufferLength < sizeof(ULONG)))
		    {	
			Irp->IoStatus.Status = STATUS_UNSUCCESSFUL;
			break;
		    }
  		    if(ulan_queryparam(opdata,*(ULONG *)ioBuffer,(ULONG *)ioBuffer) < 0)
 		      Irp->IoStatus.Status = STATUS_UNSUCCESSFUL;
	            else
                      Irp->IoStatus.Information = sizeof(ULONG);
		    break;
	    #ifdef CONFIG_OC_UL_DRV_WITH_MULTI_NET
		case UL_ROUTE :
		    LOG_FILEIO("UL_ROUTE\n");
                    if ((inputBufferLength >= sizeof(ul_route_range_t)) && (outputBufferLength >= sizeof(ul_route_range_t)))
		    {
			if(ulan_route(opdata,ioBuffer) >= 0) {
                            Irp->IoStatus.Information = sizeof(ul_route_range_t);
			    break;
                        }
		    }
		    Irp->IoStatus.Status = STATUS_UNSUCCESSFUL;
		    break;
	    #endif /*CONFIG_OC_UL_DRV_WITH_MULTI_NET*/
                default:
                {
                    LOG_FILEIO("uLan: Unknown IRP_MJ_DEVICE_CONTROL\n");
                    Irp->IoStatus.Status = STATUS_INVALID_PARAMETER;
                    break;
                }

            }
    
            break;
        }
        default:
        {
            uLan_DbgPrint ("uLan: Unhandled IRP_MJ function\n");
            Irp->IoStatus.Status = STATUS_NOT_IMPLEMENTED;
            break;
        }
    }

    //
    // DON'T get cute and try to use the status field of
    // the irp in the return status.  That IRP IS GONE as
    // soon as you call IoCompleteRequest.
    //

    ntStatus = Irp->IoStatus.Status;

    IoCompleteRequest (Irp, IO_NO_INCREMENT);

    LOG_FILEIO("uLan: DisptachRoutine exit.\n");

    //
    // We never have pending operation so always return the status code.
    //

    return ntStatus;
}


/*** IO mapping functions for kernel module ***/

int ul_io_map(ul_physaddr_t physaddr, unsigned long len, const char *name, ul_ioaddr_t *ioaddr)
{
  if ((ul_physaddr2uint(physaddr)&~UL_PHYSADDR_PIO_MASK) == 0) {
    ioaddr->ioaddr=(PVOID)physaddr.physaddr.QuadPart;
    return 0;
  } else {
    ioaddr->ioaddr=MmMapIoSpace(physaddr.physaddr,len,MmNonCached);
    if (ioaddr->ioaddr==NULL)
      return -1;
    return 0;
  }
}

void ul_io_unmap(ul_physaddr_t physaddr, unsigned long len, ul_ioaddr_t ioaddr)
{
  if ((ul_physaddr2uint(physaddr)&~UL_PHYSADDR_PIO_MASK) == 0) {
    return;
  } else {
    MmUnmapIoSpace(ioaddr.ioaddr, len);
  }
}
