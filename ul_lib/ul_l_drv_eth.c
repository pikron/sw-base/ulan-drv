/*******************************************************************
  uLan Communication - low level C and driver interface library

  ul_l_drv_eth.c     - ethernet driver interface

  (C) Copyright 1996-2016 by Pavel Pisa - project originator
        http://cmp.felk.cvut.cz/~pisa
  (C) Copyright 1996-2016 PiKRON Ltd.
        http://www.pikron.com
  (C) Copyright 2002-2016 Petr Smolik


  The uLan driver project can be used and distributed
  in compliance with any of next licenses
   - GPL - GNU Public License
     See file COPYING for details.
   - LGPL - Lesser GNU Public License
   - MPL - Mozilla Public License
   - and other licenses added by project originator

  Code can be modified and re-distributed under any combination
  of the above listed licenses. If contributor does not agree with
  some of the licenses, he/she can delete appropriate line.
  WARNING: if you delete all lines, you are not allowed to
  distribute code or sources in any form.
 *******************************************************************/

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#ifndef _WIN32
#include <unistd.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <signal.h>
#define BAD_SOCKET(s) ((s) < 0)
#define UL_SOCKET_INVALID -1
#define SLEEP(x) sleep(x)
#else
#include <winsock.h>
#include <fcntl.h>
#define BAD_SOCKET(s) ((s) == INVALID_SOCKET)
#define UL_SOCKET_INVALID INVALID_SOCKET
#define SLEEP(x) Sleep(x*1000)
#endif
#include <ul_gavlcust.h>
#include <ul_msg_buf.h>
#include <ul_l_drv_eth.h>
#include <ul_list.h>

#ifdef CONFIG_OC_ULUT
#include <ul_log.h>
extern ul_log_domain_t ulogd_drv_eth;
#define UL_LDOMAIN (&ulogd_drv_eth)
#else
#include <ul_lib/ul_l_log.h>
#define UL_LDOMAIN NULL
#endif

#ifdef _WIN32
 typedef SOCKET ul_fd_sock_t;
#else /*_WIN32*/
 typedef int ul_fd_sock_t;
 #define closesocket(s)  close(s)
#endif /*_WIN32*/

typedef struct msgbuff_inproc_item {
  gavl_node_t node;
  ul_msg_buf_t *msgbuff;
  unsigned stamp;
} msgbuff_inproc_item_t;

typedef struct msgbuff_inproc_root {
  gavl_cust_root_field_t root;
} msgbuff_inproc_root_t; 

typedef unsigned msgbuff_inproc_key_t; 

GAVL_CUST_NODE_INT_DEC(msgbuff_inproc, msgbuff_inproc_root_t, msgbuff_inproc_item_t, msgbuff_inproc_key_t,
	root, node, stamp, msgbuff_inproc_cmp_fnc) 

inline int
msgbuff_inproc_cmp_fnc(const msgbuff_inproc_key_t *a, const msgbuff_inproc_key_t *b)
{
  if (*a>*b) return 1;
  if (*a<*b) return -1;
  return 0;
}

GAVL_CUST_NODE_INT_IMP(msgbuff_inproc, msgbuff_inproc_root_t, msgbuff_inproc_item_t, msgbuff_inproc_key_t,
	root, node, stamp, msgbuff_inproc_cmp_fnc) 

typedef struct filter_inproc_item {
  ul_list_node_t node;
  ul_msginfo msginfo;
} filter_inproc_item_t;

typedef struct ul_fd_eth_context_t {
  ul_fd_context_t context;
  ul_fd_sock_t sock_fd;
  FILE *sock_wfile;
  uint32_t ipaddress;
  uint16_t port;
  ul_dbuff_t dev_name;
  ul_dbuff_t rcvbuff;
  ul_msg_buf_t *msgbuff_start;
  ul_msg_buf_t *msgbuff_proc;
  int msgbuff_idx;
  int msg_received_flg;
  unsigned stamp_eth_cnt;
  msgbuff_inproc_root_t msgbuff_inproc_root;  
  ul_list_head_t filter_inproc_root;
} ul_fd_eth_context_t;

UL_LIST_CUST_DEC(filter_inproc,
                 ul_fd_eth_context_t,filter_inproc_item_t,
                 filter_inproc_root,node);

/* forward declarations */
int ul_eth_close(ul_fd_t ul_fd);
int ul_eth_recv_responce_int(ul_fd_eth_context_t *eth_fd,const char *cmd,int sn,int *v);

static inline
ul_fd_eth_context_t *ul_fd2eth_context(ul_fd_t ul_fd)
{
  return UL_CONTAINEROF(ul_fd, ul_fd_eth_context_t, context);
}

static inline
ul_fd_sock_t ulop_eth_fd2sock_fd(ul_fd_t ul_fd)
{
  if(ul_fd==UL_FD_INVALID)
    return UL_SOCKET_INVALID;
  else
    return ul_fd2eth_context(ul_fd)->sock_fd;
}

ul_fd_direct_t ulop_eth_fd2sys_fd(ul_fd_t ul_fd)
{
  /*
   * This is not fully correct on Windows, there does not exist handle
   * type usable for both files and sockets
   */
  if(ul_fd==UL_FD_INVALID)
    return UL_FD_DIRECT_INVALID;
  else
    return ulop_eth_fd2sock_fd(ul_fd);
}

unsigned ul_eth_gen_stamp(ul_fd_eth_context_t *eth_fd)
{
  eth_fd->stamp_eth_cnt++;
  eth_fd->stamp_eth_cnt&=0x7fffffff;
  if(!eth_fd->stamp_eth_cnt)eth_fd->stamp_eth_cnt++;
  return eth_fd->stamp_eth_cnt; 
};

ul_fd_sock_t ul_eth_sock_init(uint32_t net_ipaddr,uint16_t net_port) 
{
  struct sockaddr_in name;
  ul_fd_sock_t fd;
  #ifdef _WIN32
  WORD wVersionRequested;
  WSADATA wsaData;
  int sockopt = SO_SYNCHRONOUS_NONALERT;
  #endif
    
  #ifdef _WIN32
    wVersionRequested = MAKEWORD(2, 0);
    WSAStartup(wVersionRequested, &wsaData);
    setsockopt(INVALID_SOCKET, SOL_SOCKET, SO_OPENTYPE, (char *)&sockopt, sizeof(sockopt));   
  #endif

  #ifndef _WIN32
    signal(SIGPIPE, SIG_IGN);
  #endif

  if (BAD_SOCKET(fd=socket(AF_INET, SOCK_STREAM, 0))) 
    return UL_SOCKET_INVALID;

  name.sin_family=AF_INET;
  name.sin_addr.s_addr=net_ipaddr;
  name.sin_port=net_port;
  if (BAD_SOCKET(connect(fd, (struct sockaddr *) &name, sizeof(name)))) {
    closesocket(fd);
    return UL_SOCKET_INVALID;
  }

  return fd;
}

int
ul_eth_sock_init_file(ul_fd_eth_context_t *eth_fd)
{
 int f;
 #ifndef _WIN32
  f=eth_fd->sock_fd;
 #else
  f=_open_osfhandle(eth_fd->sock_fd,0);
  if (f<0) 
    return -1;
 #endif /* _WIN32 */
  eth_fd->sock_wfile=fdopen(f,"w");
  if (!eth_fd->sock_wfile) 
    return -1;
  return 0;
}

int 
ul_eth_sock_close(ul_fd_eth_context_t *eth_fd)
{
  if (eth_fd->sock_wfile) 
    fclose(eth_fd->sock_wfile);
  eth_fd->sock_wfile=0;

  if (eth_fd->sock_fd!=UL_SOCKET_INVALID)
    closesocket(eth_fd->sock_fd);
  eth_fd->sock_fd=UL_SOCKET_INVALID;
  return 0;
}

int
ul_eth_sock_reinit(ul_fd_eth_context_t *eth_fd)
{
  int i,v;
  unsigned eth_stamp;
  filter_inproc_item_t *fitem;

  if (eth_fd->sock_fd!=UL_SOCKET_INVALID)
    return 0;

  eth_fd->sock_fd=ul_eth_sock_init(eth_fd->ipaddress,eth_fd->port);
  if (eth_fd->sock_fd==UL_SOCKET_INVALID)
    return -1;

  if (ul_eth_sock_init_file(eth_fd)) 
    return -1;

  /* send command to server request */
  eth_stamp=ul_eth_gen_stamp(eth_fd);
  fprintf(eth_fd->sock_wfile,"usc %d open %s\n",eth_stamp,eth_fd->dev_name.data);
  fflush(eth_fd->sock_wfile);
  /* wait for responce */
  i=ul_eth_recv_responce_int(eth_fd,"open",eth_stamp,&v);
  if ((i!=1) || (v==0)) {
    ul_eth_sock_close(eth_fd);
    return -1;
  }
  /* create filters */
  ul_list_for_each(filter_inproc,eth_fd,fitem) {
    ul_msginfo *msginfo=&fitem->msginfo;
    eth_stamp=ul_eth_gen_stamp(eth_fd);
    fprintf(eth_fd->sock_wfile,"usc %d addfilt %02x%02x%02x\n",eth_stamp,msginfo->sadr,msginfo->dadr,msginfo->cmd);
    fflush(eth_fd->sock_wfile);
    /* wait for responce */
    if (ul_eth_recv_responce_int(eth_fd,"addfilt",eth_stamp,&v)!=1) {
      ul_eth_sock_close(eth_fd);
      return -1;
    }
  }
  return 0;
}

static inline int
ul_eth_sock_check(ul_fd_eth_context_t *eth_fd)
{
  return ul_eth_sock_reinit(eth_fd);
}

int
ul_eth_recv_responce_int(ul_fd_eth_context_t *eth_fd,const char *cmd,int sn,int *v)
{
  char buff[64];
  int r,t=0;
  
  r=ul_eth_recv_msg(eth_fd->sock_fd,&eth_fd->rcvbuff,UL_ETH_SOCK_TIMEOUT,&t);
  if (r<0) {
    if (r==-1)
      ul_eth_sock_close(eth_fd);
    return r;
  }
  sprintf(buff,"usr %d %s %%d",sn,cmd);
  return sscanf((char*)eth_fd->rcvbuff.data,buff,v);
}

int
ul_eth_recv_responce_msgbuf(ul_fd_eth_context_t *eth_fd,const char *cmd,int sn,ul_msg_buf_t *rdbuf)
{
  char *msg_header,*msg_sn,*msg_cmd,*msg_data;
  int msg_data_len,i,r,t=0;

  r=ul_eth_recv_msg(eth_fd->sock_fd,&eth_fd->rcvbuff,UL_ETH_SOCK_TIMEOUT,&t);
  if (r<0) {
    if (r==-1)
      ul_eth_sock_close(eth_fd);
    return r;
  }
  
  msg_header = strtok ((char*)eth_fd->rcvbuff.data," ");
  if (msg_header!=NULL) {
    msg_sn = strtok (NULL," ");
    if (msg_sn!=NULL) {
      msg_cmd = strtok (NULL," ");
      if (msg_cmd!=NULL) {
        msg_data = strtok (NULL," ");
        msg_data_len=0;
        if (msg_data!=NULL) 
          msg_data_len=eth_fd->rcvbuff.len-(msg_data-(char*)eth_fd->rcvbuff.data)-1;
        if (strcmp(cmd,msg_cmd)!=0) 
          return -1;
        i=strtol(msg_sn,NULL,0);
        if (sn!=i)
          return -1;
        return ul_eth_demarshal_stream(rdbuf,msg_data,msg_data_len);
      }
    }
  }
  return -1;
}

ul_fd_ops_t ul_fd_eth_ops;

static inline ul_fd_t ul_fd_direct2ul_fd(ul_fd_sock_t fd)
{
  ul_fd_eth_context_t *eth_fd;
  if (fd==UL_SOCKET_INVALID) 
    return UL_FD_INVALID;   
  eth_fd=malloc(sizeof(*eth_fd));
  if(!eth_fd)
    return UL_FD_INVALID;
  memset(eth_fd,0,sizeof(*eth_fd));
  eth_fd->sock_fd=fd;
  eth_fd->context.fd_ops=&ul_fd_eth_ops;
  ul_dbuff_init(&eth_fd->rcvbuff, 0);
  ul_dbuff_init(&eth_fd->dev_name, 0);
  filter_inproc_init_head(eth_fd);
  msgbuff_inproc_init_root_field(&eth_fd->msgbuff_inproc_root);
  return &eth_fd->context;
}

ul_fd_t ul_eth_open(const char *dev_name, const char *options)
{
  ul_fd_eth_context_t *eth_fd;
  ul_fd_t r;
  char buff[128],*d;
  uint32_t eth_ipaddr;
  uint16_t eth_port;
  int i,v,l;
  unsigned eth_stamp;
  ul_fd_sock_t sock_fd;

  if(dev_name==NULL) dev_name=UL_DEV_NAME;
  else if(!strncmp(dev_name,"eth:",4)) dev_name+=4;
  d=strpbrk(dev_name,":");
  if (!d)
    return UL_FD_INVALID;
  memset(buff,0,sizeof(buff));
  strncpy(buff,dev_name,d-dev_name);
  eth_ipaddr=inet_addr(buff);
  dev_name=d+1;
  d=strpbrk(dev_name,":");
  if (!d)
    return UL_FD_INVALID;

  memset(buff,0,sizeof(buff));
  strncpy(buff,dev_name,d-dev_name);
  eth_port=htons(strtol(buff,NULL,0));
  dev_name=d+1;

  sock_fd=ul_eth_sock_init(eth_ipaddr,eth_port);
  r=ul_fd_direct2ul_fd(sock_fd);
  if (r==UL_FD_INVALID) 
    return UL_FD_INVALID;

  eth_fd=ul_fd2eth_context(r);
  eth_fd->ipaddress=eth_ipaddr;
  eth_fd->port=eth_port;
  if (ul_eth_sock_init_file(eth_fd)) {
    ul_eth_close(r);
    return UL_FD_INVALID;
  }

  l=strlen(dev_name)+1;
  if(ul_dbuff_set_len(&eth_fd->dev_name, l) != l) {
    ul_eth_close(r);
    return UL_FD_INVALID;
  }
  strncpy((char*)eth_fd->dev_name.data,dev_name,l);

  /* send command to server request */
  eth_stamp=ul_eth_gen_stamp(eth_fd);
  fprintf(eth_fd->sock_wfile,"usc %d open %s\n",eth_stamp,eth_fd->dev_name.data);
  fflush(eth_fd->sock_wfile);
  /* wait for responce */
  i=ul_eth_recv_responce_int(eth_fd,"open",eth_stamp,&v);
  if ((i!=1) || (v==0)) {
    ul_eth_close(r);
    return UL_FD_INVALID;
  }
  return r;
}

int ul_eth_close(ul_fd_t ul_fd)
{
  ul_fd_eth_context_t *eth_fd;
  msgbuff_inproc_item_t *mitem;
  filter_inproc_item_t *fitem;
  unsigned eth_stamp;
  int r;

  if(ul_fd==UL_FD_INVALID)
    return -1;

  eth_fd=ul_fd2eth_context(ul_fd);

  if (eth_fd->sock_fd!=UL_SOCKET_INVALID) {
    eth_stamp=ul_eth_gen_stamp(eth_fd);
    fprintf(eth_fd->sock_wfile,"usc %d close\n",eth_stamp);
    fflush(eth_fd->sock_wfile);
    /* wait for responce */
    r=-1;
    ul_eth_recv_responce_int(eth_fd,"close",eth_stamp,&r);
  } else
    r=0;

  ul_dbuff_destroy(&eth_fd->rcvbuff);
  ul_dbuff_destroy(&eth_fd->dev_name);
  if (eth_fd->msgbuff_start) {
    mitem=msgbuff_inproc_find(&eth_fd->msgbuff_inproc_root,&eth_fd->msgbuff_start->msginfo.stamp);
    if (!mitem) {
      ul_msg_buf_destroy(eth_fd->msgbuff_start);
      free(eth_fd->msgbuff_start);
    }
  }
  while((fitem=filter_inproc_cut_first(eth_fd))!=NULL) {
    free(fitem);
  }
  while((mitem=msgbuff_inproc_cut_first(&eth_fd->msgbuff_inproc_root))!=NULL) {
    ul_msg_buf_destroy(mitem->msgbuff);
    free(mitem);
  }
 
  if (eth_fd->sock_wfile) {
    fclose(eth_fd->sock_wfile);
  }

  closesocket(eth_fd->sock_fd);

  free(eth_fd);

  return r;
}

int ul_eth_drv_version(ul_fd_t ul_fd)
{
  ul_fd_eth_context_t *eth_fd;
  unsigned eth_stamp;
  int v;

  eth_fd=ul_fd2eth_context(ul_fd);
  if (ul_eth_sock_check(eth_fd)<0)
    return -1;
  eth_stamp=ul_eth_gen_stamp(eth_fd);
  fprintf(eth_fd->sock_wfile,"usc %d drv_version\n",eth_stamp);
  fflush(eth_fd->sock_wfile);
  /* wait for responce */
  if (ul_eth_recv_responce_int(eth_fd,"drv_version",eth_stamp,&v)!=1) 
    return -1;
  return v;
}


ssize_t ul_eth_read(ul_fd_t ul_fd, void *buffer, size_t size)
{
  ul_fd_eth_context_t *eth_fd;
  ssize_t s;

  eth_fd=ul_fd2eth_context(ul_fd);
  if (!eth_fd->msgbuff_proc) 
    return -1;
  
  s=size;
  if ((eth_fd->msgbuff_idx+size)>eth_fd->msgbuff_proc->msginfo.len)
    s=eth_fd->msgbuff_proc->msginfo.len-eth_fd->msgbuff_idx;
 
  memcpy(buffer,eth_fd->msgbuff_proc->data.data + eth_fd->msgbuff_idx,s);
  eth_fd->msgbuff_idx+=s;
  return s;
}

ssize_t ul_eth_write(ul_fd_t ul_fd, const void *buffer, size_t size)
{
  ul_fd_eth_context_t *eth_fd;
  ul_dbuff_t *msg_data;
  int new_len;

  eth_fd=ul_fd2eth_context(ul_fd);
  if (!eth_fd->msgbuff_proc) 
    return -1;

  msg_data=&eth_fd->msgbuff_proc->data;
  new_len = msg_data->len + size;
  if(ul_dbuff_set_len(msg_data, new_len) != new_len) 
    return -1;
  memcpy(msg_data->data + eth_fd->msgbuff_idx,buffer,size);
  eth_fd->msgbuff_idx+=size;
  eth_fd->msgbuff_proc->msginfo.len+=size;
  return size;
}

int ul_eth_newmsg(ul_fd_t ul_fd,const ul_msginfo *msginfo)
{
  ul_fd_eth_context_t *eth_fd;
  ul_msg_buf_t *msgbuff;
  int flg;

  eth_fd=ul_fd2eth_context(ul_fd);
  if (ul_eth_sock_check(eth_fd)<0)
    return -1;
  if (eth_fd->msgbuff_proc) {
    ul_msg_buf_destroy(eth_fd->msgbuff_proc);
    free(eth_fd->msgbuff_proc);
  }
  msgbuff=malloc(sizeof(ul_msg_buf_t));
  if (!msgbuff)
    return -1;
  ul_msg_buf_init(msgbuff);
  eth_fd->msgbuff_start=msgbuff;
  eth_fd->msgbuff_proc=eth_fd->msgbuff_start;
  eth_fd->msgbuff_proc->msginfo=*msginfo;
  flg=msginfo->flg;
  flg &= ~(UL_BFL_LOCK | UL_BFL_FAIL | UL_BFL_TAIL | UL_BFL_REC);
  flg |= UL_BFL_SND;
  eth_fd->msgbuff_proc->msginfo.flg=flg;
  eth_fd->msgbuff_proc->msginfo.len=0;
  eth_fd->msgbuff_idx=0;
  return 0;
}

int ul_eth_tailmsg(ul_fd_t ul_fd,const ul_msginfo *msginfo)
{
  ul_fd_eth_context_t *eth_fd;
  ul_msg_buf_t *tail;

  eth_fd=ul_fd2eth_context(ul_fd);
  if (!eth_fd->msgbuff_proc) 
    return -1;

  if(!(tail=malloc(sizeof(ul_msg_buf_t))))
    return -1;
  ul_msg_buf_init(tail);
  eth_fd->msgbuff_proc->tail=tail;
  eth_fd->msgbuff_proc=tail;

  eth_fd->msgbuff_proc->msginfo=*msginfo;
  eth_fd->msgbuff_start->msginfo.flg &= ~(UL_BFL_LOCK | UL_BFL_FAIL | UL_BFL_REC);
  if(!(eth_fd->msgbuff_start->msginfo.flg&UL_BFL_REC)) 
    eth_fd->msgbuff_start->msginfo.flg|=UL_BFL_SND;
  eth_fd->msgbuff_start->msginfo.flg|=UL_BFL_TAIL;
  eth_fd->msgbuff_idx=0;
  return 0;
}

int ul_eth_freemsg(ul_fd_t ul_fd)
{
  ul_fd_eth_context_t *eth_fd;
  int r1,r2,r=0;
  msgbuff_inproc_item_t *item;

  eth_fd=ul_fd2eth_context(ul_fd);
  if (!eth_fd->msgbuff_proc) 
    return -1;

  if (!eth_fd->msg_received_flg) {
    unsigned eth_stamp=ul_eth_gen_stamp(eth_fd);
    /* send */
    fprintf(eth_fd->sock_wfile,"usc %d send ",eth_stamp);
    r1=ul_eth_send_msg(eth_fd->sock_wfile,eth_fd->msgbuff_start);
    fprintf(eth_fd->sock_wfile,"\n");
    fflush(eth_fd->sock_wfile);
    /* wait for responce */
    r2=ul_eth_recv_responce_int(eth_fd,"send",eth_stamp,&r); 
    if ((r1<0) || (r2!=1) || (r<0)) {
      r=-1;
    } else {
      /* copy message to inproc queue in case flag UL_BFL_M2IN */
      if (eth_fd->msgbuff_start->msginfo.flg&UL_BFL_M2IN) {
        msgbuff_inproc_item_t *item;
        item=malloc(sizeof(msgbuff_inproc_item_t));
        if (item) { 
         item->msgbuff=eth_fd->msgbuff_start;
         item->stamp=r;
         msgbuff_inproc_insert(&eth_fd->msgbuff_inproc_root,item);
         eth_fd->msgbuff_start=NULL;
         eth_fd->msgbuff_proc=NULL;
         eth_fd->msgbuff_idx=0;
         eth_fd->msg_received_flg=0;
         return r;
        }
      }
    }
  } 

  /* check if this message was in inproc queue */
  if (eth_fd->msg_received_flg) {
    item=msgbuff_inproc_find(&eth_fd->msgbuff_inproc_root,&eth_fd->msgbuff_start->msginfo.stamp);
    if (item) {
      msgbuff_inproc_delete(&eth_fd->msgbuff_inproc_root,item);
      free(item);
    }
  }

  /* destroy proc message */
  ul_msg_buf_destroy(eth_fd->msgbuff_start);
  free(eth_fd->msgbuff_start);
  eth_fd->msgbuff_start=NULL;
  eth_fd->msgbuff_proc=NULL;
  eth_fd->msgbuff_idx=0;
  eth_fd->msg_received_flg=0;
  return r;
}

int ul_eth_acceptmsg(ul_fd_t ul_fd,ul_msginfo *msginfo)
{
  ul_fd_eth_context_t *eth_fd;
  msgbuff_inproc_item_t *item;
  ul_msg_buf_t *msgbuff,*tail;
  unsigned eth_stamp;

  eth_fd=ul_fd2eth_context(ul_fd);
  if (ul_eth_sock_check(eth_fd)<0) {
    /* check if some message is inproc queue */
    if (!msgbuff_inproc_is_empty(&eth_fd->msgbuff_inproc_root)) {
       item=msgbuff_inproc_first(&eth_fd->msgbuff_inproc_root);
       eth_fd->msgbuff_start=item->msgbuff;
       eth_fd->msgbuff_proc=eth_fd->msgbuff_start;
       eth_fd->msgbuff_proc->msginfo.flg|=UL_BFL_FAIL;
       eth_fd->msgbuff_proc->msginfo.len=0;
       eth_fd->msgbuff_proc->msginfo.stamp=item->stamp;
       ul_dbuff_destroy(&eth_fd->msgbuff_proc->data);
       tail=eth_fd->msgbuff_start->tail;
       while(tail) {  
         tail->msginfo.len=0;
         ul_dbuff_destroy(&tail->data);
         tail=tail->tail;
       }
       eth_fd->msgbuff_idx=0;
       eth_fd->msg_received_flg=1;
       if (msginfo)
         *msginfo=eth_fd->msgbuff_proc->msginfo;
       return 0;
    }
    return -1;
  }
  if (eth_fd->msgbuff_proc) {
    eth_fd->msgbuff_proc=eth_fd->msgbuff_start;
    eth_fd->msgbuff_idx=0;
    eth_fd->msg_received_flg=1;
    if (msginfo)
      *msginfo=eth_fd->msgbuff_proc->msginfo;
    return 0;
  }

  msgbuff=malloc(sizeof(ul_msg_buf_t));
  if (!msgbuff)
    return -1;
  ul_msg_buf_init(msgbuff);

  eth_stamp=ul_eth_gen_stamp(eth_fd);
  fprintf(eth_fd->sock_wfile,"usc %d recv\n",eth_stamp);
  fflush(eth_fd->sock_wfile);

  /* wait for responce */
  if (ul_eth_recv_responce_msgbuf(eth_fd,"recv",eth_stamp,msgbuff)!=1) 
    return -1;

  /* a new messsage is ready */
  eth_fd->msgbuff_start=msgbuff;
  eth_fd->msgbuff_proc=eth_fd->msgbuff_start;
  eth_fd->msgbuff_idx=0;
  eth_fd->msg_received_flg=1;
  if (msginfo)
    *msginfo=eth_fd->msgbuff_proc->msginfo;
  return 0;
}

int ul_eth_actailmsg(ul_fd_t ul_fd,ul_msginfo *msginfo)
{
  ul_fd_eth_context_t *eth_fd;

  eth_fd=ul_fd2eth_context(ul_fd);
  if (!eth_fd->msgbuff_proc) 
    return -1;
  if (!eth_fd->msgbuff_proc->tail)
    return -1;

  eth_fd->msgbuff_proc=eth_fd->msgbuff_proc->tail;
  eth_fd->msgbuff_idx=0;
  *msginfo=eth_fd->msgbuff_proc->msginfo;
  return 0;
}

int ul_eth_addfilt(ul_fd_t ul_fd,const ul_msginfo *msginfo)
{
  ul_fd_eth_context_t *eth_fd;
  filter_inproc_item_t *fitem;
  unsigned eth_stamp;
  int v;

  eth_fd=ul_fd2eth_context(ul_fd);
  if (ul_eth_sock_check(eth_fd)<0)
    return -1;
  fitem=malloc(sizeof(filter_inproc_item_t));
  if (!fitem)
    return -1;
  eth_stamp=ul_eth_gen_stamp(eth_fd);
  fprintf(eth_fd->sock_wfile,"usc %d addfilt %02x%02x%02x\n",eth_stamp,msginfo->sadr,msginfo->dadr,msginfo->cmd);
  fflush(eth_fd->sock_wfile);
  /* wait for responce */
  if (ul_eth_recv_responce_int(eth_fd,"addfilt",eth_stamp,&v)!=1) {
    free(fitem);
    return -1;
  }
  /* insert in filter queue */
  fitem->msginfo=*msginfo;
  filter_inproc_insert(eth_fd,fitem);
  return v;
}

int ul_eth_abortmsg(ul_fd_t ul_fd)
{
  ul_fd_eth_context_t *eth_fd;

  eth_fd=ul_fd2eth_context(ul_fd);
  if (!eth_fd->msgbuff_proc) 
    return -1;

  ul_msg_buf_destroy(eth_fd->msgbuff_start);
  free(eth_fd->msgbuff_start);
  eth_fd->msgbuff_start=NULL;
  eth_fd->msgbuff_proc=NULL;
  eth_fd->msgbuff_idx=0;
  return 0;
}

int ul_eth_rewmsg(ul_fd_t ul_fd)
{
  ul_fd_eth_context_t *eth_fd;

  eth_fd=ul_fd2eth_context(ul_fd);
  if (!eth_fd->msgbuff_proc) 
    return -1;

  eth_fd->msgbuff_idx=0;
  return 0;
}

int ul_eth_inepoll(ul_fd_t ul_fd)
{
  return ul_eth_acceptmsg(ul_fd,NULL)==0?1:0;
}

int ul_eth_debflg(ul_fd_t ul_fd,int debug_msk)
{
  return 0;
}

int ul_eth_setmyadr(ul_fd_t ul_fd, int newadr)
{
  return 0;
}

int ul_eth_setidstr(ul_fd_t ul_fd, const char *idstr)
{
  return 0;
}

int ul_eth_setbaudrate(ul_fd_t ul_fd, int baudrate)
{
  return 0;
}

int ul_eth_setpromode(ul_fd_t ul_fd, int pro_mode)
{
  return 0;
}

int ul_eth_setsubdev(ul_fd_t ul_fd, int subdevidx)
{
  return 0;
}

int ul_eth_queryparam(ul_fd_t ul_fd, unsigned long query, unsigned long *pvalue) 
{
  return 0;
}

int ul_eth_route(ul_fd_t ul_fd, ul_route_range_t *rr)
{
  return 0;
}

int ul_eth_fd_wait(ul_fd_t ul_fd, int wait_sec)
{
  ul_fd_eth_context_t *eth_fd;
  ul_msg_buf_t *msgbuff;
  unsigned eth_stamp;

  eth_fd=ul_fd2eth_context(ul_fd);
  if (ul_eth_sock_check(eth_fd)<0) {
    /* check if some message is inproc queue */
    if (!msgbuff_inproc_is_empty(&eth_fd->msgbuff_inproc_root))
      return 1;
    SLEEP(wait_sec);
    return -1;
  }
  if (eth_fd->msg_received_flg) 
    return 1;

  msgbuff=malloc(sizeof(ul_msg_buf_t));
  if (!msgbuff)
    return -1;
  ul_msg_buf_init(msgbuff);

  eth_stamp=ul_eth_gen_stamp(eth_fd);
  fprintf(eth_fd->sock_wfile,"usc %d fd_wait_recv %d\n",eth_stamp,wait_sec);
  fflush(eth_fd->sock_wfile);
  /* wait for responce */
  if (ul_eth_recv_responce_msgbuf(eth_fd,"fd_wait_recv",eth_stamp,msgbuff)!=1) {
    free(msgbuff);
    return -1;
  }
  /* a new messsage is ready */
  eth_fd->msgbuff_start=msgbuff;
  eth_fd->msgbuff_proc=eth_fd->msgbuff_start;
  eth_fd->msgbuff_idx=0;
  eth_fd->msg_received_flg=1;
  return 1;
}

int ul_eth_namematch(const char *dev_name)
{
  int ret=0;
  if(!dev_name) ret=1;
  else if(!strncmp(dev_name,"eth:",4))
    ret=100; /* 100% name match for driver */
  else if(!strchr(dev_name,':'))
    ret=5;   /* no transport selected, direct driver is reasonable for this name */
  return ret;
}

ul_fd_ops_t ul_fd_eth_ops={
  "eth",
  ul_eth_namematch,
  ul_eth_open,
  ul_eth_close,
  ul_eth_drv_version,
  ul_eth_read,
  ul_eth_write,
  ul_eth_newmsg,
  ul_eth_tailmsg,
  ul_eth_freemsg,
  ul_eth_acceptmsg,
  ul_eth_actailmsg,
  ul_eth_addfilt,
  ul_eth_abortmsg,
  ul_eth_rewmsg,
  ul_eth_inepoll,
  ul_eth_debflg,
  ul_eth_fd_wait,
  ulop_eth_fd2sys_fd,
  ul_eth_setmyadr,
  ul_eth_setidstr,
  ul_eth_setbaudrate,
  ul_eth_setpromode,
  ul_eth_setsubdev,
  ul_eth_queryparam,
  ul_eth_route
};

