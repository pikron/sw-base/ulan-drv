{ This file was automatically created by Lazarus. Do not edit!
This source is only used to compile and install the package.
 }

unit uLanlaz; 

interface

uses
  uLan, LazarusPackageIntf; 

implementation

procedure Register; 
begin
  RegisterUnit('uLan', @uLan.Register); 
end; 

initialization
  RegisterPackage('uLanlaz', @Register); 
end.
