; -- ul_drvnt.iss --
; Source for NT uLan driver setup

[Setup]
AppName=ul_drv
AppVerName=uLan driver for NT v 1.1.2
AppCopyright=Copyright � 1999-2011 Pavel Pisa
;DefaultDirName={pf}\ul_drv
;DefaultGroupName=uLan Driver
MinVersion=0,3.51
OnlyBelowVersion=0,5.0
;UninstallDisplayIcon={app}\ul_drv.exe
OutputBaseFileName=ul_drvnt-063
OutputDir=.
;DiskSpanning=yes
AdminPrivilegesRequired=yes
;AlwaysCreateUninstallIcon=yes
;ChangesAssociations=yes
ExtraDiskSpaceRequired=1000000
;InfoBeforeFile=readme.txt
InfoAfterFile=readme.txt
;LicenseFile=gnu.txt
AllowNoIcons=yes
AlwaysRestart=yes
CreateAppDir=no
DirExistsWarning=no
DisableDirPage=yes
DisableProgramGroupPage=yes

;[Dirs]
;Name: "{app}\data"

[Files]
;-{app},{win},{sys},{src},{sd},{pf},{cf},{tmp},{fonts},{dao}
; {group},{(user/common)desktop},{(u/c)desktop},{(u/c)startmenu},{(u/c)programs},{(u/c)startup}

; {sendto},{(u/c)appdata},{(u/c)docs}
; {\},{%NAME},{computername},{groupname},{hwnd},{srcexe}
;-CopyMode: normal,onlyifdoesntexist,alwaysoverwrite,alwaysskipifsameorolder
; 
Source: "ul_drv.sys"; DestDir: "{sys}\drivers"

[Icons]
;Name: "{group}\ul_drv"; Filename: "{app}\ul_drv.exe"
; NOTE: Most apps do not need registry entries to be pre-created, so if
; you are using this script as a template for a new install, you should
; probably remove these [Registry] entries.

[Registry]
Root: HKLM; Subkey: "SYSTEM\CurrentControlSet\Services\ul_drv"; Flags: deletekey uninsdeletekey
Root: HKLM; Subkey: "SYSTEM\CurrentControlSet\Services\ul_drv"; ValueType: string; ValueName: "DisplayName"; ValueData: "UL_DRV"; Flags: uninsdeletekey
Root: HKLM; Subkey: "SYSTEM\CurrentControlSet\Services\ul_drv"; ValueType: string; ValueName: "Group"; ValueData: "port"; Flags: uninsdeletekey
Root: HKLM; Subkey: "SYSTEM\CurrentControlSet\Services\ul_drv"; ValueType: dword; ValueName: "Type"; ValueData: "1"; Flags: uninsdeletekey
Root: HKLM; Subkey: "SYSTEM\CurrentControlSet\Services\ul_drv"; ValueType: dword; ValueName: "Start"; ValueData: "2"; Flags: uninsdeletekey
Root: HKLM; Subkey: "SYSTEM\CurrentControlSet\Services\ul_drv"; ValueType: dword; ValueName: "ErrorControl"; ValueData: "1"; Flags: uninsdeletekey
Root: HKLM; Subkey: "SYSTEM\CurrentControlSet\Services\ul_drv"; ValueType: dword; ValueName: "Tag"; ValueData: "1"; Flags: uninsdeletekey

Root: HKLM; Subkey: "SYSTEM\CurrentControlSet\Services\ul_drv\Parameters"; Flags: deletekey uninsdeletekey
Root: HKLM; Subkey: "SYSTEM\CurrentControlSet\Services\ul_drv\Parameters"; ValueType: dword; ValueName: "Port Address"; ValueData: "$2e8"; Flags: uninsdeletekey
Root: HKLM; Subkey: "SYSTEM\CurrentControlSet\Services\ul_drv\Parameters"; ValueType: dword; ValueName: "IRQ Line"; ValueData: "5"; Flags: uninsdeletekey
Root: HKLM; Subkey: "SYSTEM\CurrentControlSet\Services\ul_drv\Parameters"; ValueType: dword; ValueName: "Baud Rate"; ValueData: "19200"; Flags: uninsdeletekey
Root: HKLM; Subkey: "SYSTEM\CurrentControlSet\Services\ul_drv\Parameters"; ValueType: dword; ValueName: "Buffer Size"; ValueData: "$10000"; Flags: uninsdeletekey
Root: HKLM; Subkey: "SYSTEM\CurrentControlSet\Services\ul_drv\Parameters"; ValueType: dword; ValueName: "My Addr"; ValueData: "2"; Flags: uninsdeletekey
Root: HKLM; Subkey: "SYSTEM\CurrentControlSet\Services\ul_drv\Parameters"; ValueType: dword; ValueName: "Debug"; ValueData: "$19"; Flags: uninsdeletekey
Root: HKLM; Subkey: "SYSTEM\CurrentControlSet\Services\ul_drv\Parameters"; ValueType: dword; ValueName: "ScanForPCI"; ValueData: "1"; Flags: uninsdeletekey

;[HKEY_LOCAL_MACHINE\SYSTEM\CurrentControlSet\Services\ul_drv]
;"Type"=dword:00000001
;"Start"=dword:00000002
;"ErrorControl"=dword:00000001
;"DisplayName"="UL_DRV"
;"Group"="port"
;"Tag"=dword:00000001
;"ImagePath"=hex(2):5c,3f,3f,5c,44,3a,...

;[HKEY_LOCAL_MACHINE\SYSTEM\CurrentControlSet\Services\ul_drv\Parameters]
;"Port Address"=dword:000003e8
;"IRQ Line"=dword:00000005
;"Baud Rate"=dword:00004b00
;"Buffer Size"=dword:00004000
;"My Addr"=dword:00000002
;"Debug"=dword:00000019
;"ScanForPCI"=dword:00000001
