/*******************************************************************
  uLan Communication - low level C and driver interface library

  ul_msg_adr.c	- address classification and serialization

  (C) Copyright 1996-2016 by Pavel Pisa - project originator
        http://cmp.felk.cvut.cz/~pisa
  (C) Copyright 1996-2016 PiKRON Ltd.
        http://www.pikron.com
  (C) Copyright 2002-2016 Petr Smolik


  The uLan driver project can be used and distributed
  in compliance with any of next licenses
   - GPL - GNU Public License
     See file COPYING for details.
   - LGPL - Lesser GNU Public License
   - MPL - Mozilla Public License
   - and other licenses added by project originator

  Code can be modified and re-distributed under any combination
  of the above listed licenses. If contributor does not agree with
  some of the licenses, he/she can delete appropriate line.
  WARNING: if you delete all lines, you are not allowed to
  distribute code or sources in any form.
 *******************************************************************/

#include <ul_msg_adr.h>

ul_msg_adr_t ul_msg_adr_short_local(ul_msg_adr_t madr, ul_msg_adr_t net_base)
{
  ul_msg_adr_t madr_local;
  if (madr<UL_MSG_ADR_LOCAL_MAX)
    return madr;
  net_base&=UL_MSG_ADR_NET_MASK;
  if (!net_base)
    return madr;
  madr_local=madr^net_base;
  if (madr_local<UL_MSG_ADR_LOCAL_MAX)
    return madr_local;
  return madr;
}

int ul_msg_adr_log2len(ul_msg_adr_t madr)
{
  if (((unsigned long)madr<UL_MSG_ADR_LOCAL_MAX)||
      (madr==/*UL_BEG&UL_MSG_ADR_NET_MASK)*/117))
    return 0;
  if (!(madr&~0xffff))
    return 1;
  return 2;
}

uchar *ul_msg_adr2buff(uchar *ptr, ul_msg_adr_t madr, int madr_log2len)
{
  *(ptr++)=madr&0xff;
  if(!madr_log2len--)
    return  ptr;
  *(ptr++)=(madr>>8)&0xff;
  if(!madr_log2len--)
    return  ptr;
  *(ptr++)=(madr>>16)&0xff;
  *(ptr++)=(madr>>24)&0xff;
  return ptr;
}

uchar *ul_msg_buff2adr(uchar *ptr, ul_msg_adr_t *madr_p, int madr_log2len)
{
  ul_msg_adr_t madr;
  madr=*(ptr++);
  do {
    if(!madr_log2len--)
      break;
    madr|=*(ptr++)<<8;
    if(!madr_log2len--)
      break;
    madr|=*(ptr++)<<16;
    madr|=*(ptr++)<<24;
  } while(0);
  *madr_p=madr;
  return ptr;
}

int ul_msg_ext_header2len(uchar ext_header)
{
  int dadr_lg2l=ext_header&7;
  int sadr_lg2l=(ext_header>>3)&7;

  if((dadr_lg2l>2)||(sadr_lg2l>2))
    return -1;

  return (dadr_lg2l?(1<<dadr_lg2l):0)+(sadr_lg2l?(1<<sadr_lg2l):0)+1;
}

int ul_msg_ext_header2dadr_log2len(uchar ext_header)
{
  return ext_header&7;
}

int ul_msg_ext_header2sadr_log2len(uchar ext_header)
{
  return (ext_header>>3)&7;
}

uchar ul_msg_adr_log2len_to_ext_header(int dadr_lg2l, int sadr_lg2l)
{
  uchar ext_header=0;
  ext_header|=dadr_lg2l&7;
  ext_header|=(sadr_lg2l&7)<<3;
  return ext_header;
}
