/*******************************************************************
  uLan Communication - uL_DRV - multiplatform uLan driver

  ul_djgpp.c	- DOS DJGPP specific routines

  (C) Copyright 1996-2004 by Pavel Pisa - project originator
        http://cmp.felk.cvut.cz/~pisa
  (C) Copyright 1996-2004 PiKRON Ltd.
        http://www.pikron.com
  (C) Copyright 2002-2004 Petr Smolik
  

  The uLan driver project can be used and distributed 
  in compliance with any of next licenses
   - GPL - GNU Public License
     See file COPYING for details.
   - LGPL - Lesser GNU Public License
   - MPL - Mozilla Public License
   - and other licenses added by project originator

  Code can be modified and re-distributed under any combination
  of the above listed licenses. If contributor does not agree with
  some of the licenses, he/she can delete appropriate line.
  WARNING: if you delete all lines, you are not allowed to
  distribute code or sources in any form.
 *******************************************************************/

#ifdef ENABLE_UL_MEM_CHECK
void * ul_mem_check_malloc(size_t size)
{ void *ptr;
  ptr=malloc(size);
  if(ptr){
    ul_mem_check_counter++;
  }
  return ptr;
}

void ul_mem_check_free(void *ptr)
{
  if(!ptr) {
    LOG_FATAL(KERN_CRIT "ul_mem_check_free : triing to free NULL ptr\n");
  }else{
    ul_mem_check_counter--;
    free(ptr);
  }
}
#endif /* ENABLE_UL_MEM_CHECK */


