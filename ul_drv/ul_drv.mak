#----------------------------------------------------------------------------
#
# Device Driver MAKEFILE
#
# uLan Communication Driver
#
#

#****************************************************************************
#----------------------------------------------------------------------------
# Compile flags for the Kernel Mode Driver
#

# general flags, 

CFLAGS1 = /c /Gs /W3 /GF /Gz

# The following flags handle Pentium chip bugs

CFLAGS2 = /QIfdiv- /QI0f

# /Z7=Debug info flags, /Zel=Use MS extension-omit default library

CFLAGS3 = /Z7 /Zel

# /Od=Turn off ALL optimizations, /Oi=Inline functions

CFLAGS4 = /Od /Oi

# defines used by .H files and the like

CFLAGS5 = -cbstring -FIwarning.h -D_X86_=1 -Di386=1

# try compile with PCI 

CFLAGS6 = -D UL_WITH_PCI

#----------------------------------------------------------------------------
#****************************************************************************


#****************************************************************************
#----------------------------------------------------------------------------
# Flags for the User Mode EXE
#

#----------------------------------------------------------------------------
# COMPILE flags

ccommon = -c -W3 -DCRTAPI1=_cdecl -DCRTAPI2=_cdecl -nologo
cflags = $(ccommon) -D_X86_=1
cvars      = -DWIN32 $(noansi) -D_WIN32
cdebug = -Z7 -Od

CCFLAGS = $(cflags) $(cvars) $(cdebug)


#----------------------------------------------------------------------------
# LINK Flags

libcmt = libcmt.lib oldnames.lib
baselibs = kernel32.lib advapi32.lib
libc = libc.lib oldnames.lib
linklibs = $(libc) $(baselibs)

linkdebug = -debug:full -debugtype:cv

lflags = /NODEFAULTLIB /INCREMENTAL:NO /PDB:NONE /RELEASE /NOLOGO
conlflags = $(lflags) -subsystem:console,4.0 -entry:mainCRTStartup

#----------------------------------------------------------------------------
#****************************************************************************


#****************************************************************************
#----------------------------------------------------------------------------
#
# The dependency list and build rules follows

whole_enchilada : ul_drv.sys

ul_drv.sym : ul_drv.map
        MSYM ul_drv.MAP

ul_drv.map : ul_drv.sys
        DBG2MAP ul_drv.sys

#
# In the following,
# Use: ul_drv.lnr for a release link
# Use: ul_drv.lnd for a debug build
#
ul_drv.sys : ul_drv.obj
        LINK @ul_drv.lnr

ul_drv.obj : ul_drv.c  \
             ul_drv.h ul_hdep.h ul_82510.h serial_reg.h
        CL $(CFLAGS1) $(CFLAGS2) $(CFLAGS3) $(CFLAGS4)  $(CFLAGS5)  $(CFLAGS6) ul_drv.c

q_test.obj: q_test.c
        CL $(CCFLAGS) q_test.c

q_test.exe: q_test.obj
        LINK $(linkdebug) $(conflags) -out:q_test.exe q_test.obj $(linklibs)

#****************************************************************************
#****************************************************************************

