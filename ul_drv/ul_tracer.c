#define UL_TRACE_SLOTS 256

typedef struct uld_tracer_slot_t {
   unsigned long code;
   unsigned long val;
} uld_tracer_slot_t;

uld_tracer_slot_t uld_tracer_buff[UL_TRACE_SLOTS];
uld_tracer_slot_t *uld_tracer_buff_act=uld_tracer_buff+1;
uld_tracer_slot_t * const uld_tracer_buff_start=uld_tracer_buff+1;
uld_tracer_slot_t * const uld_tracer_buff_end=uld_tracer_buff+UL_TRACE_SLOTS;
unsigned long *uld_tracer_buff_loc_store=(unsigned long *)uld_tracer_buff;

volatile int uld_tracer_disable;
volatile int uld_tracer_timeout_limit = 0;
volatile int uld_tracer_timeout_sn;

void uld_tracer_log(unsigned long code, unsigned long val)
{
  uld_tracer_slot_t *p;
  if(uld_tracer_disable)
    return;
  UL_DRV_LOCK_FINI
  p = uld_tracer_buff_act;
  p->code = code;
  p->val = val;
  p++;
  if(p >= uld_tracer_buff_end) {
    uld_tracer_buff_loc_store[1]=(char *)p - (char*)uld_tracer_buff_start;
    p = uld_tracer_buff_start;
  }
  uld_tracer_buff_act = p;
  uld_tracer_buff_loc_store[0]=(char *)p - (char*)uld_tracer_buff_start;
  UL_DRV_LOCK;
  UL_DRV_UNLOCK;
}

#define UL_TRACE_CODE_BUILD_WHAT(what) ((unsigned long)(what)<<24)
#define UL_TRACE_CODE_WHAT_m	UL_TRACE_CODE_BUILD_WHAT(0xff)

#define UL_TRACE_INB		UL_TRACE_CODE_BUILD_WHAT(0x02)
#define UL_TRACE_OUTB		UL_TRACE_CODE_BUILD_WHAT(0x03)
#define UL_TRACE_FSM_POLL	UL_TRACE_CODE_BUILD_WHAT(0x04)
#define UL_TRACE_FSM_FNC	UL_TRACE_CODE_BUILD_WHAT(0x05)
#define UL_TRACE_FSM_TIMEOUT	UL_TRACE_CODE_BUILD_WHAT(0x06)
#define UL_TRACE_MARK           UL_TRACE_CODE_BUILD_WHAT(0x07)

#define ul_inb_untraced ul_inb
#define ul_outb_untraced ul_outb

#if 1

static uchar ul_inb_traced(typeof(((ul_drv*)0)->port) port)
{
  uchar val = ul_inb_untraced(port);
  uld_tracer_log(UL_TRACE_INB | val, (unsigned long)port);
  return val;
}

static void ul_outb_traced(typeof(((ul_drv*)0)->port) port, uchar val)
{
  ul_outb_untraced(port,val);
  uld_tracer_log(UL_TRACE_OUTB | val, (unsigned long)port);
}

#ifdef ul_inb
#undef ul_inb
#undef ul_outb
#endif

#define ul_inb ul_inb_traced
#define ul_outb ul_outb_traced

#endif

static void uld_tracer_log_fsm_poll(int ret)
{
  unsigned long time = RDTSC;
  time = uld_tracer_timeout_sn++;
  uld_tracer_log(UL_TRACE_FSM_POLL | (ret & ~UL_TRACE_CODE_WHAT_m), (unsigned long)time);
}

static void uld_tracer_log_fsm_fnc(ul_call_fnc *fnc, int ret)
{
  uld_tracer_log(UL_TRACE_FSM_FNC | (ret & ~UL_TRACE_CODE_WHAT_m), (unsigned long)fnc);
}

static void uld_tracer_log_fsm_timeout(int ret)
{
  int limit;
  unsigned long time = RDTSC;
  time = uld_tracer_timeout_sn++;
  uld_tracer_log(UL_TRACE_FSM_TIMEOUT | (ret & ~UL_TRACE_CODE_WHAT_m), (unsigned long)time);
  if((limit=uld_tracer_timeout_limit) != 0) {
    uld_tracer_timeout_limit = --limit;
    if(!limit)
      uld_tracer_disable = 1;
  }
}

static void uld_tracer_log_mark(int val)
{
  uld_tracer_log(UL_TRACE_MARK, (unsigned long)val);
}
