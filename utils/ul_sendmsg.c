/*******************************************************************
  uLan Communication - simple test client

  ul_spy.c	- message monitor

  (C) Copyright 1996,1999 by Pavel Pisa 

  The uLan driver is distributed under the Gnu General Public Licence. 
  See file COPYING for details.
 *******************************************************************/

#include <unistd.h>
#include <stdlib.h>
#include <fcntl.h>
#include <stdio.h>
#include <getopt.h>
#include <errno.h>
#include <string.h>
#include <sys/types.h>
#include <sys/time.h>
#include <sys/stat.h>
#include <ul_lib/ulan.h>

int si_long(char **ps,long *val,int base)
{
  char *p;
  *val=strtol(*ps,&p,base);
  if(*ps==p) return -1;
  *ps=p;
  return 1;
}

int add_to_arr(void **pdata,int *plen,int base,char *str)
{
  char *s=str;
  long val;
  void *p;
  
  do{
    while(*s && strchr(", \t:;",*s)) s++;
    if(!*s) break;
    if(si_long(&s,&val,base)<0){
      return -1;
    }
    if(*pdata==NULL){
      *plen=0;
      *pdata=p=malloc(1);
    }else{
      p=realloc(*pdata,*plen+1);
      if(p==NULL) return -1;
      *pdata=p;
    }
    ((uchar*)p)[*plen]=val;
    (*plen)++;
  } while(1);
  return 1;
}

/*******************************************************************/

char *ul_dev_name = UL_DEV_NAME;
int msg_module = 0;
int msg_cmd    = 0;
uchar *msg_data= NULL;
int msg_len  = 0;
uchar msg_flg  = 0;
int prt_modules= 0;
int debugk     = 0;
int debugk_flg = 0;
int query_flg = 0;
int logk_flg = 0;
int cid_flg = 0;
int cid_value = 0;
int routed_sadr_flg = 0;
int routed_sadr_value = 0;

int print_modules(int max_addr)
{ int ret;
  int i;
  ul_fd_t ul_fd;
  uchar *p;
  void *buf=NULL;
  int buf_len;
  int count=0; 
  ul_fd=ul_open(ul_dev_name, NULL);
  if(ul_fd==UL_FD_INVALID)
    { perror("print_modules : uLan open failed");return -1;};
  for(i=1;i<=max_addr;i++)
  {
    ret=ul_send_query_wait(ul_fd,i,UL_CMD_SID,
  			UL_BFL_NORE|UL_BFL_PRQ,NULL,0,&buf,&buf_len);
    if(ret>=0)
    { count++;
      p=(uchar*)buf;
      for(ret=0;(ret<buf_len)&&*p;ret++,p++);
      printf("%2d:  ",i);
      fwrite(buf,ret,1,stdout);
      printf("\n");
    };
    if(buf) free(buf);
    buf=NULL;
  };
  ul_close(ul_fd);
  return count;
};

int debug_kernel(int debug_msk)
{ int ret;
  ul_fd_t ul_fd;
  ul_fd=ul_open(ul_dev_name, NULL);
  if(ul_fd==UL_FD_INVALID)
    { perror("debug_kernel : uLan open failed");return -1;};
  ret=ul_drv_debflg(ul_fd,debug_msk);
  ul_close(ul_fd);
  return ret;
};

int log_kernel(void)
{ int ret;
  ul_fd_t ul_fd;
  ul_fd=ul_open(ul_dev_name, NULL);
  if(ul_fd==UL_FD_INVALID)
    { perror("log_kernel : uLan open failed");return -1;};
 #if defined(_WIN32) 
  { DWORD bytes_ret;
    ret=DeviceIoControl(ul_fd,UL_KLOGBLL,
		  NULL,0,NULL,0,
		  &bytes_ret,NULL)?0:-1;
  }
 #elif !defined(UL_DRV_IN_LIB)
  ret=ioctl(ul_fd,UL_KLOGBLL,0);
 #endif
  ul_close(ul_fd);
  return ret;
};

int inject_routed_sadr_message(ul_fd_t ul_fd)
{
  int ret;
  ul_msginfo msginfo;
  memset(&msginfo,0,sizeof(msginfo));
  msginfo.dadr=msg_module;
  msginfo.cmd=msg_cmd;
  msginfo.flg=msg_flg|UL_BFL_SANL;
  msginfo.sadr=routed_sadr_value;
  ret=ul_newmsg(ul_fd,&msginfo);
  if(ret<0) return ret;
  if(msg_len)if(ul_write(ul_fd,msg_data,msg_len)!=msg_len)
  { ul_abortmsg(ul_fd);
    return -1;
  }
  return ul_freemsg(ul_fd);
}

int send_message(void)
{
  int ret;
  ul_fd_t ul_fd;
  int i;
  uchar *bufout=NULL;
  int lenout=0;

  ul_fd=ul_open(ul_dev_name, NULL);
  if(ul_fd==UL_FD_INVALID)
    { perror("send_message : uLan open failed");return -1;};

  if(routed_sadr_flg){
    /* Experimental test of injecting routed message with non local SADR*/
    ret=ul_setpromode(ul_fd, 0x100);
    if(ret<0){
      fprintf(stderr,"ul_setpromode failed - ret %d\n",ret);
      ul_close(ul_fd);
      return -1;
    }
    ret=inject_routed_sadr_message(ul_fd);
    fprintf(stderr,"inject_routed_sadr_message ret %d\n",ret);
  }else if(!query_flg){
    /* Send regular message without wait for reply */
    ret=ul_send_command_wait(ul_fd,msg_module,msg_cmd,msg_flg,
                         msg_data,msg_len);
    fprintf(stderr,"ul_send_command_wait ret %d\n",ret);
  }else{
    /* Send regular message with wait for reply */
    ret=ul_send_query_wait(ul_fd,msg_module,msg_cmd,msg_flg,
                    msg_data,msg_len,(void**)&bufout,&lenout);
    fprintf(stderr,"ul_send_query_wait ret %d\n",ret);
    for(i=0;i<lenout;i++) printf("0x%02x ",bufout[i]);
    printf("\n");
  }

  if(bufout!=NULL) free(bufout);

  ul_close(ul_fd);
  return 0;
};

int send_cid(void)
{
  int ret;
  ul_fd_t ul_fd;
  uchar *buff;

  ul_fd=ul_open(ul_dev_name, NULL);
  if(ul_fd==UL_FD_INVALID)
    { perror("send_message : uLan open failed");return -1;};

  if (!msg_cmd)
    msg_cmd=UL_CMD_PDO;

  buff=(uchar*)malloc(msg_len+3+2+1);
  if (buff==NULL) {
    ul_close(ul_fd);
    return -1;
  }

  buff[0]=0;
  buff[1]=0;
  buff[2]=0;
  buff[3]=cid_value;
  buff[4]=cid_value>>8;
  buff[5]=msg_len;
  memcpy(&buff[6],msg_data,msg_len);

  ret=ul_send_command_wait(ul_fd,msg_module,msg_cmd,msg_flg,
                       buff,msg_len+3+2+1);
  fprintf(stderr,"ul_send_command_wait ret %d\n",ret);

  free(buff);
  ul_close(ul_fd);
  return 0;
};

static void
usage(void)
{
  printf("Usage: ul_sendmsg <parameters> <hex_bytes>\n");
  printf("  -d, --uldev <name>       name of uLan device [%s]\n", ul_dev_name);
  printf("  -m, --module <num>       destination module address\n");
  printf("  -c, --command <num>      command\n");
  printf("  -f, --flags <num>        flags\n");
  printf("  -q, --query              send query type message\n");
  printf("  -C, --cid-send <num>     send PDO with given CID number\n");
  printf("  -S, --routed-sadr <num>  test injection of routed message\n");
  printf("  -p, --print <max>        print modules to max address\n");
  printf("      --debug-kernel <m>   flags to debug kernel\n");
  printf("  -V, --version            show version\n");
  printf("  -h, --help               this usage screen\n");
}

int main(int argc,char *argv[])
{
  static struct option long_opts[] = {
    { "uldev", 1, 0, 'd' },
    { "module",1, 0, 'm' },
    { "command",1,0, 'c' },
    { "flags", 1, 0, 'f' },
    { "query", 0, 0, 'q' },
    { "cid-send", 1, 0, 'C' },
    { "souted-sadr", 1, 0, 'S' },
    { "print", 1, 0, 'p' },
    { "version",0,0, 'V' },
    { "help",  0, 0, 'h' },
    { "debug-kernel", 1, 0, 'D' },
    { "log-kernel", 0, 0, 'L' },
    { 0, 0, 0, 0}
  };
  int opt;

 #ifndef HAS_GETOPT_LONG
  while ((opt = getopt(argc, argv, "d:m:c:f:qC:S:p:VhD:L")) != EOF)
 #else
  while ((opt = getopt_long(argc, argv, "d:m:c:f:qC:S:p:VhD:L",
			    &long_opts[0], NULL)) != EOF)
 #endif
    switch (opt) {
    case 'd':
      ul_dev_name=optarg;
      break;
    case 'm':
      msg_module = strtol(optarg,NULL,0);
      break;
    case 'c':
      msg_cmd = strtol(optarg,NULL,0);
      break;
    case 'f':
      msg_flg = strtol(optarg,NULL,0);
      break;
    case 'q':
      query_flg=1;
      break;
    case 'C':
      cid_flg = 1;
      cid_value = strtol(optarg,NULL,0);
      break;
    case 'S':
      routed_sadr_flg = 1;
      routed_sadr_value = strtol(optarg,NULL,0);
      break;
    case 'p':
      prt_modules = strtol(optarg,NULL,0);
      break;
    case 'D':
      debugk = strtol(optarg,NULL,0);
      debugk_flg=1;
      break;
    case 'L':
      logk_flg=1;
      break;
    case 'V':
      fputs("uLan send message v1.1\n", stdout);
      exit(0);
    case 'h':
    default:
      usage();
      exit(opt == 'h' ? 0 : 1);
    }

  if(debugk_flg) debug_kernel(debugk);

  if(logk_flg) log_kernel();

  if(prt_modules) print_modules(prt_modules);

  for(;optind<argc;optind++){
    if(add_to_arr((void**)&msg_data,&msg_len,0,argv[optind])<0){
      fprintf(stderr,"%s: incorrect message data \"%s\"\n",argv[0],argv[optind]);
      exit(2);
    }
  }

  if (!cid_flg)
    send_message();
  else
    send_cid();

  return 0;
}
