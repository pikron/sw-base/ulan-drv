/********************************************************************/
/* uLan property page - Roman Bartosinski (C)2003               */
/********************************************************************/

#include "proppage.h"
#include <devioctl.h>


  DWORD baud_tbl[] = { 75,110,134,150,300,600,1200,1800,2400,4800,7200,
                      9600,14400,19200,38400,57600,115200,128000,0};  
  LPCTSTR debug_tbl[] = { L"none", L"Lowest", L"Lower", L"Normal", L"Higher", L"Highest", NULL};


  HANDLE g_hInst = NULL;
  TCHAR applet_name[]     = TEXT("General Property Page");

/********************************************************************/
/*
TCHAR sz_evbousb_help[] = _T("evbousb.hlp");
const DWORD HelpIDs[]=
{
    PP_PORT_NAME,       IDH_PORTSET_NAME,       // "" (ComboBox)
    PP_ULAN_BAUD,       IDH_PORTSET_BAUDRATE,   // "" (ComboBox)
    PP_ULAN_ADDR,       IDH_PORTSET_ADDRESS,    // "" (ComboBox)
    PP_ULAN_GW,         IDH_PORTSET_GW,         // "" (ComboBox)
    PP_ULAN_DEBUG,      IDH_PORTSET_DEBUGLEVEL, // "" (ComboBox)
    PP_ULAN_PMODE,      IDH_PORTSET_PROMODE,    // "" (ComboBox)
    IDC_RESTORE,        IDH_PORTSET_DEFAULTS,   // "&Restore Defaults" (Button)
    IDC_REFRESH,        IDH_PORTSET_DEFAULTS,   // "&Refresh Defaults" (Button)
    IDC_STATIC,         IDH_NONE,               // no help
    0, 0
};
*/



/********************************************************************/
/********************************************************************/
BOOL APIENTRY LibMain( HANDLE hDll, DWORD dwReason, LPVOID lpReserved)
{
  switch( dwReason ) {
    case DLL_PROCESS_ATTACH:
      g_hInst = hDll;
      DisableThreadLibraryCalls(hDll);
      break;
    case DLL_PROCESS_DETACH:
      break;
    default:
      break;
  }
  return TRUE;
}

/********************************************************************/
void InitPortParams( IN OUT PPORT_PARAMS Params, IN HDEVINFO DeviceInfoSet, IN PSP_DEVINFO_DATA DeviceInfoData)
{
  SP_DEVINFO_LIST_DETAIL_DATA detailData;

  ZeroMemory( Params, sizeof(PORT_PARAMS));
  Params->DeviceInfoSet = DeviceInfoSet;
  Params->DeviceInfoData = DeviceInfoData;
  Params->ChangesEnabled = TRUE;
  detailData.cbSize = sizeof(SP_DEVINFO_LIST_DETAIL_DATA);
  if ( SetupDiGetDeviceInfoListDetail( DeviceInfoSet, &detailData) &&
       detailData.RemoteMachineHandle != NULL) {

    Params->ChangesEnabled = FALSE;
  }
}

/********************************************************************/
HPROPSHEETPAGE InitSettingsPage(PROPSHEETPAGE *psp, OUT PPORT_PARAMS Params)
{
//  debugPrint("InitSetting");
  psp->dwSize      = sizeof( PROPSHEETPAGE);
  psp->dwFlags     = PSP_USECALLBACK; // | PSP_HASHELP;
  psp->hInstance   = g_hInst;
  psp->pszTemplate = MAKEINTRESOURCE(DLG_PPAGE);
  psp->pfnDlgProc = PortSettingsDlgProc;
  psp->lParam     = (LPARAM) Params;
  psp->pfnCallback = PortSettingsDlgCallback;
  return CreatePropertySheetPage( psp);
}

/********************************************************************/
UINT CALLBACK PortSettingsDlgCallback( HWND hwnd, UINT uMsg, LPPROPSHEETPAGE ppsp)
{
  PPORT_PARAMS params;
  
  switch (uMsg) {
    case PSPCB_CREATE:
      return TRUE;    // return TRUE to continue with creation of page
    case PSPCB_RELEASE:
      params = (PPORT_PARAMS) ppsp->lParam;
      if ( params)
        LocalFree(params);
      return 0;       // return value ignored
    default:
      break;
  }
  return TRUE;
}

/********************************************************************/
void Port_OnCommand( HWND hDlg, int ControlId, HWND ControlHwnd, UINT NotifyCode)
{
  PPORT_PARAMS params = (PPORT_PARAMS)GetWindowLongPtr( hDlg, DWLP_USER);
  DWORD i; 
  BYTE j;
  TCHAR str[20];
  BYTE changed = 0;

  switch( ControlId) {
    case PP_ULAN_BAUD:
      if ( NotifyCode == CBN_SELCHANGE) {
        //debugPrint("Select Baudrate");
        GetWindowText( GetDlgItem( hDlg, PP_ULAN_BAUD), str, 20);
        params->PortSettings.BaudRate = (DWORD) myatoi( str);
        changed = 1;
      }
      break;
    case PP_ULAN_DEBUG:    
      if ( NotifyCode == CBN_SELCHANGE) {
        //debugPrint("Select Debug");
        params->PortSettings.Debug = (DWORD) SendMessage( ControlHwnd, CB_GETCURSEL, 0, 0);
        changed = 1;
      }
      break;
    case PP_ULAN_ADDR:
      if ( NotifyCode == EN_KILLFOCUS) {
        //debugPrint("KillFocus");
        GetWindowText( GetDlgItem( hDlg, PP_ULAN_ADDR), str, 20);
        params->PortSettings.Address = (DWORD) myatoi( str);
        if ( params->PortSettings.Address < ULANADDR_MIN || params->PortSettings.Address > ULANADDR_MAX) {
          params->PortSettings.Address = params->PortSettings.DefAddress;
          wsprintf( str, TEXT("%d"), params->PortSettings.Address);
          SetWindowText( GetDlgItem( hDlg, PP_ULAN_ADDR), str);
        }
        changed = 1; // there is some bug, maybe.
      }
      break;
    case PP_ULAN_GW:
      if ( NotifyCode == EN_KILLFOCUS) {
        //debugPrint("KillFocus");
        GetWindowText( GetDlgItem( hDlg, PP_ULAN_GW), str, 20);
        params->PortSettings.GW = (DWORD) myatoi( str);
        if ( params->PortSettings.GW < ULANADDR_MIN || params->PortSettings.GW > ULANADDR_MAX) {
          params->PortSettings.GW = params->PortSettings.DefGW;
          wsprintf( str, TEXT("%d"), params->PortSettings.GW);
          SetWindowText( GetDlgItem( hDlg, PP_ULAN_GW), str);
        }
        changed = 1; // there is some bug, maybe.
      }
      break;
    case PP_ULAN_PMODE:
      if ( NotifyCode == BN_CLICKED) {
        //debugPrint("Clicked");
        if ( IsDlgButtonChecked( hDlg, PP_ULAN_PMODE) == BST_CHECKED)
          params->PortSettings.ProMode = 1;
        else
          params->PortSettings.ProMode = 0;
        changed = 1;
      }
      break;
    case IDC_REFRESH:
      MessageBox( hDlg, TEXT("Here will be IOCTL driver call."), TEXT("uLan Property Page"), MB_OK | MB_ICONINFORMATION);
      break;
    case IDC_RESTORE:
      //debugPrint("ChangeData");
      RestorePortSettings( hDlg, params);
      EnableWindow( GetDlgItem( hDlg, IDC_RESTORE), FALSE);
      PropSheet_Changed( GetParent(hDlg), hDlg);
      break; 
    case IDCANCEL: // ??? redundant control
      EndDialog( hDlg, 0); 
      return;
    case IDC_APPLY:
      PropSheet_Apply( GetParent( hDlg));
      params->PortSettings.DefBaudRate = params->PortSettings.BaudRate;
      params->PortSettings.DefAddress = params->PortSettings.Address;
      params->PortSettings.DefGW = params->PortSettings.GW;
      params->PortSettings.DefDebug = params->PortSettings.Debug;
      params->PortSettings.DefProMode = params->PortSettings.ProMode;
      EnableWindow( GetDlgItem( hDlg, IDC_RESTORE), FALSE);
      break;
  }
  if ( changed) {
    if(( params->PortSettings.DefBaudRate != params->PortSettings.BaudRate) ||
       ( params->PortSettings.DefAddress != params->PortSettings.Address) ||
       ( params->PortSettings.DefGW != params->PortSettings.GW) ||
       ( params->PortSettings.DefDebug != params->PortSettings.Debug) ||
       ( params->PortSettings.DefProMode != params->PortSettings.ProMode)) {
      PropSheet_Changed( GetParent( hDlg), hDlg);
      EnableWindow( GetDlgItem( hDlg, IDC_RESTORE), TRUE);
    } else {
      EnableWindow( GetDlgItem( hDlg, IDC_RESTORE), FALSE);
    }
  }
}

/********************************************************************/
BOOL Port_OnInitDialog( HWND hDlg, HWND FocusHwnd, LPARAM Lparam)
{
  PPORT_PARAMS params;

  if ( Lparam) {
    params = (PPORT_PARAMS) ((LPPROPSHEETPAGE)Lparam)->lParam;
    SetWindowLongPtr(hDlg, DWLP_USER, (ULONG_PTR) params);
    FillSettingCommDlg( hDlg, params);
  }
  return TRUE;  // No need for us to set the focus.
}

/********************************************************************/
BOOL Port_OnNotify( HWND hDlg, LPNMHDR NmHdr)
{
  PPORT_PARAMS params = (PPORT_PARAMS)GetWindowLongPtr( hDlg, DWLP_USER);

  switch ( NmHdr->code) { // user click on Apply or OK
    case PSN_APPLY:
      //debugPrint("Apply");
      if ( SetPortSettings( hDlg, params)) {
        //debugPrint( "changed");
//        SetupDiCallClassInstaller( DIF_PROPERTYCHANGE, params->DeviceInfoSet, params->DeviceInfoData);
        PropSheet_Changed( GetParent( hDlg), hDlg);
      }
      SetWindowLongPtr( hDlg, DWLP_MSGRESULT, PSNRET_NOERROR);
      return TRUE;        
    default:
      return FALSE;
  }
}


/********************************************************************/
INT_PTR APIENTRY PortSettingsDlgProc( IN HWND hDlg, IN UINT uMessage, IN WPARAM wParam, IN LPARAM lParam)
{
  switch(uMessage) {
    case WM_COMMAND:
      Port_OnCommand(hDlg, (int) LOWORD(wParam), (HWND)lParam, (UINT)HIWORD(wParam));
      break;
//    case WM_CONTEXTMENU:
//      WinHelp( (HWND)wParam, sz_evbousb_help, HELP_CONTEXTMENU, (ULONG_PTR) HelpIDs);
//      return FALSE;
//    case WM_HELP: 
//      if ( ((LPHELPINFO)lParam)->iContextType == HELPINFO_WINDOW) {
//        WinHelp((HWND) ((LPHELPINFO)lParam)->hItemHandle, sz_evbousb_help, HELP_WM_HELP, (ULONG_PTR) HelpIDs);
//      }
//      break;
    case WM_INITDIALOG:
      return Port_OnInitDialog(hDlg, (HWND)wParam, lParam); 
    case WM_NOTIFY:
      return Port_OnNotify(hDlg,  (NMHDR *)lParam);
  }
  return FALSE;
} /* PortSettingsDialogProc */

/********************************************************************/
/********************************************************************/
BOOL APIENTRY PropertyPageProvider(LPVOID Info, LPFNADDPROPSHEETPAGE AddFunc, LPARAM Lparam)
{
  PSP_PROPSHEETPAGE_REQUEST pprPropPageRequest;
  PROPSHEETPAGE             psp, psp_about;
  HPROPSHEETPAGE            hpsp, hpsp_about;
  PPORT_PARAMS              params = NULL; 

  pprPropPageRequest = (PSP_PROPSHEETPAGE_REQUEST) Info;
  if (pprPropPageRequest->PageRequested == SPPSR_ENUM_ADV_DEVICE_PROPERTIES) {
    params = (PPORT_PARAMS) LocalAlloc(LPTR, sizeof(PORT_PARAMS));
    if (!params) {
       MessageBox( GetFocus(), ERROR_MEMORY_STRING, applet_name, MB_OK | MB_ICONHAND | MB_SYSTEMMODAL);
       return FALSE;
    }
    InitPortParams( params, pprPropPageRequest->DeviceInfoSet, pprPropPageRequest->DeviceInfoData);
//    if ( IsMyDevice(pprPropPageRequest->DeviceInfoSet, pprPropPageRequest->DeviceInfoData) == MY_DEVICE) {
      hpsp = InitSettingsPage(&psp, params);
      if (!hpsp) {
        return FALSE;
      }    
      if ( !(*AddFunc)( hpsp, Lparam)) {
        DestroyPropertySheetPage(hpsp);
        return FALSE;
      }
//    }
  }
  return TRUE;
}



/********************************************************************/
BOOL FillSettingCommDlg( HWND hDlg, IN PPORT_PARAMS params)
{

#define DWSIZE sizeof(DWORD)

//  SHORT shIndex;
  TCHAR str[256]; //, hlp[20];
  DWORD size, i = 0; //, j;
  LONG ret; //, idx;
//  BOOL b;
//  HANDLE hCom;
  HKEY hDeviceKey;
  HWND item;

  // defualt values
  params->PortSettings.DefBaudRate = 19200;
  params->PortSettings.DefDebug = 0;
  params->PortSettings.DefAddress = 2;
  params->PortSettings.DefGW = 1;
  params->PortSettings.DefProMode = 0;
 
  hDeviceKey = SetupDiOpenDevRegKey( params->DeviceInfoSet, params->DeviceInfoData, DICS_FLAG_GLOBAL, 0, DIREG_DEV, KEY_READ);
  if ( hDeviceKey == INVALID_HANDLE_VALUE) {
    goto FillCommDlgError;
  } else {
    size = DWSIZE;
    ret = RegQueryValueEx( hDeviceKey, ULANBAUDRATE, NULL, NULL, (PBYTE) &i, &size);
    if ( ret == ERROR_SUCCESS && size==DWSIZE) {
      params->PortSettings.DefBaudRate = i;
    }
    size = DWSIZE;
    ret = RegQueryValueEx( hDeviceKey, ULANMYADDRESS, NULL, NULL, (PBYTE) &i, &size);
    if ( ret == ERROR_SUCCESS && size==DWSIZE) {
      params->PortSettings.DefAddress = i;
    }
    size = DWSIZE;
    ret = RegQueryValueEx( hDeviceKey, ULANGWADDRESS, NULL, NULL, (PBYTE) &i, &size);
    if ( ret == ERROR_SUCCESS && size==DWSIZE) {
      params->PortSettings.DefGW = i;
    }
    size = DWSIZE;
    ret = RegQueryValueEx( hDeviceKey, ULANDEBUG, NULL, NULL, (PBYTE) &i, &size);
    if ( ret == ERROR_SUCCESS && size==DWSIZE) {
      params->PortSettings.DefDebug = i;
    }
    size = DWSIZE;
    ret = RegQueryValueEx( hDeviceKey, ULANPROMODE, NULL, NULL, (PBYTE) &i, &size);
    if ( ret == ERROR_SUCCESS && size==DWSIZE) {
      params->PortSettings.DefProMode = i;
    }
    RegCloseKey( hDeviceKey);
  }

  i = 0; item = GetDlgItem( hDlg, PP_ULAN_BAUD);
  ret = 0; // def.select
  while( baud_tbl[i]) {
    wsprintf( str, L"%d", baud_tbl[i]);
    SendMessage( item, CB_ADDSTRING, 0, (LPARAM)str);
    if ( params->PortSettings.DefBaudRate == baud_tbl[i]) 
      ret = i;
	  i++;
  }
  SendMessage( item, CB_SETCURSEL, ret, 0);
  params->PortSettings.BaudRate = baud_tbl[ret];

  item = GetDlgItem( hDlg, PP_ULAN_ADDR);
  wsprintf( str, L"%d", params->PortSettings.DefAddress);
  SetWindowText( item, str);
  SendMessage( item, EM_LIMITTEXT, 3, 0); // limit text length
  params->PortSettings.Address = params->PortSettings.DefAddress;

  item = GetDlgItem( hDlg, PP_ULAN_GW);
  wsprintf( str, L"%d", params->PortSettings.DefGW);
  SetWindowText( item, str);
  SendMessage( item, EM_LIMITTEXT, 3, 0); // limit text length
  params->PortSettings.GW = params->PortSettings.DefGW;

  i = 0; item = GetDlgItem( hDlg, PP_ULAN_DEBUG);
  ret = 0; // def.select
  while( debug_tbl[i]) {
    SendMessage( item, CB_ADDSTRING, 0, (LPARAM)debug_tbl[i]);
	  i++;
  }
  SendMessage( item, CB_SETCURSEL, params->PortSettings.DefDebug, 0);
  params->PortSettings.Debug = params->PortSettings.DefDebug;

  
  CheckDlgButton( hDlg, PP_ULAN_PMODE, ( params->PortSettings.DefProMode) ? BST_CHECKED : BST_UNCHECKED);
  params->PortSettings.ProMode = params->PortSettings.DefProMode;

  EnableWindow( GetDlgItem( hDlg, IDC_RESTORE), FALSE);
//  ShowWindow( GetDlgItem( hDlg, IDC_BOK), SW_HIDE);
  return TRUE;
 FillCommDlgError:
  SetVisibleAndEnable( hDlg, FALSE, FALSE);
  return FALSE;
} /* FillCommDlg */


/********************************************************************/
void RestorePortSettings( HWND hDlg, PPORT_PARAMS params)
{
  TCHAR str[20];
  int ret;

  // Baudrate
  wsprintf( str, L"%d", params->PortSettings.DefBaudRate);
  ret = (int) SendDlgItemMessage( hDlg, PP_ULAN_BAUD, CB_FINDSTRING, -1, (LPARAM) str);
  SendDlgItemMessage( hDlg, PP_ULAN_BAUD, CB_SETCURSEL, ret, 0L);
  params->PortSettings.BaudRate = params->PortSettings.DefBaudRate;
  // Address
  wsprintf( str, L"%d", params->PortSettings.DefAddress);
  SetWindowText( GetDlgItem( hDlg, PP_ULAN_ADDR), str);
  params->PortSettings.Address = params->PortSettings.DefAddress;
  // GW
  wsprintf( str, L"%d", params->PortSettings.DefGW);
  SetWindowText( GetDlgItem( hDlg, PP_ULAN_GW), str);
  params->PortSettings.GW = params->PortSettings.DefGW;
  // Debug
  SendDlgItemMessage( hDlg, PP_ULAN_DEBUG, CB_SETCURSEL, params->PortSettings.DefDebug, 0L);
  params->PortSettings.Debug = params->PortSettings.DefDebug;
  // ProMode
  CheckDlgButton( hDlg, PP_ULAN_PMODE, ( params->PortSettings.DefProMode) ? BST_CHECKED : BST_UNCHECKED);
  params->PortSettings.ProMode = params->PortSettings.DefProMode;
}

/********************************************************************/
BOOL SetPortSettings( IN HWND hDlg, IN PPORT_PARAMS params)
{
  TCHAR  str[20];
  DWORD  size, val;
  HKEY   hDeviceKey = NULL;
  LONG   ret;
/*
wsprintf( str,L"D:0x%X,%d,%d,%d -> N:0x%X,%d,%d,%d", params->PortSettings.DefBaudRate, params->PortSettings.DefDataBits,
          params->PortSettings.DefParity,params->PortSettings.DefStopBits,params->PortSettings.BaudRate,
          params->PortSettings.DataBits,params->PortSettings.Parity,params->PortSettings.StopBits);
MessageBox( hDlg, str,L"TEST - parm",MB_OK);
*/
  // quick return - no change
  if (( params->PortSettings.DefBaudRate == params->PortSettings.BaudRate) &&
      ( params->PortSettings.DefAddress  == params->PortSettings.Address) &&
      ( params->PortSettings.DefGW  == params->PortSettings.GW) &&
      ( params->PortSettings.DefDebug    == params->PortSettings.Debug) &&
      ( params->PortSettings.DefProMode  == params->PortSettings.ProMode)) {
    return FALSE;
  }

  hDeviceKey = SetupDiOpenDevRegKey( params->DeviceInfoSet, params->DeviceInfoData, DICS_FLAG_GLOBAL, 0, DIREG_DEV, KEY_WRITE);
  if ( hDeviceKey == INVALID_HANDLE_VALUE) {
    goto SetPortSettingsError;
  }
  // BaudRate
//  if ( params->PortSettings.DefBaudRate != params->PortSettings.BaudRate) {
    GetWindowText( GetDlgItem( hDlg, PP_ULAN_BAUD), str, 20);
    val = myatoi( str);
    ret = RegSetValueEx( hDeviceKey, ULANBAUDRATE, 0, REG_DWORD, (BYTE *) &val, sizeof(val));
    if ( ret != ERROR_SUCCESS) goto SetPortSettingsError;      
//  }
  // Address
//  if ( params->PortSettings.DefAddress != params->PortSettings.Address) {
    GetWindowText( GetDlgItem( hDlg, PP_ULAN_ADDR), str, 20);
    val = myatoi( str);
    ret = RegSetValueEx( hDeviceKey, ULANMYADDRESS, 0, REG_DWORD, (BYTE *) &val, sizeof(val));
    if ( ret != ERROR_SUCCESS) goto SetPortSettingsError;      
//  }
  // GW
//  if ( params->PortSettings.DefGW != params->PortSettings.GW) {
    GetWindowText( GetDlgItem( hDlg, PP_ULAN_GW), str, 20);
    val = myatoi( str);
    ret = RegSetValueEx( hDeviceKey, ULANGWADDRESS, 0, REG_DWORD, (BYTE *) &val, sizeof(val));
    if ( ret != ERROR_SUCCESS) goto SetPortSettingsError;      
//  }
  // Debug
//  if ( params->PortSettings.DefDebug != params->PortSettings.Debug) {
    val = (DWORD) SendDlgItemMessage( hDlg, PP_ULAN_DEBUG, CB_GETCURSEL, 0 ,0);
    ret = RegSetValueEx( hDeviceKey, ULANDEBUG, 0, REG_DWORD, (BYTE *) &val, sizeof(val));
    if ( ret != ERROR_SUCCESS) goto SetPortSettingsError;      
//  }
  // ProMode
//  if ( params->PortSettings.DefProMode != params->PortSettings.ProMode) {
    if( IsDlgButtonChecked( hDlg, PP_ULAN_PMODE) == BST_CHECKED)
      val = 1;
    else
      val = 0;
    ret = RegSetValueEx( hDeviceKey, ULANPROMODE, 0, REG_DWORD, (BYTE *) &val, sizeof(val));
    if ( ret != ERROR_SUCCESS) goto SetPortSettingsError;      
//  }

  RegCloseKey( hDeviceKey);
  return TRUE;
 SetPortSettingsError:
  if ( hDeviceKey) RegCloseKey( hDeviceKey);
  return FALSE;
}

/********************************************************************/
VOID SetVisibleAndEnable( HWND hDlg, BOOL chng, BOOL ok) {
  if ( !chng) {
    EnableWindow( GetDlgItem( hDlg, PP_ULAN_BAUD), FALSE);
    EnableWindow( GetDlgItem( hDlg, PP_ULAN_ADDR), FALSE);
    EnableWindow( GetDlgItem( hDlg, PP_ULAN_GW), FALSE);
    EnableWindow( GetDlgItem( hDlg, PP_ULAN_DEBUG), FALSE);
    EnableWindow( GetDlgItem( hDlg, PP_ULAN_PMODE), FALSE);

    EnableWindow( GetDlgItem( hDlg, IDC_LBAUD), FALSE);
    EnableWindow( GetDlgItem( hDlg, IDC_LADDR), FALSE);
    EnableWindow( GetDlgItem( hDlg, IDC_LGW), FALSE);
    EnableWindow( GetDlgItem( hDlg, IDC_LDEBUG), FALSE);

    EnableWindow( GetDlgItem( hDlg, IDC_RESTORE), FALSE);
    EnableWindow( GetDlgItem(  hDlg, IDC_GSET), FALSE);
  }
//  if ( !ok) {
//    ShowWindow( GetDlgItem( hDlg, IDC_BOK), SW_HIDE);
//  }
}



/********************************************************************/
int myatoi(LPTSTR pszInt)
{
  int retval = 0;
  if ( pszInt) {
    while ( *pszInt) {
      if (( *pszInt >= TEXT('0')) && ( *pszInt <= TEXT('9'))) {
        retval = retval * 10 + (int)(*pszInt-TEXT('0'));
      }
      pszInt++;
    }
  }
  return (retval);
}
