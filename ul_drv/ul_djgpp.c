/*******************************************************************
  uLan Communication - uL_DRV - multiplatform uLan driver

  ul_djgpp.c	- DOS DJGPP specific routines

  (C) Copyright 1996-2004 by Pavel Pisa - project originator
        http://cmp.felk.cvut.cz/~pisa
  (C) Copyright 1996-2004 PiKRON Ltd.
        http://www.pikron.com
  (C) Copyright 2002-2004 Petr Smolik
  

  The uLan driver project can be used and distributed 
  in compliance with any of next licenses
   - GPL - GNU Public License
     See file COPYING for details.
   - LGPL - Lesser GNU Public License
   - MPL - Mozilla Public License
   - and other licenses added by project originator

  Code can be modified and re-distributed under any combination
  of the above listed licenses. If contributor does not agree with
  some of the licenses, he/she can delete appropriate line.
  WARNING: if you delete all lines, you are not allowed to
  distribute code or sources in any form.
 *******************************************************************/

#include <go32.h>

#ifdef ENABLE_UL_MEM_CHECK
void * ul_mem_check_malloc(size_t size)
{ void *ptr;
  ptr=malloc(size);
  if(ptr){
    ul_mem_check_counter++;
    _go32_dpmi_lock_data(ptr,size);
  }
  return ptr;
}

void ul_mem_check_free(void *ptr)
{
  if(!ptr) {
    LOG_FATAL(KERN_CRIT "ul_mem_check_free : triing to free NULL ptr\n");
  }else{
    ul_mem_check_counter--;
    free(ptr);
  }
}
#endif /* ENABLE_UL_MEM_CHECK */

static _go32_dpmi_seginfo wrapper;
static __dpmi_paddr old_handler, new_handler;
uchar pic1_old, pic2_old, pic_mask;
int pm_irq;

void *uld_djgpp_dev_id=NULL;

int uld_djgpp_irq_mask_set(int irqline, int enable)
{
  unsigned long irq_flags;
  unsigned mask;
  if(irqline>=16) return -1;
  __save_flags(irq_flags);__cli();
  if(irqline<8){
    mask=inportb(0x21);
    mask=enable?mask&~(1<<irqline):mask|(1<<irqline);
    outportb(0x21,mask);                /* Change IRQ mask */
  }else{
    outportb(0x21,inportb(0x21)&0xFB);  /* Enable IRQ2 */
    mask=inportb(0xA1);
    mask=enable?mask&~(1<<(irqline-8)):mask|(1<<(irqline-8));
    outportb(0xA1,mask);                /* Change IRQ mask */
  }
  __restore_flags(irq_flags);
  return 0;
}

void uld_djgpp_irq_eoi(int irqline)
{
  if(irqline<8){
    outportb(0x20,0x20|0x40|irqline);
  }else{
    outportb(0xA0,0x20|0x40|(irqline&7));
    outportb(0x20,0x20|0x40|0x2);
  }
}


/* !!!!!!!!!!!! */
void uld_djgpp_call_irq(void)
{
  return;
  if(ul_drv_arr[0])
    uld_irq_handler(0, ul_drv_arr[0], NULL);
}

void uld_djgpp_proc_irq(void)
{
  if(uld_djgpp_dev_id){
    uld_irq_handler(0, uld_djgpp_dev_id, NULL);
    uld_djgpp_irq_eoi(((ul_drv*)uld_djgpp_dev_id)->irq);
  }else{
    outportb(0xA0,0x20);
    outportb(0x20,0x20);
  }
}

int request_irq(int irq,void *handler,int flags,char *name,void *dev_id)
{
 #if 1
  /* !!!!!!!!!!!! */
  handler=uld_djgpp_proc_irq;
  uld_djgpp_dev_id=dev_id;

  pic1_old=inportb(0x21);
  pic2_old=inportb(0xA1);

  if(irq<8) {
    pm_irq=irq+0x08;
  } else {
    pm_irq=irq+0x68;
  }
  uld_djgpp_irq_mask_set(irq,0);

  wrapper.pm_offset=(int)handler;
  wrapper.pm_selector=_my_cs();
  _go32_dpmi_allocate_iret_wrapper(&wrapper);
  new_handler.offset32=wrapper.pm_offset;
  new_handler.selector=wrapper.pm_selector;
  __dpmi_get_protected_mode_interrupt_vector(pm_irq,&old_handler);
  if(__dpmi_set_protected_mode_interrupt_vector(pm_irq,&new_handler)<0)
  { LOG_FATAL("uLan request_irq: DPMI set interrupt failed\n");
    return -1;
  };
  uld_djgpp_irq_mask_set(irq,1);

 #endif
  return irq;
}

int free_irq(int irq,void *ctx)
{
  uld_djgpp_irq_mask_set(irq,0);
  __dpmi_set_protected_mode_interrupt_vector(irq,&old_handler);

  outportb(0x21,pic1_old);
  outportb(0xA1,pic2_old);
  _go32_dpmi_free_iret_wrapper(&wrapper);
  return 0;
}


#include <crt0.h>
int _crt0_startup_flags = _CRT0_FLAG_LOCK_MEMORY;
int uld_djgpp_data_end;
void uld_djgpp_code_end(void) {;};

void uld_djgpp_do_atexit(void)
{ int i;
  for(i=0;i<UL_MINORS;i++)
  {
    if(ul_drv_arr[i])
    {
      ul_drv_free(ul_drv_arr[i]);
      ul_drv_arr[i]=NULL;
    }
  }
  UL_PRINTF("ulan_driver : atexit finished driver\n");
  #ifdef ENABLE_UL_MEM_CHECK
    UL_PRINTF("              mem_check_counter = %ld\n",ul_mem_check_counter);
  #endif /* ENABLE_UL_MEM_CHECK */
  uld_printk_flush();
}

void uld_djgpp_do_atinit(void) __attribute__((constructor));

void uld_djgpp_do_atinit(void)
{
  if(!uld_debug_flg) uld_debug_flg=ULD_DEBUG_DEFAULT;
  if((_go32_dpmi_lock_code(&uld_djgpp_code_beg,(char*)&uld_djgpp_code_end-(char*)&uld_djgpp_code_beg))||
     (_go32_dpmi_lock_data(&uld_djgpp_data_beg,(char*)&uld_djgpp_data_end-(char*)&uld_djgpp_data_beg)))
  {
    UL_PRINTF("ulan_driver : Cannot lock code and data in memory !\n");
  }
  atexit(uld_djgpp_do_atexit);
  uld_check_timeout();

  UL_PRINTF("ulan_driver : atinit finished\n");
  uld_printk_flush();
}

