/*******************************************************************
  uLan Communication - user visible definitions

  ul_fd4inlib.h	- net access if driver directly linked as library

  (C) Copyright 1996-2016 by Pavel Pisa - project originator
        http://cmp.felk.cvut.cz/~pisa
  (C) Copyright 1996-2016 PiKRON Ltd.
        http://www.pikron.com
  (C) Copyright 2002-2016 Petr Smolik


  The uLan driver project can be used and distributed
  in compliance with any of next licenses
   - GPL - GNU Public License
     See file COPYING for details.
   - LGPL - Lesser GNU Public License
   - MPL - Mozilla Public License
   - and other licenses added by project originator

  Code can be modified and re-distributed under any combination
  of the above listed licenses. If contributor does not agree with
  some of the licenses, he/she can delete appropriate line.
  WARNING: if you delete all lines, you are not allowed to
  distribute code or sources in any form.
 *******************************************************************/

#ifndef _UL_FD_H
#define _UL_FD_H

/* The applications calls directly functions provided by driver code in library */
#undef WITH_UL_FD_INDIRECT
#define UL_DRV_IN_LIB
#define WITHOUT_SYS_SELECT

#ifdef __cplusplus
extern "C" {
#endif

#ifdef __DJGPP__
  #define UL_DEV_NAME "1"
#else /* !__DJGPP__ */
  #define UL_DEV_NAME "/dev/ulan"
#endif  /* !__DJGPP__ */

struct ul_opdata;
typedef struct ul_opdata *ul_fd_direct_t;
#define UL_FD_DIRECT_INVALID (NULL)

typedef ul_fd_direct_t ul_fd_t;
#define UL_FD_INVALID UL_FD_DIRECT_INVALID

static inline ul_fd_direct_t ul_fd2sys_fd(ul_fd_t fd)
{
  return fd;
}

#ifdef __cplusplus
} /* extern "C"*/
#endif

#endif /*_UL_FD_H*/
