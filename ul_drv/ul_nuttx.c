/*******************************************************************
  uLan Communication - uL_DRV - multiplatform uLan driver

  ul_nuttx.c	- NuttX kernel and module specific part

  (C) Copyright 1996-2019 by Pavel Pisa - project originator
        http://cmp.felk.cvut.cz/~pisa
  (C) Copyright 1996-2019 PiKRON Ltd.
        http://www.pikron.com
  (C) Copyright 2002-2004 Petr Smolik


  The uLan driver project can be used and distributed
  in compliance with any of next licenses
   - GPL - GNU Public License
     See file COPYING for details.
   - LGPL - Lesser GNU Public License
   - MPL - Mozilla Public License
   - and other licenses added by project originator

  Code can be modified and re-distributed under any combination
  of the above listed licenses. If contributor does not agree with
  some of the licenses, he/she can delete appropriate line.
  WARNING: if you delete all lines, you are not allowed to
  distribute code or sources in any form.
 *******************************************************************/

/*******************************************************************/
/* NuttX kernel and module specific part */

#include <nuttx/module.h>
#include <nuttx/lib/modlib.h>
#include <nuttx/arch.h>
#include <nuttx/signal.h>
#include <nuttx/semaphore.h>
#include <nuttx/fs/fs.h>
#include <nuttx/can/can.h>
#include <nuttx/kmalloc.h>
#include <nuttx/wqueue.h>
#include <nuttx/irq.h>
#include <assert.h>

static inline
int kc_copy_from_user(FAR void *dest, FAR const void *src, size_t n)
{
  memcpy(dest, src, n);
  return 0;
}

static inline
int kc_copy_to_user(FAR void *dest, FAR const void *src, size_t n)
{
  memcpy(dest, src, n);
  return 0;
}

#define kc_get_user_byte(ch,addr) ((ch)=*(unsigned char*)(addr), 0)
#define kc_put_user(val,addr) (*(addr)=(val), 0)
#define kc_capable(cap) (1)
#define kc_yield() do { } while(0)

int ul_io_map(ul_physaddr_t physaddr, unsigned long len, const char *name, ul_ioaddr_t *ioaddr)
{
  ioaddr->ioaddr = (void *)physaddr.physaddr;
  return 0;
}

void ul_io_unmap(ul_physaddr_t physaddr, unsigned long len, ul_ioaddr_t ioaddr)
{
}

#define UL_MINORS 4
ul_drv *ul_drv_arr[UL_MINORS]={[0 ... UL_MINORS-1]=NULL};

static int     ulan_open(FAR struct file *filep);
static int     ulan_close(FAR struct file *filep);
static ssize_t ulan_read(FAR struct file *filep, FAR char *buffer, size_t buflen);
static ssize_t ulan_write(FAR struct file *filep, FAR const char *buffer, size_t buflen);
static off_t   ulan_seek(FAR struct file *filep, off_t offset, int whence);
static int     ulan_ioctl(FAR struct file *filep, int cmd, unsigned long arg);
static int     ulan_poll(FAR struct file *filep, struct pollfd *fds, bool setup);

static int module_uninitialize(FAR void *arg);

/* static struct file_operations ulan_fops */
static const struct file_operations ulan_fops =
{
	.open  = ulan_open,	/* open                */
	.close = ulan_close,	/* close/release    */
	.read  = ulan_read,	/* read                */
	.write = ulan_write,	/* write               */
	.seek  = ulan_seek,	/* lseek              */
	.ioctl = ulan_ioctl,	/* ioctl               */
	.poll  = ulan_poll,	/* poll                */
};

/* open ulan device */
static int ulan_open(FAR struct file *filep)
{
  FAR struct inode *inode = filep->f_inode;
  ul_drv *udrv = (ul_drv *)inode->i_private;
  ul_opdata *opdata;
  if (!udrv) return -ENODEV;
  if(udrv->magic!=UL_DRV_MAGIC)
  {
    LOG_FATAL(KERN_CRIT "ulan_open: Wrong uLan MAGIC number!!!\n");
    return -ENODEV;
  }

  LOG_FILEIO(KERN_INFO "ulan_open:\n");

  if(!(filep->f_priv=MALLOC(sizeof(ul_opdata)))) {
    return(-ENOMEM);
  };
  opdata=(ul_opdata*)filep->f_priv;
  opdata->file=filep;
  opdata->magic=ULOP_MAGIC;
  opdata->message=NULL;
  opdata->udrv=udrv;
  opdata->subdevidx=0;
  opdata->pro_mode=0;
  nxsem_init(&opdata->ul_op_pollsem, 0, 1);
  nxsem_set_protocol(&opdata->ul_op_pollsem, SEM_PRIO_INHERIT);
  opdata->ul_op_pollfds=NULL;
  opdata->opprew=NULL;
  opdata->opnext=NULL;
  opdata->recchain=NULL;
  opdata->filtchain=NULL;
  { /* add us onto list of clients of udrv */
    ul_opdata *opptr;
    UL_DRV_LOCK_FINI
    UL_DRV_LOCK;
    opptr=udrv->operators;
    if(opptr) {opptr->opprew=opdata;opdata->opnext=opptr;};
    UL_MB();
    udrv->operators=opdata;
    UL_DRV_UNLOCK;
  };

  return 0;
};

static int ulan_close(FAR struct file *filep)
{
  ul_opchain *opmember;
  ul_opdata *opdata=(ul_opdata*)filep->f_priv;
  if(opdata->magic!=ULOP_MAGIC)
  {
    LOG_FATAL(KERN_CRIT "ulan_close : BAD opdata magic !!!\n");
    PANIC();
  }

  LOG_FILEIO(KERN_INFO "ulan_close:\n");

  if (opdata->message) ulan_freemsg(opdata);
  { /* delete us from list of clients of udrv */
    ul_drv *udrv;
    ul_opdata *opptr;
    UL_DRV_LOCK_FINI
    UL_DRV_LOCK;
    if((udrv=opdata->udrv)!=NULL){
      if((opptr=opdata->opnext)) opptr->opprew=opdata->opprew;
      if((opptr=opdata->opprew)) opptr->opnext=opdata->opnext;
      else udrv->operators=opdata->opnext;
    }
    UL_DRV_UNLOCK;
    if(udrv)
      if(udrv->irq>0) ul_synchronize_irq(udrv->irq);
    while((udrv=opdata->udrv)!=NULL){
      if(!uld_atomic_test_dfl(udrv,IN_BOTTOM))
        break;
      kc_yield();
    }
  }
  while((opmember=opdata->recchain))
  {
    del_from_opchain(&opdata->recchain,opmember);
    if(opmember->message) ul_dec_ref_cnt(opmember->message);
    FREE(opmember);
  };
  while((opmember=opdata->filtchain))
  {
    del_from_opchain(&opdata->filtchain,opmember);
    FREE(opmember);
  };
  nxsem_wait(&opdata->ul_op_pollsem);
  if (opdata->ul_op_pollfds!=NULL) {
    poll_notify(&opdata->ul_op_pollfds, 1, POLLERR);
    opdata->ul_op_pollfds=NULL;
  }
  nxsem_post(&opdata->ul_op_pollsem);
  nxsem_destroy(&opdata->ul_op_pollsem);
  FREE(filep->f_priv);
  return 0;
};

static int ulan_ioctl(FAR struct file *filep, int cmd, unsigned long arg)
{
  ul_msginfo msginfo;
  int ret;
  ul_opdata *opdata=(ul_opdata*)filep->f_priv;
  if(opdata->magic!=ULOP_MAGIC)
  {
    LOG_FATAL(KERN_CRIT "ulan_close : BAD opdata magic !!!\n");
    PANIC();
  }

  LOG_FILEIO(KERN_INFO "ulan_ioctl:IOCTL %d\n", cmd);

  switch (cmd) {
   case UL_DRV_VER :
	return UL_DRV_VERCODE;
   case UL_NEWMSG :
	if(kc_copy_from_user(&msginfo,(void *)arg,sizeof(msginfo)))
	   return -EFAULT;
	return ulan_newmsg(opdata,&msginfo);
   case UL_TAILMSG :
	if(kc_copy_from_user(&msginfo,(void *)arg,sizeof(msginfo)))
	   return -EFAULT;
	return ulan_tailmsg(opdata,&msginfo);
   case UL_FREEMSG :
	return ulan_freemsg(opdata);
   case UL_ACCEPTMSG :
	if(kc_copy_to_user((void *)arg,&msginfo,sizeof(msginfo)))
	   return -EFAULT;
	ret=ulan_acceptmsg(opdata,&msginfo);
	if(kc_copy_to_user((void *)arg,&msginfo,sizeof(msginfo)))
	   return -EFAULT;
	return  ret;
   case UL_ACTAILMSG :
	if(kc_copy_to_user((void *)arg,&msginfo,sizeof(msginfo)))
	   return -EFAULT;
	ret=ulan_actailmsg(opdata,&msginfo);
	if(kc_copy_to_user((void *)arg,&msginfo,sizeof(msginfo)))
	   return -EFAULT;
	return  ret;
   case UL_ADDFILT :
	if(kc_copy_from_user(&msginfo,(void *)arg,sizeof(msginfo)))
	   return -EFAULT;
	return ulan_addfilt(opdata,&msginfo);
   case UL_ABORTMSG :
	return ulan_abortmsg(opdata);
   case UL_REWMSG :
	return ulan_rewmsg(opdata);
   case UL_KLOGBLL :
        if(!opdata->udrv) return -ENODEV;
	printudrvbll(opdata->udrv);
    #ifdef ENABLE_UL_MEM_CHECK
	printudrvoperators(opdata->udrv);
    #endif /* ENABLE_UL_MEM_CHECK */
	return 0;
   case UL_STROKE :
        if(!opdata->udrv) return -ENODEV;
	ulan_stroke(opdata->udrv, 1);
	return 0;
   case UL_DEBFLG :
	uld_debug_flg=arg;
	return 0;
   case UL_INEPOLL :
        return ulan_inepoll(opdata);
   case UL_SETMYADR :
        return ulan_setmyadr(opdata,arg);
  #ifdef UL_WITH_IAC
   case UL_SETIDSTR :{
	  char idstr[41];
	  char ch;
	  int ln=0;
	  do{
	    if(kc_get_user_byte(ch,(char*)arg))
	      return -EFAULT;
	    arg++;
	    if(ln>=sizeof(idstr)-1)
	      return -EINVAL;
	    idstr[ln++]=ch;
	  }while(ch);
	  idstr[ln]=0;
          return ulan_setidstr(opdata,idstr);
	}
  #endif /*UL_WITH_IAC*/
   case UL_SETBAUDRATE :
        return ulan_setbaudrate(opdata,arg);
   case UL_SETPROMODE :
        return ulan_setpromode(opdata,arg);
  #ifdef CONFIG_OC_UL_DRV_WITH_MULTI_DEV
   case UL_SETSUBDEV :
        return ulan_setsubdev(opdata,arg);
  #endif /*CONFIG_OC_UL_DRV_WITH_MULTI_DEV*/
   case UL_QUERYPARAM :{
          unsigned long value;
          if(ulan_queryparam(opdata,arg,&value) < 0)
            return -EINVAL;
          return value;
        }
  #ifdef CONFIG_OC_UL_DRV_WITH_MULTI_NET
   case UL_ROUTE :{
        ul_route_range_t rr;
	if(kc_copy_from_user(&rr,(void *)arg,sizeof(rr)))
	   return -EFAULT;
        ret=ulan_route(opdata,&rr);
	if(kc_copy_to_user((void *)arg,&rr,sizeof(rr)))
	   return -EFAULT;
        return ret;
        }
  #endif /*CONFIG_OC_UL_DRV_WITH_MULTI_NET*/

   case UL_HWTEST :
        if(!kc_capable(CAP_SYS_ADMIN)) return -EPERM;
	return ulan_hwtest(opdata,arg);
  };


  return -EINVAL;
};

static ssize_t ulan_write(FAR struct file *filep, FAR const char *buf, size_t count)
{
  int cn;
  int len;
  ul_mem_blk *blk;
  ul_opdata *opdata=(ul_opdata*)filep->f_priv;
  if(opdata->magic!=ULOP_MAGIC)
  {
    LOG_FATAL(KERN_CRIT "ulan_write : BAD opdata magic !!!\n");
    PANIC();
  }

  LOG_FILEIO(KERN_INFO "ulan_write: size %d\n", (int)count);

  if(!opdata->udrv) return -ENODEV;
  if(!opdata->message) return -ENOMSG;

  cn=count;
  while(cn>0)
  {
    while(!ul_di_adjust(&opdata->data))
    {
      if(!(blk=ul_alloc_blk(opdata->udrv)))
	{count-=cn;cn=0;break;};
      memset(UL_BLK_NDATA(blk),0,UL_BLK_SIZE);
      opdata->data.blk->next=blk;
    };
    len=ul_di_atonce(&opdata->data);
    if(len>cn) len=cn;
    if(kc_copy_from_user(ul_di_byte(&opdata->data),buf,len))
      return -EFAULT;
    ul_di_add(&opdata->data,len);
    buf+=len;
    cn-=len;
  };

  if(opdata->data.pos>UL_BLK_HEAD(opdata->data.head_blk).len)
    UL_BLK_HEAD(opdata->data.head_blk).len=opdata->data.pos;

  return count-cn;
};

static ssize_t ulan_read(FAR struct file *filep, FAR char *buf, size_t count)
{
  int cn;
  int len;
  ul_opdata *opdata=(ul_opdata*)filep->f_priv;
  if(opdata->magic!=ULOP_MAGIC)
  {
    LOG_FATAL(KERN_CRIT "ulan_read : BAD opdata magic !!!\n");
    PANIC();
  }

  LOG_FILEIO(KERN_INFO "ulan_read: size %d\n", (int)count);

  if(!opdata->udrv) return -ENODEV;
  if(!opdata->message) return -ENOMSG;

  if(opdata->data.pos+count>UL_BLK_HEAD(opdata->data.head_blk).len)
   count=UL_BLK_HEAD(opdata->data.head_blk).len-opdata->data.pos;

  cn=count;
  while(cn>0)
  {
    if(!ul_di_adjust(&opdata->data))
    {
      while(cn--)
        if (kc_put_user(0,buf++))
          return -EFAULT;
      break;
    };
    len=ul_di_atonce(&opdata->data);
    if(len>cn) len=cn;
    if(kc_copy_to_user(buf,ul_di_byte(&opdata->data),len))
      return -EFAULT;
    ul_di_add(&opdata->data,len);
    buf+=len;
    cn-=len;
  };

  return count-cn;
};

static off_t ulan_seek(FAR struct file *filep, off_t pos, int whence)
{
  ul_opdata *opdata=(ul_opdata*)filep->f_priv;
  if(opdata->magic!=ULOP_MAGIC)
  {
    LOG_FATAL(KERN_CRIT "ulan_seek : BAD opdata magic !!!\n");
    PANIC();
  }

  LOG_FILEIO(KERN_INFO "ulan_seek:\n");

  if(!opdata->message) return -ENOMSG;

  return ul_di_seek(&opdata->data,pos,whence);
};

static int ulan_poll(FAR struct file *filep, struct pollfd *fds, bool setup)
{
  /* POLLIN | POLLOUT | POLLERR | POLLHUP | POLLRDNORM | POLLWRNORM */
  int ret;
  ul_opdata *opdata=(ul_opdata*)filep->f_priv;
  if(opdata->magic!=ULOP_MAGIC)
  {
    LOG_FATAL(KERN_CRIT "ulan_poll : BAD opdata magic !!!\n");
    PANIC();
  }

  LOG_FILEIO(KERN_INFO "ulan_poll:\n");

  ret = nxsem_wait(&opdata->ul_op_pollsem);
  if (ret < 0)
    return ret;
  if (setup) {
    if (opdata->ul_op_pollfds!=NULL) {
       poll_notify(&opdata->ul_op_pollfds, 1, POLLERR);
    }
    opdata->ul_op_pollfds=fds;
  }
  if (opdata->ul_op_pollfds) {
    if (!opdata->udrv)
      opdata->ul_op_pollfds->revents |= (POLLIN | POLLOUT | POLLERR);
    if (opdata->recchain)
      opdata->ul_op_pollfds->revents |= (POLLIN | POLLRDNORM);
  }
  if (!setup)
    opdata->ul_op_pollfds=NULL;
  nxsem_post(&opdata->ul_op_pollsem);

  return OK;
};

int ulan_bottom_task(int argc, FAR char *argv[])
{
  ul_drv* udrv;
  uintptr_t udrv_uint = 0;
  char *p;
  int i = 0;

  if ((argc < 2) || (argv[1] == NULL)) {
    syslog(KERN_ERR "ulan_bottom_task: argc < 2\n");
    return -EINVAL;
  }

  p = argv[1];
  while (*p) {
    udrv_uint |= (*(p++) - '0') << i;
    i += 6;
  }

#if 0
  syslog(KERN_INFO "ulan_bottom_task: udrv = 0x%lx\n", (unsigned long)udrv_uint);
#endif

  udrv = (ul_drv*)udrv_uint;
  if(udrv->magic!=UL_DRV_MAGIC) {
    syslog(KERN_CRIT "ulan_bottom_task: Wrong uLan MAGIC number!!!\n");
    return -ENODEV;
  }

  syslog(KERN_INFO "ulan_bottom_task: started\n");

  while (nxsem_wait(&udrv->bottom_sem) >= 0) {
    ulan_do_bh(udrv);
  }

  syslog(KERN_INFO "ulan_bottom_task: finished\n");

  return 0;
}

int ulan_create_bottom_task(ul_drv* udrv)
{
  char udrv_str[12];
  char *args[2] = {udrv_str, NULL};
  int ret;
  uintptr_t udrv_uint = (uintptr_t)udrv;
  char *p = udrv_str;

  while (udrv_uint) {
    *(p++) = (udrv_uint & 0x3f) + '0';
    udrv_uint >>= 6;
  }
  *(p++) = 0;

#if 0
  syslog(KERN_INFO "ulan_create_bottom_task: udrv = 0x%lx udrv_str = %p = %s\n",
         (unsigned long)udrv, udrv_str, udrv_str);
#endif

  ret = kthread_create("ulan_worker", sched_get_priority_max(SCHED_FIFO),
                       1024 /*stack*/, ulan_bottom_task, args);

  return ret;
}

/*** initialize kernel module ***/

int module_initialize(FAR struct mod_info_s *modinfo)
{
  char devname[16];
  ul_drv* udrv;
  int ret;
  int minor;

  syslog(LOG_INFO, "uLan v" UL_DRV_VERSION " init_module\n");
  syslog(LOG_INFO, "ul_drv module_initialize at %p\n", module_initialize);

  if (modinfo != NULL) {
    modinfo->uninitializer = module_uninitialize;
    modinfo->arg           = NULL;
    modinfo->exports       = NULL;
    modinfo->nexports      = 0;
  }

  uld_debug_flg = 0x11;

  ret=0;
 #ifdef UL_WITH_UARTS
  for(minor=0;minor<UL_MINORS;minor++)
  {
    struct nuttx_ulan_chip_data_s chip_data;
    if (nuttx_ulan_get_chip_data(minor, &chip_data) > 0) {
     /* get new ul_drv (port, irq, baud, my_adr); */
      udrv=ul_drv_new_paddr(ul_uint2physaddr(chip_data.port), chip_data.irq,
                      chip_data.baud, chip_data.my_adr,
                      chip_data.chip, chip_data.baudbase);
      ul_drv_arr[minor]=udrv;
      if(udrv){
        sprintf(devname, minor? "/dev/ulan%d": "/dev/ulan", minor);
        ret++;
        register_driver("/dev/ulan", &ulan_fops, 0666, udrv);
      }
    }
  }
 #endif /*UL_WITH_UARTS*/

  if(!ret) {
    syslog(KERN_ERR "uLan module_init : no hardware initialized, aborting !!!\n");
    ret = -ENODEV;
    goto error_no_hardware;
  }

  syslog(KERN_INFO "uLan init : driver succesfully registered\n");

  #ifdef UL_INT_TST
  udrv=ul_drv_arr[0];
  if(udrv)
  { syslog(KERN_INFO "uLan : geninfoblk\n");
    geninfoblk(udrv);
    syslog(KERN_INFO "uLan : gentestblk\n");
    gentestblk(udrv);
    syslog(KERN_INFO "uLan : uld_timeout\n");
    uld_timeout(udrv);
  };
  #endif /* UL_INT_TST */
  ulan_stroke(udrv, 1);

  return 0;

error_no_hardware:
 #ifdef UL_WITH_USB
  usb_deregister(&ul_usb_driver);
 #endif  /* UL_WITH_USB */
 #ifdef UL_WITH_OF
  of_unregister_platform_driver(&ulan_of_driver);
 #endif /*UL_WITH_OF*/
 #ifdef UL_WITH_PCI
  pci_unregister_driver (&ulan_pci_driver);
 #endif  /* UL_WITH_PCI */

  for(minor=0;minor<UL_MINORS;minor++) {
    udrv=ul_drv_arr[minor];
    if(udrv) {
      ul_drv_arr[minor]=NULL;
      sprintf(devname, minor? "/dev/ulan%d": "/dev/ulan", minor);
      unregister_driver(devname);
      ul_drv_free(udrv);
    }
  }

  return ret;
}

/*** cleanup kernel module ***/
static int module_uninitialize(FAR void *arg)
{
  ul_drv* udrv;
  int i;
  char devname[16];

  udrv=ul_drv_arr[0];
  if(udrv)
  { syslog(LOG_INFO, "uLan : printudrvbll\n");
    printudrvbll(udrv);
   #ifdef UL_WITH_FRAME_FSM
    syslog(LOG_INFO, "uLan : max cycle cnt  %d\n",ul_max_cycle_cnt);
    syslog(LOG_INFO, "uLan : max cycle time %d\n",ul_max_cycle_time);
   #endif /*UL_WITH_FRAME_FSM*/
  }

  for(i=0;i<UL_MINORS;i++)
  { udrv=ul_drv_arr[i];
    ul_drv_arr[i]=NULL;
    if(udrv){
     sprintf(devname, i? "/dev/ulan%d": "/dev/ulan", i);
     unregister_driver(devname);
     syslog(LOG_INFO, "module_uninitialize : ul_drv_free(%p)\n", udrv);
     ul_drv_free(udrv);
    }
  }

 #ifdef ENABLE_UL_MEM_CHECK
  UL_PRINTF("MEM_CHECK: malloc-free=%d\n",
		 (int)atomic_read(&ul_mem_check_counter));
 #endif /* ENABLE_UL_MEM_CHECK */

  return 0;
}

#ifdef ENABLE_UL_MEM_CHECK
void * ul_mem_check_malloc(size_t size)
{void *ptr;
 ptr=kmalloc(size,GFP_KERNEL);
 if(ptr) atomic_inc(&ul_mem_check_counter);
 return ptr;
}

void ul_mem_check_free(void *ptr)
{
  if(ptr) atomic_dec(&ul_mem_check_counter);
  kfree(ptr);
}
#endif /* ENABLE_UL_MEM_CHECK */
