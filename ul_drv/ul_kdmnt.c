/*******************************************************************
  uLan Communication - uL_DRV - multiplatform uLan driver

  ul_kdmnt.c	- Windows NT KMD specific support

  (C) Copyright 1996-2004 by Pavel Pisa - project originator
        http://cmp.felk.cvut.cz/~pisa
  (C) Copyright 1996-2004 PiKRON Ltd.
        http://www.pikron.com
  (C) Copyright 2002-2004 Petr Smolik
  

  The uLan driver project can be used and distributed 
  in compliance with any of next licenses
   - GPL - GNU Public License
     See file COPYING for details.
   - LGPL - Lesser GNU Public License
   - MPL - Mozilla Public License
   - and other licenses added by project originator

  Code can be modified and re-distributed under any combination
  of the above listed licenses. If contributor does not agree with
  some of the licenses, he/she can delete appropriate line.
  WARNING: if you delete all lines, you are not allowed to
  distribute code or sources in any form.
 *******************************************************************/

//-------------------------------------------------------------------
//
// Declare forward function references
//

VOID NTAPI UnloadDriver (IN PDRIVER_OBJECT DriverObject);

BOOLEAN ReportUsage(IN PDRIVER_OBJECT DriverObject,
	            IN PDEVICE_OBJECT DeviceObject,
                    IN INTERFACE_TYPE InterfaceType,
                    IN ULONG BusNumber,
                    IN PHYSICAL_ADDRESS PortAddress,
		    IN LONG PortRange,
		    IN KIRQL IRQLine,
                    IN BOOLEAN *ConflictDetected);

//---------------------------------------------------------------------------
// ReportUsage
//
// Description:
//  This routine registers (reports) the I/O and IRQ usage for this driver.
//
// Arguments:
//      DriverObject    - Pointer to the driver object
//      DeviceObject    - Pointer to the Device object
//      PortAddress     - Address of I/O port used
//      ConflictDetected - TRUE if a resource conflict was detected.
//
// Return Value:
//      TRUE    - If a Resource conflict was detected
//      FALSE   - If no conflict was detected
//
BOOLEAN ReportUsage(IN PDRIVER_OBJECT DriverObject,
		    IN PDEVICE_OBJECT DeviceObject,
		    IN INTERFACE_TYPE InterfaceType,
                    IN ULONG BusNumber,
                    IN PHYSICAL_ADDRESS PortAddress,
		    IN LONG PortRange,
		    IN KIRQL IRQLine,
                    IN BOOLEAN *ConflictDetected)
{
  ULONG sizeOfResourceList;
  PCM_RESOURCE_LIST resourceList;
  PCM_FULL_RESOURCE_DESCRIPTOR nextFrd;
  PCM_PARTIAL_RESOURCE_DESCRIPTOR partial;

  //
  // The size of the resource list is going to be one full descriptor
  // which already has one partial descriptor included, plus another
  // partial descriptor. One partial descriptor will be for the
  // interrupt, and the other for the port addresses.
  //

  sizeOfResourceList = sizeof(CM_FULL_RESOURCE_DESCRIPTOR);

  //
  // The full resource descriptor already contains one
  // partial.	Make room for one more.
  //
  // It will hold the irq "prd", and the port "prd".
  //	  ("prd" = partial resource descriptor)
  //

  sizeOfResourceList += sizeof(CM_PARTIAL_RESOURCE_DESCRIPTOR);

  //
  // Now we increment the length of the resource list by field offset
  // of the first frd.   This will give us the length of what preceeds
  // the first frd in the resource list.
  //	 (frd = full resource descriptor)
  //

  sizeOfResourceList += FIELD_OFFSET(CM_RESOURCE_LIST, List[0]);

  resourceList = ExAllocatePool(PagedPool, sizeOfResourceList);

  if (!resourceList) {
    return FALSE;
  }

  //
  // Zero out the list
  //

  RtlZeroMemory(resourceList, sizeOfResourceList);

  resourceList->Count = 1;
  nextFrd = &resourceList->List[0];

  nextFrd->InterfaceType = InterfaceType;
  nextFrd->BusNumber = BusNumber;

  //
  // We are going to report port addresses and interrupt
  //

  nextFrd->PartialResourceList.Count = 2;

  //
  // Now fill in the port data.  We don't wish to share
  // this port range with anyone.
  //
  // Note: the port address we pass in is the one we got
  // back from HalTranslateBusAddress.
  //
  // CmResourceShareDriverExclusive
  // CmResourceShareDeviceExclusive
  // CmResourceShareShared

  partial = &nextFrd->PartialResourceList.PartialDescriptors[0];

  partial->Type = CmResourceTypePort;
  partial->ShareDisposition = CmResourceShareDeviceExclusive;
  partial->Flags = CM_RESOURCE_PORT_IO;
  partial->u.Port.Start = PortAddress;
  partial->u.Port.Length = PortRange;

  partial++;

  //
  // Now fill in the irq stuff.
  //
  // Note: for IoReportResourceUsage, the Interrupt.Level and
  // Interrupt.Vector are bus-specific level and vector, just
  // as we passed in to HalGetInterruptVector, not the mapped
  // system vector we got back from HalGetInterruptVector.
  //

  partial->Type = CmResourceTypeInterrupt;
  partial->u.Interrupt.Level = IRQLine;
  partial->u.Interrupt.Vector = IRQLine;
  partial->ShareDisposition = InterfaceType==Isa? 
                                 CmResourceShareDeviceExclusive:
                                 CmResourceShareShared;
  partial->Flags = InterfaceType==Isa?
                     CM_RESOURCE_INTERRUPT_LATCHED:
		     CM_RESOURCE_INTERRUPT_LEVEL_SENSITIVE;

  /* Claim resources for Driver */
  /*IoReportResourceUsage(
      NULL,
      DriverObject,
      resourceList,
      sizeOfResourceList,
      NULL,
      NULL,
      0,
      FALSE,
      ConflictDetected);*/

  /* Claim resources for individual DeviceObject */
  IoReportResourceUsage(
      NULL,
      DriverObject,
      NULL,
      0,
      DeviceObject,
      resourceList,
      sizeOfResourceList,
      FALSE,
      ConflictDetected);

  //
  // The above routine sets the BOOLEAN parameter ConflictDetected
  // to TRUE if a conflict was detected.
  //

  ExFreePool(resourceList);

  return (*ConflictDetected);
}


//-------------------------------------------------------------------
// DriverEntry for WinNT style KMD
//
// Description:
//  NT device Driver Entry point
//
// Arguments:
//      DriverObject    - Pointer to this device's driver object
//      RegistryPath    - Pointer to the Unicode regsitry path name
//
// Return Value:
//      NTSTATUS
//
NTSTATUS NTAPI DriverEntry(IN PDRIVER_OBJECT DriverObject, IN PUNICODE_STRING RegistryPath)
{
    PDEVICE_OBJECT deviceObject = NULL;
    NTSTATUS status, ioConnectStatus;
    UNICODE_STRING uniNtNameString;
    UNICODE_STRING uniWin32NameString;
    UNICODE_STRING uniRegPath;
    INTERFACE_TYPE InterfaceType;
    ULONG BusNumber;
    PCI_SLOT_NUMBER SlotNumber;
    KIRQL irql = 0;
    KIRQL IRQLine = 0;
    KIRQL IRQLevel = 0;
    KIRQL OldIrql;
    KAFFINITY Affinity = 0;
    LONG TmpLong;
    LONG PortRange;
    LONG BaudRate;
    LONG BaudBase;
    LONG MyAddr;
    LONG GWAddr;
    LONG BufferSize;
    LONG ScanForPCI;
    LONG VirtualChip = 0;
    ULONG MappedVector=0, AddressSpace = 1;
    PULAN_DEVICE_EXTENSION extension;
    BOOLEAN ResourceConflict;
    PHYSICAL_ADDRESS InPortAddr, OutPortAddr;
    int ChipOptions=0;

    ULD_LARGE_INTEGER_0=RtlConvertLongToLargeInteger(0);

    uLan_DbgPrint("uLan v" UL_DRV_VERSION " Enter the driver!\n");
    uLan_DbgPrint("uLan: " __DATE__ " " __TIME__ "\n");

  #ifdef UL_KERNEL_SPINLOCK
    KeInitializeSpinLock(&uL_SpinLock);
  #endif

    if(uL_SpinLock_Irql < DISPATCH_LEVEL+1)
        uL_SpinLock_Irql = DISPATCH_LEVEL+1;

    //
    // Create counted string version of our device name.
    //

    RtlInitUnicodeString(&uniNtNameString, NT_DEVICE_NAME);

    //
    // Default configuration
    //

    ScanForPCI=0;
    InterfaceType = Isa;
    BusNumber = 0;
    InPortAddr.LowPart = DEF_PORT_ADDRESS;
    InPortAddr.HighPart = 0;
    PortRange = DEF_PORT_RANGE;
    IRQLine = DEF_IRQ_LINE;
    BaudRate = DEF_BAUD_RATE;
    BaudBase = 0;
    BufferSize=0x8000;
    MyAddr = DEF_MY_ADDR;
    GWAddr = DEF_GW_ADDR;

    //
    // Get the configuration information from the Registry
    //

    /* RtlInitUnicodeString(&uniRegPath,RegistryPath->Buffer); */
    /* RtlCopyUnicodeString(&uniRegPath,RegistryPath); */

    status=STATUS_SUCCESS;
    RtlInitUnicodeString(&uniRegPath, NULL);
    uniRegPath.MaximumLength = RegistryPath->Length + sizeof(L"\\Parameters")+1;
    uniRegPath.Buffer = ExAllocatePool(PagedPool, uniRegPath.MaximumLength*sizeof(WCHAR));

    if (!uniRegPath.Buffer) {
        uLan_DbgPrint("uLan: ExAllocatePool failed for Path in DriverEntry\n");
        status = STATUS_UNSUCCESSFUL;
    }
    else
    {
        RtlZeroMemory(uniRegPath.Buffer,uniRegPath.MaximumLength*sizeof(WCHAR));
        RtlAppendUnicodeStringToString(&uniRegPath, RegistryPath);
        RtlAppendUnicodeToString(&uniRegPath, L"\\Parameters");
    }

    if (NT_SUCCESS(status))
        status=ulan_GetRegistryDword(uniRegPath.Buffer,L"VirtualChip",&VirtualChip);
    if (NT_SUCCESS(status))
        status=ulan_GetRegistryDword(uniRegPath.Buffer,L"ScanForPCI",&ScanForPCI);
    if (NT_SUCCESS(status))
	status=ulan_GetRegistryDword(uniRegPath.Buffer,L"Port Address",&InPortAddr.LowPart);
    if (NT_SUCCESS(status))
	status=ulan_GetRegistryDword(uniRegPath.Buffer,L"Port Range",&PortRange);
    TmpLong=IRQLine;
    if (NT_SUCCESS(status))
	status=ulan_GetRegistryDword(uniRegPath.Buffer,L"IRQ Line",&TmpLong);
    IRQLine=(KIRQL)TmpLong;
    IRQLevel=(KIRQL)TmpLong;
    if (NT_SUCCESS(status))
        status=ulan_GetRegistryDword(uniRegPath.Buffer,L"Baud Rate",&BaudRate);
    if (NT_SUCCESS(status))
        status=ulan_GetRegistryDword(uniRegPath.Buffer,L"Baud Base",&BaudBase);
    if (NT_SUCCESS(status))
        status=ulan_GetRegistryDword(uniRegPath.Buffer,L"My Addr",&MyAddr);
    if (NT_SUCCESS(status))
        status=ulan_GetRegistryDword(uniRegPath.Buffer,L"GW Addr",&GWAddr);
    if (NT_SUCCESS(status))
        status=ulan_GetRegistryDword(uniRegPath.Buffer,L"Buffer Size",&BufferSize);
    TmpLong=uld_debug_flg;
    if (NT_SUCCESS(status))
        status=ulan_GetRegistryDword(uniRegPath.Buffer,L"Debug",&TmpLong);
    uld_debug_flg=TmpLong;
    if(uniRegPath.Buffer)
	ExFreePool(uniRegPath.Buffer);
    if (!NT_SUCCESS(status)) {
        uLan_DbgPrint("uLan: GetConfiguration failed\n");
        return status;
    }

    if(VirtualChip) {
       #ifdef CONFIG_OC_UL_DRV_WITH_VIRTUAL
        ChipOptions = UL_VIRTUAL_HW;
       #else /*CONFIG_OC_UL_DRV_WITH_VIRTUAL*/
        uLan_DbgPrint("uLan: Virtual chip requested but not compiled in driver\n");
        return STATUS_UNSUCCESSFUL;
       #endif /*CONFIG_OC_UL_DRV_WITH_VIRTUAL*/
    }

   #ifdef UL_WITH_PCI
    if(ScanForPCI && !VirtualChip){
        pci_device_id_t *device_id;
        uLan_DbgPrint("uLan: Calling ScanForPCICard\n");
        if(ScanForPCICard(&BusNumber,&SlotNumber,TRUE,&device_id)){
	    /*PCI_COMMON_CONFIG PCIConfig;*/
            /*HalGetBusData(PCIConfiguration,BusNumber,SlotNumber.u.AsULONG,
                             PCIConfig,sizeof(PCIConfig));*/
            uLan_DbgPrint("uLan: ScanForPCICard found slot %02X:%02X.%1X\n",
                (int)BusNumber,(int)SlotNumber.u.bits.DeviceNumber,
                (int)SlotNumber.u.bits.FunctionNumber);
            InterfaceType=PCIBus;
            ChipOptions=device_id->driver_data; /*0x16954000*/
        }
    }
   #endif /*UL_WITH_PCI*/

    //
    // Create the device object, multi-thread access (FALSE)
    //

    status = IoCreateDevice(DriverObject, sizeof(ULAN_DEVICE_EXTENSION),
                            &uniNtNameString, FILE_DEVICE_UNKNOWN, 0,
                            /* TRUE */ FALSE, &deviceObject);

    if (!NT_SUCCESS (status) ) {
        uLan_DbgPrint("uLan: IoCreateDevice failed\n");
        return status;
    }

    //
    // Set the FLAGS field
    //

    deviceObject->Flags |= DO_BUFFERED_IO;

    extension = (PULAN_DEVICE_EXTENSION) deviceObject->DeviceExtension;
    extension->flag_CHIPOK=0;

    //
    // Claim PnP busses resources
    //

   #ifdef UL_WITH_PCI
    if((InterfaceType!=Isa) && !VirtualChip){
        PCM_RESOURCE_LIST  AllocatedResources;
        PCM_PARTIAL_RESOURCE_DESCRIPTOR PartialDescriptor;
        int PartialCount, i;
        InPortAddr.LowPart=IRQLine=0;
        status = HalAssignSlotResources(RegistryPath,NULL,DriverObject,
                     deviceObject,InterfaceType,BusNumber,
                     SlotNumber.u.AsULONG,&AllocatedResources);

        if(!AllocatedResources->Count)
            status = STATUS_INSUFFICIENT_RESOURCES;
        if(NT_SUCCESS(status)){
            PartialCount=AllocatedResources->List[0].PartialResourceList.Count;
            PartialDescriptor=AllocatedResources->List[0].PartialResourceList.PartialDescriptors;
            for(i=0;i<PartialCount;i++){
                if((PartialDescriptor[i].Type==CmResourceTypePort)&&
                  (!InPortAddr.LowPart)){
                    InPortAddr=PartialDescriptor[i].u.Port.Start;
                    PortRange=PartialDescriptor[i].u.Port.Length;
                }
                if((PartialDescriptor[i].Type==CmResourceTypeInterrupt)&&
                  (!IRQLine)){
                    IRQLevel=(KIRQL)PartialDescriptor[i].u.Interrupt.Level;
                    IRQLine=(KIRQL)PartialDescriptor[i].u.Interrupt.Vector;
                }
            }
        }
        if(!NT_SUCCESS(status)){
            uLan_DbgPrint("uLan: HalAssignSlotResources error %X\n",status);
            ExFreePool(AllocatedResources);
            IoDeleteDevice(deviceObject);
            return status;
        }
        if(!InPortAddr.LowPart || !IRQLine)
            status = STATUS_INSUFFICIENT_RESOURCES;
        uLan_DbgPrint("uLan: Found PCI resources Port=%04X IRQLevel=%02X IRQVector=%02X\n",
                       InPortAddr.LowPart,IRQLevel,IRQLine);
        ExFreePool(AllocatedResources);
    }
   #endif /*UL_WITH_PCI*/

    //
    // This call will map our IRQ to a system vector. It will also fill
    // in the IRQL (the kernel-defined level at which our ISR will run),
    // and affinity mask (which processors our ISR can run on).
    //
    // We need to do this so that when we connect to the interrupt, we
    // can supply the kernel with this information.
    //

    if(!VirtualChip) {
        irql=IRQLevel;
        MappedVector = HalGetInterruptVector(
            InterfaceType, // Interface type
            BusNumber,     // Bus number
            IRQLevel,	   // BusInterruptLevel
            IRQLine,       // BusInterruptVector
            &irql,         // IRQ level
            &Affinity      // Affinity mask
            );

        //
        // A little known Windows NT fact,
        // If MappedVector==0, then HalGetInterruptVector failed.
        //

        if (MappedVector == 0) {
            uLan_DbgPrint("uLan: HalGetInterruptVector failed\n");
            IoDeleteDevice(deviceObject);
            return (STATUS_INVALID_PARAMETER);
        }

        uLan_DbgPrint("uLan: driver mappedvector=%d irql=%d, dispatch irql=%d\n",
		   MappedVector,irql,DISPATCH_LEVEL);
        if(uL_SpinLock_Irql<irql)
  		uL_SpinLock_Irql=irql;
        uLan_DbgPrint("uLan: spin lock irql=%d\n",uL_SpinLock_Irql);

        //
        // Translate the base port address to a system mapped address.
        // This will be saved in the device extension after IoCreateDevice,
        // because we use the translated port address to access the ports.
        //

        if (!HalTranslateBusAddress(InterfaceType, BusNumber, InPortAddr,
                                    &AddressSpace, &OutPortAddr)) {
            uLan_DbgPrint("uLan: HalTranslateBusAddress failed\n");
            return STATUS_SOME_NOT_MAPPED;
        }
    } 

    if ( NT_SUCCESS(status) ) {

	//
        // Create dispatch points for create/open, close, unload, and ioctl
        //

        DriverObject->MajorFunction[IRP_MJ_CREATE] = DispatchRoutine;
        DriverObject->MajorFunction[IRP_MJ_CLOSE] = DispatchRoutine;
        DriverObject->MajorFunction[IRP_MJ_READ] = DispatchRoutine;
        DriverObject->MajorFunction[IRP_MJ_WRITE] = DispatchRoutine;
        DriverObject->MajorFunction[IRP_MJ_DEVICE_CONTROL] = DispatchRoutine;
        DriverObject->MajorFunction[IRP_MJ_CLEANUP] = DispatchRoutine;
        DriverObject->DriverUnload = UnloadDriver;

        //
        // check if resources (ports and interrupt) are available
        //

		
       #ifdef UL_WITH_PCI
        if((InterfaceType==Isa) && !VirtualChip)
       #endif /*UL_WITH_PCI*/
        {
	    ReportUsage(DriverObject,deviceObject,InterfaceType,BusNumber,
                        InPortAddr,PortRange,IRQLine,&ResourceConflict);

            if (ResourceConflict) {
                uLan_DbgPrint("uLan: Couldn't get resources\n");
                IoDeleteDevice(deviceObject);
                return STATUS_INSUFFICIENT_RESOURCES;
            }
        }
		

        //
        // fill in the device extension
        //
        extension = (PULAN_DEVICE_EXTENSION) deviceObject->DeviceExtension;
        extension->DeviceObject = deviceObject;
        extension->physbase=OutPortAddr.LowPart;
		

        status=ul_drv_init_ext(extension,extension->physbase,IRQLine,BaudRate,
				BaudBase,ChipOptions,BufferSize,MyAddr);

        if ( !NT_SUCCESS (status) ) {
            uLan_DbgPrint("uLan: Initialization failed\n");
            IoDeleteDevice(deviceObject);
            return status;
        }

       #ifdef UL_WITH_MULTI_NET
        extension->gw_adr=(int)uLanGWAddress;
       #endif /*UL_WITH_MULTI_NET*/

	/* Store all IRQ related information pieces */
	extension->Irql = irql;
	extension->irq = MappedVector;
	extension->InterruptAffinity = Affinity;
	extension->InterruptMode = InterfaceType==Isa? Latched: LevelSensitive;

        KeInitializeDpc(&extension->bottom_dpc,ulan_bottom_dpc,extension);
        KeInitializeDpc(&extension->wd_timer_dpc,ulan_wd_dpc,extension);
	KeInitializeTimer(&extension->wd_timer);

	if(ul_drv_new_start(extension,BufferSize) < 0) {
            uLan_DbgPrint("uLan: Couldn't start driver - ul_drv_new_start failed\n");
            IoDeleteDevice(deviceObject);
            return STATUS_INSUFFICIENT_RESOURCES;
        }

        uLan_DbgPrint("uLan: just about ready!\n");

        //
        // Create counted string version of our Win32 device name.
        //

        RtlInitUnicodeString( &uniWin32NameString, DOS_DEVICE_NAME);

        //
        // Create a link from our device name to a name in the Win32 namespace.
        //

        /* IoDeleteSymbolicLink (&uniWin32NameString); */
        status = IoCreateSymbolicLink( &uniWin32NameString, &uniNtNameString );

        if (!NT_SUCCESS(status)) {
            uLan_DbgPrint("uLan: Couldn't create the symbolic link\n");
            IoDeleteDevice (DriverObject->DeviceObject);
        } else {

            //
            // Setup the Dpc for ISR routine
            //
			/*

            IoInitializeDpcRequest (DriverObject->DeviceObject, uLan_Dpc_Routine);

            //
            // Initialize the device (enable IRQ's, hit the hardware)
            //

            Initialize_uLan (extension);
		    */

            uLan_DbgPrint("uLan: All initialized!\n");

            #ifdef UL_INT_TST
			geninfoblk(extension);
            printudrvbll(extension);
			extension->flag_NACTIV=1;
			KeRaiseIrql(DISPATCH_LEVEL,&OldIrql);
            uld_timeout(extension);
			KeLowerIrql(OldIrql);
            #endif  /* UL_INT_TST */

        }

    } else {
        uLan_DbgPrint("uLan: Couldn't create the device\n");
    }
    return status;
	
}



//---------------------------------------------------------------------------
// UnloadDriver
//
// Description:
//     Free all the allocated resources, etc.
//
// Arguments:
//     DriverObject - pointer to a driver object
// 
// Return Value:
//      None
// 
VOID NTAPI UnloadDriver (IN PDRIVER_OBJECT DriverObject)
{
    WCHAR                  deviceLinkBuffer[]  = DOS_DEVICE_NAME;
    UNICODE_STRING         deviceLinkUnicodeString;
    PULAN_DEVICE_EXTENSION extension;

    extension = DriverObject->DeviceObject->DeviceExtension;

    #ifdef UL_INT_TST
    if(extension->flag_CHIPOK);
      printudrvbll(extension);
    #endif  /* UL_INT_TST */

    //
    // Deactivate uLan 
    //

    ul_drv_done_ext(extension);

    //
    // Disconnect IRQ and DPC sources
    //
	
    KeCancelTimer(&extension->wd_timer);
    KeRemoveQueueDpc(&extension->wd_timer_dpc);
    KeRemoveQueueDpc(&extension->bottom_dpc);
		
    //
    // Delete the symbolic link
    //
	

    RtlInitUnicodeString (&deviceLinkUnicodeString, deviceLinkBuffer);

    IoDeleteSymbolicLink (&deviceLinkUnicodeString);

    //
    // Delete the device object
    //

    IoDeleteDevice (DriverObject->DeviceObject);

   #ifdef ENABLE_UL_MEM_CHECK
    UL_PRINTF("MEM_CHECK: malloc-free=%d\n",
		 (int)atomic_read(&ul_mem_check_counter));
   #endif /* ENABLE_UL_MEM_CHECK */
    uLan_DbgPrint ("uLan: Unloaded\n");
    return;
}

