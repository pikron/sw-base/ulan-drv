/*
 * include/linux/ul_82510.h
 *
 */

#ifndef _LINUX_U510_REG_H
#define _LINUX_U510_REG_H

/*
 *  For Intel 82510 serial port
 */

/*
 *  Port Address
 */


#define	U510_BAL	0x0000  /* Pokud DLAB =1 */
#define	U510_BAH	0x0001  /* Pokud DLAB =1 */
#define U510_GER	0x0001
#define U510_GIR	0x0002  /* 0x2002 */
#define U510_LCR	0x0003
#define U510_MCR	0x0004
#define U510_LSR	0x0005
#define U510_MSR	0x0006  /* 0x2006 */
#define U510_ACR0	0x0007
#define U510_RXD	0x2000
#define U510_RXF	0x2001
#define U510_TXD	0x2000
#define U510_TXF	0x2001
#define U510_TMST	0x2003
#define U510_TMCR	0x2003
#define U510_FLR	0x2004
#define U510_RST	0x2005
#define U510_RCM	0x2005
#define U510_GSR	0x2007
#define U510_ICM	0x2007
#define U510_FMD	0x4001
#define U510_TMD	0x4003
#define U510_IMD	0x4004
#define U510_ACR1	0x4005
#define U510_RIE	0x4006
#define U510_RMD	0x4007
#define U510_CLCF	0x6000
#define U510_BACF	0x6001
#define U510_BBL	0x6000  /* Pokud DLAB =1 */
#define U510_BBH	0x6001  /* Pokud DLAB =1 */
#define U510_BBCF	0x6003
#define U510_PMD	0x6004
#define U510_MIE	0x6005
#define U510_TMIE	0x6006

/*
 *  Register masks
 */

#define U510_MCR_OEQ	0x02
#define U510_MCR_OE	0x06
#define U510_MCR_IE	0x04

#define U510_GIR_NO_INT	0x01	/* No interrupts pending */
#define U510_GIR_ID	0x0E	/* Mask for the interrupt ID */

#define U510_GER_RI	0x01	/* RxFIFO int enable */
#define U510_GER_TI	0x02	/* TxFIFO int enable */
#define U510_GER_RM	0x04	/* Rx machine int enable */
#define U510_GER_MI	0x08	/* modem interrupt enable */
#define U510_GER_TM	0x10	/* Tx machine int enable */
#define U510_GER_TIE	0x20	/* Timer int enable */

#define U510_LCR_C	0x03	/* vysilani nebo prijem */
#define U510_LCR_B	0x40	/* vysilani pretrzeni */
#define U510_LCR_DLAB	0x80	/* zadavani BA a BB */

#define U510_LSR_RI	0x01	/* data prijata */
#define U510_LSR_C	0x04	/* prijata ridici instrukce */
#define U510_LSR_ERR	0x1A	/* chyba v prijmu */
#define U510_LSR_TRE	0x20	/* vystupni registr prazdny */
#define U510_LSR_TME	0x40	/* vystup ukoncen */

#define U510_RCM_FLUSH	0xB4	/* vymazani zbytku a prijem */

#define U510_MSR_TxD	0x20	/* TxD = 0 ? */
#define U510_MSR_RxD	0x40	/* RxD = 0 ? */


#ifndef _WIN32

/*
 *  82510 bank switching, if _U510_SBANK_SHADOW_VAR defines variable name
 *  bank switching is used only when necessary
 */


 #ifndef _U510_SBANK_SHADOW_VAR
  #define u510_sbank(base,port) \
	(ul_outb(ul_ioaddr_add((base),U510_GIR),(unsigned)(port)>>8),\
	 ul_ioaddr_add((base),(port)&0xFF))
 #else
  #define u510_sbank(base,port) \
	({\
	 if (/*(!__builtin_constant_p(_U510_SBANK_SHADOW_VAR))||*/\
	    ((unsigned)(port)>>8!=_U510_SBANK_SHADOW_VAR))\
	  ul_outb(ul_ioaddr_add((base),U510_GIR),(unsigned)(port)>>8);\
	 _U510_SBANK_SHADOW_VAR=(unsigned)(port)>>8;\
	 (ul_ioaddr_add((base),(port)&0xFF));\
	})
 #endif 

#else

/*
 *  82510 bank switching, if _U510_SBANK_SHADOW_VAR defines variable name
 *  bank switching is used only when necessary
 */


 #ifndef _U510_SBANK_SHADOW_VAR
  #define u510_sbank(base,port) \
	(ul_outb(ul_ioaddr_add((base),U510_GIR),(unsigned)(port)>>8),\
	 ul_ioaddr_add((base),(port)&0xFF))
 #else
  #define u510_sbank(base,port) \
	((\
	  ((unsigned)(port)>>8!=_U510_SBANK_SHADOW_VAR)?\
	  ul_outb(ul_ioaddr_add((base),U510_GIR),(unsigned)(port)>>8):0,\
	 _U510_SBANK_SHADOW_VAR=(unsigned)(port)>>8,\
	 (ul_ioaddr_add((base),(port)&0xFF))\
	))
 #endif 

#endif

/* Input output routines */

#define u510_inb(base,port)	(ul_inb(u510_sbank(base,port)))
#define u510_outb(base,port,val) (ul_outb(u510_sbank(base,port),val))
#define u510_pool_gir(base)	(!(ul_inb(ul_ioaddr_add((base),U510_GIR))&U510_GIR_NO_INT))

#endif /* _LINUX_U510_REG_H */

