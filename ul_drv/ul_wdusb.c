/*******************************************************************
  uLan Communication - uL_DRV - multiplatform uLan driver

  ul_wdusb.c	- WDM support for USB devices

  (C) Copyright 1996-2004 by Pavel Pisa - project originator
        http://cmp.felk.cvut.cz/~pisa
  (C) Copyright 1996-2004 PiKRON Ltd.
        http://www.pikron.com
  (C) Copyright 2002-2004 Petr Smolik
  

  The uLan driver project can be used and distributed 
  in compliance with any of next licenses
   - GPL - GNU Public License
     See file COPYING for details.
   - LGPL - Lesser GNU Public License
   - MPL - Mozilla Public License
   - and other licenses added by project originator

  Code can be modified and re-distributed under any combination
  of the above listed licenses. If contributor does not agree with
  some of the licenses, he/she can delete appropriate line.
  WARNING: if you delete all lines, you are not allowed to
  distribute code or sources in any form.
 *******************************************************************/

#ifndef __WINDOWS_USB_H 
#include "ul_wdusb.h"
#endif

/**
 * usb_create_dev - 
 * @dev: pointer to the usb device
 * @fdo: 
 *
 * Return Value: NTSTATUS
 */   
NTSTATUS
usb_create_dev(IN usb_device **pdev,IN PDEVICE_OBJECT fdo) 
{
    usb_device *dev;
    dev=MALLOC(sizeof(usb_device));
    if(!dev)
         return STATUS_INSUFFICIENT_RESOURCES;
    RtlZeroBytes(dev,sizeof(usb_device));
    dev->fdo=fdo;
    *pdev=dev;
    return 0;
}

/**
 * usb_destroy_dev - delete usb device resources
 * @dev: pointer to the usb device
 *
 * Return Value: NTSTATUS
 */   
NTSTATUS
usb_destroy_dev(IN usb_device *dev) 
{
    usb_reset_configuration(dev);
#if DBG_USB
    uLan_DbgPrint("ulan: device unconfigured\n");
#endif
    /* deviceDescriptor */
    if (dev->descriptor) {
      FREE(dev->descriptor);
#if DBG_USB
      uLan_DbgPrint("ulan: deviceDescriptor freed\n");
#endif
    }
    dev->descriptor=NULL;
    
    /* configurationDescriptor */
    if (dev->rawdescriptors) {
      FREE(dev->rawdescriptors);
#if DBG_USB
      uLan_DbgPrint("ulan: rawdescriptor freed\n");
#endif
    }
    dev->rawdescriptors=NULL;
    
    FREE(dev);
        
    return 0;
}

/**
 * usb_alloc_urb - allocates urb structure
 * @size: size
 * @mem_flags: not used at this moment
 *
 * Return Value: pointer to the created urb or NULL if it fails
 */   
struct urb *
usb_alloc_urb(IN LONG size, IN PLONG mem_flags)
{
  struct urb *urb;
  urb=MALLOC(sizeof(struct urb));
  if(!urb)
    return NULL;
  RtlZeroBytes(urb,sizeof(struct urb));
  urb->urb=NULL;
  if (size==0)
    return urb;
  urb->urb = MALLOC(size);
  if (!urb->urb) {
    FREE(urb);
    return NULL;
  }
  RtlZeroMemory(urb->urb, size);
  return urb;
}

/**
 * usb_free_urb - frees a urb structure
 * @size: size
 * @mem_flags: not used at this moment
 *
 * Return Value: 
 */   
void usb_free_urb(struct urb *urb)
{
  if (!urb) return;
  if (urb->urb)
    FREE(urb->urb);
  FREE(urb);
}

/**
 * usb_submit_urb - Passes a URB to the USBD class driver
 * @usb: pointer to an already-formatted Urb request block
 * @mem_flags: not used at this moment
 *
 * Return Value: NTSTATUS
 */   
NTSTATUS 
usb_submit_urb( IN struct urb *urb, IN LONG mem_flags)
{
    NTSTATUS ntStatus, status = STATUS_SUCCESS;
    PIRP irp;
    KEVENT event;
    PIO_STACK_LOCATION nextStack;
#if DBG_USB
    uLan_DbgPrint("ulan: _usb_submit_urb : enter\n");
#endif
    if (!urb->complete_fn)
      KeInitializeEvent( &event, NotificationEvent, FALSE);
    urb->irp = IoBuildDeviceIoControlRequest( IOCTL_INTERNAL_USB_SUBMIT_URB, urb->dev->fdo,
                                              NULL, 0, NULL, 0, FALSE, (!urb->complete_fn) ? &event : NULL, &urb->ioStatus);  
    nextStack = IoGetNextIrpStackLocation( urb->irp);
    nextStack->Parameters.Others.Argument1 = urb->urb;
    nextStack->MajorFunction = IRP_MJ_INTERNAL_DEVICE_CONTROL;
    if (urb->complete_fn) {
      IoSetCompletionRoutine( urb->irp, urb->complete_fn, urb->context, TRUE, TRUE, TRUE);
    }
    ntStatus = IoCallDriver( urb->dev->fdo, urb->irp);
    if (urb->complete_fn) {
#if DBG_USB
      uLan_DbgPrint("uLan: _usb_submit_urb done (rtn) with status:0x%X\n",ntStatus);    
#endif
      return ntStatus;
    }
#if DBG_USB
    uLan_DbgPrint("ulan: _usb_submit_urb : return from IoCallDriver USBD %x\n", ntStatus);
#endif
    if (ntStatus == STATUS_PENDING) {
        status = KeWaitForSingleObject( &event, Suspended, KernelMode, FALSE, NULL);
    } else {
        urb->ioStatus.Status = ntStatus;
    }
    ntStatus = urb->ioStatus.Status;
#if DBG_USB
    uLan_DbgPrint("ulan: _usb_submit_urb : URB status = %x status = %x irp status %x\n",
                                      urb->urb->UrbHeader.Status, status, urb->ioStatus.Status);
    uLan_DbgPrint("uLan: _usb_submit_urb done with status:0x%X\n",ntStatus);    
#endif
    return ntStatus;
}

/**
 * usb_control_msg - Builds a control urb, sends it off and waits for completion
 * @dev: pointer to the usb device
 * @pipe: endpoint "pipe" to send the message to
 * @request: USB message request value
 * @requesttype: USB message request type value (only USB_TYPE_VENDOR)
 * @value: USB message value 
 * @index: USB message value 
 * @data: pointer to the data to send 
 * @size: length in bytes of the data to send 
 * @time: time to wait for the message to complete before timing out (if 0 the wait is forever)
 *
 * Return Value: NTSTATUS (data len)
 */   
NTSTATUS
usb_control_msg(IN usb_device *dev,IN ULONG pipe,
    IN UCHAR request,IN USHORT requesttype, IN USHORT value, IN USHORT index,
    IN PVOID data, IN ULONG size, IN ULONG time) 
{
    NTSTATUS ntStatus = STATUS_SUCCESS;
    LARGE_INTEGER timeout;
    ULONG flags = 0;
    struct urb *urb=NULL;

#if DBG_USB
    uLan_DbgPrint("uLan: usb_control_msg pipe=0x%X, req=0x%X, reqtype=0x%X, value=0x%X, idx=0x%X, size=%d\n", 
                  pipe, request, requesttype, value , index, size);
#endif    
    if (!usb_pipecontrol(pipe)) {
        return STATUS_INVALID_PARAMETER;
    }

    if (usb_pipein(pipe))
        flags = USBD_TRANSFER_DIRECTION_IN | USBD_SHORT_TRANSFER_OK;

    switch (requesttype & USB_TYPE_MASK) {
       case USB_TYPE_VENDOR:
       case USB_TYPE_CLASS:
           urb = usb_alloc_urb(sizeof( struct _URB_CONTROL_VENDOR_OR_CLASS_REQUEST),0);
           if ( !urb) {
              uLan_DbgPrint("uLan: no memory for URB!!!\n");
              return STATUS_INSUFFICIENT_RESOURCES;
           }
	   
           urb->dev=dev;
           UsbBuildVendorRequest( urb->urb, URB_FUNCTION_VENDOR_DEVICE, 
                            sizeof(struct _URB_CONTROL_VENDOR_OR_CLASS_REQUEST),
                            flags, 0, request, value, index, data, NULL, size, NULL);
           break;
       case USB_TYPE_STANDARD:
           /* to do */
           /* urb = MALLOC(sizeof( struct _URB_CONTROL_TRANSFER)); */
           break;
       default:
           break;
    }
    
    timeout.QuadPart = ((LONGLONG)time)*(-10000); /* time is in [ms] */
    ntStatus = usb_submit_urb( urb, 0);
    
    if (NT_SUCCESS(ntStatus)) {
        ntStatus = (LONG) urb->urb->UrbControlVendorClassRequest.TransferBufferLength;
    }
    
    usb_free_urb(urb);
#if DBG_USB
    uLan_DbgPrint("uLan: usb_control_msg done with status:0x%X\n",ntStatus);    
#endif    
    return ntStatus;  
}

/**
 * usb_fill_bulk_urb  - builds a bulk urb
 * @urb: pointer to filling urb
 * @usb_dev: pointer to the usb device to send the message to
 * @pipe: endpoint "pipe" to send the message to
 * @data: pointer to the data to send
 * @len: length in bytes of the data to send
 *
 * This function create bulk urb
 *  
 * Return Value: NTSTATUS
 */    
NTSTATUS
usb_fill_bulk_urb ( IN struct urb *urb, IN usb_device *dev,IN ULONG pipe,
    IN PVOID data, IN LONG len, IN usb_complete_t complete_fn, IN PVOID context)
{
    PUSBD_PIPE_INFORMATION pipe_info=NULL;
    PUSBD_INTERFACE_INFORMATION interface;
    ULONG flags = 0;
    ULONG endpoint;
    unsigned int j;

    if ((!usb_pipebulk(pipe)) || (!urb) || (!urb->urb) || (!dev)) {
        return STATUS_INVALID_PARAMETER;
    }

    /* get handle of pipe */
    endpoint=usb_pipeendpoint(pipe) | (pipe & USB_DIR_IN);
#if DBG_USB
    uLan_DbgPrint("uLan: usb_bulk_msg selected endpoint=0x%X\n",endpoint);
#endif    
    interface=dev->interfaces.Interface;
    if ( !interface) {
       return STATUS_INVALID_PARAMETER;
    }
    /* trace all pipes */
    for (j=0; j<interface->NumberOfPipes; j++) {
        pipe_info=&interface->Pipes[j];
        if (pipe_info->EndpointAddress==endpoint) break;
        pipe_info=NULL;
    }
    if (!pipe_info) {
        return STATUS_INVALID_PARAMETER;
    }    
    /* usb_show_endpoint_information(pipe_info); */
    
    if (usb_pipein(pipe))
        flags = USBD_TRANSFER_DIRECTION_IN;

    urb->dev=dev;
    urb->complete_fn=complete_fn;
    urb->context=context;
    
    UsbBuildInterruptOrBulkTransferRequest( urb->urb, 
                   sizeof(struct _URB_BULK_OR_INTERRUPT_TRANSFER), 
                   pipe_info->PipeHandle, 
                   data, 
                   NULL, 
                   len, 
                   flags | USBD_SHORT_TRANSFER_OK,
                   NULL );
        
    return STATUS_SUCCESS;
}

/**
 * usb_bulk_msg - Builds a bulk urb, sends it off and waits for completion
 * @usb_dev: pointer to the usb device to send the message to
 * @pipe: endpoint "pipe" to send the message to
 * @data: pointer to the data to send
 * @len: length in bytes of the data to send
 * @actual_length: pointer to a location to put the actual length transferred in bytes
 * @timeout: time to wait for the message to complete before timing out (if 0 the wait is forever)
 *
 * This function sends a simple bulk message to a specified endpoint
 * and waits for the message to complete, or timeout.
 *  
 * Return Value: NTSTATUS (data len)
 */    
NTSTATUS
usb_bulk_msg(IN usb_device *dev,IN ULONG pipe,
    IN PVOID data, IN LONG len, IN LONG *actual_length,IN ULONG time)
{
    NTSTATUS ntStatus = STATUS_SUCCESS;
    LARGE_INTEGER timeout;
    struct urb *urb;

#if DBG_USB
    uLan_DbgPrint("uLan: usb_bulk_msg pipe=0x%X, len=0x%X\n", 
                  pipe, len);
#endif    

    urb = usb_alloc_urb(sizeof( struct _URB_BULK_OR_INTERRUPT_TRANSFER),0);
    if ( !urb) {
       uLan_DbgPrint("uLan: no memory for URB!!!\n");
       return STATUS_INSUFFICIENT_RESOURCES;
    }
    
    ntStatus=usb_fill_bulk_urb (urb,dev,pipe,data,len,NULL,NULL);
    if ( !NT_SUCCESS( ntStatus)) {
        uLan_DbgPrint("uLan: error in filling bulk urb\n");
        return ntStatus;
    }
    
    ntStatus = usb_submit_urb(urb,0);

    *actual_length = (LONG) urb->urb->UrbBulkOrInterruptTransfer.TransferBufferLength;

    usb_free_urb(urb);
#if DBG_USB
    uLan_DbgPrint("uLan: usb_bulk_msg done with status:0x%X\n",ntStatus);    
#endif    
    return ntStatus;
}

/**
 * _usb_get_descriptor - Retrieves a descriptor from a device's default control pipe 
 * @dev: pointer to the usb device
 * @desctype: (USB_DEVICE_DESCRIPTOR_TYPE,
 *             USB_CONFIGURATION_TYPE,
 *             USB_STRING_DESCRIPTOR_TYPE) 
 * @descindex: the number of the descriptor 
 * @buf: pointer ot pointer where to put the descriptor 
 * @size: how big is buf?
 *
 * Return Value: NTSTATUS
 */   
NTSTATUS 
_usb_get_descriptor(IN usb_device *dev, IN UCHAR desctype,
    IN UCHAR descindex, IN PVOID *buf, IN SHORT size) 
{
    NTSTATUS ntStatus = STATUS_SUCCESS;
    struct urb *urb;
    ULONG true_size = 4;
    PVOID new_ptr;

#if DBG_USB
    uLan_DbgPrint("uLan: _usb_get_descriptor (type=0x%X,idx=0x%X)\n", desctype, descindex);
#endif    
    if (( *buf != NULL) && (size == -1)) {
        return STATUS_INVALID_PARAMETER;
    }
    urb = usb_alloc_urb(sizeof( struct _URB_CONTROL_DESCRIPTOR_REQUEST),0);
    if ( !urb) {
        uLan_DbgPrint("uLan: no memory for URB\n");
        return STATUS_INSUFFICIENT_RESOURCES;
    }
    if ( size != -1) {
        true_size = (ULONG) size;
    }

   AgainSendGetDescriptor:
    if (( size == -1) || (*buf == NULL)) {
        new_ptr = MALLOC(true_size);
    } else {
        new_ptr = *buf;
    }
    if ( new_ptr) {
        UsbBuildGetDescriptorRequest(urb->urb,
                                    (USHORT) sizeof (struct _URB_CONTROL_DESCRIPTOR_REQUEST),
                                     desctype,
                                     descindex,
                                     0, /* lang */
                                     new_ptr,
                                     NULL,
                                     true_size,
                                     NULL);
        urb->dev=dev;
        ntStatus = usb_submit_urb(urb,0);
    } else {
        ntStatus = STATUS_INSUFFICIENT_RESOURCES;
    }
    if (( size == -1) && NT_SUCCESS(ntStatus)) {
        if ( desctype == USB_CONFIGURATION_DESCRIPTOR_TYPE)
            true_size = (ULONG) *(((PUSHORT)new_ptr)+1); /* total_size */
        else
            true_size = (ULONG) *((PUCHAR)new_ptr);
        size = 0;
        FREE( new_ptr);
        goto AgainSendGetDescriptor;
    }
    if ( NT_SUCCESS(ntStatus) && (*buf == NULL)) {
        *buf = new_ptr;
    } else if ( new_ptr) {
        FREE( new_ptr);
    }
    usb_free_urb(urb);
#if DBG_USB
    uLan_DbgPrint("uLan: _usb_get_descriptor done with status:0x%X\n",ntStatus);    
#endif    
    return ntStatus;
}

/**
 * usb_get_device_descriptor - 
 * @dev: pointer to the usb device
 *
 * Return Value: NTSTATUS
 */   
NTSTATUS
usb_get_device_descriptor(IN usb_device *dev) 
{
    NTSTATUS ntStatus;
    
    /* deviceDescriptor */
    if (dev->descriptor)
        FREE(dev->descriptor);
    dev->descriptor=NULL;
    
    ntStatus = _usb_get_descriptor(dev,
                           USB_DEVICE_DESCRIPTOR_TYPE,
                           0,
                           &dev->descriptor,
                           sizeof(USB_DEVICE_DESCRIPTOR));
#if DBG_USB
    uLan_DbgPrint("uLan: usb_device_descriptor done with status:0x%X\n",ntStatus);    
#endif    
    return ntStatus;
}

/**
 * usb_get_configuration - 
 * @dev: pointer to the usb device
 *
 * Return Value: NTSTATUS
 */   
NTSTATUS
usb_get_configuration(IN usb_device *dev) 
{
    NTSTATUS ntStatus;
    char cfgno;
    
#if DBG_USB
    uLan_DbgPrint("uLan: usb_get_configuration\n");
#endif    
    if ((!dev) || (!dev->descriptor))
        return STATUS_INVALID_PARAMETER;
    
    if (dev->descriptor->bNumConfigurations < 1) {
        return STATUS_INVALID_PARAMETER;
    }   
    
    dev->rawdescriptors = (char **)MALLOC(sizeof(char *) *
        dev->descriptor->bNumConfigurations);
    if (!dev->rawdescriptors) {
        return STATUS_INSUFFICIENT_RESOURCES;
    } 

    /* read all configurations */
    for (cfgno = 0; cfgno < dev->descriptor->bNumConfigurations; cfgno++) {     
        dev->rawdescriptors[cfgno]=NULL;
        ntStatus = _usb_get_descriptor(dev,
                              USB_CONFIGURATION_DESCRIPTOR_TYPE,
                              cfgno,
                              &dev->rawdescriptors[cfgno],
                              -1);

        if ( !NT_SUCCESS(ntStatus)) break; 
    }
#if DBG_USB
    uLan_DbgPrint("uLan: usb_get_configuration (num. cfg.:%d)\n",
                  dev->descriptor->bNumConfigurations);
    uLan_DbgPrint("uLan: usb_get_configuration done with status:0x%X\n",ntStatus);    
#endif    
    return ntStatus;
}

/**
 * usb_set_configuration - 
 * @dev: pointer to the usb device
 * @configuration: number of configuration
 *
 * Return Value: NTSTATUS
 */   
NTSTATUS
usb_set_configuration(IN usb_device *dev,IN int configuration) 
{
    PUSBD_INTERFACE_LIST_ENTRY ifc_list = NULL, ifc_tmp = NULL;
    PUSB_INTERFACE_DESCRIPTOR ifc_desc = NULL;
    PUSB_CONFIGURATION_DESCRIPTOR cp=NULL; 
    NTSTATUS ntStatus = STATUS_SUCCESS;
    struct urb *urb = NULL;
    int i;

#if DBG_USB
    uLan_DbgPrint("uLan: usb_set_configuration (%d)\n",configuration);
#endif    

    /* if device has no device descriptor, dump it */
    if (!dev->descriptor) {
      ntStatus=usb_get_device_descriptor(dev);
      if ( !NT_SUCCESS( ntStatus)) {
          uLan_DbgPrint("uLan: error in usb_get_device_descriptor\n");
          return ntStatus;
      }
    }

    /* if device has no configuration informations, dump it */
    if (dev->rawdescriptors==NULL) {
      ntStatus = usb_get_configuration(dev);
      if ( !NT_SUCCESS( ntStatus)) {
          uLan_DbgPrint("uLan: error in usb_get_configuration\n");
          return ntStatus;
      }
    }

    for (i=0; i<dev->descriptor->bNumConfigurations; i++) {
      PUSB_CONFIGURATION_DESCRIPTOR c =
               (PUSB_CONFIGURATION_DESCRIPTOR)dev->rawdescriptors[i];
        if (c->bConfigurationValue == configuration) {
            cp = c;
            break;
        }
    }
    if (!cp) {
      return STATUS_INVALID_PARAMETER;
    }

    ifc_tmp = ifc_list = 
        MALLOC(sizeof( struct _USBD_INTERFACE_LIST_ENTRY) * ( cp->bNumInterfaces+1));
    if ( !ifc_tmp) {
      return STATUS_INSUFFICIENT_RESOURCES;
    }
    for( i = 0; i < cp->bNumInterfaces; i++) {
        ifc_desc = USBD_ParseConfigurationDescriptorEx( cp, cp, i, -1, -1, -1, -1);
#if DBG_USB
        uLan_DbgPrint("uLan: next Ifc on 0x%X\n", ifc_desc);
#endif    
        ifc_list->InterfaceDescriptor = ifc_desc;
        ifc_list++;
    }
    ifc_list->InterfaceDescriptor = NULL;
    ifc_list = ifc_tmp;
    /* create select configuration urb */
    urb = usb_alloc_urb(0,0);
    if ( !urb) {
        uLan_DbgPrint("uLan: no memory for URB\n");
        return STATUS_INSUFFICIENT_RESOURCES;
    }
    urb->dev=dev;
    /* The URB is allocated by obsoleted ExAllocatePool routine without tag */
    urb->urb=USBD_CreateConfigurationRequestEx( cp, ifc_tmp);
    if( !urb->urb) {
        uLan_DbgPrint("uLan: create config request\n");
        usb_free_urb(urb);
        ntStatus = STATUS_INSUFFICIENT_RESOURCES;
    }
    /* Send select configure urb to usb device */
    if ( NT_SUCCESS(ntStatus)) {
        ntStatus = usb_submit_urb(urb, 0);
        if( !NT_SUCCESS( ntStatus)) {
#if DBG_USB
            uLan_DbgPrint("uLan: send select configuration request (0x%X)\n", ntStatus);
#endif    
        } else {
            dev->cfghandle = urb->urb->UrbSelectConfiguration.ConfigurationHandle;
            /* for selected interface make copy */
            dev->interfaces.InterfaceDescriptor = ifc_tmp->InterfaceDescriptor;
            dev->interfaces.Interface = MALLOC( ifc_tmp->Interface->Length);
            if ( !dev->interfaces.Interface) {
               uLan_DbgPrint("uLan: no memory for usb.interfaces\n", ntStatus);
               ntStatus = STATUS_INSUFFICIENT_RESOURCES;
            } else {
               RtlCopyMemory( dev->interfaces.Interface, 
                              ifc_tmp->Interface, 
                              ifc_tmp->Interface->Length);
            }
        }
    }
    if (urb) {
        /*
         * The ExFreePool has to be called explicitly to not mismatch
         * allocation without tag with tagged free in usb_free_urb
         */
        if (urb->urb != NULL)
            ExFreePool(urb->urb);
        urb->urb = NULL;
        usb_free_urb(urb);
    }
    if ( ifc_list) 
        FREE( ifc_list);
    dev->actconfig = cp;
#if DBG_USB
    uLan_DbgPrint("uLan: usb_set_configuration done with status:0x%X\n",ntStatus);            
#endif    
    return ntStatus;
} 

/**
 * usb_reset_configuration - 
 * @dev: pointer to the usb device
 *
 * Return Value: NTSTATUS
 */   
NTSTATUS
usb_reset_configuration(IN usb_device *dev) 
{
    NTSTATUS ntStatus = STATUS_SUCCESS;
    struct urb *urb = NULL;

#if DBG_USB
    uLan_DbgPrint("uLan: usb_reset_configuration \n");
#endif    
    urb = usb_alloc_urb(sizeof( struct _URB_SELECT_CONFIGURATION),0);
    if ( !urb) {
        uLan_DbgPrint("uLan: no memory for URB\n");
        return STATUS_INSUFFICIENT_RESOURCES;
    }
    urb->dev=dev;
    UsbBuildSelectConfigurationRequest( urb->urb, sizeof( struct _URB_SELECT_CONFIGURATION), NULL);
    ntStatus = usb_submit_urb( urb, 0);
    if( !NT_SUCCESS( ntStatus)) {
        uLan_DbgPrint("uLan: error select configurations request (0x%X)\n",ntStatus);
    }
    usb_free_urb(urb);
#if DBG_USB
    uLan_DbgPrint("uLan: usb_reset_configuration done with status:0x%X\n",ntStatus);    
#endif    
    return ntStatus;  
}

/**
 * usb_clear_halt - 
 * @dev: pointer to the usb device
 * @pipe:
 *
 * Return Value: NTSTATUS
 */   
NTSTATUS
usb_clear_halt(IN usb_device *dev,IN int pipe)
{
    PUSBD_PIPE_INFORMATION pipe_info=NULL;
    PUSBD_INTERFACE_INFORMATION interface;
    NTSTATUS ntStatus = STATUS_SUCCESS;
    ULONG endpoint;
    struct urb *urb = NULL;
    unsigned int j;

    /* get handle of pipe */
    endpoint=usb_pipeendpoint(pipe) | (pipe & USB_DIR_IN);
#if DBG_USB
    uLan_DbgPrint("uLan: usb_clear_halt selected endpoint=0x%X\n",endpoint);
#endif    
    interface=dev->interfaces.Interface;
    if ( !interface) {
       return STATUS_INVALID_PARAMETER;
    }
    /* trace all pipes */
    for (j=0; j<interface->NumberOfPipes; j++) {
        pipe_info=&interface->Pipes[j];
        if (pipe_info->EndpointAddress==endpoint) break;
        pipe_info=NULL;
    }
    if (!pipe_info) {
        return STATUS_INVALID_PARAMETER;
    }    
    /* usb_show_endpoint_information(pipe_info); */

    urb = usb_alloc_urb(sizeof( struct _URB_PIPE_REQUEST),0);
    if ( !urb) {
        uLan_DbgPrint("uLan: no memory for URB\n");
        return STATUS_INSUFFICIENT_RESOURCES;
    }
    urb->dev=dev;
    urb->urb->UrbHeader.Length = (USHORT) sizeof( struct _URB_PIPE_REQUEST);
    urb->urb->UrbHeader.Function = URB_FUNCTION_RESET_PIPE;
    urb->urb->UrbPipeRequest.PipeHandle = pipe_info->PipeHandle;
    ntStatus = usb_submit_urb( urb, 0);
    usb_free_urb(urb);
#if DBG_USB
    uLan_DbgPrint("uLan: usb_clear_halt done with status:0x%X\n",ntStatus);    
#endif
    return ntStatus;
}

/**
 * usb_show_device_descriptor - dump a device descriptor
 * deviceDescriptor@: pointer to the deviceDescriptor
 */   
void
usb_show_device_descriptor(IN PUSB_DEVICE_DESCRIPTOR deviceDescriptor) 
{
    if ( !deviceDescriptor ) return;
    uLan_DbgPrint("-------------------------\n");
    uLan_DbgPrint("Device Descriptor:\n");
    uLan_DbgPrint("-------------------------\n");
    uLan_DbgPrint("bLength %d\n", deviceDescriptor->bLength);
    uLan_DbgPrint("bDescriptorType 0x%x\n", deviceDescriptor->bDescriptorType);
    uLan_DbgPrint("bcdUSB 0x%x\n", deviceDescriptor->bcdUSB);
    uLan_DbgPrint("bDeviceClass 0x%x\n", deviceDescriptor->bDeviceClass);
    uLan_DbgPrint("bDeviceSubClass 0x%x\n", deviceDescriptor->bDeviceSubClass);
    uLan_DbgPrint("bDeviceProtocol 0x%x\n", deviceDescriptor->bDeviceProtocol);
    uLan_DbgPrint("bMaxPacketSize0 0x%x\n", deviceDescriptor->bMaxPacketSize0);
    uLan_DbgPrint("idVendor 0x%x\n", deviceDescriptor->idVendor);
    uLan_DbgPrint("idProduct 0x%x\n", deviceDescriptor->idProduct);
    uLan_DbgPrint("bcdDevice 0x%x\n", deviceDescriptor->bcdDevice);
    uLan_DbgPrint("iManufacturer 0x%x\n", deviceDescriptor->iManufacturer);
    uLan_DbgPrint("iProduct 0x%x\n", deviceDescriptor->iProduct);
    uLan_DbgPrint("iSerialNumber 0x%x\n", deviceDescriptor->iSerialNumber);
    uLan_DbgPrint("bNumConfigurations 0x%x\n", deviceDescriptor->bNumConfigurations);
}

/**
 * usb_show_config_descriptor - dump a configuration descriptor
 * @configurationDescriptor: pointer to the configurationDescriptor
 */   
void
usb_show_config_descriptor(IN PUSB_CONFIGURATION_DESCRIPTOR configurationDescriptor) 
{
    if ( !configurationDescriptor ) return;
    uLan_DbgPrint("-------------------------\n");
    uLan_DbgPrint("Configuration Descriptor:\n");
    uLan_DbgPrint("-------------------------\n");
    uLan_DbgPrint("bLength %d\n", configurationDescriptor->bLength);
    uLan_DbgPrint("bDescriptorType 0x%x\n", configurationDescriptor->bDescriptorType);
    uLan_DbgPrint("wTotalLength 0x%x\n", configurationDescriptor->wTotalLength);
    uLan_DbgPrint("bNumInterfaces 0x%x\n", configurationDescriptor->bNumInterfaces);
    uLan_DbgPrint("bConfigurationValue 0x%x\n", configurationDescriptor->bConfigurationValue);
    uLan_DbgPrint("iConfiguration 0x%x\n", configurationDescriptor->iConfiguration);
    uLan_DbgPrint("bmAttributes 0x%x\n", configurationDescriptor->bmAttributes);
    uLan_DbgPrint("MaxPower 0x%x\n", configurationDescriptor->MaxPower);
}

/**
 * usb_show_interface_information - dump a interface information
 * @interface: pointer to the interface
 */   
void 
usb_show_interface_information(IN PUSBD_INTERFACE_INFORMATION interface) 
{
    if ( !interface ) return;
    uLan_DbgPrint("-------------------------\n");
    uLan_DbgPrint("Interface Information:\n");
    uLan_DbgPrint("-------------------------\n");
    uLan_DbgPrint("NumberOfPipes 0x%x\n", interface->NumberOfPipes);
    uLan_DbgPrint("Length 0x%x\n", interface->Length);
    uLan_DbgPrint("Alt Setting 0x%x\n", interface->AlternateSetting);
    uLan_DbgPrint("Interface Number 0x%x\n", interface->InterfaceNumber);
    uLan_DbgPrint("Class, subclass, protocol 0x%x 0x%x 0x%x\n",
                  interface->Class,
                  interface->SubClass,
                  interface->Protocol);
}

/**
 * usb_show_endpoint_information - dump a endpoint information
 * @pipe: pointer to the pipe
 */   
void 
usb_show_endpoint_information(IN PUSBD_PIPE_INFORMATION pipe) 
{
    if ( !pipe ) return;
    uLan_DbgPrint("-------------------------\n");
    uLan_DbgPrint("Pipe Information:\n");
    uLan_DbgPrint("-------------------------\n");
    uLan_DbgPrint("PipeType 0x%x\n", pipe->PipeType);
    uLan_DbgPrint("EndpointAddress 0x%x\n", pipe->EndpointAddress);
    uLan_DbgPrint("MaxPacketSize 0x%x\n", pipe->MaximumPacketSize);
    uLan_DbgPrint("Interval 0x%x\n", pipe->Interval);
    uLan_DbgPrint("Handle 0x%x\n", pipe->PipeHandle);
    uLan_DbgPrint("MaximumTransferSize 0x%x\n", pipe->MaximumTransferSize);
}

/**
 * usb_show_device - dump a usb device informations
 * @dev: pointer to the usb device
 */   
void 
usb_show_device(usb_device *dev) 
{
    PUSBD_INTERFACE_INFORMATION interface;
    unsigned int j;
    
    if ( !dev ) return;
    /* device descriptor */
    usb_show_device_descriptor(dev->descriptor);
    
    /* selected configuration */
    if ( !dev->descriptor) return;
    usb_show_config_descriptor(dev->cfghandle);
    
    /* interfaces */
    interface=dev->interfaces.Interface;
    if ( !interface) return;
    usb_show_interface_information(interface);

    /* pipes */
    for (j=0; j<interface->NumberOfPipes; j++) {
        usb_show_endpoint_information(&interface->Pipes[j]);
    }
}
