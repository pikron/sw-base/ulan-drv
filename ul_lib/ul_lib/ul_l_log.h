/*******************************************************************
  uLan Communication - low level C and driver interface library

  ul_l_log.h	- simple logging support

  (C) Copyright 1996-2016 by Pavel Pisa - project originator
        http://cmp.felk.cvut.cz/~pisa
  (C) Copyright 1996-2016 PiKRON Ltd.
        http://www.pikron.com
  (C) Copyright 2002-2016 Petr Smolik


  The uLan driver project can be used and distributed
  in compliance with any of next licenses
   - GPL - GNU Public License
     See file COPYING for details.
   - LGPL - Lesser GNU Public License
   - MPL - Mozilla Public License
   - and other licenses added by project originator

  Code can be modified and re-distributed under any combination
  of the above listed licenses. If contributor does not agree with
  some of the licenses, he/she can delete appropriate line.
  WARNING: if you delete all lines, you are not allowed to
  distribute code or sources in any form.
 *******************************************************************/

#ifndef _UL_L_LOG_H
#define _UL_L_LOG_H

#ifdef __cplusplus
extern "C" {
#endif

#include <stdarg.h>

/* debugging support routines */
int uloi_debug_flg;

#ifndef UL_LOGL_FATAL

#define UL_LOGL_MASK (0xff)
#define UL_LOGL_CONT (0x1000)

#define UL_LOGL_FATAL   1
#define UL_LOGL_ERR     2
#define UL_LOGL_MSG     3
#define UL_LOGL_INF     4
#define UL_LOGL_DEB     5
#define UL_LOGL_TRASH   6

#endif /*UL_LOGL_FATAL*/

struct ul_log_domain;

typedef void (ul_log_fnc_t)(struct ul_log_domain *domain, int level,
	const char *format, va_list ap);

void ul_log(struct ul_log_domain *domain, int level,
	const char *format, ...) UL_ATTR_PRINTF (3, 4);

void ul_log_redir(ul_log_fnc_t *log_fnc, int add_flags);

#ifdef __cplusplus
} /* extern "C"*/
#endif

#endif /* _UL_L_LOG_H */

