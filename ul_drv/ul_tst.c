/*******************************************************************
  uLan Communication - uL_DRV - multiplatform uLan driver

  ul_tst.c	- some routines for kernel internal tests of driver

  (C) Copyright 1996-2004 by Pavel Pisa - project originator
        http://cmp.felk.cvut.cz/~pisa
  (C) Copyright 1996-2004 PiKRON Ltd.
        http://www.pikron.com
  (C) Copyright 2002-2004 Petr Smolik
  

  The uLan driver project can be used and distributed 
  in compliance with any of next licenses
   - GPL - GNU Public License
     See file COPYING for details.
   - LGPL - Lesser GNU Public License
   - MPL - Mozilla Public License
   - and other licenses added by project originator

  Code can be modified and re-distributed under any combination
  of the above listed licenses. If contributor does not agree with
  some of the licenses, he/she can delete appropriate line.
  WARNING: if you delete all lines, you are not allowed to
  distribute code or sources in any form.
 *******************************************************************/

/*******************************************************************/
/* Some internal test routines */

/* standard command codes 
   00H .. 3FH    store to buffer 
   40H .. 7FH    store to buffer without ACK
   80H .. 9FH    proccess at onece
   A0H .. BFH    process with additional receive
   C0H .. FFH    process with additional send
*/

#define UL_CMD_RES	0x80	/* Reinitialize RS485 */
#define UL_CMD_SFT	0x81	/* Test free space in input buffer */
#define UL_CMD_SID	0xF0	/* Send identification */
#define UL_CMD_SFI	0xF1	/* Send amount of free space in IB */
#define UL_CMD_TF0	0x98	/* End of stepping */
#define UL_CMD_TF1	0x99	/* Begin of stepping */
#define UL_CMD_STP	0x9A	/* Do step */
#define UL_CMD_DEB	0x9B	/* Additional debug commands */
#define UL_CMD_SPC	0xDA	/* Send state - for 8051 PCL PCH PSW ACC */

#define UL_CMD_RDM	0xF8	/* Read memory   T T B B L L */
#define UL_CMD_WRM	0xB8	/* Write mamory  T T B B L L */

void genrdmemblk(ul_drv *udrv, uchar dadr, int mtyp, int mbeg, int mlen)
{
  ul_mem_blk *blk, *beg_blk;
  ul_data_it di;
  blk=ul_new_frame_head(udrv, 3, 2, UL_CMD_RDM,
       UL_BFL_M2IN|UL_BFL_TAIL|UL_BFL_SND|
       UL_BFL_PRQ|UL_BFL_ARQ);
  if(blk==NULL) return;
  beg_blk=blk;
  ul_di_init(&di,blk);
  /* ul_di_write1(&di,0); */
  ul_di_write1(&di,(uchar)mtyp);
  ul_di_write1(&di,(uchar)(mtyp>>8));
  ul_di_write1(&di,(uchar)mbeg);
  ul_di_write1(&di,(uchar)(mbeg>>8));
  ul_di_write1(&di,(uchar)mlen);
  ul_di_write1(&di,(uchar)(mlen>>8));
  UL_BLK_HEAD(blk).len=di.pos;

  blk=ul_new_frame_head(udrv, 0, 0, 0,
       UL_BFL_M2IN|UL_BFL_REC|
       UL_BFL_PRQ|UL_BFL_ARQ /*|UL_BFL_LNMM*/);
  if(blk==NULL) { ul_free_blk(udrv,beg_blk); return;};
  /* UL_BLK_HEAD(blk).len=mlen; */
  ul_tail_frame_head(beg_blk,blk);
  ul_bll_move_mes(&udrv->prep_bll,beg_blk);
};

void geninfoblk(ul_drv *udrv)
{
  static int seq_num=0;
  ul_mem_blk *blk, *beg_blk;
  ul_data_it di;
  blk=ul_new_frame_head(udrv, 3, 2, UL_CMD_SID,
       UL_BFL_M2IN|UL_BFL_TAIL|UL_BFL_SND|
       UL_BFL_PRQ|UL_BFL_ARQ);
  if(blk==NULL) return;
  beg_blk=blk;
  ul_di_init(&di,blk);
  /* ul_di_write1(&di,0); */
  ul_di_write1(&di,1);
  ul_di_write1(&di,2);
  ul_di_write1(&di,3);
  ul_di_write1(&di,(uchar)(seq_num++));
  UL_BLK_HEAD(blk).len=di.pos;

  blk=ul_new_frame_head(udrv, 0, 0, 0,
       UL_BFL_M2IN|UL_BFL_REC|
       UL_BFL_PRQ|UL_BFL_ARQ);
  if(blk==NULL) { ul_free_blk(udrv,beg_blk); return;};
  ul_tail_frame_head(beg_blk,blk);
  ul_bll_move_mes(&udrv->prep_bll,beg_blk);
};

void gentestblk(ul_drv *udrv)
{
  static int seq_num=0;
  ul_mem_blk *blk;
  ul_data_it di;
  blk=ul_new_frame_head(udrv, 3, 2, 0x23,
       UL_BFL_M2IN|UL_BFL_SND|UL_BFL_ARQ);
  if(blk==NULL) return;
  ul_di_init(&di,blk);
  /* ul_di_write1(&di,0); */
  ul_di_write1(&di,0x11);
  ul_di_write1(&di,0x12);
  ul_di_write1(&di,0x13);
  ul_di_write1(&di,(uchar)(seq_num++));
  UL_BLK_HEAD(blk).len=di.pos;

  ul_bll_move_mes(&udrv->prep_bll,blk);
};

