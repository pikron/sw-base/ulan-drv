/*******************************************************************
  uLan Communication - low level C and driver interface library

  ul_l_drv.c	- driver interface

  (C) Copyright 1996-2016 by Pavel Pisa - project originator
        http://cmp.felk.cvut.cz/~pisa
  (C) Copyright 1996-2016 PiKRON Ltd.
        http://www.pikron.com
  (C) Copyright 2002-2016 Petr Smolik


  The uLan driver project can be used and distributed
  in compliance with any of next licenses
   - GPL - GNU Public License
     See file COPYING for details.
   - LGPL - Lesser GNU Public License
   - MPL - Mozilla Public License
   - and other licenses added by project originator

  Code can be modified and re-distributed under any combination
  of the above listed licenses. If contributor does not agree with
  some of the licenses, he/she can delete appropriate line.
  WARNING: if you delete all lines, you are not allowed to
  distribute code or sources in any form.
 *******************************************************************/

#include <sys/types.h>
#ifndef _MSC_VER
#include <sys/stat.h>
#include <sys/time.h>
#include <unistd.h>
#include <fcntl.h>
#include <errno.h>
#endif /*_MSC_VER*/
#include <string.h>
#include <stdlib.h>
#include <ul_lib/ulan.h>

#ifndef WITH_UL_FD_INDIRECT

#define UL_LIB_DRV_FNC_SPEC

#define ul_direct_open ul_open
#define ul_direct_close ul_close
#define ul_direct_drv_version ul_drv_version
#define ul_direct_read ul_read
#define ul_direct_write ul_write
#define ul_direct_newmsg ul_newmsg
#define ul_direct_tailmsg ul_tailmsg
#define ul_direct_freemsg ul_freemsg
#define ul_direct_acceptmsg ul_acceptmsg
#define ul_direct_actailmsg ul_actailmsg
#define ul_direct_addfilt ul_addfilt
#define ul_direct_abortmsg ul_abortmsg
#define ul_direct_rewmsg ul_rewmsg
#define ul_direct_inepoll ul_inepoll
#define ul_direct_drv_debflg ul_drv_debflg
#define ul_direct_fd_wait ul_fd_wait
#define ul_direct_setmyadr ul_setmyadr
#define ul_direct_setbaudrate ul_setbaudrate
#define ul_direct_setpromode ul_setpromode
#define ul_direct_setsubdev ul_setsubdev
#define ul_direct_setidstr ul_setidstr
#define ul_direct_queryparam ul_queryparam
#define ul_direct_route ul_route

int ul_fd_is_valid(ul_fd_t ul_fd)
{
  return (ul_fd != UL_FD_INVALID);
}

static inline ul_fd_t ul_direct_fd2ul_fd(ul_fd_t fd)
{
  return fd;
}

static inline void ul_direct_fd_free(ul_fd_t ul_fd)
{
}

#define ul_direct_overlapped_wait(x,y,z) -1 
#define WIN32_FILE_OVERLAPPED_FINI

#else /*WITH_UL_FD_INDIRECT*/

#define UL_LIB_DRV_FNC_SPEC static

typedef struct ul_fd_direct_context_t {
  ul_fd_context_t context;
  ul_fd_direct_t sys_fd;
#ifdef _WIN32
  OVERLAPPED cOverlapped;
  OVERLAPPED cOverlapped_wait;
  int wait_pending;
#endif /* WIN32 */  
} ul_fd_direct_context_t;

ul_fd_ops_t ul_fd_direct_ops;

static inline ul_fd_t ul_direct_fd2ul_fd(ul_fd_direct_t fd)
{
  ul_fd_direct_context_t *ul_direct_ctx;
  if (fd==UL_FD_DIRECT_INVALID)
    return UL_FD_INVALID;
  ul_direct_ctx=malloc(sizeof(ul_fd_direct_context_t));
  if(!ul_direct_ctx)
    return UL_FD_INVALID;
  memset(ul_direct_ctx,0,sizeof(ul_fd_direct_context_t));
  ul_direct_ctx->sys_fd=fd;
  ul_direct_ctx->context.fd_ops=&ul_fd_direct_ops;
#ifdef WIN32_FILE_OVERLAPPED
  ul_direct_ctx->wait_pending = 0;
  ul_direct_ctx->cOverlapped.hEvent = CreateEvent(NULL, TRUE, FALSE, NULL);
  ul_direct_ctx->cOverlapped.Offset = 0;
  ul_direct_ctx->cOverlapped.OffsetHigh = 0;
  ul_direct_ctx->cOverlapped_wait.hEvent = CreateEvent(NULL, TRUE, FALSE, NULL);
  if ((ul_direct_ctx->cOverlapped.hEvent == NULL) ||
      (ul_direct_ctx->cOverlapped_wait.hEvent == NULL)) {
    if (ul_direct_ctx->cOverlapped.hEvent != NULL)
      CloseHandle(ul_direct_ctx->cOverlapped.hEvent);
    if (ul_direct_ctx->cOverlapped_wait.hEvent != NULL)
      CloseHandle(ul_direct_ctx->cOverlapped_wait.hEvent);
    free(ul_direct_ctx);
    return UL_FD_INVALID;
  }
#endif /* WIN32_FILE_OVERLAPPED */
  return &ul_direct_ctx->context;
}

static inline void ul_direct_fd_free(ul_fd_t ul_fd)
{
  ul_fd_direct_context_t *ul_direct_ctx;
  if(ul_fd==UL_FD_INVALID)
    return;

  ul_direct_ctx=UL_CONTAINEROF(ul_fd, ul_fd_direct_context_t, context);
#ifdef WIN32_FILE_OVERLAPPED
  CloseHandle(ul_direct_ctx->cOverlapped.hEvent);
  CloseHandle(ul_direct_ctx->cOverlapped_wait.hEvent);
  ul_direct_ctx->cOverlapped.hEvent=NULL;
  ul_direct_ctx->cOverlapped_wait.hEvent=NULL;
#endif /* WIN32_FILE_OVERLAPPED */
  free(ul_direct_ctx);
}

ul_fd_direct_t ul_direct_fd2sys_fd(ul_fd_t ul_fd)
{
  if(ul_fd==UL_FD_INVALID)
    return UL_FD_DIRECT_INVALID;
  else
    return UL_CONTAINEROF(ul_fd, ul_fd_direct_context_t, context)->sys_fd;
}

#ifdef WIN32_FILE_OVERLAPPED
int ul_direct_overlapped_wait(ul_fd_direct_context_t *ul_direct_ctx, DWORD *bytes_ret, int ms_timeout)
{
  int ret=-1;
  if (GetLastError() != ERROR_IO_PENDING) return -1;
  if (WaitForSingleObject(ul_direct_ctx->cOverlapped.hEvent, ms_timeout) == WAIT_OBJECT_0) {
    ret=GetOverlappedResult(ul_direct_ctx->sys_fd,&ul_direct_ctx->cOverlapped,
                            bytes_ret, FALSE);
  }
  CancelIo(ul_direct_ctx->sys_fd);
  return ret;
}

#define WIN32_FILE_OVERLAPPED_FINI                                     \
 ul_fd_direct_context_t *ul_direct_ctx;                                \
 ul_direct_ctx=UL_CONTAINEROF(ul_fd, ul_fd_direct_context_t, context); \
 pOverlapped=&ul_direct_ctx->cOverlapped;

#else /* WIN32_FILE_OVERLAPPED */

 #define ul_direct_overlapped_wait(x,y,z) -1 
 #define WIN32_FILE_OVERLAPPED_FINI

#endif /* WIN32_FILE_OVERLAPPED */

#endif /* WITH_UL_FD_INDIRECT */

#if !defined(_WIN32) && !defined(UL_DRV_IN_LIB)

#include <sys/select.h>

#ifdef CONFIG_OC_UL_DRV_NUTTX
#ifndef S_IWRITE
#define S_IWRITE 0
#endif
#ifndef S_IREAD
#define S_IREAD 0
#endif
#endif /*CONFIG_OC_UL_DRV_NUTTX*/

UL_LIB_DRV_FNC_SPEC
ul_fd_t ul_direct_open(const char *dev_name, const char *options)
{
  if(dev_name==NULL) dev_name=UL_DEV_NAME;
  else if(!strncmp(dev_name,"drv:",4)) dev_name+=4;
  return ul_direct_fd2ul_fd(open(dev_name,O_RDWR, S_IWRITE | S_IREAD));
}

UL_LIB_DRV_FNC_SPEC
int ul_direct_close(ul_fd_t ul_fd)
{
  int res;
  res=close(ul_fd2sys_fd(ul_fd));
  ul_direct_fd_free(ul_fd);
  return res;
}

UL_LIB_DRV_FNC_SPEC
int ul_direct_drv_version(ul_fd_t ul_fd)
{
  return ioctl(ul_fd2sys_fd(ul_fd),UL_DRV_VER);
}


UL_LIB_DRV_FNC_SPEC
ssize_t ul_direct_read(ul_fd_t ul_fd, void *buffer, size_t size)
{
  return read(ul_fd2sys_fd(ul_fd),buffer,size);
}

UL_LIB_DRV_FNC_SPEC
ssize_t ul_direct_write(ul_fd_t ul_fd, const void *buffer, size_t size)
{
  return write(ul_fd2sys_fd(ul_fd),buffer,size);
}

UL_LIB_DRV_FNC_SPEC
int ul_direct_newmsg(ul_fd_t ul_fd,const ul_msginfo *msginfo)
{
  return ioctl(ul_fd2sys_fd(ul_fd),UL_NEWMSG,msginfo);
}

UL_LIB_DRV_FNC_SPEC
int ul_direct_tailmsg(ul_fd_t ul_fd,const ul_msginfo *msginfo)
{
  return ioctl(ul_fd2sys_fd(ul_fd),UL_TAILMSG,msginfo);
}

UL_LIB_DRV_FNC_SPEC
int ul_direct_freemsg(ul_fd_t ul_fd)
{
  return ioctl(ul_fd2sys_fd(ul_fd),UL_FREEMSG);
}

UL_LIB_DRV_FNC_SPEC
int ul_direct_acceptmsg(ul_fd_t ul_fd,ul_msginfo *msginfo)
{
  return ioctl(ul_fd2sys_fd(ul_fd),UL_ACCEPTMSG,msginfo);
}

UL_LIB_DRV_FNC_SPEC
int ul_direct_actailmsg(ul_fd_t ul_fd,ul_msginfo *msginfo)
{
  return ioctl(ul_fd2sys_fd(ul_fd),UL_ACTAILMSG,msginfo);
}

UL_LIB_DRV_FNC_SPEC
int ul_direct_addfilt(ul_fd_t ul_fd,const ul_msginfo *msginfo)
{
  return ioctl(ul_fd2sys_fd(ul_fd),UL_ADDFILT,msginfo);
}

UL_LIB_DRV_FNC_SPEC
int ul_direct_abortmsg(ul_fd_t ul_fd)
{
  return ioctl(ul_fd2sys_fd(ul_fd),UL_ABORTMSG);
}

UL_LIB_DRV_FNC_SPEC
int ul_direct_rewmsg(ul_fd_t ul_fd)
{
  return ioctl(ul_fd2sys_fd(ul_fd),UL_REWMSG);
}

UL_LIB_DRV_FNC_SPEC
int ul_direct_inepoll(ul_fd_t ul_fd)
{
  return ioctl(ul_fd2sys_fd(ul_fd),UL_INEPOLL);
}

UL_LIB_DRV_FNC_SPEC
int ul_direct_drv_debflg(ul_fd_t ul_fd,int debug_msk)
{
  return ioctl(ul_fd2sys_fd(ul_fd),UL_DEBFLG,debug_msk);
}

int ul_direct_fd_wait(ul_fd_t ul_fd, int wait_sec)
{
  int ret;
  struct timeval timeout;
  fd_set set;

  FD_ZERO (&set);
  FD_SET (ul_fd2sys_fd(ul_fd), &set);
  timeout.tv_sec = wait_sec;
  timeout.tv_usec = 0;
  while ((ret=select(FD_SETSIZE,&set, NULL, NULL,&timeout))==-1
          &&errno==-EINTR);
  return ret;
}

UL_LIB_DRV_FNC_SPEC
int ul_direct_setmyadr(ul_fd_t ul_fd, int newadr)
{
  return ioctl(ul_fd2sys_fd(ul_fd),UL_SETMYADR,newadr);
}

UL_LIB_DRV_FNC_SPEC
int ul_direct_setidstr(ul_fd_t ul_fd, const char *idstr)
{
  return ioctl(ul_fd2sys_fd(ul_fd),UL_SETIDSTR,idstr);
}

UL_LIB_DRV_FNC_SPEC
int ul_direct_setbaudrate(ul_fd_t ul_fd, int baudrate)
{
  return ioctl(ul_fd2sys_fd(ul_fd),UL_SETBAUDRATE,baudrate);
}

UL_LIB_DRV_FNC_SPEC
int ul_direct_setpromode(ul_fd_t ul_fd, int pro_mode)
{
  return ioctl(ul_fd2sys_fd(ul_fd),UL_SETPROMODE,pro_mode);
}

UL_LIB_DRV_FNC_SPEC
int ul_direct_setsubdev(ul_fd_t ul_fd, int subdevidx)
{
  return ioctl(ul_fd2sys_fd(ul_fd),UL_SETSUBDEV,subdevidx);
}

UL_LIB_DRV_FNC_SPEC
int ul_direct_queryparam(ul_fd_t ul_fd, unsigned long query, unsigned long *pvalue)
{
  long value;
  value=ioctl(ul_fd2sys_fd(ul_fd),UL_QUERYPARAM,query);
  if (value>=0) {
    *pvalue=value;
    return 0;
  }
  return value;
}

UL_LIB_DRV_FNC_SPEC
int ul_direct_route(ul_fd_t ul_fd, ul_route_range_t *rr)
{
  return ioctl(ul_fd2sys_fd(ul_fd),UL_ROUTE,rr);
}

/*******************************************************************/
#elif defined(_WIN32)

ul_fd_t ul_direct_open(const char *dev_name, const char *options)
{
  DWORD attr=FILE_ATTRIBUTE_NORMAL;
  if(dev_name==NULL) dev_name=UL_DEV_NAME;
  else if(!strncmp(dev_name,"drv:",4)) dev_name+=4;

#ifdef WIN32_FILE_OVERLAPPED 
  attr=FILE_FLAG_OVERLAPPED; 
#endif /* WIN32_FILE_OVERLAPPED */
  
  return ul_direct_fd2ul_fd(CreateFile(dev_name,GENERIC_READ | GENERIC_WRITE,
                    0, 0, OPEN_EXISTING, attr, 0));
}

int ul_direct_close(ul_fd_t ul_fd)
{
  int res=0;
  if(!CloseHandle(ul_fd2sys_fd(ul_fd))) res=-1;
  ul_direct_fd_free(ul_fd);
  return res;
}

int ul_direct_drv_version(ul_fd_t ul_fd)
{
  DWORD ret,bytes_ret;
  LPOVERLAPPED pOverlapped=NULL;
  WIN32_FILE_OVERLAPPED_FINI;
  if(!DeviceIoControl(ul_fd2sys_fd(ul_fd),UL_DRV_VER,
                      NULL,0,&ret,sizeof(ret),
                      &bytes_ret,pOverlapped)) 
    ret=ul_direct_overlapped_wait(ul_direct_ctx,&bytes_ret,INFINITE);  
  return ret;
}

ssize_t ul_direct_read(ul_fd_t ul_fd, void *buffer, size_t size)
{
  DWORD ret;
  LPOVERLAPPED pOverlapped=NULL;
  WIN32_FILE_OVERLAPPED_FINI;
  if(!ReadFile(ul_fd2sys_fd(ul_fd),buffer,size,&ret,pOverlapped)) 
    ul_direct_overlapped_wait(ul_direct_ctx,&ret,INFINITE);  
  return ret;
}

ssize_t ul_direct_write(ul_fd_t ul_fd, const void *buffer, size_t size)
{
  DWORD ret;
  LPOVERLAPPED pOverlapped=NULL;
  WIN32_FILE_OVERLAPPED_FINI;
  if(!WriteFile(ul_fd2sys_fd(ul_fd),buffer,size,&ret,pOverlapped)) 
    ul_direct_overlapped_wait(ul_direct_ctx,&ret,INFINITE);  
  return ret;
}

int ul_direct_newmsg(ul_fd_t ul_fd,const ul_msginfo *msginfo)
{
  DWORD bytes_ret;
  LPOVERLAPPED pOverlapped=NULL;
  WIN32_FILE_OVERLAPPED_FINI;
  if(!DeviceIoControl(ul_fd2sys_fd(ul_fd),UL_NEWMSG,
                      (void *)msginfo,sizeof(ul_msginfo),NULL,0,
                      &bytes_ret,pOverlapped)) 
    return(ul_direct_overlapped_wait(ul_direct_ctx,&bytes_ret,INFINITE));  
  return 0;
}

int ul_direct_tailmsg(ul_fd_t ul_fd,const ul_msginfo *msginfo)
{
  DWORD bytes_ret;
  LPOVERLAPPED pOverlapped=NULL;
  WIN32_FILE_OVERLAPPED_FINI;
  if(!DeviceIoControl(ul_fd2sys_fd(ul_fd),UL_TAILMSG,
                      (void *)msginfo,sizeof(ul_msginfo),NULL,0,
                      &bytes_ret,pOverlapped)) 
    return(ul_direct_overlapped_wait(ul_direct_ctx,&bytes_ret,INFINITE));  
  return 0;
}

int ul_direct_freemsg(ul_fd_t ul_fd)
{
  DWORD bytes_ret;
  int ret=0;
  LPOVERLAPPED pOverlapped=NULL;
  WIN32_FILE_OVERLAPPED_FINI;
  if(!DeviceIoControl(ul_fd2sys_fd(ul_fd),UL_FREEMSG,NULL,0,&ret,sizeof(ret),
                      &bytes_ret,pOverlapped)) 
    if (!ul_direct_overlapped_wait(ul_direct_ctx,&bytes_ret,INFINITE)) 
      return -1;
  if(bytes_ret!=sizeof(ret)) return -1;
  return ret;
}

int ul_direct_acceptmsg(ul_fd_t ul_fd,ul_msginfo *msginfo)
{
  DWORD bytes_ret;
  LPOVERLAPPED pOverlapped=NULL;
  WIN32_FILE_OVERLAPPED_FINI;
  if(!DeviceIoControl(ul_fd2sys_fd(ul_fd),UL_ACCEPTMSG,
                      NULL,0,msginfo,sizeof(ul_msginfo),
                      &bytes_ret,pOverlapped))
    if (!ul_direct_overlapped_wait(ul_direct_ctx,&bytes_ret,INFINITE)) 
      return -1;
  if(bytes_ret!=sizeof(ul_msginfo)) return -1;
  return 0;
}

int ul_direct_actailmsg(ul_fd_t ul_fd,ul_msginfo *msginfo)
{
  DWORD bytes_ret;
  LPOVERLAPPED pOverlapped=NULL;
  WIN32_FILE_OVERLAPPED_FINI;
  if(!DeviceIoControl(ul_fd2sys_fd(ul_fd),UL_ACTAILMSG,
                      NULL,0,msginfo,sizeof(ul_msginfo),
                      &bytes_ret,pOverlapped)) 
    if (!ul_direct_overlapped_wait(ul_direct_ctx,&bytes_ret,INFINITE)) 
      return -1;
  if(bytes_ret!=sizeof(ul_msginfo)) return -1;
  return 0;
}

int ul_direct_addfilt(ul_fd_t ul_fd,const ul_msginfo *msginfo)
{
  DWORD bytes_ret;
  LPOVERLAPPED pOverlapped=NULL;
  WIN32_FILE_OVERLAPPED_FINI;
  if(!DeviceIoControl(ul_fd2sys_fd(ul_fd),UL_ADDFILT,
                      (void *)msginfo,sizeof(ul_msginfo),NULL,0,
                      &bytes_ret,pOverlapped)) return -1;
    if (!ul_direct_overlapped_wait(ul_direct_ctx,&bytes_ret,INFINITE)) 
      return -1;
  return 0;
}

int ul_direct_abortmsg(ul_fd_t ul_fd)
{
  DWORD bytes_ret;
  LPOVERLAPPED pOverlapped=NULL;
  WIN32_FILE_OVERLAPPED_FINI;
  if(!DeviceIoControl(ul_fd2sys_fd(ul_fd),UL_ABORTMSG,NULL,0,NULL,0,
                      &bytes_ret,pOverlapped))
    if (!ul_direct_overlapped_wait(ul_direct_ctx,&bytes_ret,INFINITE)) 
      return -1;
  return 0;
}

int ul_direct_rewmsg(ul_fd_t ul_fd)
{
  DWORD bytes_ret;
  LPOVERLAPPED pOverlapped=NULL;
  WIN32_FILE_OVERLAPPED_FINI;
  if(!DeviceIoControl(ul_fd2sys_fd(ul_fd),UL_REWMSG,NULL,0,NULL,0,
                      &bytes_ret,pOverlapped)) 
    if (!ul_direct_overlapped_wait(ul_direct_ctx,&bytes_ret,INFINITE)) 
      return -1;
  return 0;
}

int ul_direct_inepoll(ul_fd_t ul_fd)
{
  DWORD bytes_ret;
  int ret;
  LPOVERLAPPED pOverlapped=NULL;
  WIN32_FILE_OVERLAPPED_FINI;
  if(!DeviceIoControl(ul_fd2sys_fd(ul_fd),UL_INEPOLL,NULL,0,&ret,sizeof(ret),
                      &bytes_ret,pOverlapped)) 
    if (!ul_direct_overlapped_wait(ul_direct_ctx,&bytes_ret,INFINITE)) 
      return -1;
  if(bytes_ret!=sizeof(ret)) return -1;
  return ret;
}

int ul_direct_drv_debflg(ul_fd_t ul_fd,int debug_msk)
{
  DWORD bytes_ret;
  LPOVERLAPPED pOverlapped=NULL;
  WIN32_FILE_OVERLAPPED_FINI;
  if(!DeviceIoControl(ul_fd2sys_fd(ul_fd),UL_DEBFLG,
		  &debug_msk,sizeof(debug_msk),NULL,0,
		  &bytes_ret,pOverlapped))
    if (!ul_direct_overlapped_wait(ul_direct_ctx,&bytes_ret,INFINITE)) 
      return -1;
  return 0;
}

#ifndef WITHOUT_SYS_SELECT

int ul_direct_fd_wait(ul_fd_t ul_fd, int wait_sec)
{
  int ret;
  struct timeval timeout;
  fd_set set;

  FD_ZERO (&set);
  FD_SET (ul_fd2sys_fd(ul_fd), &set);
  timeout.tv_sec = wait_sec;
  timeout.tv_usec = 0;
  while ((ret=select(FD_SETSIZE,&set, NULL, NULL,&timeout))==-1
          &&errno==-EINTR);
  return ret;
}

#else /* WITHOUT_SYS_SELECT */

#ifndef WIN32_FILE_OVERLAPPED
int ul_direct_fd_wait(ul_fd_t ul_fd, int wait_sec)
{
  int ret;
  long loops=0;
  do
  { ret=ul_inepoll(ul_fd);
    if(ret) return ret;
    Sleep(100);
  }while(loops++<(wait_sec*10));
 
  return 0;
}
#else /* WIN32_FILE_OVERLAPPED */
int ul_direct_fd_wait(ul_fd_t ul_fd, int wait_sec)
{
  DWORD bytes_ret;
  DWORD waitres;
  DWORD ms_timeout=wait_sec*1000;
  LPOVERLAPPED pOverlapped=NULL;
  WIN32_FILE_OVERLAPPED_FINI;
  pOverlapped=&ul_direct_ctx->cOverlapped_wait;

  if (!ul_direct_ctx->wait_pending) {
    ul_direct_ctx->wait_pending = 1;
    if(DeviceIoControl(ul_fd2sys_fd(ul_fd),UL_WAITREC,
		  NULL,0,NULL,0,
		  &bytes_ret,pOverlapped)) {
      ul_direct_ctx->wait_pending = 0;
      return 1;
    }
    if (GetLastError() != ERROR_IO_PENDING) {
      ul_direct_ctx->wait_pending = 0;
      return -1;
    }
  }

  waitres=WaitForSingleObject(pOverlapped->hEvent, ms_timeout);

  if (waitres == WAIT_OBJECT_0) {
    ul_direct_ctx->wait_pending = 0;
    if(GetOverlappedResult(ul_direct_ctx->sys_fd,pOverlapped,
                          &bytes_ret, FALSE))
      return 1;
  }
  if (waitres == WAIT_TIMEOUT)
    return 0;

  ul_direct_ctx->wait_pending = 0;
  return -1;
}
#endif  /* WIN32_FILE_OVERLAPPED */

#endif /* WITHOUT_SYS_SELECT */

UL_LIB_DRV_FNC_SPEC
int ul_direct_setmyadr(ul_fd_t ul_fd, int newadr)
{
  DWORD bytes_ret;
  LPOVERLAPPED pOverlapped=NULL;
  WIN32_FILE_OVERLAPPED_FINI;
  if(!DeviceIoControl(ul_fd2sys_fd(ul_fd),UL_SETMYADR,
		  &newadr,sizeof(newadr),NULL,0,
		  &bytes_ret,pOverlapped)) 
    if (!ul_direct_overlapped_wait(ul_direct_ctx,&bytes_ret,INFINITE)) 
      return -1;
  return 0;
}

UL_LIB_DRV_FNC_SPEC
int ul_direct_setidstr(ul_fd_t ul_fd, const char *idstr)
{
  DWORD bytes_ret;
  PVOID *iobuff = (PVOID *)idstr;
  LPOVERLAPPED pOverlapped=NULL;
  WIN32_FILE_OVERLAPPED_FINI;
  if(!DeviceIoControl(ul_fd2sys_fd(ul_fd),UL_SETIDSTR,
		  iobuff,strlen(idstr),NULL,0,
		  &bytes_ret,pOverlapped)) 
    if (!ul_direct_overlapped_wait(ul_direct_ctx,&bytes_ret,INFINITE)) 
      return -1;
  return 0;
}

UL_LIB_DRV_FNC_SPEC
int ul_direct_setbaudrate(ul_fd_t ul_fd, int baudrate)
{
  DWORD bytes_ret;
  LPOVERLAPPED pOverlapped=NULL;
  WIN32_FILE_OVERLAPPED_FINI;
  if(!DeviceIoControl(ul_fd2sys_fd(ul_fd),UL_SETBAUDRATE,
		  &baudrate,sizeof(baudrate),NULL,0,
		  &bytes_ret,pOverlapped)) 
    if (!ul_direct_overlapped_wait(ul_direct_ctx,&bytes_ret,INFINITE)) 
      return -1;
  return 0;
}

UL_LIB_DRV_FNC_SPEC
int ul_direct_setpromode(ul_fd_t ul_fd, int pro_mode)
{
  DWORD bytes_ret;
  LPOVERLAPPED pOverlapped=NULL;
  WIN32_FILE_OVERLAPPED_FINI;
  if(!DeviceIoControl(ul_fd2sys_fd(ul_fd),UL_SETPROMODE,
		  &pro_mode,sizeof(pro_mode),NULL,0,
		  &bytes_ret,pOverlapped)) 
    if (!ul_direct_overlapped_wait(ul_direct_ctx,&bytes_ret,INFINITE)) 
      return -1;
  return 0;
}

UL_LIB_DRV_FNC_SPEC
int ul_direct_setsubdev(ul_fd_t ul_fd, int subdevidx)
{
  DWORD bytes_ret;
  LPOVERLAPPED pOverlapped=NULL;
  WIN32_FILE_OVERLAPPED_FINI;
  if(!DeviceIoControl(ul_fd2sys_fd(ul_fd),UL_SETSUBDEV,
		  &subdevidx,sizeof(subdevidx),NULL,0,
		  &bytes_ret,pOverlapped)) 
    if (!ul_direct_overlapped_wait(ul_direct_ctx,&bytes_ret,INFINITE)) 
      return -1;
  return 0;
}

UL_LIB_DRV_FNC_SPEC
int ul_direct_queryparam(ul_fd_t ul_fd, unsigned long query, unsigned long *pvalue)
{
  DWORD bytes_ret;
  LPOVERLAPPED pOverlapped=NULL;
  WIN32_FILE_OVERLAPPED_FINI;
  if(!DeviceIoControl(ul_fd2sys_fd(ul_fd),UL_QUERYPARAM,
		  &query,sizeof(query),pvalue,sizeof(unsigned long),
		  &bytes_ret,pOverlapped)) 
    if (!ul_direct_overlapped_wait(ul_direct_ctx,&bytes_ret,INFINITE)) 
      return -1;
  return 0;
}

UL_LIB_DRV_FNC_SPEC
int ul_direct_route(ul_fd_t ul_fd, ul_route_range_t *rr)
{
  DWORD bytes_ret;
  LPOVERLAPPED pOverlapped=NULL;
  WIN32_FILE_OVERLAPPED_FINI;
  if(!DeviceIoControl(ul_fd2sys_fd(ul_fd),UL_ROUTE,
                  (void *)rr,sizeof(ul_route_range_t),(void*)rr,sizeof(ul_route_range_t),
		  &bytes_ret,pOverlapped))
    if (!ul_direct_overlapped_wait(ul_direct_ctx,&bytes_ret,INFINITE)) 
      return -1;
  return 0;
}

#elif defined(__DJGPP__)

int ul_direct_fd_wait(ul_fd_t ul_fd, int wait_sec)
{
  int ret;
  long loops=0;
  long int starttime;
  struct timeval actual;
  gettimeofday(&actual,NULL);
  starttime=actual.tv_sec+1;
  do
  { ret=ul_inepoll(ul_fd);
    if(ret) return ret;
    /* uld_djgpp_call_irq(); */
    gettimeofday(&actual,NULL);
    loops++;
  }while((wait_sec+starttime-(long)actual.tv_sec)>0);
 
  return 0;
}

#elif defined(UL_DRV_IN_LIB)

int ul_direct_fd_wait(ul_fd_t ul_fd, int wait_sec)
{
  int ret;
  long loops=0;
  do
  { ret=ul_inepoll(ul_fd);
    if(ret) return ret;
    #ifndef CONFIG_OC_UL_DRV_SYSLESS
     usleep(100*1000);
    #endif 
    /* uld_djgpp_call_irq(); */
  }while(loops++<(wait_sec*10));
 
  return 0;
}

#else
  #error Interface to driver not defined for target
#endif

#ifdef WITH_UL_FD_INDIRECT

int ul_direct_namematch(const char *dev_name)
{
  int ret=0;
  if(!dev_name) ret=1;
  else if(!strncmp(dev_name,"drv:",4))
    ret=100; /* 100% name match for driver */
  else if(!strchr(dev_name,':'))
    ret=5;   /* no transport selected, direct driver is reasonable for this name */
  return ret;
}

ul_fd_ops_t ul_fd_direct_ops={
  "direct",
  ul_direct_namematch,
  ul_direct_open,
  ul_direct_close,
  ul_direct_drv_version,
  ul_direct_read,
  ul_direct_write,
  ul_direct_newmsg,
  ul_direct_tailmsg,
  ul_direct_freemsg,
  ul_direct_acceptmsg,
  ul_direct_actailmsg,
  ul_direct_addfilt,
  ul_direct_abortmsg,
  ul_direct_rewmsg,
  ul_direct_inepoll,
  ul_direct_drv_debflg,
  ul_direct_fd_wait,
  ul_direct_fd2sys_fd,
  ul_direct_setmyadr,
  ul_direct_setidstr,
  ul_direct_setbaudrate,
  ul_direct_setpromode,
  ul_direct_setsubdev,
  ul_direct_queryparam,
  ul_direct_route
};

#endif /*WITH_UL_FD_INDIRECT*/
