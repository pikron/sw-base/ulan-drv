# Microsoft Developer Studio Project File - Name="ul_wdm" - Package Owner=<4>
# Microsoft Developer Studio Generated Build File, Format Version 6.00
# ** DO NOT EDIT **

# TARGTYPE "Win32 (x86) External Target" 0x0106

CFG=ul_wdm - Win32 Debug
!MESSAGE This is not a valid makefile. To build this project using NMAKE,
!MESSAGE use the Export Makefile command and run
!MESSAGE 
!MESSAGE NMAKE /f "ul_wdm.mak".
!MESSAGE 
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE 
!MESSAGE NMAKE /f "ul_wdm.mak" CFG="ul_wdm - Win32 Debug"
!MESSAGE 
!MESSAGE Possible choices for configuration are:
!MESSAGE 
!MESSAGE "ul_wdm - Win32 Release" (based on "Win32 (x86) External Target")
!MESSAGE "ul_wdm - Win32 Debug" (based on "Win32 (x86) External Target")
!MESSAGE 

# Begin Project
# PROP AllowPerConfigDependencies 0
# PROP Scc_ProjName ""
# PROP Scc_LocalPath ""

!IF  "$(CFG)" == "ul_wdm - Win32 Release"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 0
# PROP BASE Output_Dir "Release"
# PROP BASE Intermediate_Dir "Release"
# PROP BASE Cmd_Line "NMAKE /f ul_wdm.mak"
# PROP BASE Rebuild_Opt "/a"
# PROP BASE Target_File "ul_wdm.exe"
# PROP BASE Bsc_Name "ul_wdm.bsc"
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 0
# PROP Output_Dir "Release"
# PROP Intermediate_Dir "Release"
# PROP Cmd_Line "nmake /f "ul_wdm.mak""
# PROP Rebuild_Opt "/a"
# PROP Target_File "ul_wdm.sys"
# PROP Bsc_Name ""
# PROP Target_Dir ""

!ELSEIF  "$(CFG)" == "ul_wdm - Win32 Debug"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 1
# PROP BASE Output_Dir "Debug"
# PROP BASE Intermediate_Dir "Debug"
# PROP BASE Cmd_Line "NMAKE /f ul_wdm.mak"
# PROP BASE Rebuild_Opt "/a"
# PROP BASE Target_File "ul_wdm.exe"
# PROP BASE Bsc_Name "ul_wdm.bsc"
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 1
# PROP Output_Dir "Debug"
# PROP Intermediate_Dir "Debug"
# PROP Cmd_Line "nmake /f "ul_wdm.mak""
# PROP Rebuild_Opt "/a"
# PROP Target_File "ul_wdm.sys"
# PROP Bsc_Name ""
# PROP Target_Dir ""

!ENDIF 

# Begin Target

# Name "ul_wdm - Win32 Release"
# Name "ul_wdm - Win32 Debug"

!IF  "$(CFG)" == "ul_wdm - Win32 Release"

!ELSEIF  "$(CFG)" == "ul_wdm - Win32 Debug"

!ENDIF 

# Begin Group "Source Files"

# PROP Default_Filter "cpp;c;cxx;rc;def;r;odl;idl;hpj;bat"
# Begin Source File

SOURCE=.\ul_base.c
# End Source File
# Begin Source File

SOURCE=.\ul_c450.c
# End Source File
# Begin Source File

SOURCE=.\ul_c510.c
# End Source File
# Begin Source File

SOURCE=.\ul_c950pci.c
# End Source File
# Begin Source File

SOURCE=.\ul_cps1.c
# End Source File
# Begin Source File

SOURCE=.\ul_debug.c
# End Source File
# Begin Source File

SOURCE=.\ul_devtab.c
# End Source File
# Begin Source File

SOURCE=.\ul_di.c
# End Source File
# Begin Source File

SOURCE=.\ul_drv.c
# End Source File
# Begin Source File

SOURCE=.\ul_frame.c
# End Source File
# Begin Source File

SOURCE=.\ul_mem.c
# End Source File
# Begin Source File

SOURCE=.\ul_tors.c
# End Source File
# Begin Source File

SOURCE=.\ul_tst.c
# End Source File
# Begin Source File

SOURCE=.\ul_ufsm.c
# End Source File
# Begin Source File

SOURCE=.\ul_wdbase.c
# End Source File
# Begin Source File

SOURCE=.\ul_wdent.c
# End Source File
# Begin Source File

SOURCE=.\ul_wdpnp.c
# End Source File
# Begin Source File

SOURCE=.\ul_wdusb.c
# End Source File
# End Group
# Begin Group "Header Files"

# PROP Default_Filter "h;hpp;hxx;hm;inl"
# Begin Source File

SOURCE=.\serial_reg.h
# End Source File
# Begin Source File

SOURCE=.\ul_82510.h
# End Source File
# Begin Source File

SOURCE=.\ul_drv.h
# End Source File
# Begin Source File

SOURCE=.\ul_hdep.h
# End Source File
# Begin Source File

SOURCE=.\ul_wdbase.h
# End Source File
# Begin Source File

SOURCE=.\ul_wdinc.h
# End Source File
# Begin Source File

SOURCE=.\ul_wdusb.h
# End Source File
# End Group
# Begin Group "Resource Files"

# PROP Default_Filter "ico;cur;bmp;dlg;rc2;rct;bin;rgs;gif;jpg;jpeg;jpe"
# End Group
# Begin Group "Additional Files"

# PROP Default_Filter ""
# Begin Source File

SOURCE=.\ul_wdm.err
# End Source File
# Begin Source File

SOURCE=.\ul_wdm.lnr
# End Source File
# Begin Source File

SOURCE=.\ul_wdm.mak
# End Source File
# End Group
# End Target
# End Project
