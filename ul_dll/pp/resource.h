//{{NO_DEPENDENCIES}}
// Microsoft Developer Studio generated include file.
// Used by proppage.rc
//
#define DLG_PPAGE                       17
#define IDI_ICON1                       109
#define PP_ULAN_BAUD                    841
#define PP_ULAN_DEBUG                   843
#define IDC_RESTORE                     844
#define IDC_OK                          1044
#define IDC_LBAUD                       1045
#define IDC_LADDR                       1046
#define IDC_LGW                         1047
#define IDC_LDEBUG                      1048
#define IDC_GSET                        1049
#define PP_ULAN_ADDR                    1056
#define PP_ULAN_GW                      1057
#define PP_ULAN_PMODE                   1058
#define IDC_REFRESH                     1059
#define IDC_APPLY                       1060
#define IDC_STATIC                      -1

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NO_MFC                     1
#define _APS_NEXT_RESOURCE_VALUE        110
#define _APS_NEXT_COMMAND_VALUE         40001
#define _APS_NEXT_CONTROL_VALUE         1060
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
