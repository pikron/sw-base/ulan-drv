/*******************************************************************
  uLan Communication - low level and link driver

  ul_hdep.h	- host system dependant driver routines

  (C) Copyright 1996,1997 by Pavel Pisa 

  The uLan driver is distributed under the Gnu General Public Licence. 
  See file COPYING for details.

  Author reserves right to use and publish sources for embedded 
  applications under different conditions too.
 *******************************************************************/

#ifndef _LINUX_UL_HDEP_H
#define _LINUX_UL_HDEP_H

#include "ul_drv.h"

#define  ULAN_MAJOR 248

#ifdef CONFIG_OC_UL_DRV_WITH_MEM_CHECK
#define  ENABLE_UL_MEM_CHECK
#endif

#define UL_LOG_ENABLE

#ifdef KERNEL
 #ifndef ENABLE_UL_MEM_CHECK
  #define MALLOC(x) kmalloc(x,GFP_KERNEL)
  #define FREE(x)   kfree(x)
 #else
  #define MALLOC ul_mem_check_malloc
  #define FREE   ul_mem_check_free
  void * ul_mem_check_malloc(size_t size);
  void ul_mem_check_free(void *ptr);
 #endif
 #define UL_CHK_IOS(port,cnt)      check_region(port, cnt)
 #define UL_REQ_IOS(port,cnt,name) (kc_request_region(port, cnt, name)?0:-1)
 #define UL_REL_IOS(port,cnt)      kc_release_region(port, cnt)
 #ifndef FOR_LINUX_KERNEL
  #define FOR_LINUX_KERNEL
 #endif
 #define ULD_HZ HZ
#elif defined(_WIN32)
 #ifndef ENABLE_UL_MEM_CHECK
  #define MALLOC(x) ExAllocatePoolWithTag(NonPagedPool,x,'uL1')
  #define FREE(x)   ExFreePoolWithTag(x,'uL1')
 #else
  #define MALLOC ul_mem_check_malloc
  #define FREE   ul_mem_check_free
  void * ul_mem_check_malloc(size_t size);
  void ul_mem_check_free(void *ptr);
 #endif
 #define UL_CHK_IOS(port,cnt)      (0)
 #define UL_REQ_IOS(port,cnt,name) (0)
 #define UL_REL_IOS(port,cnt)      (0)
 #define ULD_HZ 1000000 
#elif defined(__DJGPP__)
 #include <malloc.h>
 #include <dpmi.h>
 #ifndef ENABLE_UL_MEM_CHECK
  #define MALLOC(_size) ({void *_p=malloc(_size);\
                        _go32_dpmi_lock_data(_p,_size);\
                        _p;})
  #define FREE   free
 #else
  #define MALLOC ul_mem_check_malloc
  #define FREE   ul_mem_check_free
  void * ul_mem_check_malloc(size_t size);
  void ul_mem_check_free(void *ptr);
 #endif
 #define UL_CHK_IOS(port,cnt)      ({0;})
 #define UL_REQ_IOS(port,cnt,name) ({0;})
 #define UL_REL_IOS(port,cnt)      ({0;})
 #define UL_DRV_IN_LIB
 #define error perror
 #define ULD_HZ UCLOCKS_PER_SEC
 long int uld_jiffies;
 #define jiffies uld_jiffies
 #define JIFFIES_FROM_GETTIMEOFDAY
#elif defined(CONFIG_OC_UL_DRV_SYSLESS)
 #include <malloc.h>
 #include <system_def.h>
 #ifndef ENABLE_UL_MEM_CHECK
  #define MALLOC(_size) malloc(_size)
  #define FREE   free
 #else
  #define MALLOC ul_mem_check_malloc
  #define FREE   ul_mem_check_free
  void * ul_mem_check_malloc(size_t size);
  void ul_mem_check_free(void *ptr);
 #endif
 void *(* const ul_drv_reserve_ram)(size_t size);
 #define ul_drv_reserve_ram ul_drv_reserve_ram
 #define UL_CHK_IOS(port,cnt)      ({0;})
 #define UL_REQ_IOS(port,cnt,name) ({0;})
 #define UL_REL_IOS(port,cnt)      ({0;})
 #define UL_DRV_IN_LIB
 #define UL_DRV_IN_LIB_WITH_PRINTK
 #define error perror
 #define ULD_HZ SYS_TIMER_HZ
 long int uld_jiffies;
 #define jiffies uld_jiffies
 #define sleep(x) {volatile unsigned int s;for(s=0;s<50000;s++);}
 #define ENABLE_UL_STARTUP_RECEIVE
#elif defined(CONFIG_OC_UL_DRV_NUTTX)
 #include <syslog.h>
 #include <poll.h>
 #include <nuttx/arch.h>
 #include <nuttx/semaphore.h>
 #include <nuttx/kthread.h>
 #include <nuttx/wdog.h>
 #include <nuttx/signal.h>
 #include <nuttx/spinlock.h>
 #include <arch/board/board.h>
 #ifndef FOR_NUTTX_KERNEL
  #define FOR_NUTTX_KERNEL
 #endif
 #ifndef ENABLE_UL_MEM_CHECK
  #define MALLOC(_size) malloc(_size)
  #define FREE   free
 #else
  #define MALLOC ul_mem_check_malloc
  #define FREE   ul_mem_check_free
  void * ul_mem_check_malloc(size_t size);
  void ul_mem_check_free(void *ptr);
 #endif
 #define UL_CHK_IOS(port,cnt)      ({0;})
 #define UL_REQ_IOS(port,cnt,name) ({0;})
 #define UL_REL_IOS(port,cnt)      ({0;})
 #define error perror
 #define ULD_HZ TICK_PER_SEC
 #define jiffies (clock_systime_ticks())
 #define sleep(x) {volatile unsigned int s;for(s=0;s<50000;s++);}
 #define ENABLE_UL_STARTUP_RECEIVE
#else
 #include <malloc.h>
 #include <sys/time.h>
 #define MALLOC malloc
 #define FREE   free
 #define UL_CHK_IOS(port,cnt) \
	({int i=ioperm((long)port, (long)cnt, 1);\
 	  ioperm((long)port, (long)cnt, 0);\
 	  i<0?i:0;\
	})
 #define UL_REQ_IOS(port,cnt,name) ioperm((long)port, (long)cnt, 1)
 #define UL_REL_IOS(port,cnt)      ioperm((long)port, (long)cnt, 0)
 #define error perror
 #define ULD_HZ 1000000
 #define jiffies ({struct timeval tv;gettimeofday(&tv,NULL);\
                  tv.tv_usec+tv.tv_sec*1000000;})
#endif


#ifndef INLINE
  #define INLINE static inline
#endif

#ifndef uchar
 #define uchar unsigned char
#endif

#if defined(__DJGPP__)
  /* Taken from Linux kernel - Copyright 1992, Linus Torvalds. */
  #define __sti() __asm__ __volatile__ ("sti": : :"memory")
  #define __cli() __asm__ __volatile__ ("cli": : :"memory")
  #define __save_flags(x) \
  __asm__ __volatile__("pushfl ; popl %0":"=g" (x): /* no input */ :"memory")
  #define __restore_flags(x) \
  __asm__ __volatile__("pushl %0 ; popfl": /* no output */ :"g" (x):"memory")
  INLINE void set_bit(int nr, volatile int * addr)
    { __asm__ __volatile__("btsl %1,%0":"=m" (*addr):"Ir" (nr));}
  INLINE void clear_bit(int nr, volatile int * addr)
    { __asm__ __volatile__("btrl %1,%0":"=m" (*addr):"Ir" (nr));}
  INLINE int test_and_set_bit(int nr, volatile int * addr)
    {int oldbit;__asm__ __volatile__("btsl %2,%1\n\tsbbl %0,%0"
      :"=r" (oldbit),"=m" (*addr):"Ir" (nr));return oldbit;}
  INLINE int test_bit(int nr, volatile int * addr)
    {int oldbit;__asm__ __volatile__("btl %2,%1\n\tsbbl %0,%0"
      :"=r" (oldbit):"m" (*addr),"Ir" (nr));return oldbit;}
  #define udelay usleep
  INLINE void schedule(void) {;}
  int request_irq(int irq,void *handler,int flags,char *name,void *dev_id);
  int free_irq(int irq,void *ctx);
  INLINE void synchronize_irq() {asm volatile ("":::"memory");}
#elif defined(USER_SPACE_EMU_IRQ)
  INLINE void schedule(void) {;}
  #define request_irq request_emu_irq
  #define free_irq release_emu_irq
  INLINE void synchronize_irq() {asm volatile ("":::"memory");}
#elif defined(CONFIG_OC_UL_DRV_NUTTX)
  #define request_irq(M_irqnum, M_handler, M_flags, M_name, M_context) \
           (irq_attach((M_irqnum), (M_handler), (M_context)) == OK? \
            (up_enable_irq(M_irqnum), 0) : -1)
  #define free_irq(M_irqnum, M_context) \
           do { up_disable_irq(M_irqnum); \
                irq_detach(M_irqnum); } while(0)
  #define synchronize_irq() \
           spin_unlock_irqrestore(&ul_irq_lock_spinlock,\
                                  spin_lock_irqsave(&ul_irq_lock_spinlock))
#elif defined(CONFIG_OC_UL_DRV_SYSLESS)
  #define __cli(x) cli(x)
  #define __restore_flags(x)  restore_flags(x)
  #define __save_flags(x) save_flags(x)
  inline void schedule(void) {;}
  /*int request_irq(int irq,void *handler,int flags,char *name,void *dev_id);*/
  /*int free_irq(int irq,void *ctx);*/
  inline void synchronize_irq(void) {asm volatile ("":::"memory");}
#endif /* USER_SPACE_EMU_IRQ */

#if defined(CONFIG_OC_UL_DRV_SYSLESS)
  #define U450_BYTES_PER_REGS 4
  #define U450_BAUD_BASE_DEFAULT ((PCLK+8)/16)
#elif defined(CONFIG_OC_UL_DRV_NUTTX)
  #define U450_BYTES_PER_REGS 4
  #define U450_BAUD_BASE_DEFAULT 0
#else
  #define U450_BYTES_PER_REGS 1
  #define U450_BAUD_BASE_DEFAULT 0x1C200
#endif /* CONFIG_OC_UL_DRV_SYSLESS */

/*******************************************************************/
/* locking, irq and memory management */

/* disables another IRQ on same CPU when uLan IRQ is handled */
#ifdef FOR_LINUX_KERNEL
 #define UL_IRQF_FLAGS IRQF_SHARED
#else
 #define UL_IRQF_FLAGS 0
#endif

#ifdef USER_SPACE_EMU_IRQ
 #define UL_IRQ_LOCK_FINI
 #define ul_synchronize_irq(irqnum) synchronize_irq()
 int INLINE irq_lock_cnt(int i)
 {static int lock_counter=0;
  return lock_counter+=i;
 };
 #define UL_IRQ_LOCK   {if(irq_lock_cnt(1)==1) emu_irq_cli();}
 #define UL_IRQ_UNLOCK {if(irq_lock_cnt(-1)==0)emu_irq_sti();}
 #define UL_MB()       {asm volatile ("":::"memory");}
 #define UL_IRQ_HANDLER_ARGS(intno, dev_id) \
       int intno, void *dev_id, struct pt_regs *regs
 #define ul_irqreturn_t void
 #define UL_IRQ_RETVAL(retval)
 #define UL_IRQ_NONE
 #define UL_IRQ_HANDLED
#elif defined(FOR_LINUX_KERNEL)
 #define ul_synchronize_irq kc_synchronize_irq
 #if 1
  spinlock_t ul_irq_lock_spinlock;
  #define UL_IRQ_LOCK_GINI \
    KC_DEFINE_SPINLOCK(ul_irq_lock_spinlock);
  #define UL_IRQ_LOCK_FINI unsigned long ul_irq_lock_flags;
  #define UL_IRQ_LOCK \
    {spin_lock_irqsave(&ul_irq_lock_spinlock,ul_irq_lock_flags);}
  #define UL_IRQ_UNLOCK \
    {spin_unlock_irqrestore(&ul_irq_lock_spinlock,ul_irq_lock_flags);}
 #else
  #define UL_IRQ_LOCK_FINI
  #define UL_IRQ_LOCK   {cli();}
  #define UL_IRQ_UNLOCK {sti();}
 #endif
 #define UL_MB()       mb()
 #define UL_IRQ_HANDLER_ARGS KC_IRQ_HANDLER_ARGS
 #define ul_irqreturn_t irqreturn_t
 #define UL_IRQ_RETVAL  IRQ_RETVAL
 #define UL_IRQ_NONE    IRQ_NONE
 #define UL_IRQ_HANDLED IRQ_HANDLED
#elif defined(CONFIG_OC_UL_DRV_NUTTX)
 extern spinlock_t ul_irq_lock_spinlock;
  #define UL_IRQ_LOCK_GINI \
    spinlock_t ul_irq_lock_spinlock;
 #define UL_IRQ_LOCK_FINI irqstate_t ul_irq_lock_flags=0;
 #define UL_IRQ_LOCK \
   (ul_irq_lock_flags=spin_lock_irqsave(&ul_irq_lock_spinlock))
 #define UL_IRQ_UNLOCK \
   spin_unlock_irqrestore(&ul_irq_lock_spinlock,ul_irq_lock_flags)
 #ifdef SP_DMB
   #define UL_MB()   SP_DMB()
 #else
   #define UL_MB()   __asm__ __volatile__("": : : "memory")
 #endif
 #define ul_synchronize_irq(irqnum) synchronize_irq()
 #define UL_IRQ_HANDLER_ARGS(intno, dev_id) \
      int intno, void *context, void *dev_id
 #ifdef IRQ_HANDLER_FNC
   #define UL_IRQ_HANDLER_FNC IRQ_HANDLER_FNC
   #define ul_irq_handler_get_irqnum irq_handler_get_irqnum
   #define ul_irq_handler_get_context irq_handler_get_context
 #endif
 #define ul_irqreturn_t int
 #define UL_IRQ_RETVAL(retval) OK
 #define UL_IRQ_NONE    OK
 #define UL_IRQ_HANDLED OK
#elif defined(_WIN32)
 typedef LONG atomic_t;
 KIRQL  uL_SpinLock_Irql;
 #define UL_IRQ_LOCK_FINI \
	 KIRQL uL_IRQ_OldIrql;
 #define UL_KERNEL_SPINLOCK
 #ifdef UL_KERNEL_SPINLOCK
  KSPIN_LOCK uL_SpinLock;
  #define UL_IRQ_LOCK_GINI \
     KIRQL      uL_SpinLock_Irql=0; \
     KSPIN_LOCK uL_SpinLock;
  #define UL_IRQ_LOCK \
    {KeRaiseIrql (uL_SpinLock_Irql,&uL_IRQ_OldIrql); \
     KeAcquireSpinLockAtDpcLevel(&uL_SpinLock); \
    }
  #define UL_IRQ_UNLOCK \
    {KeReleaseSpinLockFromDpcLevel(&uL_SpinLock); \
     KeLowerIrql(uL_IRQ_OldIrql); \
    }
 #else /*UL_KERNEL_SPINLOCK*/
  #ifndef YieldProcessor
   #define UL_YieldProcessor() do { __asm { rep nop }; } while(0)
  #else
   #define UL_YieldProcessor() YieldProcessor()
  #endif
  LONG   uL_SpinLock_Flag;
  #define UL_IRQ_LOCK_GINI \
     KIRQL  uL_SpinLock_Irql=0; \
     LONG   uL_SpinLock_Flag=0;
  #define UL_IRQ_LOCK \
    {KeRaiseIrql (uL_SpinLock_Irql,&uL_IRQ_OldIrql); \
     while(InterlockedExchange(&uL_SpinLock_Flag,1)) \
       UL_YieldProcessor(); \
    }
  #define UL_IRQ_UNLOCK \
    {InterlockedExchange(&uL_SpinLock_Flag,0); \
     KeLowerIrql(uL_IRQ_OldIrql); \
    }
 #endif /*UL_KERNEL_SPINLOCK*/
 #define UL_MB()       { KeMemoryBarrier();}
 #define UL_IRQ_HANDLER_ARGS(intno, dev_id) \
       IN PKINTERRUPT intno, IN PVOID dev_id
 #define UL_IRQ_HANDLER_API NTAPI
 #define ul_irqreturn_t BOOLEAN
 #define UL_IRQ_RETVAL(retval)  ((retval)?TRUE:FALSE)
 #define UL_IRQ_NONE    FALSE
 #define UL_IRQ_HANDLED TRUE
 /* not known how to convert PKINTERRUPT to something usable for debugging */
 #define ul_irq_handler_get_irqnum() 0
#elif defined(__DJGPP__)
  typedef long atomic_t;
  #define UL_IRQ_LOCK_FINI unsigned long ul_irq_lock_flags;
  #define UL_IRQ_LOCK \
    {__save_flags(ul_irq_lock_flags);__cli();}
  #define UL_IRQ_UNLOCK \
    {__restore_flags(ul_irq_lock_flags);}
  #define UL_MB()       {asm volatile ("":::"memory");}
  #define ul_synchronize_irq(irqnum) synchronize_irq()
  #define UL_IRQ_HANDLER_ARGS(intno, dev_id) \
       int intno, void *dev_id, struct pt_regs *regs
  #define ul_irqreturn_t void
  #define UL_IRQ_RETVAL(retval)
  #define UL_IRQ_NONE
  #define UL_IRQ_HANDLED
#elif defined(CONFIG_OC_UL_DRV_SYSLESS)
  typedef long atomic_t;
  #define UL_IRQ_LOCK_FINI unsigned long ul_irq_lock_flags=0;
  #define UL_IRQ_LOCK \
    {__save_flags(ul_irq_lock_flags);__cli();}
  #define UL_IRQ_UNLOCK \
    {__restore_flags(ul_irq_lock_flags);}
  #define UL_MB()       {asm volatile ("":::"memory");}
  #define ul_synchronize_irq(irqnum) synchronize_irq()
  #define UL_IRQ_HANDLER_ARGS(intno, dev_id) \
       int intno, void *dev_id
  #ifdef IRQ_HANDLER_FNC
    #define UL_IRQ_HANDLER_FNC IRQ_HANDLER_FNC
    #define ul_irq_handler_get_irqnum irq_handler_get_irqnum
    #define ul_irq_handler_get_context irq_handler_get_context
  #endif
  #define ul_irqreturn_t void
  #define UL_IRQ_RETVAL(retval)
  #define UL_IRQ_NONE
  #define UL_IRQ_HANDLED
#endif

#ifndef UL_IRQ_HANDLER_FNC
  #ifndef UL_IRQ_HANDLER_API
    #define UL_IRQ_HANDLER_API
  #endif
  #define UL_IRQ_HANDLER_FNC(M_fnc_name) \
    ul_irqreturn_t UL_IRQ_HANDLER_API \
      M_fnc_name(UL_IRQ_HANDLER_ARGS(__irq_handler_irqnum,__irq_handler_context))
  #ifndef ul_irq_handler_get_irqnum
    #define ul_irq_handler_get_irqnum() (__irq_handler_irqnum)
  #endif
  #define ul_irq_handler_get_context() (__irq_handler_context)
#endif /*UL_IRQ_HANDLER_FNC*/

#ifndef ATOMIC_INIT
  #define ATOMIC_INIT(i) (i)
#ifndef atomic_read
  #define atomic_read(v) (*(v))
#endif
#ifndef atomic_set
  #define atomic_set(v,i) (*(v)=(i))
#endif
#endif /*ATOMIC_INIT*/

#define UL_DRV_LOCK_FINI UL_IRQ_LOCK_FINI
#define UL_DRV_LOCK {\
  UL_IRQ_LOCK;\
 }
#define UL_DRV_UNLOCK {\
  UL_IRQ_UNLOCK;\
 }

/* enable to globaly lock CPU through uLan events handling, bad
   use only for special test purposes */
#if 0
  spinlock_t ul_global_irq_lock_spinlock;
  #define UL_GLOBAL_IRQ_LOCK_GINI \
    KC_DEFINE_SPINLOCK(ul_global_irq_lock_spinlock);
  #define UL_GLOBAL_IRQ_LOCK_FINI unsigned long ul_global_irq_lock_flags;
  #define UL_GLOBAL_IRQ_LOCK \
    {spin_lock_irqsave(&ul_global_irq_lock_spinlock,ul_global_irq_lock_flags);}
  #define UL_GLOBAL_IRQ_UNLOCK \
    {spin_unlock_irqrestore(&ul_global_irq_lock_spinlock,ul_global_irq_lock_flags);}
#endif


#ifdef CONFIG_OC_UL_DRV_SYSLESS
  #define UL_GLOBAL_IRQ_LOCK_GINI 
  #define UL_GLOBAL_IRQ_LOCK_FINI UL_IRQ_LOCK_FINI;
  #define UL_GLOBAL_IRQ_LOCK UL_IRQ_LOCK
  #define UL_GLOBAL_IRQ_UNLOCK UL_IRQ_UNLOCK
#endif

/* get one free block or NULL */
INLINE ul_mem_blk *ul_alloc_blk(ul_drv *udrv)
{
  UL_DRV_LOCK_FINI
  ul_mem_blk *p;
  UL_DRV_LOCK;
  if((p=udrv->free_blk)!=NULL)
  {
    udrv->free_blk_cnt--;
    udrv->free_blk=p->next;
    p->next=NULL;
  };
  UL_DRV_UNLOCK;
  return p;
};

/* free one block */
INLINE void ul_free_blk(ul_drv *udrv, ul_mem_blk *blk)
{
  UL_DRV_LOCK_FINI
  UL_DRV_LOCK;
  blk->next=udrv->free_blk;
  udrv->free_blk=blk;
  udrv->free_blk_cnt++;
  UL_DRV_UNLOCK;
};

#ifdef FOR_WIN_WDM
INLINE int ulan_in_sleep_mode(ul_drv *udrv)
{
  return (udrv->CurrentDevicePowerState != PowerDeviceD0) ? 1:0;
}
#endif

/* bottom half definitions */
#ifdef FOR_LINUX_KERNEL
  void ulan_do_bh(kc_tasklet_data_type data);
  typedef kc_tasklet_data_type ulan_do_bh_data_type;
  #define SCHEDULE_BH(udrv) \
        kc_tasklet_schedule(&(udrv)->bottom)
  #define INIT_UDRV_BH(udrv) \
	kc_tasklet_init(&(udrv)->bottom,ulan_do_bh,(long)udrv)
  #define KILL_UDRV_BH(udrv) \
	kc_tasklet_kill(&(udrv)->bottom)
  #define DECLARE_UDRV_BH struct kc_tasklet_struct bottom;
  #define uld_test_dfl(udrv,_DFL_flag) \
	test_bit(UL_DFLB_##_DFL_flag,&(udrv)->flags)
  #define uld_set_dfl(udrv,_DFL_flag) \
	set_bit(UL_DFLB_##_DFL_flag,&(udrv)->flags)
  #define uld_clear_dfl(udrv,_DFL_flag) \
	clear_bit(UL_DFLB_##_DFL_flag,&(udrv)->flags)
  #define uld_test_and_set_dfl(udrv,_DFL_flag) \
	test_and_set_bit(UL_DFLB_##_DFL_flag,&(udrv)->flags)
  #define uld_atomic_test_dfl(udrv,_DFL_flag) \
	test_bit(UL_DFLB_##_DFL_flag,&(udrv)->flags)
  #define uld_atomic_clear_dfl(udrv,_DFL_flag) \
	clear_bit(UL_DFLB_##_DFL_flag,&(udrv)->flags)
  #define uld_atomic_set_dfl(udrv,_DFL_flag) \
	set_bit(UL_DFLB_##_DFL_flag,&(udrv)->flags)
    
  #define uld_kwt_wq_init(udrv) \
	do { udrv->kwt = NULL; \
	     init_waitqueue_head(&(udrv)->kwt_wq); \
	     uld_atomic_clear_dfl(udrv,KWTKILL); \
	     uld_atomic_clear_dfl(udrv,KWTRUN); \
	     uld_atomic_clear_dfl(udrv,ASK_ISR); } while(0)
  #define uld_kwt_wake(udrv) \
	do { uld_atomic_set_dfl(udrv,ASK_ISR); wake_up(&(udrv)->kwt_wq); } while(0)
  #define uld_kwt_wait(udrv) \
	wait_event_killable(udrv->kwt_wq, uld_test_dfl(udrv,ASK_ISR))
  #define ULD_KWT_WAIT_SUCCESS 0
  #define uld_kwt_confirm(udrv) \
	uld_atomic_clear_dfl(udrv,ASK_ISR)
  #define uld_kwt_test_and_set_prepared(udrv) \
	uld_test_and_set_dfl(udrv,KWTRUN)
  #define uld_kwt_kill(udrv) \
	do { uld_atomic_set_dfl(udrv,KWTKILL); \
	     uld_atomic_set_dfl(udrv,ASK_ISR); wake_up(&(udrv)->kwt_wq); } while(0)
  #define uld_kwt_set_stopped(udrv) \
	do { uld_atomic_clear_dfl(udrv,KWTRUN); wake_up(&(udrv)->kwt_wq); } while(0)
  #define uld_kwt_is_running(udrv) \
	uld_test_dfl(udrv,KWTRUN)
#if (LINUX_VERSION_CODE < VERSION(2,6,11))  /* old Linux kernel tread support */
  #define uld_kwt_should_stop(udrv) \
	uld_atomic_test_dfl(udrv,KWTKILL)
  #define uld_kwt_finished(udrv) \
	do { lock_kernel(); uld_kwt_set_stopped(udrv); unlock_kernel();} while(0)
  #define uld_kwt_wait_stopped(udrv) \
	wait_event(udrv->kwt_wq, !uld_test_dfl(udrv,KWTRUN))
 #if (LINUX_VERSION_CODE < VERSION(2,5,41))
  #define uld_kwt_run(udrv, thread_fnc, thread_name) \
	(kernel_thread(thread_fnc, udrv, CLONE_FS | CLONE_FILES | CLONE_SIGNAL))
 #else /* 2.5.41 */
  #define uld_kwt_run(udrv, thread_fnc, thread_name) \
	(kernel_thread(thread_fnc, udrv, CLONE_KERNEL))
 #endif /* 2.5.41 */
#else  /* 2.6.0 kernel threads */
  #define uld_kwt_should_stop(udrv) \
	(uld_atomic_test_dfl(udrv,KWTKILL) || kthread_should_stop())
  #define uld_kwt_finished(udrv) \
	do { uld_kwt_set_stopped(udrv); } while(0)
  #define uld_kwt_wait_stopped(udrv) \
	do { struct task_struct *kwt1 =  xchg(&udrv->kwt, NULL); if(kwt1!=NULL) kthread_stop(kwt1); } while(0)
  #define uld_kwt_run(udrv, thread_fnc, thread_name) \
	({ struct task_struct *kwt1 = kthread_run(thread_fnc, udrv, thread_name); \
	   if (!IS_ERR(kwt1)) udrv->kwt = kwt1; IS_ERR(kwt1)? -1: 0; })
#endif

#elif defined(_WIN32)
			/* must be used only at IRQL >= DISPATCH_LEVEL */
  #define SCHEDULE_BH(udrv) \
    do{ \
	UL_DRV_LOCK_FINI \
	UL_DRV_LOCK; \
	(udrv)->flag_ASK_BOTTOM=1; \
	KeInsertQueueDpc(&(udrv)->bottom_dpc,NULL,NULL); \
	UL_DRV_UNLOCK; \
    }while(0)
  #define INIT_UDRV_BH(udrv) /* not used search Dpc */
  #define KILL_UDRV_BH(udrv)
  #define DECLARE_UDRV_BH	/* not used search for Dpc */
  PIRP xch_pending_irp(PIRP *irp_ptr, PIRP new_irp); /* for user app waitting */
  void abort_pending_irp(PIRP *irp_ptr, NTSTATUS status);
  #define uld_test_dfl(udrv,_DFL_flag) \
    ((udrv)->flag_##_DFL_flag)
  #define uld_set_dfl(udrv,_DFL_flag) \
    ((udrv)->flag_##_DFL_flag=1)
  #define uld_clear_dfl(udrv,_DFL_flag) \
    ((udrv)->flag_##_DFL_flag=0)
  #define uld_test_and_set_dfl(udrv,_DFL_flag) \
    (InterlockedExchange(&(udrv)->flag_##_DFL_flag,1))
  #define uld_atomic_test_dfl(udrv,_DFL_flag) \
    ((udrv)->flag_##_DFL_flag)
  #define uld_atomic_clear_dfl(udrv,_DFL_flag) \
    (InterlockedExchange(&(udrv)->flag_##_DFL_flag,0))
  #define uld_atomic_set_dfl(udrv,_DFL_flag) \
    (InterlockedExchange(&(udrv)->flag_##_DFL_flag,1))
    

  #define uld_kwt_wq_init(udrv) \
	do { \
	  KeInitializeEvent( &(udrv)->kwt_wake, NotificationEvent, FALSE); \
          KeInitializeEvent( &(udrv)->kwt_stopped, NotificationEvent, TRUE); \
	  uld_atomic_clear_dfl(udrv,KWTKILL); \
	} while(0)
  #define uld_kwt_wake(udrv) \
	KeSetEvent( &(udrv)->kwt_wake, IO_NO_INCREMENT, FALSE)
  #define uld_kwt_wait(udrv) \
	KeWaitForSingleObject( &(udrv)->kwt_wake, Executive, KernelMode, FALSE, NULL)
  #define ULD_KWT_WAIT_SUCCESS STATUS_SUCCESS
  #define uld_kwt_confirm(udrv) \
	KeClearEvent(&(udrv)->kwt_wake)
  #define uld_kwt_test_and_set_prepared(udrv) \
	(!KeResetEvent(&(udrv)->kwt_stopped))
  #define uld_kwt_kill(udrv) \
	do { uld_atomic_set_dfl(udrv,KWTKILL); \
	     KeSetEvent( &(udrv)->kwt_wake, IO_NO_INCREMENT, FALSE); } while(0)
  #define uld_kwt_set_stopped(udrv) \
	do { uld_atomic_clear_dfl(udrv,KWTKILL); \
	     KeSetEvent( &(udrv)->kwt_stopped, IO_NO_INCREMENT, FALSE) ; } while(0)
  #define uld_kwt_wait_stopped(udrv) \
      	do { KeWaitForSingleObject( &(udrv)->kwt_stopped, Executive, KernelMode, FALSE, NULL); \
             KeWaitForSingleObject( (udrv)->kwt_threadobj, Executive, KernelMode, FALSE, NULL); \
             ObDereferenceObject( (udrv)->kwt_threadobj); \
        } while(0)
  /* KeReadStateEvent cannot be used on Win98 */
  //#define uld_kwt_is_running(udrv) (!KeReadStateEvent(&(udrv)->kwt_stopped))
  #define uld_kwt_is_running(udrv) \
  	(STATUS_SUCCESS != KeWaitForSingleObject( &(udrv)->kwt_stopped, Executive, KernelMode, FALSE, &ULD_LARGE_INTEGER_0))
  #define uld_kwt_should_stop(udrv) \
	(uld_atomic_test_dfl(udrv,KWTKILL))
  #define uld_kwt_finished(udrv) \
	do { uld_kwt_set_stopped(udrv); PsTerminateSystemThread( STATUS_SUCCESS);} while(0)
  #define uld_kwt_run(udrv, thread_fnc, thread_name) \
	( !NT_SUCCESS(PsCreateSystemThread( &udrv->kwt, THREAD_ALL_ACCESS, NULL, \
		NULL, NULL, thread_fnc, udrv)) ?  -1: \
	  !NT_SUCCESS(ObReferenceObjectByHandle(udrv->kwt, THREAD_ALL_ACCESS, \
		NULL, KernelMode, &udrv->kwt_threadobj, NULL ))? -1 : 0)

  // This variable has to be initialized by RtlConvertLongToLargeInteger(0) in DriverEntry
  extern LARGE_INTEGER ULD_LARGE_INTEGER_0;
#elif defined(FOR_NUTTX_KERNEL)
  typedef void * ulan_do_bh_data_type;
  CODE int ulan_bottom_task(int argc, FAR char *argv[]);
  CODE int ulan_create_bottom_task(ul_drv* udrv);
  #define SCHEDULE_BH(udrv) do { \
       uld_set_dfl(udrv, ASK_BOTTOM); \
       nxsem_post(&(udrv->bottom_sem)); \
    } while(0)
  #define INIT_UDRV_BH(udrv) do { \
      nxsem_init(&(udrv->bottom_sem), 0, 1); \
      nxsem_set_protocol(&(udrv->bottom_sem), SEM_PRIO_NONE); \
      udrv->bottom_pid = ulan_create_bottom_task(udrv); \
    } while(0)
  #define KILL_UDRV_BH(udrv) do { \
      if (udrv->bottom_pid > 0) { \
        /*kthread_delete(udrv->bottom_pid);*/ \
        (void)nxsig_kill(udrv->bottom_pid, SIGALRM); \
      } \
      udrv->bottom_pid = 0; \
    } while(0)
  #define DECLARE_UDRV_BH
  #define uld_test_dfl(udrv,_DFL_flag) \
    (__sync_fetch_and_or(&(udrv)->flags,0)&(1<<(UL_DFLB_##_DFL_flag)))
  #define uld_set_dfl(udrv,_DFL_flag) \
    (__sync_fetch_and_or(&(udrv)->flags,(1<<(UL_DFLB_##_DFL_flag))))
  #define uld_clear_dfl(udrv,_DFL_flag) \
    (__sync_fetch_and_and(&(udrv)->flags,~(1<<(UL_DFLB_##_DFL_flag))))
  #define uld_test_and_set_dfl(udrv,_DFL_flag) \
    ({ typeof((udrv)->flags) __bitmsk = 1<<(UL_DFLB_##_DFL_flag); \
       __sync_fetch_and_or(&(udrv)->flags,__bitmsk)&__bitmsk?1:0; \
    })
  #define uld_atomic_test_dfl(udrv,_DFL_flag) \
    (__sync_fetch_and_or(&(udrv)->flags,0)&(1<<(UL_DFLB_##_DFL_flag)))
  #define uld_atomic_clear_dfl(udrv,_DFL_flag) \
    (__sync_fetch_and_and(&(udrv)->flags,~(1<<(UL_DFLB_##_DFL_flag))))
  #define uld_atomic_set_dfl(udrv,_DFL_flag) \
    (__sync_fetch_and_or(&(udrv)->flags,(1<<(UL_DFLB_##_DFL_flag))))
#else
  typedef void * ulan_do_bh_data_type;
  #define SCHEDULE_BH(udrv) \
    do{set_bit(UL_DFLB_ASK_BOTTOM,&(udrv->flags));}while(0)
  #define INIT_UDRV_BH(udrv)
  #define KILL_UDRV_BH(udrv)
  #define DECLARE_UDRV_BH
  #define uld_test_dfl(udrv,_DFL_flag) \
    test_bit(UL_DFLB_##_DFL_flag,&(udrv)->flags)
  #define uld_set_dfl(udrv,_DFL_flag) \
    set_bit(UL_DFLB_##_DFL_flag,&(udrv)->flags)
  #define uld_clear_dfl(udrv,_DFL_flag) \
    clear_bit(UL_DFLB_##_DFL_flag,&(udrv)->flags)
  #define uld_test_and_set_dfl(udrv,_DFL_flag) \
    test_and_set_bit(UL_DFLB_##_DFL_flag,&(udrv)->flags)
  #define uld_atomic_test_dfl(udrv,_DFL_flag) \
    test_bit(UL_DFLB_##_DFL_flag,&(udrv)->flags)
  #define uld_atomic_clear_dfl(udrv,_DFL_flag) \
    clear_bit(UL_DFLB_##_DFL_flag,&(udrv)->flags)
  #define uld_atomic_set_dfl(udrv,_DFL_flag) \
    set_bit(UL_DFLB_##_DFL_flag,&(udrv)->flags)
#endif

/* link watchdog timer definitions */
#ifdef FOR_LINUX_KERNEL
  void ulan_do_wd_timer(KC_TIMER_HANDLER_ARGS);
  #define SCHEDULE_UDRV_WDTIM(udrv,expires) \
       mod_timer(&(udrv)->wd_timer,expires);
  #define INIT_UDRV_WDTIM(udrv) \
    do{ \
       timer_setup(&(udrv)->wd_timer, \
                   ulan_do_wd_timer, 0); \
    }while(0)
  #define TIMER_HANDLER_ARGS2UDRV \
        (container_of(KC_TIMER_HANDLER_ARGS2TIMER,ul_drv,wd_timer))
  #define DONE_UDRV_WDTIM(udrv) \
       del_timer(&(udrv)->wd_timer)
  #define DECLARE_UDRV_WDTIM struct timer_list wd_timer
#elif defined(FOR_NUTTX_KERNEL)
  #define SCHEDULE_UDRV_WDTIM(udrv,expires) \
    do { \
       wd_cancel(&(udrv)->wd_timer); \
       wd_start(&(udrv)->wd_timer, expires-jiffies, ulan_do_wd_timer, (wdparm_t)(udrv)); \
    } while(0)
  #define INIT_UDRV_WDTIM(udrv) \
    do{ \
       memset(&(udrv)->wd_timer, 0, sizeof((udrv)->wd_timer)); \
    }while(0)
  #define TIMER_HANDLER_ARGS2UDRV ((ul_drv*)data)
  #define DONE_UDRV_WDTIM(udrv) \
       wd_cancel(&(udrv)->wd_timer)
  #define DECLARE_UDRV_WDTIM
#elif defined(_WIN32)
  #define SCHEDULE_UDRV_WDTIM(udrv,expires)
  #define INIT_UDRV_WDTIM(udrv)
  #define TIMER_HANDLER_ARGS2UDRV ((ul_drv*)data)
  #define DONE_UDRV_WDTIM(udrv)
  #define DECLARE_UDRV_WDTIM
#else
  #define SCHEDULE_UDRV_WDTIM(udrv,expires) \
    (udrv->wd_timer_expires=expires,\
     set_bit(UL_DFLB_WDSCHED,&(udrv->flags)))
  #define INIT_UDRV_WDTIM(udrv) \
    (clear_bit(UL_DFLB_WDSCHED,&(udrv->flags)))
  #define TIMER_HANDLER_ARGS2UDRV ((ul_drv*)data)
  #define DONE_UDRV_WDTIM(udrv) \
    (clear_bit(UL_DFLB_WDSCHED,&(udrv->flags)))
  #define DECLARE_UDRV_WDTIM
#endif

/* chip level high resolution timer support */
#ifdef UL_WITH_CHIP_TIMER
#ifdef FOR_LINUX_KERNEL
  #define uld_chiptimer_init(udrv) \
    hrtimer_init(&(udrv)->chip_timer, CLOCK_MONOTONIC, HRTIMER_MODE_REL)
  #define uld_chiptimer_start(udrv, time_ns) \
    hrtimer_start(&(udrv)->chip_timer, ns_to_ktime(time_ns), HRTIMER_MODE_REL)
  #define uld_chiptimer_cancel_sync(udrv) \
    do { \
      hrtimer_cancel(&(udrv)->chip_timer); \
      uld_atomic_clear_dfl((udrv),CHIPTIMER); \
    } while(0)
  #define uld_chiptimer_cancel(udrv) \
    do { \
      if (uld_test_dfl((udrv),IN_CHIPTIMER)) \
        hrtimer_try_to_cancel(&(udrv)->chip_timer); \
      else \
        hrtimer_cancel(&(udrv)->chip_timer); \
      uld_atomic_clear_dfl((udrv),CHIPTIMER); \
    } while(0)
#endif /* FOR_LINUX_KERNEL */
#endif /* UL_WITH_CHIP_TIMER */

/* compatible generic io-ports access */
#ifdef _WIN32
INLINE UCHAR ioread8(PVOID ioaddr)
{
  if ((((ULONG_PTR)ioaddr)&~UL_PHYSADDR_PIO_MASK) == 0) {
     return READ_PORT_UCHAR(ioaddr);
  } else  {
     return READ_REGISTER_UCHAR(ioaddr);
  }
}
INLINE void iowrite8(UCHAR val,PVOID ioaddr)
{
  if ((((ULONG_PTR)ioaddr)&~UL_PHYSADDR_PIO_MASK) == 0) {
     WRITE_PORT_UCHAR(ioaddr,val);
  } else  {
     WRITE_REGISTER_UCHAR(ioaddr,val);
  }
}
 #define ul_inb(port)	 ioread8((port).ioaddr)
 #define ul_outb(port,val) iowrite8(val,(port).ioaddr)
#elif defined(__DJGPP__)
 #include <pc.h>
 #define ul_inb(port)	 inportb((unsigned long)((port).ioaddr))
 #define ul_outb(port,val) outportb((unsigned long)((port).ioaddr),val)
#elif defined(CONFIG_OC_UL_DRV_NUTTX)
 #define getreg8(a)      (*(volatile uint8_t *)(a))
 #define putreg8(v,a)    (*(volatile uint8_t *)(a) = (v))
 #define ul_inb(port)	 getreg8((port).ioaddr)
 #define ul_outb(port,val) putreg8((val),(port).ioaddr)
#elif defined(CONFIG_OC_UL_DRV_SYSLESS)
 #define ul_inb(port)	 inb((unsigned long)((port).ioaddr))
 #define ul_outb(port,val) outb((unsigned long)((port).ioaddr),val)
#else
 #define ul_inb(port)      ioread8((port).ioaddr)
 #define ul_outb(port,val) iowrite8(val,(port).ioaddr)
 #define ul_inw(port)      ioread32((port).ioaddr)
 #define ul_outw(port,val) iowrite32(val,(port).ioaddr)
#endif

/*******************************************************************/
/* PCI support types */

#if !defined(FOR_LINUX_KERNEL) && defined(UL_WITH_PCI)

#ifndef PCI_ANY_ID
#define PCI_ANY_ID 0xffff
typedef struct pci_device_id {
  USHORT vendor;
  USHORT device;
  USHORT subvendor;
  USHORT subdevice;
  USHORT class_val,clask_mask;
  ULONG  driver_data;
} pci_device_id_t;
#endif /*PCI_ANY_ID*/

typedef struct usb_device_id {
  USHORT idVendor;
  USHORT idProduct;
  ULONG  driver_info;
} usb_device_id_t;
	
	

#endif /*!FOR_LINUX_KERNEL && UL_WITH_PCI*/

/*******************************************************************/
/* ulan file access structure */

#define ULOP_MAGIC 0x754c2233

struct	ul_opchain;

/* uLan driver operator/user structure pointed by file private/extension */
typedef struct ul_opdata {
	int	magic;		/* magic number */
	ul_drv	*udrv;		/* uLan driver pointer */
	int     subdevidx;	/* subdevice index for this operator/user */
	int	pro_mode;	/* promiscuous mode for given operator/user */
	struct ul_opdata *opnext, *opprew; /* per udrv clients bll */
  #if defined(FOR_LINUX_KERNEL) || defined(FOR_NUTTX_KERNEL)
	struct file *file;	/* back ptr to file */
  #elif defined(_WIN32)
	PFILE_OBJECT  file;
    PIRP   wait_irp;		/* IRP waitting for nonempty recchain */ 
  #endif /* _WIN32 */
	ul_mem_blk *message;	/* first block of prepared message */ 
	ul_data_it data;	/* ul_mem_blk data iterator */
  #ifdef FOR_LINUX_KERNEL
	wait_queue_head_t wqrec; /* wait queue for received packet */
  #endif
  #ifdef FOR_NUTTX_KERNEL
	sem_t  ul_op_pollsem;
	struct pollfd  *ul_op_pollfds;
  #endif
	struct ul_opchain *recchain; /* received messages */
	struct ul_opchain *filtchain; /* filter for incoming messages */
} ul_opdata;

/* list of filtered mesage types also used for per operator receive queue */
typedef struct	ul_opchain {
	struct	ul_opchain *next; /* Cyklic BLL of received mesages */
	struct	ul_opchain *prev; /* or input filters */
	ul_mem_blk *message;	/* first block of waiting message */ 
	unsigned stamp;		/* unigue message number */
	uchar    cmd;		/* command or service */
	ul_nadr_t dadr;		/* destination address */
	ul_nadr_t sadr;		/* source address */
	uchar    state;		/* state of filtering */
} ul_opchain;

#define UL_OPST_FREE	0	/* free ul_opchain block */
#define UL_OPST_ONCE	1	/* wait for message only once */
#define UL_OPST_FILT	2	/* filter incomming messages */
#define UL_OPST_MES	3	/* message on recchain list */
#define UL_OPST_MESLOOP	4       /* looped self message on recchain list */
#define UL_OPST_FILTNEW	5	/* new filter check for repeat */

typedef int ul_chip_init_fnc(ul_drv *udrv, ul_physaddr_t physbase, int irq, int baud,
                             long baudbase, int options);

typedef struct ul_chip_type_ent {
	const char *chip_name;
	ul_chip_init_fnc *chip_init;
	int chip_flags;
	int chip_chans;
	int chip_options;
}ul_chip_type_ent;

#define UL_CHIPT_RQPORT	1	/* port number requested */
#define UL_CHIPT_RQIRQ	2	/* irq umber requested */
#define UL_CHIPT_PCI	4	/* pci type */
#define UL_CHIPT_NOPROBE 8	/* do not probe if type undefined */


/*******************************************************************/
/* user space visible structures */

typedef struct ul_msginfo {
	int	dadr;		/* destignation address */
	int	sadr;		/* source address */
	int	cmd;		/* command/socket number */
	int	flg;		/* message flags */
	int	len;		/* length of frame */
	unsigned stamp;		/* unigue message number */
} ul_msginfo;

/*******************************************************************/
/* ioctl definitions */

#ifdef _WIN32

/* 32768-65535 are reserved for use by customers.*/
#define FILE_DEVICE_ULAN  0x0000A000

/* function codes 2048-4095 are reserved for customers. */
#define UL_IOCTL_INDEX  0xA00

#define UL_IO(function) \
	CTL_CODE(FILE_DEVICE_ULAN,UL_IOCTL_INDEX+function,METHOD_BUFFERED,FILE_ANY_ACCESS)
#define UL_IOR(function,size) \
	CTL_CODE(FILE_DEVICE_ULAN,UL_IOCTL_INDEX+function,METHOD_BUFFERED,FILE_ANY_ACCESS)
#define UL_IOW(function,size) \
	CTL_CODE(FILE_DEVICE_ULAN,UL_IOCTL_INDEX+function,METHOD_BUFFERED,FILE_ANY_ACCESS)
#define UL_IOWR(function,size) \
	CTL_CODE(FILE_DEVICE_ULAN,UL_IOCTL_INDEX+function,METHOD_BUFFERED,FILE_ANY_ACCESS)

#elif defined(FOR_NUTTX_KERNEL)

#define UL_IOCTL        0x7500

#define UL_IO(function)        _IOC(UL_IOCTL,function)
#define UL_IOR(function,size)  _IOC(UL_IOCTL,function)
#define UL_IOW(function,size)  _IOC(UL_IOCTL,function)
#define UL_IOWR(function,size) _IOC(UL_IOCTL,function)

#else /* Linux */

#define	UL_IOCTL	0x75

#define UL_IO(function)        _IO(UL_IOCTL,function)
#define UL_IOR(function,size)  _IOR(UL_IOCTL,function,size)
#define UL_IOW(function,size)  _IOW(UL_IOCTL,function,size)
#define UL_IOWR(function,size) _IOWR(UL_IOCTL,function,size)

#endif /* Linux */

#define UL_DRV_VER	UL_IO(0x10)
#define UL_NEWMSG	UL_IOW(0x11,ul_msginfo)
#define UL_TAILMSG	UL_IOW(0x12,ul_msginfo)
#define UL_FREEMSG	UL_IO(0x13)
#define UL_ACCEPTMSG	UL_IOR(0x14,ul_msginfo)
#define UL_ACTAILMSG	UL_IOR(0x15,ul_msginfo)
#define UL_ADDFILT	UL_IOW(0x16,ul_msginfo)
#define UL_ABORTMSG	UL_IO(0x17)
#define UL_REWMSG	UL_IO(0x18)
#define UL_INEPOLL	UL_IO(0x19)
#define UL_WAITREC	UL_IO(0x1A)
#define UL_SETMYADR	UL_IO(0x1B)  /* arg = new station address */
#define UL_SETIDSTR	UL_IO(0x1C)  /* arg = "C" string */
#define UL_SETBAUDRATE	UL_IO(0x1D)  /* arg = new baudrate */
#define UL_SETPROMODE	UL_IO(0x1E)  /* arg = new pro_mode */
#define UL_SETSUBDEV	UL_IO(0x1F)  /* arg = sub device */
#define UL_QUERYPARAM   UL_IO(0x20)  /* arg = query */
#define UL_ROUTE	UL_IOWR(0x21,ul_route_range_t)  /* arg = route */
#define UL_KLOGBLL	UL_IO(0x41)
#define UL_STROKE	UL_IO(0x42)
#define UL_DEBFLG	UL_IO(0x43)  /* arg = new debug mask */
#define UL_HWTEST	UL_IO(0x44)  /* arg = test subcommand */

/*******************************************************************/
/* define unix error codes */

#ifdef _WIN32

#define ENOMEM (-STATUS_NO_MEMORY)
#define EPERM  (-STATUS_PRIVILEGE_NOT_HELD)
#define ENOMSG (-STATUS_END_OF_FILE)
#define EINVAL (-STATUS_INVALID_PARAMETER)
#define ENODEV (-STATUS_INVALID_PARAMETER)

#endif /* _WIN32 */

#ifdef __DJGPP__

#define ENOMSG ENOENT

#endif /* _WIN32 */

/*******************************************************************/
/* debug support */

/* initial value for uld_debug_flg */
#ifndef ULD_DEBUG_DEFAULT 
  #ifndef UL_DRV_IN_LIB
    #define ULD_DEBUG_DEFAULT (0x01|0x10|0x40)
  #else
    #define ULD_DEBUG_DEFAULT (0x01|0x40)
  #endif
#endif /* ULD_DEBUG_DEFAULT */

#ifdef FOR_LINUX_KERNEL
 #define UL_PRINTF printk
#elif defined(FOR_NUTTX_KERNEL)
 #define UL_PRINTF(args...) \
   do { \
     if (!up_interrupt_context()) \
       syslog(args); \
   } while(0)
 #define UL_PRINTF_CONT(x,args...)
 #define KERN_CRIT LOG_CRIT,
 #define KERN_ERR  LOG_ERR,
 #define KERN_WARNING LOG_WARNING,
 #define KERN_INFO LOG_INFO,
#elif defined(_WIN32)
 #define UL_PRINTF DbgPrint
#elif defined(UL_DRV_IN_LIB)
 #ifdef UL_LOG_ENABLE
   int uld_printk(const char *format, ...) __attribute__((format(printf,1,2)));
   void uld_printk_flush(void);
   #define UL_PRINTF uld_printk
 #else
   #define uld_printk_flush(x)
   #define UL_PRINTF(x,args...) 
 #endif
#else
 #define UL_PRINTF printf
#endif

#ifndef UL_PRINTF_CONT
 #define UL_PRINTF_CONT UL_PRINTF
#endif

#ifdef UL_LOG_ENABLE
 #define LOG_FATAL	if(uld_debug_flg&0x01) UL_PRINTF
 #define LOG_CHIO	if(uld_debug_flg&0x02) UL_PRINTF
 #define LOG_IRQ	if(uld_debug_flg&0x04) UL_PRINTF
 #define LOG_MESSAGES	if(uld_debug_flg&0x08) UL_PRINTF
 #define LOG_FAILS	if(uld_debug_flg&0x10) UL_PRINTF
 #define LOG_SEQNUM	if(uld_debug_flg&0x20) UL_PRINTF
 #define LOG_PORTS	if(uld_debug_flg&0x40) UL_PRINTF
 #define LOG_FILEIO	if(uld_debug_flg&0x80) UL_PRINTF
 #define LOG_FILTS	if(uld_debug_flg&0x100) UL_PRINTF
 #define LOG_IAC	if(uld_debug_flg&0x200) UL_PRINTF
 #define LOG_MSK_DISABLE  0x10000   /* for use with UL_DRV_IN_LIB */
#else
 #define LOG_FATAL(x,args...)
 #define LOG_CHIO(x,args...)
 #define LOG_IRQ(x,args...)
 #define LOG_MESSAGES(x,args...)
 #define LOG_FAILS(x,args...)
 #define LOG_SEQNUM(x,args...)
 #define LOG_PORTS(x,args...)
 #define LOG_FILEIO(x,args...)
 #define LOG_FILTS(x,args...)
 #define LOG_IAC(x,args...)
#endif

#if defined(FOR_NUTTX_KERNEL)
 #undef LOG_CHIO
 #define LOG_CHIO(x,args...)
 #undef LOG_SEQNUM
 #define LOG_SEQNUM(x,args...)
#endif

#ifndef KERN_CRIT
  #define KERN_CRIT
#endif
#ifndef KERN_ERR
  #define KERN_ERR
#endif
#ifndef KERN_WARNING
  #define KERN_WARNING
#endif
#ifndef KERN_INFO
  #define KERN_INFO
#endif

#if (defined(CPU)&&(CPU>=586)||\
     defined(CONFIG_X86_TSC))&&!defined(_WIN32)
 #define RDTSC \
   ({unsigned long l,h;\
    asm volatile ("rdtsc" : "=a" (l), "=d" (h));\
    l;\
   })
#else
 #define RDTSC 0
#endif

#endif /* _LINUX_UL_HDEP_H */
