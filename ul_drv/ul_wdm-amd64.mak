#----------------------------------------------------------------------------
#
# Device Driver MAKEFILE
#
# uLan Communication Driver
#
#

#****************************************************************************
#----------------------------------------------------------------------------
# Compile flags for the Window Mode Driver
#

# general flags,
# /Zel=Use MS extension-omit default library
# /Zp8=Pack 8
# /Gy=Enables function-level linking
# /GF=Pools strings and places them in read-only memory 
# /Gz=Uses the __stdcall calling convention

CFLAGS1 = /c /Ze1 /Zp8 /Gy /W3 /Gy /GF /Gz /Gi- /Gm- /GX- /GR- /GS-

# The following flags handle Pentium chip bugs FDiv and 0x0f

CFLAGS2 = /QIfdiv- /QI0f

# /Z7=Debug info flags
# /Zi=PDB debugging
# /QIf=Generates additional debugging information for kernal-mode device drivers
CFLAGS3 = /Zi -DFOR_WIN_WDM -DCONFIG_OC_UL_DRV_WITH_MULTI_DEV=1 -DCONFIG_OC_UL_DRV_WITH_MULTI_NET=1 -DUL_WITH_IAC=1

# /Od=Turn off ALL optimizations, /Oi=Inline functions
# /Oy=Omits frame pointer, /Oxs=Uses maximum optimization (/Ob1gity /Gs)

CFLAGS4 = /Oy /Oxs 

# defines used by .H files and the like

CFLAGS5 = -FIwarning.h -D_AMD64_=1 -DSTD_CALL -DCONDITION_HANDLING=1 
CFLAGS6 = -DNT_INST=0 -DWIN32=100 -D_NT1X_=100 -DWINNT=1 -D_WIN32_WINNT=0x0501 -DWINVER=0x0501 
CFLAGS7 = -DWIN32_LEAN_AND_MEAN=1 -DDEVL=1 -DFPO=1 -DNDEBUG -D_DLL=1  
CFLAGS8 = -I$(WDM_INC_PATH) -I$(CRT_INC_PATH)

#----------------------------------------------------------------------------
#****************************************************************************


#****************************************************************************
#----------------------------------------------------------------------------
# Flags for the User Mode EXE
#

#----------------------------------------------------------------------------
# COMPILE flags

ccommon = -c -W3 -DCRTAPI1=_cdecl -DCRTAPI2=_cdecl -nologo
cflags = $(ccommon) -D_X86_=1
cvars      = -DWIN32 $(noansi) -D_WIN32
cdebug = -Z7 -Od

CCFLAGS = $(cflags) $(cvars) $(cdebug)


#----------------------------------------------------------------------------
# LINK Flags

libcmt = libcmt.lib oldnames.lib
baselibs = kernel32.lib advapi32.lib
libc = libc.lib oldnames.lib
linklibs = $(libc) $(baselibs)

linkdebug = -debug:full -debugtype:cv

lflags = /NODEFAULTLIB /INCREMENTAL:NO /PDB:NONE /RELEASE /NOLOGO
conlflags = $(lflags) -subsystem:console,4.0 -entry:mainCRTStartup

#----------------------------------------------------------------------------
#****************************************************************************


#****************************************************************************
#----------------------------------------------------------------------------
#
# The dependency list and build rules follows

whole_enchilada : ul_wdm.sys

ul_drv.sym : ul_drv.map
        MSYM ul_drv.MAP

ul_drv.map : ul_drv.sys
        DBG2MAP ul_drv.sys

#
# In the following,
# Use: ul_drv.lnr for a release link
# Use: ul_drv.lnd for a debug build
#
# -LIBPATH:$(DDK_LIB_PATH)

ul_wdm.sys : ul_drv.obj 
        LINK -LIBPATH:$(DDK_LIB_DEST)\$(_BUILDARCH) @ul_wdm-$(_BUILDARCH).lnr

ul_drv.obj : ul_drv.c ul_wdbase.c ul_wdent.c ul_wdpnp.c ul_wdusb.c ul_wdpwr.c ul_base.c \
	     ul_c450.c ul_c510.c ul_c950pci.c ul_cps1.c \
	     ul_debug.c ul_devtab.c ul_di.c ul_frame.c ul_mem.c ul_tors.c ul_tst.c ul_ufsm.c \
          ul_drv.h ul_hdep.h ul_82510.h serial_reg.h ul_wdbase.h ul_wdinc.h ul_wdusb.h
        CL $(CFLAGS1) $(CFLAGS2) $(CFLAGS3) $(CFLAGS4) $(CFLAGS5) $(CFLAGS6) $(CFLAGS7) $(CFLAGS8) ul_drv.c

q_test.obj: q_test.c
        CL $(CCFLAGS) q_test.c

q_test.exe: q_test.obj
        LINK  $(linkdebug) $(conflags) -out:q_test.exe q_test.obj $(linklibs)

#****************************************************************************
#****************************************************************************
