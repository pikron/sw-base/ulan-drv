/*******************************************************************
  uLan Communication - uL_DRV - multiplatform uLan driver

  ul_debug.c	- common debugging routines

  (C) Copyright 1996-2004 by Pavel Pisa - project originator
        http://cmp.felk.cvut.cz/~pisa
  (C) Copyright 1996-2004 PiKRON Ltd.
        http://www.pikron.com
  (C) Copyright 2002-2004 Petr Smolik
  

  The uLan driver project can be used and distributed 
  in compliance with any of next licenses
   - GPL - GNU Public License
     See file COPYING for details.
   - LGPL - Lesser GNU Public License
   - MPL - Mozilla Public License
   - and other licenses added by project originator

  Code can be modified and re-distributed under any combination
  of the above listed licenses. If contributor does not agree with
  some of the licenses, he/she can delete appropriate line.
  WARNING: if you delete all lines, you are not allowed to
  distribute code or sources in any form.
 *******************************************************************/

void printblk(ul_mem_blk *blk)
{
  ul_mem_blk *prev_blk=NULL;
  while(blk)
  {
    if(prev_blk) if(UL_BLK_HEAD(blk).prev!=prev_blk)
       UL_PRINTF(KERN_CRIT "Inconsistency in BLL list !!!\n");

    UL_PRINTF(KERN_INFO "BLK : dadr=%02X sadr=%02X cmd=%02X flg=%04X retry=%02X len=%04X\n",
     UL_BLK_HEAD(blk).dadr,
     UL_BLK_HEAD(blk).sadr,
     UL_BLK_HEAD(blk).cmd,
     UL_BLK_HEAD(blk).flg,
     UL_BLK_HEAD(blk).retry_cnt,
     UL_BLK_HEAD(blk).len
    );
    {
      ul_data_it di;
      uchar ch;
      int cnt=128;
      UL_PRINTF_CONT("      |");
      ul_di_init(&di,blk);
      di.trans_len=UL_BLK_HEAD(blk).len;
      while((di.pos<di.trans_len)&& --cnt)
      {
	ch=ul_di_read1(&di);
	if(ch>=0x20&&ch<0x7F) UL_PRINTF_CONT("%c",ch);
	else UL_PRINTF_CONT("<%X>",ch);
      };
      if(!cnt) UL_PRINTF_CONT("<...>...");
      UL_PRINTF_CONT("|\n");
    };
    prev_blk=blk;
    blk=UL_BLK_HEAD(blk).next;
  };
};

void printudrvbll(ul_drv *udrv)
{
  UL_PRINTF(KERN_INFO "prep_bll : frames = %d\n",udrv->prep_bll.cnt);
  printblk(udrv->prep_bll.first);

  UL_PRINTF(KERN_INFO "work_bll : frames = %d\n",udrv->work_bll.cnt);
  printblk(udrv->work_bll.first);

  UL_PRINTF(KERN_INFO "proc_bll : frames = %d\n",udrv->proc_bll.cnt);
  printblk(udrv->proc_bll.first);
  UL_PRINTF(KERN_INFO "opan_bll : frames = %d\n",udrv->opan_bll.cnt);
  printblk(udrv->opan_bll.first);
  UL_PRINTF(KERN_INFO "free_blk = %d\n",udrv->free_blk_cnt);
 #ifdef ENABLE_UL_MEM_CHECK
  UL_PRINTF(KERN_INFO "MEM_CHECK: malloc-free=%d\n",
		 (int)atomic_read(&ul_mem_check_counter));
 #endif /* ENABLE_UL_MEM_CHECK */
};

void printudrvoperators(ul_drv *udrv)
{
  UL_DRV_LOCK_FINI
  ul_opdata *opdata;
  ul_opchain *opmember,**opchain;
  int reccnt,filtcnt,opcnt;
  UL_PRINTF(KERN_INFO "ul_drv operators (open handles)\n");
  UL_DRV_LOCK;
  opcnt=0;
  for(opdata=udrv->operators;opdata;opdata=opdata->opnext)
  { opchain=&opdata->recchain;reccnt=0;
    if((opmember=*opchain)!=NULL)do{
      reccnt++; if(reccnt>1000) break;
    }while((opmember=opmember->next)!=*opchain);
    opchain=&opdata->filtchain;filtcnt=0;
    if((opmember=*opchain)!=NULL)do{
      filtcnt++; if(filtcnt>1000) break;
    }while((opmember=opmember->next)!=*opchain);
    UL_PRINTF(KERN_INFO "  operator %d, in recchain %d, in filtchain %d\n",
		    opcnt,reccnt,filtcnt);
    opcnt++;if(opcnt>100) break;
  }
  UL_DRV_UNLOCK;
}

void printudrvfncstack(ul_drv *udrv)
{
  ul_call_fnc **p;
  UL_PRINTF(KERN_INFO "ul_drv: fnc_act:%08lx fnc_stack:",(long)udrv->fnc_act);
  p=udrv->fnc_sp;
  while(p!=&udrv->fnc_stack[0]){
    p--;
    UL_PRINTF_CONT("%08lx ",(long)*p);
  };
  UL_PRINTF_CONT("\n");
}

