/*******************************************************************
  uLan Communication - uL_DRV - multiplatform uLan driver

  ul_frame.c	- sending and reception of uLan message frames
                  for UART chips

  (C) Copyright 1996-2004 by Pavel Pisa - project originator
        http://cmp.felk.cvut.cz/~pisa
  (C) Copyright 1996-2004 PiKRON Ltd.
        http://www.pikron.com
  (C) Copyright 2002-2004 Petr Smolik
  

  The uLan driver project can be used and distributed 
  in compliance with any of next licenses
   - GPL - GNU Public License
     See file COPYING for details.
   - LGPL - Lesser GNU Public License
   - MPL - Mozilla Public License
   - and other licenses added by project originator

  Code can be modified and re-distributed under any combination
  of the above listed licenses. If contributor does not agree with
  some of the licenses, he/she can delete appropriate line.
  WARNING: if you delete all lines, you are not allowed to
  distribute code or sources in any form.
 *******************************************************************/

static int uld_adr_match(ul_drv *udrv, ul_nadr_t adr)
{
  int i;
 #ifdef UL_WITH_MULTI_NET
  if (udrv->my_net_base)
    if (((adr^udrv->my_net_base)&UL_ADR_NET_MASK)==0)
      adr&=0xff;
 #endif /*UL_WITH_MULTI_NET*/
  if (adr==0x00)
    return UL_ADR_MATCH_BROADCAST;
  for(i=0;i<UL_SUBDEVNUM_MAX;i++)
    if(adr==(udrv->my_adr_arr[i]))
      return UL_ADR_MATCH_MINE;
  if (udrv->pro_mode&UL_PRO_MODE_CAPTURE_MASK) {
    if ((adr>=0x00)&&(adr<=UL_ADR_LOCAL_MAX))
      return Ul_ADR_MATCH_PROMISUOUS;
   #ifdef UL_WITH_MULTI_NET
    if (adr&UL_ADR_NET_MASK)
      return Ul_ADR_MATCH_PROMISUOUS;
   #endif /*UL_WITH_MULTI_NET*/
  }
  return UL_ADR_MATCH_NONE;
}

static int uld_get_arb_addr(ul_drv *udrv)
{
 #ifdef CONFIG_OC_UL_DRV_WITH_MULTI_DEV
  int i=0;
  while((i<UL_SUBDEVNUM_MAX-1)&&!udrv->my_adr_arr[i])
    i++;
  return udrv->my_adr_arr[i];
 #else /*CONFIG_OC_UL_DRV_WITH_MULTI_DEV*/
  return udrv->my_adr_arr[0];
 #endif /*CONFIG_OC_UL_DRV_WITH_MULTI_DEV*/
}

/*******************************************************************/
/* Frame begin/end */

/*-- Helper functions for sndbeg --*/

#ifdef UL_WITH_MULTI_NET

static int uld_sndbeg_4(ul_drv *udrv, int ret_code)
{
  if(ret_code<0) {UL_FRET;return ret_code;};
  if (udrv->con_pos>=udrv->con_ext_len) {
    udrv->char_buff=udrv->con_cmd;
    UL_FNEXT(*udrv->chip_ops->fnc_sndch); /* Send CMD */
    return UL_RC_PROC;
  }
  udrv->char_buff=udrv->con_ext_data[udrv->con_pos++];
  UL_FCALL(*udrv->chip_ops->fnc_sndch); /* Send ExtAdresses */
  return UL_RC_PROC;
};

static int uld_sndbeg_3(ul_drv *udrv, int ret_code)
{
  if(ret_code<0) {UL_FRET;return ret_code;};
  udrv->con_pos=0;
  udrv->char_buff=udrv->con_ext_header;
  UL_FCALL2(*udrv->chip_ops->fnc_sndch,uld_sndbeg_4); /* Send ExtHeader */
  return UL_RC_PROC;
};

#endif /*UL_WITH_MULTI_NET*/

static int uld_sndbeg_2(ul_drv *udrv, int ret_code)
{
  if(ret_code<0) {UL_FRET;return ret_code;};
 #ifdef UL_WITH_MULTI_NET
  if (udrv->con_ext_header) {
    udrv->char_buff=UL_CMD_EADR;
    UL_FCALL2(*udrv->chip_ops->fnc_sndch,uld_sndbeg_3); /* Send CMD */
  } else
 #endif /*UL_WITH_MULTI_NET*/
  {
    udrv->char_buff=udrv->con_cmd;
    UL_FNEXT(*udrv->chip_ops->fnc_sndch); /* Send CMD */
  }
  return UL_RC_PROC;
};

static int uld_sndbeg_1(ul_drv *udrv, int ret_code)
{
  if(ret_code<0) {UL_FRET;return ret_code;};
 #ifdef UL_WITH_MULTI_NET
  udrv->char_buff=udrv->con_sadr_local;
 #else /*UL_WITH_MULTI_NET*/
  udrv->char_buff=udrv->con_sadr;
 #endif /*UL_WITH_MULTI_NET*/
  UL_FCALL2(*udrv->chip_ops->fnc_sndch,uld_sndbeg_2); /* Send SAdr */
  return UL_RC_PROC;
};

/*** Send frame begin ***/
int uld_sndbeg(ul_drv *udrv, int ret_code)
{
  unsigned dadr_local=UL_BEG;
 #ifdef UL_WITH_MULTI_NET
  udrv->con_ext_header=0;
  udrv->con_ext_len=0;
 #endif /*UL_WITH_MULTI_NET*/
  if(!(udrv->con_flg&4)) {
   #ifdef UL_WITH_MULTI_NET
    int dadr_match;
    int dadr_lg2l;
    int sadr_lg2l;
    uchar *ptr=udrv->con_ext_data;

    udrv->con_ext_header=0;
    dadr_local=ul_dadr2dadr_local(udrv,udrv->con_dadr,&dadr_lg2l);
    udrv->con_sadr_local=ul_sadr2sadr_local(udrv,udrv->con_sadr,&sadr_lg2l);
    if (dadr_lg2l) {
      ptr=ul_nadr2buff(ptr,udrv->con_dadr,dadr_lg2l);
      udrv->con_ext_header|=dadr_lg2l;
    }
    if (sadr_lg2l) {
      ptr=ul_nadr2buff(ptr,udrv->con_sadr,sadr_lg2l);
      udrv->con_ext_header|=sadr_lg2l<<3;
    }
    if(udrv->con_ext_header)
      *(ptr++)=udrv->con_hops;
    udrv->con_ext_len=ptr-udrv->con_ext_data;
    /* Match with mine address has to be repeated because device can act as GW */
    dadr_match=uld_adr_match(udrv, dadr_local);
    if((dadr_match==UL_ADR_MATCH_BROADCAST)||
       (dadr_match==UL_ADR_MATCH_MINE)) {
      udrv->con_flg&=~1; /* do not expect external acknowledge */
      if((udrv->con_adr_match!=UL_ADR_MATCH_MINE) &&
         (udrv->con_adr_match!=UL_ADR_MATCH_BROADCAST))
        udrv->con_adr_match=Ul_ADR_MATCH_ROUTER;
    }
    dadr_local|=0x100;
   #else /*UL_WITH_MULTI_NET*/
    dadr_local=udrv->con_dadr|0x100;
   #endif /*UL_WITH_MULTI_NET*/
  }
  udrv->xor_sum=0;
  udrv->char_buff=dadr_local;
  UL_FCALL2(*udrv->chip_ops->fnc_sndch,uld_sndbeg_1); /* Send B8+DAdr */
  return UL_RC_PROC;
};

/*-- Helper functions for sndend --*/

static int uld_sndend_3(ul_drv *udrv, int ret_code)
{
  if(ret_code<0) {UL_FRET;return ret_code;};
  UL_FRET;
  if(ret_code<0) return ret_code;
  if(udrv->char_buff==UL_ACK) return UL_RC_PROC;
  if(udrv->char_buff==UL_NAK) return UL_RC_ENAK;
  if(udrv->char_buff==UL_WAK) return UL_RC_EWAK;
  return UL_RC_EREPLY;
};

static int uld_sndend_2(ul_drv *udrv, int ret_code)
{
  if(ret_code<0) {UL_FRET;return ret_code;};
  if(!(udrv->con_flg&1))
  {
    UL_FRET;
    return UL_RC_PROC;
  };
  udrv->wait_time=4;
  UL_FCALL2(*udrv->chip_ops->fnc_wait,uld_sndend_3); /* Wait for UL_ACK */
  return UL_RC_PROC;
};

static int uld_sndend_1(ul_drv *udrv, int ret_code)
{
  if(ret_code<0) {UL_FRET;return ret_code;};
  udrv->char_buff=udrv->xor_sum&0xFF;
  UL_FCALL2(*udrv->chip_ops->fnc_sndch,uld_sndend_2); /* Send XOR sum */
  return UL_RC_PROC;
};

/*** Send frame end ***/
int uld_sndend(ul_drv *udrv, int ret_code)
{
  static const int sndend_arr[]={UL_END,UL_ARQ,UL_PRQ,UL_AAP};
  udrv->char_buff=sndend_arr[udrv->con_flg&3];
  UL_FCALL2(*udrv->chip_ops->fnc_sndch,uld_sndend_1); /* Send B8+UL_END,ARQ,PRQ or AAP*/
  return UL_RC_PROC;
};

/*-- Helper functions for recbeg --*/

#ifdef UL_WITH_MULTI_NET

static int uld_recbeg_6(ul_drv *udrv, int ret_code)
{
  int dadr_lg2l=udrv->con_ext_header&7;
  int sadr_lg2l=(udrv->con_ext_header>>3)&7;
  uchar *ptr=udrv->con_ext_data;

  if(ret_code<0) {UL_FRET;return ret_code;};
  if(udrv->char_buff&0xFF00) {UL_FRET; return UL_RC_ERECBEG;};
  udrv->con_cmd=(uchar)udrv->char_buff;

  if(dadr_lg2l)
    ptr=ul_buff2nadr(ptr,&udrv->con_dadr,dadr_lg2l);
  if(sadr_lg2l)
    ptr=ul_buff2nadr(ptr,&udrv->con_sadr,sadr_lg2l);
  udrv->con_hops=*(ptr++);
  UL_FRET;
  return UL_RC_PROC;
};

static int uld_recbeg_5(ul_drv *udrv, int ret_code)
{
  if(ret_code<0) {UL_FRET;return ret_code;};
  if(udrv->char_buff&0xFF00) {UL_FRET; return UL_RC_ERECBEG;};
  udrv->con_ext_data[udrv->con_pos++]=(uchar)udrv->char_buff;
  udrv->wait_time=4;
  if (udrv->con_pos<udrv->con_ext_len)
    UL_FCALL(*udrv->chip_ops->fnc_wait);      /* Wait for more ext data */
  else
    UL_FCALL2(*udrv->chip_ops->fnc_wait,uld_recbeg_6); /* Wait for CMD */
  return UL_RC_PROC;
};

static int uld_recbeg_4(ul_drv *udrv, int ret_code)
{
  int ext_ln;
  if(ret_code<0) {UL_FRET;return ret_code;};
  if(udrv->char_buff&0xFF00) {UL_FRET; return UL_RC_ERECBEG;};
  udrv->con_ext_header=(uchar)udrv->char_buff;
  ext_ln=ul_ext_header2len(udrv->con_ext_header);
  if((ext_ln<0)||(udrv->con_ext_header&0xC0)) {
    UL_FRET; return UL_RC_ERECBEG;
  }
  udrv->con_ext_len=ext_ln;
  udrv->con_pos=0;
  udrv->wait_time=4;
  if (udrv->con_ext_len)
    UL_FCALL2(*udrv->chip_ops->fnc_wait,uld_recbeg_5); /* Wait for ext data */
  else
    UL_FCALL2(*udrv->chip_ops->fnc_wait,uld_recbeg_6); /* Wait for CMD */
  return UL_RC_PROC;
};

#endif /*UL_WITH_MULTI_NET*/

static int uld_recbeg_3(ul_drv *udrv, int ret_code)
{
  if(ret_code<0) {UL_FRET;return ret_code;};
  if(udrv->char_buff&0xFF00) {UL_FRET; return UL_RC_ERECBEG;};
 #ifdef UL_WITH_MULTI_NET
  udrv->con_ext_header=0;
  udrv->con_hops=0;
  if (udrv->char_buff==UL_CMD_EADR) {
    udrv->wait_time=4;
    UL_FCALL2(*udrv->chip_ops->fnc_wait,uld_recbeg_4); /* Wait for ExtHeader */
  } else
 #endif /*UL_WITH_MULTI_NET*/
  {
    udrv->con_cmd=(uchar)udrv->char_buff;
    UL_FRET;
  }
  return UL_RC_PROC;
};

static int uld_recbeg_2(ul_drv *udrv, int ret_code)
{
  if(ret_code<0) {UL_FRET;return ret_code;};
  if(udrv->char_buff&0xFF00) {UL_FRET; return UL_RC_ERECBEG;};
  udrv->con_sadr=(uchar)udrv->char_buff;
 #ifdef UL_WITH_MULTI_NET
  udrv->con_sadr_local=udrv->char_buff;
 #endif /*UL_WITH_MULTI_NET*/
  udrv->wait_time=4;
  UL_FCALL2(*udrv->chip_ops->fnc_wait,uld_recbeg_3); /* Wait for CMD */
  return UL_RC_PROC;
};

int uld_recbeg_1(ul_drv *udrv, int ret_code)
{
  int ret = UL_ADR_MATCH_NONE;
  if(udrv->char_buff&0x100)
    ret=uld_adr_match(udrv,udrv->char_buff&0xff);
  if((ret==UL_ADR_MATCH_NONE) &&
     ((udrv->char_buff!=UL_BEG)||(!(udrv->con_flg&8))))
  { UL_FRET;
    if((udrv->char_buff&0x180)==0x100)
     return UL_RC_ERECADR;
    else
     return UL_RC_ERECBEG;
  };
  udrv->con_dadr=udrv->char_buff&0xFF;
  udrv->con_flg|=8;
  if (ret==Ul_ADR_MATCH_PROMISUOUS)
    udrv->con_flg|=0x10;
  udrv->wait_time=4;
  UL_FCALL2(*udrv->chip_ops->fnc_wait,uld_recbeg_2); /* Wait for SAdr */
  return UL_RC_PROC;
};

/*** Receive frame begin ***/
int uld_recbeg(ul_drv *udrv, int ret_code)
{
  udrv->xor_sum=0;
  udrv->wait_time=4;
  UL_FCALL2(*udrv->chip_ops->fnc_wait,uld_recbeg_1); /* Wait for B8+DArd */
  return UL_RC_PROC;
};

/*-- Helper functions for recend --*/

static int uld_recend_4(ul_drv *udrv, int ret_code)
{
  UL_FRET;
  if((udrv->char_buff!=UL_ACK) || (ret_code<0))
    UL_BLK_HEAD(udrv->con_frame).flg|=UL_BFL_FAIL;
  return UL_RC_PROC;
}

static int uld_recend_3(ul_drv *udrv, int ret_code)
{
  UL_FRET;
  if(udrv->con_flg&0xC0) return UL_RC_ERECEND;
  return UL_RC_PROC;
};

static int uld_recend_2(ul_drv *udrv, int ret_code)
{
  if(ret_code<0) {UL_FRET;return ret_code;};
  if(udrv->char_buff&0xFF00) {UL_FRET; return UL_RC_ERECPROT;};
  if((udrv->xor_sum-1)&0xFF) udrv->con_flg|=0x80;
  if(!(udrv->con_flg&1))
  {
    UL_FRET;
    if(udrv->con_flg&0xC0) return UL_RC_ERECEND;
    return UL_RC_PROC;
  };
  if (!(udrv->con_flg&0x10))
  {
    if(udrv->con_flg&0x80) udrv->char_buff=UL_NAK;
    else if(udrv->con_flg&0x40) udrv->char_buff=UL_WAK;
    else udrv->char_buff=UL_ACK;
    UL_FCALL2(*udrv->chip_ops->fnc_sndch,uld_recend_3); /* Send UL_ACK,NAK or WAK */
  } else {
    udrv->wait_time=4;
    UL_FCALL2(*udrv->chip_ops->fnc_wait,uld_recend_4); /* Wait for other station UL_ACK */
  }
  return UL_RC_PROC;
};

static int uld_recend_1(ul_drv *udrv, int ret_code)
{
  if(ret_code<0) {UL_FRET;return ret_code;};
  if(!(udrv->char_buff&0x100))
  {
    udrv->wait_time=4;
    UL_FCALL(*udrv->chip_ops->fnc_wait); /* Wait for B8+UL_END,ARQ,PRQ or AAP */
    udrv->con_flg|=0x80;
    return UL_RC_PROC;
  };
  udrv->con_flg&=~3;
  switch(udrv->char_buff)
  {
    case UL_END: break;
    case UL_ARQ: udrv->con_flg|=1; break;
    case UL_PRQ: udrv->con_flg|=2; break;
    case UL_AAP: udrv->con_flg|=3; break;
    default: UL_FRET; return UL_RC_ERECPROT;
  };
  UL_BLK_HEAD(udrv->con_frame).flg|=udrv->con_flg&(UL_BFL_PRQ|UL_BFL_ARQ)&3;
  udrv->wait_time=4;
  UL_FCALL2(*udrv->chip_ops->fnc_wait,uld_recend_2); /* Wait for XOR sum */
  return UL_RC_PROC;
};

/*** Receive frame end ***/
int uld_recend(ul_drv *udrv, int ret_code)
{
  if((udrv->char_buff&0xFF00)==0x100) 
  {
    UL_FNEXT(uld_recend_1);
  }else{
    udrv->wait_time=4;
    UL_FCALL2(*udrv->chip_ops->fnc_wait,uld_recend_1); /* Skip data and wait for B8+UL_END,ARQ,PRQ or AAP */
  };
  return UL_RC_PROC;
};


/*******************************************************************/
/* Message and frame proccessor */

int uld_prmess_frame(ul_drv *udrv, int ret_code);
int uld_prmess(ul_drv *udrv, int ret_code);

/*-- Helper functions for prmess --*/
int uld_prmess_error(ul_drv *udrv, int ret_code)
{
  UL_FRET;
  return ret_code;
};

int uld_prmess_next(ul_drv *udrv, int ret_code)
{
  if(ret_code<0) {UL_FNEXT(uld_prmess_error);return UL_RC_EPRMESS;};

 #ifdef UL_WITH_IAC
  if(udrv->con_flg&0x20) {
    ret_code=uld_iac_immediate(udrv, udrv->con_frame);
    if(ret_code!=UL_RC_ENOREGIAC) {
      if(ret_code==UL_RC_FREEMSG) {
        UL_FRET;
        return UL_RC_PROC;
      }
    }
  }
 #endif /*UL_WITH_IAC*/

  if(UL_BLK_HEAD(udrv->con_frame).flg&UL_BFL_TAIL)
  {
    if(UL_BLK_HEAD(udrv->con_frame).next)
    {
      udrv->con_frame=UL_BLK_HEAD(udrv->con_frame).next;
      UL_FNEXT(uld_prmess_frame);
      return UL_RC_PROC;
    };
    UL_FRET;
    return UL_RC_ENOTAIL;
  };
  UL_FRET;
  return UL_RC_PROC;
};

int uld_prmess_rec2(ul_drv *udrv, int ret_code)
{
  if(ret_code<0) {UL_FNEXT(uld_prmess_error);return UL_RC_EPRMESS;};
  UL_FCALL2(uld_recend,uld_prmess_next); /* Receive end of frame */
  return UL_RC_PROC;
};

int uld_prmess_rec1(ul_drv *udrv, int ret_code)
{
  if(ret_code<0) {UL_FNEXT(uld_prmess_error);return UL_RC_EPRMESS;};
  UL_BLK_HEAD(udrv->con_frame).dadr=udrv->con_dadr;
  UL_BLK_HEAD(udrv->con_frame).sadr=udrv->con_sadr;
  UL_BLK_HEAD(udrv->con_frame).cmd=udrv->con_cmd;
  UL_FCALL2(uld_recdata,uld_prmess_rec2); /* Receive data bytes */
  return UL_RC_PROC;
};

int uld_prmess_snd2(ul_drv *udrv, int ret_code)
{
  if(ret_code<0) {UL_FNEXT(uld_prmess_error);return UL_RC_EPRMESS;};
  UL_FCALL2(uld_sndend,uld_prmess_next); /* Send end of frame */
  return UL_RC_PROC;
};

int uld_prmess_snd1(ul_drv *udrv, int ret_code)
{
  if(ret_code<0) {UL_FNEXT(uld_prmess_error);return UL_RC_EPRMESS;};
  UL_FCALL2(uld_snddata,uld_prmess_snd2); /* Send data bytes */
  return UL_RC_PROC;
};

int uld_prmess_frame(ul_drv *udrv, int ret_code)
{
  if(ret_code<0) {UL_FNEXT(uld_prmess_error);return UL_RC_EPRMESS;};
  udrv->con_flg=0;
  if(UL_BLK_HEAD(udrv->con_frame).flg&UL_BFL_SND)
  {
    int dadr_match;
    udrv->con_dadr=UL_BLK_HEAD(udrv->con_frame).dadr;
    udrv->con_sadr=UL_BLK_HEAD(udrv->con_frame).sadr;
    udrv->con_cmd=UL_BLK_HEAD(udrv->con_frame).cmd;
    udrv->con_flg=UL_BLK_HEAD(udrv->con_frame).flg&3;
   #ifdef UL_WITH_MULTI_NET
    udrv->con_hops=UL_BLK_HEAD(udrv->con_frame).hops;
    udrv->con_sadr_local=UL_BLK_HEAD(udrv->con_frame).sadr_local;
   #endif /*UL_WITH_MULTI_NET*/

    dadr_match=uld_adr_match(udrv, udrv->con_dadr);
    if((dadr_match==UL_ADR_MATCH_BROADCAST)||
       (dadr_match==UL_ADR_MATCH_MINE)) {
      if(udrv->con_adr_match!=UL_ADR_MATCH_MINE)
        udrv->con_adr_match=dadr_match;
      udrv->con_flg&=~1; /* do not expect external acknowledge */
     #ifdef UL_WITH_IAC
      if(udrv->con_flg&2)
        udrv->con_flg|=0x20; /* local IAC processing may be required */
     #endif /*UL_WITH_IAC*/
    } else {
      if(udrv->con_adr_match==UL_ADR_MATCH_NONE)
        udrv->con_adr_match=dadr_match;
    }
    UL_FCALL2(uld_sndbeg,uld_prmess_snd1); /* Send begin of frame */
  }
  else if(UL_BLK_HEAD(udrv->con_frame).flg&UL_BFL_REC)
  {
    udrv->con_flg=8;
    UL_FCALL2(uld_recbeg,uld_prmess_rec1); /* Receive begin of frame */
  }
  else UL_FNEXT(uld_prmess_next);
  return UL_RC_PROC;
};

/*** Proccess one outgoing message ***/
int uld_prmess(ul_drv *udrv, int ret_code)
{
  if(!udrv->con_message)
  {
    UL_FRET;
    return UL_RC_PROC;
  };
  udrv->con_frame=udrv->con_message;
  UL_FNEXT(uld_prmess_frame);
  return UL_RC_PROC;
};

