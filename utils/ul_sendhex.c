/*******************************************************************
  uLan Communication - simple test client

  ul_sendhex.c	- intelhex downloader

  (C) Copyright 1996,1999 by Pavel Pisa 

  The uLan driver is distributed under the Gnu General Public Licence. 
  See file COPYING for details.
 *******************************************************************/

#define _GNU_SOURCE

#ifndef _MSC_VER
#include <sys/time.h>
#include <unistd.h>
#endif /*_MSC_VER*/
#include <stdlib.h>
#include <string.h>
#include <fcntl.h>
#include <stdio.h>
#include <getopt.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <ul_lib/ulan.h>

int ul_new_memrq_head(ul_fd_t ul_fd, int dadr, int cmd, 
                      int mtype, int start, int len)
{
  int ret;
  int i;
  ul_msginfo msginfo;
  uchar buf[8];
  msginfo.dadr=dadr;
  msginfo.cmd=cmd;
  msginfo.flg=UL_BFL_ARQ|UL_BFL_PRQ|UL_BFL_M2IN;
  ret=ul_newmsg(ul_fd,&msginfo);
  if(ret<0) return ret;
  i=0;
  buf[i++]=(uchar)mtype; buf[i++]=mtype>>8;
  buf[i++]=(uchar)start; buf[i++]=start>>8;
  if(mtype&0x100)
    {buf[i++]=start>>16; buf[i++]=start>>24;};
  buf[i++]=(uchar)len;   buf[i++]=len>>8;
  if(ul_write(ul_fd,buf,i)!=i)
  { ul_abortmsg(ul_fd);
    ret=-1;
  };
  return ret;
};

int ul_new_memrq_read(ul_fd_t ul_fd, int dadr,
                      int mtype, int start, int len)
{
  int ret;
  ul_msginfo msginfo;
  ret=ul_new_memrq_head(ul_fd,dadr,UL_CMD_RDM,mtype,start,len);
  if (ret<0) return ret;
  msginfo.dadr=dadr;
  msginfo.cmd=UL_CMD_RDM&0x7F;
  msginfo.flg=UL_BFL_REC|UL_BFL_LNMM|UL_BFL_M2IN;
  msginfo.len=len;
  ret=ul_tailmsg(ul_fd,&msginfo);
  if (ret<0) {ul_abortmsg(ul_fd);return ret;};
  ret=ul_freemsg(ul_fd);
  return ret;
};

int ul_new_memrq_write(ul_fd_t ul_fd, int dadr,
                       int mtype, int start, int len, void *buf)
{
  int ret;
  ul_msginfo msginfo;
  ret=ul_new_memrq_head(ul_fd,dadr,UL_CMD_WRM,mtype,start,len);
  if(ret<0) return ret;
  msginfo.dadr=dadr;
  msginfo.cmd=UL_CMD_WRM&0x7F;
  msginfo.flg=UL_BFL_SND|UL_BFL_LNMM|UL_BFL_ARQ|UL_BFL_M2IN;
  msginfo.len=len;
  ret=ul_tailmsg(ul_fd,&msginfo);
  if(ret<0) {ul_abortmsg(ul_fd);return ret;};
  if(buf)
  { ret=ul_write(ul_fd,buf,len);
    if(ret<0) {ul_abortmsg(ul_fd);return ret;};
    ret=ul_freemsg(ul_fd);
  };
  return ret;
};

int ul_mem_read_wait(ul_fd_t ul_fd, int dadr,
                     int mtype, int start, int len, void *ptr)
{
  int ret, stamp;
  ul_msginfo msginfo;
 
  stamp=ret=ul_new_memrq_read(ul_fd,dadr,mtype,start,len);
  if(ret<0) return ret;

  /* ioctl(ul_fd,UL_KLOGBLL); */

  /* ioctl(ul_fd,UL_STROKE); */

  do {
    ul_freemsg(ul_fd);
    ret=ul_fd_wait(ul_fd,10);
    if(ret<0) {printf("Select returned %d\n",ret);return ret;};
    if(!ret) {printf("Select - timeout\n");return -1;};
    ret=ul_acceptmsg(ul_fd,&msginfo);
    if(ret<0)
    { printf("Accept msg returned %d\n",ret);
      return ret;
    };
  } while(msginfo.stamp!=stamp);
  if(msginfo.flg&UL_BFL_FAIL) 
  { printf("Failed msg flg=0x%X\n",msginfo.flg);
    ul_freemsg(ul_fd);return -1;
  };
 
  ret=ul_actailmsg(ul_fd,&msginfo);
  if(ret<0) 
  { printf("Accept tail returned %d\n",ret);
    ul_freemsg(ul_fd);
    return ret;
  };
  if((ret=ul_read(ul_fd,ptr,len))!=len)
  { printf("Bad read len %d, rq %d, msg %d\n",ret,len,msginfo.len);
    ul_freemsg(ul_fd);return -1;
  };
  
  ul_freemsg(ul_fd);
  return len;
};

/*******************************************************************/

typedef struct tform_file {
	uchar *buf;
	int   buf_len;
	int   buf_addr;
	int   buf_bytes;
	int   buf_rem_bytes;
	FILE *file;
	uchar *line_buf;
	int  line_addr;
	int  ext_base_addr;
	int  line_bytes;
	int  line_offs;
	int  start_addr;
	int (*read)(struct tform_file *tform);
	int (*done)(struct tform_file *tform);
  } tform_file;

#ifndef HAS_GETDELIM
int getdelim(char **line,size_t *linelen,char delim,FILE *F)
{
 char c;
 size_t  l=0;
 do{
  if(l+1>=*linelen)
  {
   *linelen=l+20;
   if(!*line) *line=(char *)malloc(*linelen);
    else *line=(char *)realloc(*line,*linelen);
  }
  c=fgetc(F);
  if(feof(F)) {if(l) break; else return -1;}
  if(c!='\r') (*line)[l++]=c;
 } while(c!=delim);
 (*line)[l]=0;
 return l;
};
#endif

int get_hex(char **p, unsigned *v, int chars)
{
  unsigned u=0;
  char c;
  *v=0;
  while(**p==' ') (*p)++;
  while(chars--)
  { u<<=4;
    c=**p;
    if((c>='0')&&(c<='9')) u+=c-'0';
    else if((c>='A')&&(c<='F')) u+=c-'A'+10;
    else return -1;
    (*p)++;
  };
  *v=u;
  return 0;
};

int tform_init(tform_file *tform, int buf_len)
{
  if(!buf_len) buf_len=1024;
  tform->file=NULL;
  tform->buf_len=buf_len;
  tform->buf=malloc(tform->buf_len);
  tform->buf_addr=0;
  tform->line_buf=NULL;
  tform->line_offs=0;
  tform->line_bytes=0;
  tform->start_addr=-1;
  tform->buf_bytes=0;
  tform->buf_rem_bytes=0;
  tform->read=NULL;
  tform->done=NULL;
  tform->ext_base_addr=0;
  return 0;
};

int tform_done(tform_file *tform)
{
  if(tform->done)
    return tform->done(tform);
  fclose(tform->file);
  if(tform->buf)free(tform->buf);
  if(tform->line_buf)free(tform->line_buf);
  return 0;
};

int tform_read(tform_file *tform)
{
  return tform->read(tform);
}

int tform_read_ihex(tform_file *tform)
{ int cn,len=0;
  int addr=0;
  unsigned u,v;
  char *p;
  uchar *r;
  char *line=NULL;
  size_t line_len=0;

  if(tform->buf_rem_bytes){
    int aoffs=tform->line_bytes-tform->buf_rem_bytes;
    memmove(tform->buf,tform->buf+aoffs,tform->buf_rem_bytes);
    addr=tform->buf_addr+=aoffs;
    len=tform->buf_rem_bytes;
  }

  while(len<tform->buf_len)
  {
    if(!tform->line_bytes)
    { int checksum=0;
      int ihex_type=0;

      tform->line_offs=0;
      if(getdelim(&line,&line_len,'\n',tform->file)==-1) break;
      p=line;
      if(*p++!=':') printf("tform_read : strange line %s\n",line);
      else {
       if(get_hex(&p,&u,2)<0) {printf("tform_read_ihex : bad ihex cnt\n");return -1;};
       checksum+=cn=tform->line_bytes=u;
       if(!tform->line_buf) tform->line_buf=malloc(cn);
       else tform->line_buf=realloc(tform->line_buf,cn);
       if(get_hex(&p,&u,2)<0) {printf("tform_read_ihex : bad ihex addr\n");return -1;};
       if(get_hex(&p,&v,2)<0) {printf("tform_read_ihex : bad ihex addr\n");return -1;};
       checksum+=u+v;
       tform->line_addr=(u<<8)+v;
       if(get_hex(&p,&u,2)<0) {printf("tform_read_ihex : bad ihex type\n");return -1;};
       checksum+=ihex_type=u;       
       if((ihex_type>=0)&&(ihex_type<=5))
       {
         r=tform->line_buf;
         while(cn--)
         { if(get_hex(&p,&u,2)<0) {printf("tform_read_ihex : bad ihex data\n");return -1;};
           checksum+=*r++=u;

         };
         if(get_hex(&p,&u,2)<0) {printf("tform_read_ihex : bad ihex csum\n");return -1;};
         checksum+=u;
         if(checksum&0xff) 
         { printf("tform_read_ihex : error ihex csum %d\n",
                   checksum);return -1;
         };
         while((u=*p++)) if(u!=' '&&u!='\n'&&u!='\r')
           {printf("tform_read_ihex : residual chars on line\n");return -1;};
       };
       if(ihex_type==1)
       { tform->line_bytes=0;
         if(tform->start_addr==-1)
	 tform->start_addr=tform->line_addr;
       };
       tform->line_addr+=tform->ext_base_addr;
       if((ihex_type>=2)&&(ihex_type<=5))
       {
         cn=tform->line_bytes;
         r=tform->line_buf;
         addr=0;

         while(cn-->0)
         {
           addr<<=8;
           addr+=*(r++);
         }
         if(ihex_type==2)
           tform->ext_base_addr=addr<<4;
         else if(ihex_type==4)
           tform->ext_base_addr=addr<<16;
         else if(ihex_type==5)
           tform->start_addr=addr;

         tform->line_bytes=0;
       };
      };
    };
    if(tform->line_bytes)
    { if(!len) addr=tform->buf_addr=tform->line_addr+tform->line_offs;
      else if(addr!=tform->line_addr+tform->line_offs) break;
      cn=tform->line_bytes-tform->line_offs;
      if(cn+len>tform->buf_len) cn=tform->buf_len-len;
      memcpy(tform->buf+len,tform->line_buf+tform->line_offs,cn);
      len+=cn;
      addr+=cn;
      tform->line_offs+=cn;
      if(tform->line_bytes==tform->line_offs) tform->line_bytes=0;
    };
  };
  tform->buf_bytes=len;
  tform->buf_rem_bytes=0;
  return len;
};

int tform_read_binary(tform_file *tform)
{
  int len=0;

  tform->buf_addr+=tform->buf_bytes;
  len=fread(tform->buf,1,tform->buf_len,tform->file);
  if(len<0) {
    perror("tform_read_binary : read error");
    return -1;
  }

  tform->buf_bytes=len;
  return len;
}

int tform_open(tform_file *tform,char *file_name,
               char *format, int buf_len, int wr_fl)
{
  FILE *file;

  if(!format || !strcmp("ihex",format))
  {
    if((file=fopen(file_name,"r"))==NULL)
    {perror("download_file : hex file open");
      return -1;
    };
    tform_init(tform,buf_len);
    tform->file=file;
    tform->read=tform_read_ihex;
  } 
  else if(!strcmp("binary",format))
  {
    if((file=fopen(file_name,"rb"))==NULL)
    {perror("download_file : binary file open");
      return -1;
    };
    tform_init(tform,buf_len);
    tform->file=file;
    tform->read=tform_read_binary;
  } else {
    fprintf(stderr, "requested unknown format %s\n", format);
    return -1;
  }
  return 1;
}

/*******************************************************************/

int si_long(char **ps,long *val,int base)
{
  char *p;
  *val=strtol(*ps,&p,base);
  if(*ps==p) return -1;
  *ps=p;
  return 1;
}

int add_to_arr(void **pdata,int *plen,int base,char *str)
{
  char *s=str;
  long val;
  void *p;

  do{
    while(*s && strchr(", \t:;",*s)) s++;
    if(!*s) break;
    if(si_long(&s,&val,base)<0){
      return -1;
    }
    if(*pdata==NULL){
      *plen=0;
      *pdata=p=malloc(1);
    }else{
      p=realloc(*pdata,*plen+1);
      if(p==NULL) return -1;
      *pdata=p;
    }
    ((unsigned char*)p)[*plen]=val;
    (*plen)++;
  } while(1);
  return 1;
}

/*******************************************************************/

char *ul_dev_name = UL_DEV_NAME;
int module     = 3;
int upload_flg = 0;
int mem_type   = 2;
int mem_start  = 0;
int mem_length = 0xff00;
int max_block  = 1024;
int align_order= 0;
int go_addr    = 3;
int go_flg     = 0;
int reset_flg  = 0;
int prt_modules= 0;
int debugk     = 0;
int debugk_flg = 0;
int fill_flg   = 0;
int erase_flg  = 0;
int boot_flg   = 0;
int query      = 0;
int query_flg  = 0;
int drv_info_flg = 0;
void *fill_pat_val;
int fill_pat_len;
char *file_format=NULL;
unsigned int sn=0;

int download_file(char *file_name, char *format)
{
  ul_fd_t ul_fd;
  tform_file tform;
  int len;  
  int ret=0;
  int stamp;
  ul_msginfo msginfo;
  int in_proc_msg=0;
  int shift_addr=0;
  int addr;
  unsigned align_size=(1<<align_order);

  shift_addr=mem_start;

  ul_fd=ul_open(ul_dev_name, NULL);
  if(ul_fd==UL_FD_INVALID)
    { perror("download_file : uLan open failed");return -1;};
  /* ul_drv_debflg(ul_fd,0x11); */ /* 0x9 0x11 */ 

  if (tform_open(&tform, file_name, format, max_block, 0)<0)
  {
    ul_close(ul_fd);
    return -1;
  }

  do
  { 
    while(in_proc_msg<3)
    { len=tform_read(&tform);
      if(!len) break;
      if(len<0) 
      { perror("download_file : ihex");
        ret=-1;
        break;
      };

      printf("addr %4X len %4X\n",tform.buf_addr+shift_addr,len);

      addr=tform.buf_addr+shift_addr;

      if(align_order){
        int rem=len+((align_size-1)&addr);
        if(rem>align_size){
	  rem-=align_size;
	  len-=rem;
	  tform.buf_rem_bytes=rem;
	}
      }

      if((stamp=ul_new_memrq_write(ul_fd,module,mem_type,addr,len,tform.buf))<0)
      { printf("download_file : send message error\n");
        ret=-1; break;
      };
      in_proc_msg++;
    };
    if(ret<0) break;
    if(in_proc_msg)
    { if(ul_fd_wait(ul_fd,10)<=0) 
      { printf("download_file : select error\n");
        ret=-1; break;
      };
      if(ul_acceptmsg(ul_fd,&msginfo)<0)
      { printf("download_file : accept msg error\n");
        ret=-1; break;
      };
      if(msginfo.flg&UL_BFL_FAIL)
      { printf("download_file : target system error\n");
        ret=-1; break;
      };
      ul_freemsg(ul_fd);
      in_proc_msg--;
      /* printf(">>> "); */
    };
  } while(in_proc_msg);
  
  if(tform.start_addr!=-1) 
    printf("Found start address %4X\n",tform.start_addr);
  tform_done(&tform);

  ul_close(ul_fd);

  return ret;
};

int send_cmd_go(int addr)
{ int ret;
  int i;
  ul_fd_t ul_fd;
  uchar buf[8];
  ul_fd=ul_open(ul_dev_name, NULL);
  if(ul_fd==UL_FD_INVALID)
    { perror("send_cmd_go : uLan open failed");return -1;};
  i=0;
  buf[i++]=0x10;
  buf[i++]=(uchar)mem_type; buf[i++]=mem_type>>8;
  buf[i++]=(uchar)go_addr;  buf[i++]=go_addr>>8;
  if(mem_type&0x100)
    {buf[i++]=go_addr>>16;  buf[i++]=go_addr>>24;};
  ret=ul_send_command_wait(ul_fd,module,UL_CMD_DEB,
  			UL_BFL_ARQ|UL_BFL_PRQ,buf,i);
  if(ret<0) printf("Goto to %4X ERROR\n",addr);
  else printf("Goto to %4X OK\n",addr);
  ul_close(ul_fd);
  return ret;
};

int send_cmd_res(void)
{ int ret;
  int i;
  ul_fd_t ul_fd;
  uchar buf[8];
  ul_fd=ul_open(ul_dev_name, NULL);
  if(ul_fd==UL_FD_INVALID)
    { perror("send_cmd_res : uLan open failed");return -1;};
  i=0;
  buf[i++]=ULRES_CPU;
  buf[i++]=0x55;
  buf[i++]=0xaa;
  ret=ul_send_command_wait(ul_fd,module,UL_CMD_RES,
  			UL_BFL_ARQ|UL_BFL_PRQ,buf,i);
  if(ret<0) printf("Module %d reset ERROR\n", module);
  else printf("Module %d reset OK\n", module);
  ul_close(ul_fd);
  return ret;
};

#define MEM_FILL_ATONCE 1024

int mem_fill(int mem_type, unsigned long start, unsigned long len,
		unsigned char *pat_val, int pat_len)
{
  ul_fd_t ul_fd;
  int in_proc_msg=0;
  ul_msginfo msginfo;
  int ret, stamp;
  unsigned long addr=start;
  unsigned char buff[MEM_FILL_ATONCE];
  int blen=MEM_FILL_ATONCE;
  unsigned char *p;
  int cnt;
  int i;

  if(pat_len<=0) return -1;

  ul_fd=ul_open(ul_dev_name, NULL);
  if(ul_fd==UL_FD_INVALID)
    { perror("mem_fill : uLan open failed");return -1;};

  if(blen>max_block) blen=max_block;

  if((pat_len<=blen/2)&&(len>pat_len)){
    i=blen<len+pat_len?blen:len+pat_len-1;
    if((pat_len==1)){
      blen=i;
      memset(buff,*pat_val,blen);
    }else{
      i=i/pat_len;
      blen=i*pat_len;
      for(p=buff;i;i--,p+=pat_len){
        memcpy(p,pat_val,pat_len);
      }
    }
    pat_val=buff;
  } else {
    blen=pat_len;
  }
  
  while(len || in_proc_msg){
    while(in_proc_msg<3){
      cnt=(len>blen)?blen:len;
      stamp=ret=ul_new_memrq_write(ul_fd, module, mem_type, addr, cnt, pat_val);
      if(ret<0){
	fprintf(stderr,"mem_fill: uniload_mem_write returned %d\n", ret);
	ul_close(ul_fd);
	return -1;
      }
      len-=cnt;
      addr+=cnt;
      in_proc_msg++;
    }
    if(in_proc_msg){
      if(ul_fd_wait(ul_fd,10)<=0) 
      { printf("mem_fill : select error\n");
        ret=-1; break;
      };
      if(ul_acceptmsg(ul_fd,&msginfo)<0)
      { printf("mem_fill : accept msg error\n");
        ret=-1; break;
      };
      if(msginfo.flg&UL_BFL_FAIL)
      { printf("mem_fill : target system error\n");
        ret=-1; break;
      };
      ul_freemsg(ul_fd);
      in_proc_msg--;
    }
  }
  ul_close(ul_fd);
  return 0;  
}

int print_modules(int max_addr)
{ int ret;
  int i;
  ul_fd_t ul_fd;
  uchar *p;
  void *buf=NULL;
  int buf_len;
  int count=0; 
  ul_fd=ul_open(ul_dev_name, NULL);
  if(ul_fd==UL_FD_INVALID)
    { perror("print_modules : uLan open failed");return -1;};
  for(i=1;i<=max_addr;i++)
  {
    ret=ul_send_query_wait(ul_fd,i,UL_CMD_SID,
  			UL_BFL_NORE|UL_BFL_PRQ,NULL,0,&buf,&buf_len);
    if(ret>=0)
    { count++;
      p=(uchar*)buf;
      for(ret=0;(ret<buf_len)&&*p;ret++,p++);
      printf("%2d:  ",i);
      fwrite(buf,ret,1,stdout);
      printf("\n");
    };
    if(buf) free(buf);
    buf=NULL;
  };
  ul_close(ul_fd);
  return count;
};

int debug_kernel(int debug_msk)
{ int ret;
  ul_fd_t ul_fd;
  ul_fd=ul_open(ul_dev_name, NULL);
  if(ul_fd==UL_FD_INVALID)
    { perror("debug_kernel : uLan open failed");return -1;};
  ret=ul_drv_debflg(ul_fd,debug_msk);
  ul_close(ul_fd);
  return ret;
};

int queryparam(unsigned long query)
{ int ret;
  unsigned long value;
  ul_fd_t ul_fd;
  ul_fd=ul_open(ul_dev_name, NULL);
  if(ul_fd==UL_FD_INVALID)
    { perror("queryparam : uLan open failed");return -1;};
  ret=ul_queryparam(ul_fd,query, &value);
  ul_close(ul_fd);
  if (ret>=0)
    printf("query %lu=%lu\n",query,value);
  else
    printf("query %lu error %d\n",query,ret);
  return ret;
};

int drv_info(void)
{ int ret;
  unsigned long value;
  ul_fd_t ul_fd;
  ul_fd=ul_open(ul_dev_name, NULL);
  if(ul_fd==UL_FD_INVALID)
    { perror("drv_info : uLan open failed");return -1;};
  ret=ul_drv_version(ul_fd);
  printf("ul_drv\n");
  printf("  version:       %d.%d.%d\n",(ret>>16)&0xff,(ret>>8)&0xff,ret&0xff);
  ret=ul_queryparam(ul_fd,0,&value);
  if (ret>=0)
    printf("  my_adr:        %lu\n",value);
  ret=ul_queryparam(ul_fd,1,&value);
  if (ret>=0)
    printf("  baudrate:      %lu\n",value);
  ret=ul_queryparam(ul_fd,2,&value);
  if (ret>=0)
    printf("  promode:       %lu\n",value);
  ret=ul_queryparam(ul_fd,3,&value);
  if (ret>=0)
    printf("  subdevnum_max: %lu\n",value);
  ul_close(ul_fd);
  return ret;
};

int upload_file(char *file_name, char *format)
{
  ul_fd_t ul_fd;
  FILE *file;
  int ret;
  uchar buf[0x400];
  uchar *p;
  char *mode="w";
  enum {fmt_ihex, fmt_binary, fmt_dump} fmt;
  int i,l,csum; 
  unsigned long mem_adr=mem_start;
  unsigned long mem_len=mem_length;
  unsigned long len;
  unsigned long ext_addr = 0; /* for Intel HEX format */

  if(max_block>0x400) max_block=0x400;

  if(!format || !strcmp("ihex",format)){
    fmt=fmt_ihex;
  } else if(!strcmp("binary",format)){
    mode="wb";
    fmt=fmt_binary;
  } else if(!strcmp("dump",format)){
    fmt=fmt_dump;
  } else {
    fprintf(stderr, "requested unknown format %s\n", format);
    return -1;
  }
  
  if(!strcmp(file_name,"-")) file_name=NULL;

  ul_fd=ul_open(ul_dev_name, NULL);
  if(ul_fd==UL_FD_INVALID)
    { perror("upload_file : open failed");return -1;};
  /* ul_drv_debflg(ul_fd,0x11); */ /* 0x9 0x11 */

  if(file_name){
    if((file=fopen(file_name,mode))==NULL)
    {perror("upload_file : file open");
     ul_close(ul_fd);
     return -1;
    }
  } else file=stdout;

  while(mem_len)
  { len=mem_len<max_block?mem_len:max_block;
    ret=ul_mem_read_wait(ul_fd,module,mem_type,mem_adr,len,buf);
    if(ret<0) 
    { printf("Mem read returns %d\n",ret);
      break;
    }
    if(file_name) printf("%04lX\n",mem_adr);
    switch(fmt){
      case fmt_ihex:
	p=buf; i=0;
	while(i<len)
	{
          unsigned long a=mem_adr+i;

          if((a&~0xFFFF)!=ext_addr)
          {
            unsigned long val;
            unsigned char b;

            ext_addr=a&~0xFFFF;
            val=ext_addr>>16;
            l=2;
            val>>=16;
            while(val!=0)
            {
              val>>=8;
              l++;
            }
            fprintf(file,":%02X000004",l);
            csum=l+4;
            val=ext_addr>>16;
            while(l--)
            {
              b=(val>>(8*l))&0xff;
              fprintf(file,"%02X",b);
              csum+=b;
            }
            fprintf(file,"%02X\n",(-csum)&0xFF);
          }
	  l=len-i;
	  if(l>16) l=16;
	  csum=l+a+(a>>8);
	  fprintf(file,":%02X%04lX00",l,a&0xFFFF);
	  while(l--) {fprintf(file,"%02X",buf[i]);csum+=buf[i++];};
	  fprintf(file,"%02X\n",(-csum)&0xFF);
	}
	break;
      case fmt_binary:
	if(fwrite(buf,len,1,file)!=1){
	  perror("upload_file : file write");
	  return -1;
	}
	break;
      case fmt_dump:
        i=0; p=buf;
	while(i<len){
	  if(i&0xf) printf(" %02X",*(p++));
	  else printf(i?"\n%04lX:%02X":"%04lX:%02X",mem_adr+i,*(p++));
	  i++;
	}
	printf("\n");
	break;
    }
    mem_adr+=len;
    mem_len-=len;
  }

  if(fmt==fmt_ihex)
    fprintf(file,":00000001FF\n");

  if(file_name) fclose(file); 
  ul_close(ul_fd);
 
  return 0;
};

int mem_erase(int mem_type, int start, int len)
{
  int ret;
  int i;
  ul_fd_t ul_fd;
  uchar buf[10];
  ul_fd=ul_open(ul_dev_name, NULL);
  if(ul_fd==UL_FD_INVALID)
    { perror("mem_erase : uLan open failed");return -1;};
  i=0;
  buf[i++]=(uchar)mem_type; buf[i++]=mem_type>>8;
  buf[i++]=(uchar)start;  buf[i++]=start>>8;
  if(mem_type&0x100)
    {buf[i++]=start>>16;  buf[i++]=start>>24;};
  buf[i++]=(uchar)len;   buf[i++]=len>>8;
  if(mem_type&0x100)
    {buf[i++]=len>>16;  buf[i++]=len>>24;};
  ret=ul_send_command_wait(ul_fd,module,UL_CMD_ERM,
  			UL_BFL_ARQ|UL_BFL_PRQ,buf,i);
  if(ret<0) printf("erase from:0x%4X to:0x%4X ERROR\n",start,start+len);
  else printf("erase from:0x%4X to:0x%4X OK\n",start,start+len);
  ul_close(ul_fd);
  return ret;
}

int activate_boot(void)
{
  ul_fd_t ul_fd;
  int ret,in_boot=0;
  ul_msginfo msginfo;
  unsigned int rsn;
  uchar buf[5];

  ul_fd=ul_open(ul_dev_name, NULL);
  if(ul_fd==UL_FD_INVALID)
    { perror("stay_in_boot : uLan open failed");return -1;};
  memset(&msginfo,0,sizeof(msginfo));
  msginfo.cmd=UL_CMD_NCS;   
  ret=ul_addfilt(ul_fd,&msginfo);
  if(ret<0) { printf("stay_in_boot : add filter failed\n");return ret;};   
  while(!in_boot) {
    if(ul_acceptmsg(ul_fd,&msginfo)<0) {
      buf[0]=(uchar)ULNCS_BOOT_ACT;
      buf[1]=(uchar)sn;  buf[2]=sn>>8;
      buf[3]=sn>>16;  buf[4]=sn>>24;
      ul_send_command(ul_fd,module,UL_CMD_NCS,0,buf,5);
      #ifdef _WIN32
        Sleep(1000);
      #else
        sleep(1);
      #endif 
    } else {
      if ((msginfo.cmd==UL_CMD_NCS) && (msginfo.len==5)) {
        if((ret=ul_read(ul_fd,&buf,5))!=msginfo.len) { 
          printf("Bad read len %d, rq %d, msg %d\n",ret,5,msginfo.len);
          ul_freemsg(ul_fd);return -1;
        };
        rsn=(unsigned long)buf[1];
        rsn+=(unsigned long)buf[2]<<8;
        rsn+=(unsigned long)buf[3]<<16;
        rsn+=(unsigned long)buf[4]<<24;
        if ((buf[0]==ULNCS_BOOT_ACK) && (rsn==sn))
          in_boot=1;
      }  
      ul_freemsg(ul_fd);
    }
  }
  ul_close(ul_fd);
  return 0;
}

static void
usage(void)
{
  printf("Usage: ul_sendhex <parameters> <hex_file>\n");
  printf("  -d, --uldev <name>       name of uLan device [%s]\n", ul_dev_name);
  printf("  -m, --module <num>       download/upload target uLan module\n");
  printf("  -t, --type <num>         target module memory space\n");
  printf("  -s, --start <addr>       start address of transfer\n");
  printf("  -l, --length <num>       length of upload block\n");
  printf("  -b, --block <num>        maximal block length\n");
  printf("  -A, --align-order <num>  force block alignment\n");
  printf("  -g, --go <addr>          start program from address\n");
  printf("  -r, --reset              reset before download\n");
  printf("  -u, --upload             upload memory block [download]\n");
  printf("  -F, --fill <num>         fill region defined by -s -l by value\n");
  printf("  -e, --erase              erase memory defined by -s -l -t\n");
  printf("  -o, --bootloader <sn>    activate bootloader\n");
  printf("  -f, --format <format>    format of data file [ihex]\n");
  printf("  -p, --print <max>        print modules to max address\n");
  printf("      --debug-kernel <m>   flags to debug kernel\n");
  printf("  -q  --queryparam <q>     query for a uLan driver parameter\n");
  printf("  -i  --drv-info           show driver informations\n");
  printf("  -V, --version            show version\n");
  printf("  -h, --help               this usage screen\n");
}

int main(int argc,char *argv[])
{
  static struct option long_opts[] = {
    { "uldev", 1, 0, 'd' },
    { "module",1, 0, 'm' },
    { "type",  1, 0, 't' },
    { "start", 1, 0, 's' },
    { "length",1, 0, 'l' },
    { "block", 1, 0, 'b' },
    { "align-order", 1, 0, 'A' },
    { "go",    1, 0, 'g' },
    { "reset", 0, 0, 'r' },
    { "upload",0, 0, 'u' },
    { "fill",  1, 0, 'F' },
    { "erase", 0, 0, 'e' },
    { "bootloader", 1, 0, 'o' },
    { "format",1, 0, 'f' },
    { "print", 1, 0, 'p' },
    { "version",0,0, 'V' },
    { "help",  0, 0, 'h' },
    { "debug-kernel", 1, 0, 'D' },
    { "queryparam", 1, 0, 'q' },
    { "drv-info",  0, 0, 'i' },
    { 0, 0, 0, 0}
  };
  int opt;

 #ifndef HAS_GETOPT_LONG
  while ((opt = getopt(argc, argv, "d:m:t:s:l:b:g:ruF:ef:o:p:VhD:q:i")) != EOF)
 #else
  while ((opt = getopt_long(argc, argv, "d:m:t:s:l:b:g:ruF:ef:o:p:Vhq:i",
			    &long_opts[0], NULL)) != EOF)
 #endif
    switch (opt) {
    case 'd':
      ul_dev_name=optarg;
      break;
    case 'm':
      module = strtol(optarg,NULL,0);
      break;
    case 't':
      mem_type = strtol(optarg,NULL,0);
      break;
    case 's':
      mem_start = strtol(optarg,NULL,0);
      break;
    case 'l':
      mem_length = strtol(optarg,NULL,0);
      break;
    case 'b':
      max_block = strtol(optarg,NULL,0);
      break;
    case 'A':
      align_order = strtol(optarg,NULL,0);
      break;
    case 'g':
      go_addr = strtol(optarg,NULL,0);
      go_flg = 1;
      break;
    case 'r':
      reset_flg = 1;
      break;
    case 'u':
      upload_flg = 1;
      break;
    case 'F':
      fill_flg=1;
      if(add_to_arr((void**)&fill_pat_val,&fill_pat_len,0,optarg)<0){
	fprintf(stderr,"%s: incorrect patern data \"%s\"\n",argv[0],optarg);
	exit(2);
      }
      if(!fill_pat_len){
	fprintf(stderr,"%s: incorrect patern data - empty value\n",argv[0]);
	exit(2);
      }
      break;
    case 'e':
      erase_flg = 1;
      break;
    case 'o':
      boot_flg = 1;
      sn = strtol(optarg,NULL,0);
      break;
    case 'f':
      file_format=optarg;
      break;
    case 'p':
      prt_modules = strtol(optarg,NULL,0);
      break;
    case 'D':
      debugk = strtol(optarg,NULL,0);
      debugk_flg=1;
      break;
    case 'q':
      query = strtol(optarg,NULL,0);
      query_flg=1;
      break;
    case 'i':
      drv_info_flg=1;
      break;
    case 'V':
      fputs("uLan sendhex v1.1\n", stdout);
      exit(0);
    case 'h':
    default:
      usage();
      exit(opt == 'h' ? 0 : 1);
    }

  if ((optind >= argc)&&!go_flg&&!reset_flg&&!prt_modules&&
      !debugk_flg&&!erase_flg&&!boot_flg&&!query_flg&&!drv_info_flg) 
  { usage();
    exit(1);
  }
  
  if(debugk_flg) debug_kernel(debugk);

  if(query_flg) queryparam(query);

  if(drv_info_flg) drv_info();

  if(prt_modules) print_modules(prt_modules);

  if(reset_flg) {
    if(send_cmd_res()<0) exit(2);
  }

  if(boot_flg) {
    if(activate_boot()<0) exit(2);
  }

  if(fill_flg) {
    mem_fill(mem_type, mem_start, mem_length?mem_length:fill_pat_len, fill_pat_val, fill_pat_len);
  }

  if(erase_flg) {
    mem_erase(mem_type, mem_start, mem_length);
  }

  if(!upload_flg)
  { while (optind < argc)
      if(download_file(argv[optind++], file_format)<0) exit(2);
  }else{
    if(optind+1!=argc) 
    { printf("upload_file : needs exactly one filename\n");
      exit(1);
    };
    if(upload_file(argv[optind], file_format)<0) exit(2);
  };
  
  if(go_flg)
  { if(send_cmd_go(go_addr)<0) exit(2);;
  };

  return 0;
}
