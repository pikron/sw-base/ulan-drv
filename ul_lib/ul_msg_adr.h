/*******************************************************************
  uLan Communication - low level C and driver interface library

  ul_msg_adr.h	- address classification and serialization

  (C) Copyright 2001 by Pavel Pisa - Originator

  (C) Copyright 2002-2014 by Pavel Pisa - Originator

  The uLan C interface library can be used, copied and modified
  under next licenses
    - GPL - GNU General Public License
    - LGPL - GNU Lesser General Public License
    - MPL - Mozilla Public License
    - and other licenses added by project originators
  Code can be modified and re-distributed under any combination
  of the above listed licenses. If contributor does not agree with
  some of the licenses, he/she can delete appropriate line.
  Warning, if you delete all lines, you are not allowed to
  distribute source code and/or binaries utilizing code.
  
  See files COPYING and README for details.

 *******************************************************************/

#ifndef _UL_MSG_ADR_H
#define _UL_MSG_ADR_H

#include <ul_lib/ulan.h>

#ifdef __cplusplus
extern "C" {
#endif

ul_msg_adr_t ul_msg_adr_short_local(ul_msg_adr_t madr, ul_msg_adr_t net_base);
int ul_msg_adr_log2len(ul_msg_adr_t madr);
uchar *ul_msg_adr2buff(uchar *ptr, ul_msg_adr_t madr, int madr_log2len);
uchar *ul_msg_buff2adr(uchar *ptr, ul_msg_adr_t *madr_p, int madr_log2len);
int ul_msg_ext_header2len(uchar ext_header);
int ul_msg_ext_header2dadr_log2len(uchar ext_header);
int ul_msg_ext_header2sadr_log2len(uchar ext_header);
uchar ul_msg_adr_log2len_to_ext_header(int dadr_lg2, int sadr_lg2l);

#ifdef __cplusplus
} /* extern "C"*/
#endif

#endif /* _UL_MSG_ADR_H */
