/*******************************************************************
  uLan Communication - low level C and driver interface library

  ul_l_msg.c	- simple message operations

  (C) Copyright 1996-2016 by Pavel Pisa - project originator
        http://cmp.felk.cvut.cz/~pisa
  (C) Copyright 1996-2016 PiKRON Ltd.
        http://www.pikron.com
  (C) Copyright 2002-2016 Petr Smolik


  The uLan driver project can be used and distributed
  in compliance with any of next licenses
   - GPL - GNU Public License
     See file COPYING for details.
   - LGPL - Lesser GNU Public License
   - MPL - Mozilla Public License
   - and other licenses added by project originator

  Code can be modified and re-distributed under any combination
  of the above listed licenses. If contributor does not agree with
  some of the licenses, he/she can delete appropriate line.
  WARNING: if you delete all lines, you are not allowed to
  distribute code or sources in any form.
 *******************************************************************/

//#ifndef _MSC_VER
//#include <unistd.h>
//#endif /*_MSC_VER*/
#include <stdlib.h>
//#include <fcntl.h>
//#include <stdio.h>
#include <sys/types.h>
//#include <sys/stat.h>
#include <string.h>
#include <ul_lib/ulan.h>

/* simple message operations */

int ul_send_command(ul_fd_t ul_fd,int dadr,int cmd,int flg,
                    const void *buf,int len)
{ int ret;
  ul_msginfo msginfo;
  memset(&msginfo,0,sizeof(msginfo));
  msginfo.dadr=dadr;
  msginfo.cmd=cmd;
  msginfo.flg=UL_BFL_M2IN|flg;
  ret=ul_newmsg(ul_fd,&msginfo);
  if(ret<0) return ret;
  if(len)if(ul_write(ul_fd,buf,len)!=len)
  { ul_abortmsg(ul_fd);
    return -1;
  };
  return ul_freemsg(ul_fd);
};

int ul_send_command_wait(ul_fd_t ul_fd,int dadr,int cmd,int flg,
                         const void *buf,int len)
{ int stamp;
  int ret;
  ul_msginfo msginfo;
  stamp=ul_send_command(ul_fd,dadr,cmd,flg,buf,len);
  if(stamp<0) return stamp;
  while(1) 
  { ret=ul_fd_wait(ul_fd,10);
    if(ret<=0) return ret?ret:-1;
    ret=ul_acceptmsg(ul_fd,&msginfo);
    if(ret<0) return ret;
    ul_freemsg(ul_fd);
    if(msginfo.stamp==stamp)
    { if(msginfo.flg&UL_BFL_FAIL) return -2;
      else return 1;
    };
  };
};

int ul_send_query(ul_fd_t ul_fd,int dadr,int cmd,int flg,
                  const void *buf,int len)
{ int ret;
  ul_msginfo msginfo;
  memset(&msginfo,0,sizeof(msginfo));
  msginfo.dadr=dadr;
  msginfo.cmd=cmd;
  msginfo.flg=UL_BFL_M2IN|flg;
  ret=ul_newmsg(ul_fd,&msginfo);
  if(ret<0) return ret;
  if(len)if(ul_write(ul_fd,buf,len)!=len)
  { ul_abortmsg(ul_fd);
    return -1;
  };
  memset(&msginfo,0,sizeof(msginfo));
  msginfo.flg=UL_BFL_REC|UL_BFL_M2IN;
  if(ul_tailmsg(ul_fd,&msginfo)<0)
  { ul_abortmsg(ul_fd);
    return -1;
  };
  return ul_freemsg(ul_fd);
};

int ul_send_query_wait(ul_fd_t ul_fd,int dadr,int cmd,int flg,
		    const void *bufin,int lenin,void **bufout,int *lenout)
{ int stamp;
  int ret;
  int len;
  ul_msginfo msginfo;
  stamp=ul_send_query(ul_fd,dadr,cmd,flg,bufin,lenin);
  if(stamp<0) return stamp;
  while(1) 
  { ret=ul_fd_wait(ul_fd,10);
    if(ret<=0) return ret?ret:-1;
    ret=ul_acceptmsg(ul_fd,&msginfo);
    if(ret<0) return ret;
    if(msginfo.stamp==stamp)
    { if(msginfo.flg&UL_BFL_FAIL)
        {ul_freemsg(ul_fd); return -2;};
      ret=ul_actailmsg(ul_fd,&msginfo);
      if(ret<0) {ul_freemsg(ul_fd); return ret;};
      if(bufout&&lenout)
      { len=msginfo.len;
        if(!*bufout) *bufout=malloc(len);
        else if(*lenout<len) len=*lenout;
        if(ul_read(ul_fd,*bufout,len)!=len)
          {ul_freemsg(ul_fd); return -3;};
        *lenout=len;
      };
      ul_freemsg(ul_fd);
      return msginfo.len;
    };
    ul_freemsg(ul_fd);
  };
};

