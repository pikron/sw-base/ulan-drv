#ifndef _UL_WDMBASE_H
#define _UL_WDMBASE_H

#define uLan_DbgPrint DbgPrint

//
// Defaults
//

#define DEF_PORT_ADDRESS    0x2F8
#define DEF_PORT_RANGE      0x08
#define DEF_IRQ_LINE        0x03
#define DEF_BAUD_RATE       19200
#define DEF_BUFFER_SIZE     2048
#define DEF_GW_ADDR         1
#define DEF_MY_ADDR         2


//-------------------------------------------------------------------
//
// Device extension 
// 

typedef struct ul_drv ULAN_DEVICE_EXTENSION,
                      *PULAN_DEVICE_EXTENSION;

//-------------------------------------------------------------------
//
// Define the driver names
// 
#define NT_DEVICE_NAME	    L"\\Device\\ul_drv"
#define DOS_DEVICE_NAME     L"\\DosDevices\\ul_drv"


NTSTATUS NTAPI DispatchRoutine (IN PDEVICE_OBJECT DeviceObject, IN PIRP Irp);


VOID NTAPI ulan_bottom_dpc(IN PKDPC Dpc,IN PVOID contex,
		     IN PVOID arg1,IN PVOID arg2);


#endif /*_UL_WDMBASE_H*/
