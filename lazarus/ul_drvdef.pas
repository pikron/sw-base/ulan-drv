unit ul_drvdef;
interface

{$DEFINE UL_INDIR}

{$IFDEF FPC}
{$PACKRECORDS C}
{$ENDIF}

//uses
//  Windows;


  const
     UL_BFL_LOCK = $8000;
  { Message must be received by some proccess  }
     UL_BFL_MSST = $4000;
  { After succesfull proccessing move to proc_bll  }
     UL_BFL_M2IN = $2000;
  { Length of received frame must match expected len  }
     UL_BFL_LNMM = $1000;
  { Message cannot be proccessed - error  }
     UL_BFL_FAIL = $0800;
  { Multiframe message continues by next bll block  }
     UL_BFL_TAIL = $0400;
  { Send this frame  }
     UL_BFL_SND = $0200;
  { Receive answer frame into this bll block  }
     UL_BFL_REC = $0100;
  { Verify free space in buffer of destination station  }
     UL_BFL_VERL = $0040;
  { Do not try to repeat if error occurs  }
     UL_BFL_NORE = $0020;
  { If error occurs do wait with retry  }
     UL_BFL_REWA = $0010;
  { Request imediate proccessing of frame by receiver station  }
     UL_BFL_PRQ = $0002;
  { Request imediate acknowledge by receiving station  }
     UL_BFL_ARQ = $0001;

  type
     {$IFDEF UL_INDIR}
       ul_fd_t = Pointer;
     {$ELSE}
       ul_fd_t = THandle;
     {$ENDIF}

     ul_msginfo = record
          dadr : longint;
          sadr : longint;
          cmd : longint;
          flg : longint;
          len : longint;
          stamp : longword;
       end;

  const
    {$IFDEF UL_INDIR}
       UL_FD_INVALID = nil;
    {$ELSE}
       UL_FD_INVALID = INVALID_HANDLE_VALUE;
    {$ENDIF}


    {$IF Defined(WINDOWS)}
       UL_DEV_NAME = '\\.\UL_DRV';
    {$ELSEIF Defined(UNIX)}
       UL_DEV_NAME = '/dev/ulan';
    {$ELSE}
      {$MESSAGE ERROR 'invalid OS'}
    {$ENDIF}

  {***************************************************************** }
  { command definitions  }
  { standard command codes
     00H .. 3FH    store to buffer
     40H .. 7FH    store to buffer without ACK
     80H .. 9FH    proccess at onece
     A0H .. BFH    process with additional receive
     C0H .. FFH    process with additional send
   }

  { Reinitialize RS485  }

  const
     UL_CMD_RES = $80;
  { Test free space in input buffer  }
     UL_CMD_SFT = $81;
  { Send identification  }
     UL_CMD_SID = $F0;
  { Send amount of free space in IB  }
     UL_CMD_SFI = $F1;
  { End of stepping  }
     UL_CMD_TF0 = $98;
  { Begin of stepping  }
     UL_CMD_TF1 = $99;
  { Do step  }
     UL_CMD_STP = $9A;
  { Additional debug commands  }
     UL_CMD_DEB = $9B;
  { Send state - for 8051 PCL PCH PSW ACC  }
     UL_CMD_SPC = $DA;
  { Read memory   T T B B L L  }
     UL_CMD_RDM = $F8;
  { Write mamory  T T B B L L  }
     UL_CMD_WRM = $B8;
  { Erase memory T T B B L L  }
     UL_CMD_ERM = $88;

implementation
end.
