/*******************************************************************
  uLan Communication - uL_DRV - multiplatform uLan driver

  ul_drv.h	- driver definitions and types

  (C) Copyright 1996-2004 by Pavel Pisa - project originator
        http://cmp.felk.cvut.cz/~pisa
  (C) Copyright 1996-2004 PiKRON Ltd.
        http://www.pikron.com
  (C) Copyright 2002-2004 Petr Smolik
  

  The uLan driver project can be used and distributed 
  in compliance with any of next licenses
   - GPL - GNU Public License
     See file COPYING for details.
   - LGPL - Lesser GNU Public License
   - MPL - Mozilla Public License
   - and other licenses added by project originator

  Code can be modified and re-distributed under any combination
  of the above listed licenses. If contributor does not agree with
  some of the licenses, he/she can delete appropriate line.
  WARNING: if you delete all lines, you are not allowed to
  distribute code or sources in any form.
 *******************************************************************/

#ifndef _UL_DRV_INIT_H
#define _UL_DRV_INIT_H

struct ul_drv;

int ul_drv_add_dev(struct ul_drv *udrv);
struct ul_drv *ul_drv_new(int port, int irq, int baud, int my_adr, const char *chip_name, long baudbase);

#endif /* _UL_DRV_INIT_H */
