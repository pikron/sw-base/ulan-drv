/*******************************************************************
  uLan Communication - uL_DRV - multiplatform uLan driver

  ul_drv_iac.h	- driver definitions and types

  (C) Copyright 1996-2004 by Pavel Pisa - project originator
        http://cmp.felk.cvut.cz/~pisa
  (C) Copyright 1996-2004 PiKRON Ltd.
        http://www.pikron.com
  (C) Copyright 2002-2004 Petr Smolik
  

  The uLan driver project can be used and distributed 
  in compliance with any of next licenses
   - GPL - GNU Public License
     See file COPYING for details.
   - LGPL - Lesser GNU Public License
   - MPL - Mozilla Public License
   - and other licenses added by project originator

  Code can be modified and re-distributed under any combination
  of the above listed licenses. If contributor does not agree with
  some of the licenses, he/she can delete appropriate line.
  WARNING: if you delete all lines, you are not allowed to
  distribute code or sources in any form.
 *******************************************************************/

#ifndef _UL_DRV_IAC_H
#define _UL_DRV_IAC_H

struct ul_drv;
struct ul_opdata;

#define UL_IAC_RC_ERR		-1
#define UL_IAC_RC_PROC		0
#define UL_IAC_RC_FREEMSG	1

#define UL_IAC_OP_CALLBACK	0
#define UL_IAC_OP_SNDBUFF	1
#define UL_IAC_OP_SND		2
#define UL_IAC_OP_REC		3

#define UL_IAC_BFL_FREEBUFF	0x01
#define UL_IAC_BFL_CB_OFFLT	0x02 /* before calling the IAP CB routine switch off line transmitter */

typedef struct ul_iac_data {
	char    *buff;		/* buffer */
	unsigned ptr;      	/* buffer ptr */
	unsigned len;		/* len */
	void    *ctx;           /* context */
} ul_iac_data; 

typedef int(ul_iac_call_fnc)(struct ul_drv *udrv,ul_msginfo *msginfo,char *idata,ul_iac_data *iac_data);

int ul_drv_add_iac(struct ul_drv *udrv,int cmd,int op,ul_iac_call_fnc *fnc,char *buff, unsigned len,unsigned flg,void *ctx,int subdevidx);
int ul_drv_del_iac(struct ul_drv *udrv,int cmd,int subdevidx);

int ul_opdata_add_iac(struct ul_opdata *opdata,int cmd,int op,ul_iac_call_fnc *fnc,char *buff, unsigned len,unsigned flg,void *ctx);
int ul_opdata_del_iac(struct ul_opdata *opdata,int cmd);

#endif /* _UL_DRV_INIT_H */
