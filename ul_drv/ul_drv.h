/*******************************************************************
  uLan Communication - uL_DRV - multiplatform uLan driver

  ul_drv.h	- driver definitions and types

  (C) Copyright 1996-2004 by Pavel Pisa - project originator
        http://cmp.felk.cvut.cz/~pisa
  (C) Copyright 1996-2004 PiKRON Ltd.
        http://www.pikron.com
  (C) Copyright 2002-2004 Petr Smolik


  The uLan driver project can be used and distributed
  in compliance with any of next licenses
   - GPL - GNU Public License
     See file COPYING for details.
   - LGPL - Lesser GNU Public License
   - MPL - Mozilla Public License
   - and other licenses added by project originator

  Code can be modified and re-distributed under any combination
  of the above listed licenses. If contributor does not agree with
  some of the licenses, he/she can delete appropriate line.
  WARNING: if you delete all lines, you are not allowed to
  distribute code or sources in any form.
 *******************************************************************/

#ifndef _LINUX_UL_DRV_H
#define _LINUX_UL_DRV_H

#define UL_DRV_VERSION "1.1.2"
#define UL_DRV_VERCODE 0x010102

#if !defined(_WIN32)||defined(__GNUC__)
  #ifndef INLINE
    #define INLINE static inline
  #endif
#else
  #ifndef INLINE
    #define INLINE _inline
  #endif
#endif

#ifdef KERNEL
  #define UL_PHYSADDR_PIO_MASK 0xffff
  typedef struct { phys_addr_t physaddr; } ul_physaddr_t;
  typedef struct { void __iomem *ioaddr; } ul_ioaddr_t;
#elif defined(_WIN32)
  #define UL_PHYSADDR_PIO_MASK 0xffff
  typedef struct { PHYSICAL_ADDRESS physaddr; } ul_physaddr_t;
  typedef struct { PVOID ioaddr; } ul_ioaddr_t;
#else
  #define UL_PHYSADDR_PIO_MASK 0xffff
  typedef struct { unsigned long physaddr; } ul_physaddr_t;
  typedef struct { void *ioaddr; } ul_ioaddr_t;
#endif

int ul_io_map(ul_physaddr_t physaddr, unsigned long len, const char *name, ul_ioaddr_t *ioaddr);
void ul_io_unmap(ul_physaddr_t physaddr, unsigned long len, ul_ioaddr_t ioaddr);

INLINE ul_ioaddr_t ul_ioaddr_add(ul_ioaddr_t addr, int offs)
{
  addr.ioaddr=(((unsigned char *)(addr.ioaddr)) + (offs));
  return addr;
}

INLINE unsigned long ul_physaddr2uint(ul_physaddr_t addr)
{
#ifndef _WIN32
  return addr.physaddr;
#else
  return (unsigned long)addr.physaddr.QuadPart;
#endif
}

INLINE ul_physaddr_t ul_uint2physaddr(unsigned long addr)
{
  ul_physaddr_t physaddr;
#ifndef _WIN32
  physaddr.physaddr=addr;
#else
  physaddr.physaddr.QuadPart=(LONGLONG)addr;
#endif
  return physaddr;
}

#ifndef uchar
 #define uchar unsigned char
#endif

#ifndef UL_WITH_MULTI_NET
 typedef uchar ul_nadr_t;
#else
 typedef int ul_nadr_t;
#endif

/* Transfer controll byte with B8=1 */

#define	UL_ERR	0x17F
#define	UL_ARQ	0x17A
#define	UL_PRQ	0x179
#define	UL_AAP	0x176
#define	UL_BEG	0x175
#define	UL_END	0x17C

/* Transfer acknowledge byte with B8=0 */

#define	UL_ACK	0x19
#define	UL_NAK	0x7F
#define	UL_WAK	0x25

/* Special commands defined on protocol level */

#define UL_CMD_EADR	0x82	/* Extended addresses follows LN ED.. ES.. CMD*/

/* Memory structures */

#define UL_BLK_SIZE 64
#define UL_BLK_FSIZE (UL_BLK_SIZE-sizeof(ul_mess_head))
#define UL_BLK_FDATA(mem_blk) (mem_blk->blk.head.fdata)
#define UL_BLK_NDATA(mem_blk) (mem_blk->blk.ndata)
#define UL_BLK_HEAD(mem_blk) (mem_blk->blk.head)

/* ul_mess_head flags definition defines proccessing
   of message or its frames stored in bll */
#define UL_BFL_PMES 0x10000	/* promiscuous message */
#define UL_BFL_LOCK 0x8000	/* locked message is pointed only once */
#define UL_BFL_MSST 0x4000      /* Message must be received by some proccess */
#define UL_BFL_M2IN 0x2000      /* After succesfull proccessing move to proc_bll */
#define UL_BFL_LNMM 0x1000      /* Length of received frame must match expected len */
#define UL_BFL_FAIL 0x0800      /* Message cannot be proccessed - error */
#define UL_BFL_TAIL 0x0400      /* Multiframe message continues by next bll block */
#define UL_BFL_SND  0x0200      /* Send this frame */
#define UL_BFL_REC  0x0100      /* Receive answer frame into this bll block */
#define UL_BFL_SANL 0x0080      /* Source address not local */
#define UL_BFL_VERL 0x0040      /* Verify free space in buffer of destination station */
#define UL_BFL_NORE 0x0020      /* Do not try to repeat if error occurs */
#define UL_BFL_REWA 0x0010      /* If error occurs do wait with retry */
#define UL_BFL_PRQ  0x0002      /* Request imediate proccessing of frame by receiver station */
#define UL_BFL_ARQ  0x0001      /* Request imediate acknowledge by receiving station */

/* error handling */
#define UL_RETRY_CNT 4		/* number of retries of messages */

typedef struct ul_mess_head {	/* head of frame */
	struct ul_mem_blk *next;/* BLL of frames */
	struct ul_mem_blk *prev;
	struct ul_blk_bll *bll;	/* BLL in which is head */
	int	 ref_cnt;	/* usage count */
	unsigned flg;		/* frame flags */
	unsigned len;		/* length of message in bytes */
	ul_nadr_t dadr;		/* destination address */
	ul_nadr_t sadr;		/* source address */
    #ifdef UL_WITH_MULTI_NET
	uchar    sadr_local;	/* local/routing node source address */
	uchar    hops;		/* number of hops the packet already draversed */
    #endif /**/
	uchar    cmd;		/* command or service */
	uchar    retry_cnt;	/* actual number of retries */
	unsigned stamp;		/* unigue message number */
	uchar    fdata[0]; 	/* data in first block */
} ul_mess_head;

typedef struct ul_mem_blk {
	struct ul_mem_blk *next; /* next relating memory block */
	union {	ul_mess_head head;
		uchar	ndata[UL_BLK_SIZE];
	} blk;
} ul_mem_blk;

typedef struct ul_blk_bll {	/* bidir linked list of frames */
	ul_mem_blk *first;
	ul_mem_blk *last;
	int cnt;
	struct ul_drv *udrv;	/* owener of bll list */
} ul_blk_bll;

/* Data iterator structure */

typedef struct ul_data_it {
	ul_mem_blk *blk;	/* actual proccessed memory block */
	uchar	*ptr;		/* pointer to actual data byte */
	int	pos;		/* actual position for seek operations */
	ul_mem_blk *head_blk;	/* first memory block of message */
	int	trans_len;	/* for send/rec routines */
} ul_data_it;

INLINE void ul_di_init(ul_data_it *di,ul_mem_blk *head_blk)
{
  di->blk=di->head_blk=head_blk;
  di->pos=0;
  di->ptr=UL_BLK_HEAD(head_blk).fdata;
};

INLINE void ul_di_inc(ul_data_it *di)
{
  di->ptr++;
  di->pos++;
};

INLINE void ul_di_add(ul_data_it *di,int len)
{
  di->ptr+=len;
  di->pos+=len;
};

INLINE int ul_di_adjust(ul_data_it *di)
{
  int offs;
  while((offs=(int)(di->ptr-UL_BLK_NDATA(di->blk)-UL_BLK_SIZE))>=0)
  {
    if(!di->blk->next) return 0;
    di->blk=di->blk->next;
    di->ptr=UL_BLK_NDATA(di->blk)+offs;
  };
  return 1;
};

INLINE uchar *ul_di_byte(ul_data_it *di)
{
  return di->ptr;
};

INLINE int ul_di_atonce(ul_data_it *di)
{
  return (int)(UL_BLK_SIZE-(di->ptr-UL_BLK_NDATA(di->blk)));
};

/* Promiscuous and router mode control */

#define UL_PRO_MODE_CAPTURE_MASK   0x00ff
#define UL_PRO_MODE_ROUTER_MASK    0xff00

/* Routing */
#define UL_ROUTE_OP_SET         0
#define UL_ROUTE_OP_GET         1

/* uLan frame address checking functions */

#define UL_ADR_LOCAL_MAX      100
#define UL_ADR_NET_MASK       (~0xffUL)

#define UL_ADR_MATCH_NONE	0
#define UL_ADR_MATCH_MINE	1
#define UL_ADR_MATCH_BROADCAST	2
#define Ul_ADR_MATCH_ROUTER	3 /* used to mark frame during frame processing*/
#define Ul_ADR_MATCH_PROMISUOUS	4

static int uld_adr_match(struct ul_drv *udrv, ul_nadr_t adr);

static int uld_get_arb_addr(struct ul_drv *udrv);

/* driver generic functions return codes */

int ul_drv_stop(struct ul_drv *udrv);
int ul_drv_start(struct ul_drv *udrv,int options);

/* driver subfunctions return codes */

#define UL_RC_WIRQ	0	/* process fnc_act at next IRQ */
#define UL_RC_PROC	1	/* imediately proccess fnc_act */
#define UL_RC_ACTIVATE	2	/* timeout or stroke activation */
#define UL_RC_FREEMSG   3       /* free message after receive processing */
#define UL_RC_CHIPTIMER 4       /* chip timing support timer expired */
#define UL_RC_EFRAME	-1	/* received char frame error */
#define UL_RC_ETIMEOUT	-2	/* wait for char timeout */
#define UL_RC_EBADCHR	-3	/* bad character received */
#define UL_RC_EARBIT	-4	/* connect arbit. proc. lose */
#define UL_RC_ENOFNC	-5	/* bad function code */
#define UL_RC_EPORT	-6	/* port access error */
#define UL_RC_ENAK	-7	/* slave sends NAK */
#define UL_RC_EWAK	-8	/* slave bussy now and sends WAK */
#define UL_RC_EREPLY	-9	/* bad reply from slave */
#define UL_RC_ERECBEG	-10	/* bad char received */
#define UL_RC_ERECADR	-11	/* bad address in imediate reply */
#define UL_RC_ERECEND	-12	/* bad char or xorsum received */
#define UL_RC_ERECPROT	-13	/* nonrecoverable protocol error */
#define UL_RC_ENOMEM	-14	/* no memory for data */
#define UL_RC_ENOTAIL	-15	/* no frame tail when flag set */
#define UL_RC_EPRMESS	-16	/* message processor error */
#define UL_RC_EKWT	-17	/* error related to kernel worker thread */
#define UL_RC_EBAUD	-18	/* error baudrate value */
#define UL_RC_EUSB	-19	/* error related to usb */
#define UL_RC_ENOREGIAC	-20	/* no immediate action for given command */
#define UL_RC_EBADIRQ	-21	/* error with IRQ */
#define UL_RC_EQPNOSER  -22     /* queryparam no service */

/* uLan driver flags stored in ul_drv.flags */

#ifndef _WIN32
#define UL_DFL_CHIPOK	  0x10	/* chip driver initialized OK	*/
#define UL_DFLB_CHIPOK	    4	/* 		bit num.	*/
#define UL_DFL_IN_ISR 	  0x20	/* some CPU is running in ISR	*/
#define UL_DFLB_IN_ISR	    5	/* 		bit num.	*/
#define UL_DFL_ASK_ISR	  0x40	/* some CPU likes but can't get ISR */
#define UL_DFLB_ASK_ISR	    6	/* 		bit num.	*/
#define UL_DFL_NACTIV     0x80	/* no activity of driver - timeout ? */
#define UL_DFLB_NACTIV	    7	/* 		bit num.	*/
#define UL_DFL_IN_BOTTOM  0x100	/* some CPU is running in ulan_do_bh */
#define UL_DFLB_IN_BOTTOM   8	/* 		bit num.	*/
#define UL_DFL_ASK_BOTTOM 0x200	/* request for ulan_do_bh       */
#define UL_DFLB_ASK_BOTTOM  9	/* 		bit num.	*/
#define UL_DFL_CHECK_FILT 0x400	/* check filters for UL_OPST_FILTNEW */
#define UL_DFLB_CHECK_FILT  10	/* 		bit num.	*/
#define UL_DFL_WDSCHED    0x800	/* SCHEDULE_UDRV_WDTIM expires  */
#define UL_DFLB_WDSCHED     11 	/* 		bit num.	*/
#define UL_DFL_KWTRUN    0x1000	/* kernel worker thread running */
#define UL_DFLB_KWTRUN      12 	/* 		bit num.	*/
#define UL_DFL_KWTKILL   0x2000	/* stop kernel worker thread    */
#define UL_DFLB_KWTKILL     13 	/* 		bit num.	*/
#define UL_DFL_WDFORCED  0x4000	/* forced ativation of timeout  */
#define UL_DFLB_WDFORCED    14 	/* 		bit num.	*/
#define UL_DFL_KWTACTIVE 0x8000	/* active kernel worker thread  */
#define UL_DFLB_KWTACTIVE   15 	/* 		bit num.	*/
#ifdef UL_WITH_CHIP_TIMER
#define UL_DFL_CHIPTIMER 0x10000 /* chip timming event pending  */
#define UL_DFLB_CHIPTIMER   16 	/*              bit num.        */
#define UL_DFL_IN_CHIPTIMER 0x20000 /* running in chip timming event handler  */
#define UL_DFLB_IN_CHIPTIMER 17	/*              bit num.        */
#endif /*UL_WITH_CHIP_TIMER*/
#endif  /* _WIN32 */

/* chip start status */
#define UL_CSS_PINIT        1
#define UL_CSS_RQIRQ        2

/* uLan queryparam services */
#define UL_QP_MYADR         0
#define UL_QP_BAUDRATE      1
#define UL_QP_PROMODE       2
#define UL_QP_SUBDEVNUM_MAX 3
#define UL_QP_MY_NET_BASE   4
#define UL_QP_DEF_GW_ADR    5
#define UL_QP_FULL_NET_ADR  6

/* Driver controll structure */

#define UL_DRV_MAGIC 0x754c1122

#ifdef CONFIG_OC_UL_DRV_WITH_MULTI_DEV
#define UL_SUBDEVNUM_MAX 4
#else /*CONFIG_OC_UL_DRV_WITH_MULTI_DEV*/
#define UL_SUBDEVNUM_MAX 1
#endif /*CONFIG_OC_UL_DRV_WITH_MULTI_DEV*/

typedef int(ul_call_fnc)(struct ul_drv *udrv, int ret_code);
struct ul_opdata;
struct ul_bottom;
struct ul_iac_chain;
struct ul_phys_dev_ptr;

typedef struct ul_drv_chip_ops {
	const char *chip_type;	/*text type identification*/
	ul_call_fnc *fnc_recch;	/* character receive function */
	ul_call_fnc *fnc_sndch;	/* character send function */
	ul_call_fnc *fnc_wait;	/* waits for time or receive */
	ul_call_fnc *fnc_connect;/* connect to line */
	ul_call_fnc *fnc_finishtx;/* connect to line */
	int (*fnc_pool)(struct ul_drv *udrv);/* test interrupt flags state */
	int (*fnc_cctrl)(struct ul_drv *udrv,int ctrl_fnc,int param);/* chip drv ctrl */
	int (*fnc_stroke)(struct ul_drv *udrv);/* stroke for nonstandard targets */
	/* Chip driver ctrl functions */
	int (*fnc_txoe)(struct ul_drv *udrv, int enable); /* switch on/off line transmitter */
	int (*fnc_pinit)(struct ul_drv *udrv); /* initialize ports */
	int (*fnc_pdone)(struct ul_drv *udrv); /* deinitialize ports */
	void (*fnc_activate)(struct ul_drv *udrv); /* activate */
	int (*fnc_genirq)(struct ul_drv *udrv, int event); /* generate irq for irq_probe */
	int (*fnc_rqirq)(struct ul_drv *udrv); /* request IRQ handling */
	int (*fnc_freeirq)(struct ul_drv *udrv); /* release IRQ handling */
	int (*fnc_hwtest)(struct ul_drv *udrv, int param); /* hardware testings */
	int (*fnc_setmyadr)(struct ul_drv *udrv, int subdevidx, int newadr); /* station address changed */
	int (*fnc_setpromode)(struct ul_drv *udrv, int pro_mode); /* promode changed */
} ul_drv_chip_ops;

int ul_drv_common_fnc_rqirq_noprobe(struct ul_drv *udrv);
int ul_drv_common_fnc_rqirq_probe(struct ul_drv *udrv);
int ul_drv_common_fnc_freeirq(struct ul_drv *udrv);
int ul_drv_common_fnc_setmyadr(struct ul_drv *udrv, int subdevidx, int newadr);
int ul_drv_common_fnc_setpromode(struct ul_drv *udrv, int pro_mode);

typedef struct ul_drv {
	int	magic;		/* magic number */
	/*** parameters used by chip driver ***/
  #ifndef _WIN32
	volatile unsigned long flags;	/* flags */
  #else /* _WIN32 */
	PDEVICE_OBJECT	DeviceObject; /* Functional Device Object for WDM  */
	PKINTERRUPT	InterruptObject;
	KIRQL		Irql;
	KAFFINITY       InterruptAffinity;
	ULONG		InterruptMode;
        BOOLEAN         InterruptShare;
	LONG	flag_IN_ISR;	/* some CPU is running in ISR	*/
	LONG	flag_NACTIV;	/* no activity of driver - timeout ? */
	volatile LONG flag_ASK_ISR; /* some CPU likes but can't get ISR */
	volatile LONG flag_CHIPOK; /* chip driver initialized OK	*/
  #endif /* _WIN32 */
	ul_physaddr_t physbase;	/* IO base port/physical address */
	ul_ioaddr_t   iobase;	/* IO base port/virtual address */
	int	irq;		/* irq number */
	int	baud_div;	/* used baud divisor */
	int	baud_val;	/* selected speed */
	long	baud_base;	/* XTAL base clocks */
	int	chip_options;	/* additional chip minor diferences */
	void	*chip_info;	/* additional info for chip */
	/*** function sequencer ***/
	ul_call_fnc *fnc_act;	/* actual state call function */
	ul_call_fnc *fnc_stack[10]; /* stack of interupted calls */
	ul_call_fnc **fnc_sp;	/* pointer into stack */
	const ul_drv_chip_ops *chip_ops; /* pointer to chip specific operations*/
	unsigned chip_start_status;
	unsigned char_buff;	/* rec/snd char buffer */
	unsigned wait_time;	/* time limit for fnc_wait */
	unsigned xor_sum;	/* xor+1 checksum */
	unsigned last_ctrl;	/* last snd/rec controll char */
	unsigned chip_temp;	/* temporary variable for chip driver */
	unsigned chip_buff[6];	/* buffer for chip driver */
	/*** message and frame proccessor ***/
	int (*fnc_timeout)(struct ul_drv *udrv);/* timeout or error function */
	ul_mem_blk *con_message;/* proccessed message chain */
	ul_mem_blk *con_frame;	/* actual proccessed frame */
	int	   pro_mode;	/* promiscuous mode */
	uchar      my_adr_arr[UL_SUBDEVNUM_MAX];	/* addreses of this uLan interface */
    #ifdef UL_WITH_MULTI_NET
	ul_nadr_t  my_net_base;	/* local network base address */
	uchar      gw_adr;	/* local address of gateway to other networks */
	uchar      con_ext_data[9]; /* buffer to collect extended addresses  */
	uchar      con_ext_header;  /* header of extended adressing etc. */
	uchar      con_ext_len;	/* length of extended adresses data */
	uchar      con_hops;	/* number of the routers hops packet has already traversed */
	uchar      con_pos;	/* position in data extension during processing */
	uchar      con_sadr_local; /* local frame source address */
    #endif /*UL_WITH_MULTI_NET*/
	ul_nadr_t  con_dadr;	/* connection destination address */
	ul_nadr_t  con_sadr;	/* connection sender address */
	uchar      con_cmd;	/* connection command */
	uchar      con_flg;	/* connection frame flags */
	uchar      con_adr_match; /* found drress match during frame processing */
	ul_data_it con_di;	/* data iterator for transmit/receive */
	/*** memory management ***/
	void	   *mem_ptr;	/* pointer to allocated memory */
	ul_mem_blk *free_blk;	/* single linked list of free blks */
	int     free_blk_cnt;	/* number of block on free list */
	ul_blk_bll prep_bll;	/* list of frames prepared for output */
	ul_blk_bll work_bll;	/* list for receive operation */
	ul_blk_bll proc_bll;	/* list of proccessed frames */
	ul_blk_bll opan_bll;	/* list of announced frames */
	/*** operators - clients ***/
	struct ul_opdata *operators; /* double linked list of clients */
	/*** kernel integration ***/
	struct ul_drv *next_chan; /* used for multichanel PCI and PnP devices */
	/*** immediately action commands ***/
      #ifdef UL_WITH_IAC
	struct ul_iac_chain *iac_chain;
	struct ul_iac_chain *iac_act;
	int 	   iac_ret_code;
      #endif /* UL_WITH_IAC */
  #ifdef KERNEL
	struct kc_tasklet_struct bottom; /* deferring of time consumptive tasks */
	struct timer_list wd_timer; /* link protecting timeout timer */
    #ifdef UL_WITH_CHIP_TIMER
	struct hrtimer chip_timer;  /* timer available for chip level functions */
    #endif /*UL_WITH_CHIP_TIMER*/
    #ifdef UL_WITH_DEVFS
	kc_devfs_handle_t devfs_handle; /* for devfs */
    #endif /* UL_WITH_DEVFS */
	struct ul_phys_dev_ptr *dev;	/* kernel PCI device information */
	wait_queue_head_t kwt_wq; /* wait queue for kernel worker thread */
	struct task_struct *kwt;  /* kernel worker thread handle */
  #elif defined(FOR_NUTTX_KERNEL)
	pid_t bottom_pid;
	sem_t bottom_sem;
	struct wdog_s wd_timer;
  #elif defined(_WIN32)
	LONG flag_IN_BOTTOM;	/* some CPU is running in bottom Dpc	*/
	LONG flag_ASK_BOTTOM;	/* need to run of bottom Dpc */
	LONG flag_CHECK_FILT;	/* check for UL_OPST_FILTNEW */
	LONG flag_KWTKILL;	/* stop kernel worker thread    */
	LONG flag_WDFORCED;	/* forced watchdog activation */
	LONG flag_KWTACTIVE;	/* active kernel worker thread    */
    #ifdef UL_WITH_CHIP_TIMER
	LONG flag_CHIPTIMER;	/* chip timming event pending */
	LONG flag_IN_CHIPTIMER;	/* running in chip timer event handler */
    #endif /*UL_WITH_CHIP_TIMER*/
	KDPC bottom_dpc;
	KDPC wd_timer_dpc;
	KTIMER wd_timer;
	HANDLE 		kwt;  /* thread */
	PETHREAD        kwt_threadobj;  /* */
	KEVENT 		kwt_wake;       /* event for wakeup kwt */
	KEVENT 		kwt_stopped;    /* event for wakeup kwt */
	int             usb_bus;        /* set for usb bus */
    #ifdef FOR_WIN_WDM
	void 		*dev;	/* kernel USB device information */
        UNICODE_STRING	ntdev_name; /* NT Device Name */
        UNICODE_STRING	link_name;  /* DOS Link Name */
	PDEVICE_OBJECT	PhysicalDeviceObject;
	PDEVICE_OBJECT	DeviceToSendIrpsTo;
	ULONG		State;	  /* State for PnP Purposes */
	LONG		OutstandingIO; /* Number of unfinished IRPs */
	KEVENT		RemoveEvent;   /* Set when no PnP IRP pending */
	/*LONG flag_INITIALIZED;*/	/* DriverObject already initialized */
    #endif /* FOR_WIN_WDM */
    #ifdef UL_WITH_WIN_PWR
        DEVICE_CAPABILITIES DeviceCapabilities;
        DEVICE_POWER_STATE CurrentDevicePowerState;
        BOOLEAN         EnabledForWakeup;
	ULONG           PowerDownLevel;
        PIRP            PowerIrp;
        BOOLEAN         SelfPowerIrp;
        BOOLEAN         SystemPowerStatePending;
        KEVENT          SelfRequestedPowerIrpEvent;
    #endif /* UL_WITH_WIN_PWR */
  #else
        long int wd_timer_expires; /* helper variable for odd systems */
  #endif
} ul_drv;

#define UL_FNEXT(FNEXT)	do{udrv->fnc_act=&FNEXT;}while(0)
#define UL_FCALL(FCALL)	do{*(udrv->fnc_sp++)=udrv->fnc_act;\
			   udrv->fnc_act=&FCALL;}while(0)
#define UL_FCALL2(FCALL,FNEXT) \
			do{*(udrv->fnc_sp++)=FNEXT;\
			   udrv->fnc_act=&FCALL;}while(0)
#define UL_FRET		do{udrv->fnc_act=*(--udrv->fnc_sp);}while(0)

#ifdef UL_WITH_MULTI_NET

typedef struct ul_route_range_t {
   int operation;
   int route_flg;
   ul_nadr_t first;
   ul_nadr_t last;
   ul_nadr_t gw;
   int idx;
} ul_route_range_t;

INLINE int ul_nadr_log2len(struct ul_drv *udrv, ul_nadr_t nadr)
{
  if (((unsigned long)nadr<UL_ADR_LOCAL_MAX)||
      (nadr==(UL_BEG&UL_ADR_NET_MASK)))
    return 0;
  if (udrv->my_net_base!=0)
    if ((unsigned long)(nadr^udrv->my_net_base)<UL_ADR_LOCAL_MAX)
      return 0;
  if (!(nadr&~0xffff))
    return 1;
  return 2;
}

INLINE int ul_sadr2sadr_local(struct ul_drv *udrv, ul_nadr_t sadr, int *log2len_p)
{
  int lg2l=ul_nadr_log2len(udrv, sadr);
  *log2len_p=lg2l;
  if (!lg2l)
    return sadr;
  return uld_get_arb_addr(udrv);
}

INLINE int ul_dadr2dadr_local(struct ul_drv *udrv, ul_nadr_t dadr, int *log2len_p)
{
  int lg2l=ul_nadr_log2len(udrv, dadr);
  *log2len_p=lg2l;
  if (!lg2l)
    return dadr;
  return udrv->gw_adr;
}

INLINE uchar *ul_nadr2buff(uchar *ptr, ul_nadr_t nadr, int nadr_log2len)
{
  *(ptr++)=nadr&0xff;
  if(!nadr_log2len--)
    return  ptr;
  *(ptr++)=(nadr>>8)&0xff;
  if(!nadr_log2len--)
    return  ptr;
  *(ptr++)=(nadr>>16)&0xff;
  *(ptr++)=(nadr>>24)&0xff;
  return ptr;
}

INLINE uchar *ul_buff2nadr(uchar *ptr, ul_nadr_t *nadr_p, int nadr_log2len)
{
  ul_nadr_t nadr;
  nadr=*(ptr++);
  do {
    if(!nadr_log2len--)
      break;
    nadr|=*(ptr++)<<8;
    if(!nadr_log2len--)
      break;
    nadr|=*(ptr++)<<16;
    nadr|=*(ptr++)<<24;
  } while(0);
  *nadr_p=nadr;
  return ptr;
}

INLINE int ul_ext_header2len(uchar ext_header)
{
  int dadr_lg2l=ext_header&7;
  int sadr_lg2l=(ext_header>>3)&7;

  if((dadr_lg2l>2)||(sadr_lg2l>2))
    return -1;

  return (dadr_lg2l?(1<<dadr_lg2l):0)+(sadr_lg2l?(1<<sadr_lg2l):0)+1;
}

#endif /*UL_WITH_MULTI_NET*/

#endif /* _LINUX_UL_DRV_H */
