/*******************************************************************
  uLan Communication - low level C and driver interface library

  ul_utsubst.h	- compiler support defines when uLUt is not used

  (C) Copyright 1996-2016 by Pavel Pisa - project originator
        http://cmp.felk.cvut.cz/~pisa
  (C) Copyright 1996-2016 PiKRON Ltd.
        http://www.pikron.com
  (C) Copyright 2002-2016 Petr Smolik


  The uLan driver project can be used and distributed
  in compliance with any of next licenses
   - GPL - GNU Public License
     See file COPYING for details.
   - LGPL - Lesser GNU Public License
   - MPL - Mozilla Public License
   - and other licenses added by project originator

  Code can be modified and re-distributed under any combination
  of the above listed licenses. If contributor does not agree with
  some of the licenses, he/she can delete appropriate line.
  WARNING: if you delete all lines, you are not allowed to
  distribute code or sources in any form.
 *******************************************************************/

#ifndef _UL_UTSUBST_H
#define _UL_UTSUBST_H

#ifndef UL_OFFSETOF
/* offset of structure field */
#define UL_OFFSETOF(_type,_member) \
                ((size_t)&(((_type*)0)->_member))
#endif /*UL_OFFSET*/

#ifndef UL_CONTAINEROF
#ifdef  __GNUC__
#define UL_CONTAINEROF(_ptr, _type, _member) ({ \
        const typeof( ((_type *)0)->_member ) *__mptr = (_ptr); \
        (_type *)( (char *)__mptr - UL_OFFSETOF(_type,_member) );})
#else /*!__GNUC__*/
#define UL_CONTAINEROF(_ptr, _type, _member) \
        ((_type *)( (char *)_ptr - UL_OFFSETOF(_type,_member)))
#endif /*__GNUC__*/
#endif /*UL_CONTAINEROF*/

/* GNUC neat features */

#ifdef	__GNUC__
#ifndef UL_ATTR_UNUSED
#define UL_ATTR_PRINTF( format_idx, arg_idx )	\
  __attribute__((format (printf, format_idx, arg_idx)))
#define UL_ATTR_SCANF( format_idx, arg_idx )	\
  __attribute__((format (scanf, format_idx, arg_idx)))
#define UL_ATTR_FORMAT( arg_idx )		\
  __attribute__((format_arg (arg_idx)))
#define UL_ATTR_NORETURN			\
  __attribute__((noreturn))
#define UL_ATTR_CONST				\
  __attribute__((const))
#define	UL_ATTR_UNUSED				\
  __attribute__((unused))
#define UL_ATTR_CONSTRUCTOR			\
  __attribute__((constructor))
#define	UL_ATTR_DESCRUCTOR			\
  __attribute__((destructor))
#define	UL_ATTR_ALWAYS_INLINE			\
  __attribute__((always_inline))
#define	UL_ATTR_WEAK				\
  __attribute__((weak))
#endif  /*UL_ATTR_UNUSED*/
#else	/* !__GNUC__ */
#ifndef UL_ATTR_UNUSED
#define UL_ATTR_PRINTF( format_idx, arg_idx )
#define UL_ATTR_SCANF( format_idx, arg_idx )
#define UL_ATTR_FORMAT( arg_idx )
#define UL_ATTR_NORETURN
#define UL_ATTR_CONST
#define UL_ATTR_UNUSED
#define	UL_ATTR_CONSTRUCTOR
#define	UL_ATTR_DESCRUCTOR
#define	UL_ATTR_ALWAYS_INLINE
#define UL_ATTR_WEAK
#endif  /*UL_ATTR_UNUSED*/
#endif	/* !__GNUC__ */

#ifndef UL_ATTR_REENTRANT
#if !defined(SDCC) && !defined(__SDCC)
  #define UL_ATTR_REENTRANT
#else
  #define UL_ATTR_REENTRANT __reentrant
#endif
#endif /*UL_ATTR_REENTRANT*/

#endif /*_UL_UTSUBST_H*/
