/*******************************************************************
  uLan Communication - simple test client

  ul_spy.c	- message monitor

  (C) Copyright 1996,1999 by Pavel Pisa 

  The uLan driver is distributed under the Gnu General Public Licence. 
  See file COPYING for details.
 *******************************************************************/

#include <unistd.h>
#include <stdlib.h>
#include <fcntl.h>
#include <stdio.h>
#include <getopt.h>
#include <errno.h>
#include <string.h>
#include <sys/types.h>
#include <sys/time.h>
#include <sys/stat.h>
#include <ul_lib/ulan.h>

/*******************************************************************/

#define WITHOUT_SYS_SELECT

char *ul_dev_name = UL_DEV_NAME;
int filt_module= 0;
int filt_sadr  = 0;
int filt_dadr  = 0;
int filt_cmd   = 0;
int prt_modules= 0;
int debugk     = 0;
int debugk_flg = 0;
int promode    = 0;
int promode_flg= 0;
int new_myadr  = 0;
int new_myadr_flg=0;
int logk_flg = 0;
int all_data   = 0;
int num_messages = 0;
int num_messages_flg = 0;
#ifdef CONFIG_OC_UL_DRV_WITH_MULTI_NET
#define ROUTE_RANGE_ENTRIES 4
ul_route_range_t route_range_table[ROUTE_RANGE_ENTRIES];
int route_range_filled=0;
#endif /*CONFIG_OC_UL_DRV_WITH_MULTI_NET*/

int print_modules(int max_addr)
{ int ret;
  int i;
  ul_fd_t ul_fd;
  uchar *p;
  void *buf=NULL;
  int buf_len;
  int count=0; 
  ul_fd=ul_open(ul_dev_name, NULL);
  if(ul_fd==UL_FD_INVALID)
    { perror("print_modules : uLan open failed");return -1;};
  for(i=1;i<=max_addr;i++)
  {
    ret=ul_send_query_wait(ul_fd,i,UL_CMD_SID,
  			UL_BFL_NORE|UL_BFL_PRQ,NULL,0,&buf,&buf_len);
    if(ret>=0)
    { count++;
      p=(uchar*)buf;
      for(ret=0;(ret<buf_len)&&*p;ret++,p++);
      printf("%2d:  ",i);
      fwrite(buf,ret,1,stdout);
      printf("\n");
    };
    if(buf) free(buf);
    buf=NULL;
  };
  ul_close(ul_fd);
  return count;
};

int debug_kernel(int debug_msk)
{ int ret;
  ul_fd_t ul_fd;
  ul_fd=ul_open(ul_dev_name, NULL);
  if(ul_fd==UL_FD_INVALID)
    { perror("debug_kernel : uLan open failed");return -1;};
  ret=ul_drv_debflg(ul_fd,debug_msk);
  ul_close(ul_fd);
  return ret;
};

int promode_kernel(int mode)
{ int ret;
  ul_fd_t ul_fd;
  ul_fd=ul_open(ul_dev_name, NULL);
  if(ul_fd==UL_FD_INVALID)
    { perror("promode_kernel : uLan open failed");return -1;};
  ret=ul_setpromode(ul_fd,mode);
  ul_close(ul_fd);
  return ret;
};

int log_kernel(void)
{ int ret;
  ul_fd_t ul_fd;
  ul_fd=ul_open(ul_dev_name, NULL);
  if(ul_fd==UL_FD_INVALID)
    { perror("log_kernel : uLan open failed");return -1;};
 #if defined(_WIN32) 
  { DWORD bytes_ret;
    ret=DeviceIoControl(ul_fd2sys_fd(ul_fd),UL_KLOGBLL,
		  NULL,0,NULL,0,
		  &bytes_ret,NULL)?0:-1;
  }
 #elif !defined(UL_DRV_IN_LIB)
  ret=ioctl(ul_fd2sys_fd(ul_fd),UL_KLOGBLL,0);
 #endif
  ul_close(ul_fd);
  return ret;
};

int set_new_myadr(int myadr)
{ int ret;
  ul_fd_t ul_fd;
  ul_fd=ul_open(ul_dev_name, NULL);
  if(ul_fd==UL_FD_INVALID)
    { perror("set_new_myadr : uLan open failed");return -1;};

  ret = ul_setmyadr(ul_fd, myadr);
  if(ret<0)
    { perror("set_new_myadr :set new address failed");};

  ul_close(ul_fd);
  return ret;
}

#ifdef CONFIG_OC_UL_DRV_WITH_MULTI_NET

int set_route_table(ul_route_range_t *rrtable, int ranges)
{
  int idx;
  ul_fd_t ul_fd;
  ul_fd=ul_open(ul_dev_name, NULL);
  if(ul_fd==UL_FD_INVALID)
    { perror("log_kernel : uLan open failed");return -1;};

  for (idx = 0; idx < ranges; idx++, rrtable++) {
    if (ul_route(ul_fd, rrtable) < 0) {
      fprintf(stderr, "set route range index %d error %s\n",
              idx, strerror(errno));
      ul_close(ul_fd);
      return -1;
    }
  }

  ul_close(ul_fd);
  return 0;
}

int parse_route_range(ul_route_range_t *rr, char *str)
{
  long val;
  char *p;
  memset(rr, 0, sizeof(*rr));
  rr->operation = UL_ROUTE_OP_SET;
  if(*str == 'S')
    str++;
  if((*str == ':') || (*str == ','))
    str++;

  val = strtol(str, &p, 0);
  if (p == str)
    return -1;
  str = p;
  if (!*str) {
    rr->gw = val;
    return 1;
  }
  if (*str != ',')
    return -1;
  str++;
  rr->first = val;
  val = strtol(str, &p, 0);
  if (p == str)
    return -1;
  str = p;
  rr->last = val;
  if (*str != ',')
    return -1;
  str++;
  val = strtol(str, &p, 0);
  if (p == str)
    return -1;
  rr->gw = val;
  return 2;
}
#endif /*CONFIG_OC_UL_DRV_WITH_MULTI_NET*/

int spy_messages(void)
{
  int ret;
  ul_fd_t ul_fd;
  ul_msginfo msginfo;
  char *state;
 #ifndef WITHOUT_SYS_SELECT
  struct timeval timeout;
  fd_set set;
 #endif /* WITHOUT_SYS_SELECT */
  int i;
  uchar uc;
  int rec_messages=0;

  ul_fd=ul_open(ul_dev_name, NULL);
  if(ul_fd==UL_FD_INVALID)
    { perror("spy_messages : uLan open failed");return -1;};
  memset(&msginfo,0,sizeof(msginfo));
  msginfo.dadr=filt_dadr;
  msginfo.sadr=filt_sadr;
  msginfo.cmd=filt_cmd;
  if(filt_module)
  { msginfo.dadr=filt_module;
    ret=ul_addfilt(ul_fd,&msginfo);
    if(ret<0) { printf("spy_messages : add filter failed\n");return ret;};
    msginfo.dadr=filt_dadr;
    msginfo.sadr=filt_module;
    ret=ul_addfilt(ul_fd,&msginfo);
    if(ret<0) { printf("spy_messages : add filter failed\n");return ret;};
  } else {
    ret=ul_addfilt(ul_fd,&msginfo);
    if(ret<0) { printf("spy_messages : add filter failed\n");return ret;};
  };

  if(promode_flg&&((promode==1)||(promode&~0xff)))
    ret=ul_setpromode(ul_fd,promode);

  for(;;)
  { 
    if(num_messages_flg&&(rec_messages>=num_messages))
      break;
  #ifndef WITHOUT_SYS_SELECT
    FD_ZERO (&set);
    FD_SET (ul_fd2sys_fd(ul_fd), &set);
    FD_SET (0, &set);
    timeout.tv_sec = 10;
    timeout.tv_usec = 0;
    while ((ret=select(FD_SETSIZE,&set, NULL, NULL,&timeout))==-1
            &&errno==-EINTR);
    if(FD_ISSET(0,&set))
      {printf("\nspy_messages : break by user\n");break;};
    if(ret<0) return ret;
    if(!ret) continue;
  #else /* WITHOUT_SYS_SELECT */
    while (ul_fd_wait(ul_fd,1)<=0);
  #endif /* WITHOUT_SYS_SELECT */
    ret=ul_acceptmsg(ul_fd,&msginfo);
    if(ret<0) 
    { printf("spy_messages : problem to accept message\n");
      return ret;
    };
    rec_messages++;
    do
    { if(msginfo.flg&UL_BFL_FAIL) state="FAIL";
      else if(msginfo.flg&UL_BFL_M2IN) state="PROC";
      else state="REC ";
      printf("%s:(d:%d,s:%d,c:0x%X,f:0x%X,l:%d)",
             state,msginfo.dadr,msginfo.sadr,msginfo.cmd,
             msginfo.flg,msginfo.len);
      if(all_data) for(i=0;i<msginfo.len;i++)
        { ul_read(ul_fd,&uc,1); printf(" 0x%X",uc); };
      while((msginfo.flg&UL_BFL_TAIL)&&(ret>=0))
      { ret=ul_actailmsg(ul_fd,&msginfo);
        if(msginfo.flg&UL_BFL_FAIL) state="fail";
        else if(msginfo.flg&UL_BFL_M2IN) state="proc";
        else state="rec ";
        printf(" %s:(d:%d,s:%d,c:0x%X,f:0x%X,l:%d)",
               state,msginfo.dadr,msginfo.sadr,msginfo.cmd,
               msginfo.flg,msginfo.len);
        if(all_data) for(i=0;i<msginfo.len;i++)
          { ul_read(ul_fd,&uc,1); printf(" 0x%X",uc); };
      };
      printf("\n");
    }while(0);
   ul_freemsg(ul_fd);
  };
  ul_close(ul_fd);
  return 0;
};

static void
usage(void)
{
  printf("Usage: ul_spy <parameters> <hex_file>\n");
  printf("  -d, --uldev <name>       name of uLan device [%s]\n", ul_dev_name);
  printf("  -m, --module <num>       messages from/to module\n");
  printf("  -s, --source <num>       source of messages\n");
  printf("  -t, --target <num>       destination of messages\n");
  printf("  -c, --command <num>      commands of printed messages\n");
  printf("  -a, --all-data           show all messages data\n");
  printf("  -N, --num-messages <max> limit number of messages to wait for\n");
 #ifdef CONFIG_OC_UL_DRV_WITH_MULTI_NET
  printf("  -R, --route <default_gw> set default gateway for routed messages\n");
  printf("  -R, --route <first_addr>,<last_addr>,<gw>\n");
 #endif /*CONFIG_OC_UL_DRV_WITH_MULTI_NET*/
  printf("  -p, --print <max>        print modules to max address\n");
  printf("  -D  --debug-kernel <m>   flags to debug kernel\n");
  printf("  -P  --pro-mode <m>       set promiscuous mode of driver\n");
  printf("  -A  --set-myadr <num>    set new own address to the interface\n");
  printf("  -V, --version            show version\n");
  printf("  -h, --help               this usage screen\n");
}

int main(int argc,char *argv[])
{
  static struct option long_opts[] = {
    { "uldev", 1, 0, 'd' },
    { "module",1, 0, 'm' },
    { "source",1, 0, 's' },
    { "target",1, 0, 't' },
    { "command",1,0, 'c' },
    { "all-data",0,0,'a' },
    { "num-messages",1,0,'N' },
    { "route", 1, 0, 'R'},
    { "print", 1, 0, 'p' },
    { "version",0,0, 'V' },
    { "help",  0, 0, 'h' },
    { "debug-kernel", 1, 0, 'D' },
    { "pro-mode", 1, 0, 'P' },
    { "set-myadr", 1, 0, 'A' },
    { "log-kernel", 0, 0, 'L' },
    { 0, 0, 0, 0}
  };
  int opt;

 #ifndef HAS_GETOPT_LONG
  while ((opt = getopt(argc, argv, "d:m:s:t:c:aN:R:p:A:VhD:P:L")) != EOF)
 #else
  while ((opt = getopt_long(argc, argv, "d:m:s:t:c:aN:R:p:A:VhD:P:L",
			    &long_opts[0], NULL)) != EOF)
 #endif
    switch (opt) {
    case 'd':
      ul_dev_name=optarg;
      break;
    case 'm':
      filt_module = strtol(optarg,NULL,0);
      break;
    case 's':
      filt_sadr = strtol(optarg,NULL,0);
      break;
    case 't':
      filt_dadr = strtol(optarg,NULL,0);
      break;
    case 'c':
      filt_cmd = strtol(optarg,NULL,0);
      break;
    case 'a':
      all_data=1;
      break;
    case 'N':
      num_messages = strtol(optarg,NULL,0);
      num_messages_flg=1;
      break;
    case 'R':
   #ifdef CONFIG_OC_UL_DRV_WITH_MULTI_NET
      if(route_range_filled >= ROUTE_RANGE_ENTRIES) {
        fprintf(stderr, "%s:only %d route ranges can be set\n",
                argv[0], ROUTE_RANGE_ENTRIES);
        exit(1);
      }
      if (parse_route_range(&route_range_table[route_range_filled], optarg) < 0) {
        fprintf(stderr, "%s:set route range \"%s\" format error\n", argv[0], optarg);
        exit(1);
      }
      route_range_filled++;
   #else /*CONFIG_OC_UL_DRV_WITH_MULTI_NET*/
      fprintf(stderr, "%s:multiple network support noot compiled in\n",
              argv[0]);
      exit(1);
   #endif /*CONFIG_OC_UL_DRV_WITH_MULTI_NET*/
      break;
    case 'p':
      prt_modules = strtol(optarg,NULL,0);
      break;
    case 'D':
      debugk = strtol(optarg,NULL,0);
      debugk_flg=1;
      break;
    case 'P':
      promode = strtol(optarg,NULL,0);
      promode_flg=1;
      break;
    case 'A':
      new_myadr = strtol(optarg,NULL,0);
      new_myadr_flg=1;
      break;
    case 'L':
      logk_flg=1;
      break;
    case 'V':
      fputs("uLan spy v1.1\n", stdout);
      exit(0);
    case 'h':
    default:
      usage();
      exit(opt == 'h' ? 0 : 1);
    }

  if(new_myadr_flg) set_new_myadr(new_myadr);

 #ifdef CONFIG_OC_UL_DRV_WITH_MULTI_NET
  if (route_range_filled) {
    if (set_route_table(route_range_table, route_range_filled) < 0) {
      fprintf(stderr, "%s:set route ranges set error\n", argv[0]);
      exit(1);
    }
  }
 #endif /*CONFIG_OC_UL_DRV_WITH_MULTI_NET*/

  if(promode_flg) promode_kernel(promode);

  if(debugk_flg) debug_kernel(debugk);

  if(logk_flg) log_kernel();

  if(prt_modules) print_modules(prt_modules);

  if(!num_messages_flg||(num_messages>0))
    spy_messages();

  return 0;
}
