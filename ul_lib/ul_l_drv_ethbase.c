/*******************************************************************
  uLan Communication - low level C and driver interface library

  ul_l_drv_ethbase.c     - ethernet driver interface

  (C) Copyright 1996-2016 by Pavel Pisa - project originator
        http://cmp.felk.cvut.cz/~pisa
  (C) Copyright 1996-2016 PiKRON Ltd.
        http://www.pikron.com
  (C) Copyright 2002-2016 Petr Smolik


  The uLan driver project can be used and distributed
  in compliance with any of next licenses
   - GPL - GNU Public License
     See file COPYING for details.
   - LGPL - Lesser GNU Public License
   - MPL - Mozilla Public License
   - and other licenses added by project originator

  Code can be modified and re-distributed under any combination
  of the above listed licenses. If contributor does not agree with
  some of the licenses, he/she can delete appropriate line.
  WARNING: if you delete all lines, you are not allowed to
  distribute code or sources in any form.
 *******************************************************************/

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#ifndef _WIN32
#include <unistd.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <signal.h>
#define BAD_SOCKET(s) ((s) < 0)
#define UL_SOCKET_INVALID -1
#define SLEEP(x) sleep(x)
#else
#include <winsock.h>
#include <fcntl.h>
#define BAD_SOCKET(s) ((s) == INVALID_SOCKET)
#define UL_SOCKET_INVALID INVALID_SOCKET
#define SLEEP(x) Sleep(x*1000)
#endif
#include <ul_gavlcust.h>
#include <ul_msg_buf.h>
#include <ul_l_drv_eth.h>
#include <ul_list.h>

#ifdef CONFIG_OC_ULUT
#include <ul_log.h>
ul_log_domain_t ulogd_drv_eth   = {0, "drv_ethbase"};
#define UL_LDOMAIN (&ulogd_drv_eth)
#else
#include <ul_lib/ul_l_log.h>
#define UL_LDOMAIN NULL
#endif

/**
 * ul_eth_recv_msg - 
 * @fd: 
 * @rcvbuff: 
 * @timeout:
 *
 * Returns number of received bytes or negative error code
 *  Error codes: -1 connection was terminated by server
 *               -2 xxx
 *               -3 memory allocation error
 *               -4 timeout
 *               -5 terminated 
 */
int
ul_eth_recv_msg(int fd,ul_dbuff_t *rcvbuff,int timeout, int *terminated)
{

  ul_dbuff_set_len(rcvbuff, 0);

  while ((*terminated)==0) {
    struct timeval stimeout = {1, 0};
    int r,nread;
    fd_set fds;

    FD_ZERO(&fds);
    FD_SET(fd,&fds);
    r=select(FD_SETSIZE,&fds,NULL,NULL,&stimeout);

    if (timeout>0) {
      timeout--;
      if (!timeout)
        return -4; 
    }

    if (r<0) {
      return -1;
    }

    if (r==0) 
      continue;

    if(FD_ISSET(fd, &fds)) {

      #ifndef _WIN32
        ioctl(fd, FIONREAD, &nread);
      #else
        u_long unread;
        ioctlsocket(fd, FIONREAD, &unread);
        nread=unread;
      #endif /* _WIN32 */

      if(nread != 0) {
        int n;
        int old_len = rcvbuff->len;
        int new_len = old_len + nread;
        int eofmsg = 0;
        if(ul_dbuff_set_len(rcvbuff, new_len) == new_len) {
          nread = recv(fd, rcvbuff->data + old_len, nread,0);
          // try to find '\10' in new data to check out the on of message
          for(n = nread-1; n>=0; n--) { 
            if((rcvbuff->data + old_len)[n] == 10) {
              eofmsg = 1; 
              break;
            }
          }
        } else {
          /* Can't allocate memory */
          return -3;
        }
        if(eofmsg) {
          // if you got the whole message append terminating 0 & process it
          ul_dbuff_append_byte(rcvbuff, '\0');
          ul_dbuff_trim(rcvbuff);
          return rcvbuff->len;
        } 
      } else 
        return -1;
    } else {
      // this should never happen if the program work well
      // outread unexpected data from buffer
      int fd;
      for(fd = 0; fd < FD_SETSIZE; fd++) {
        if(FD_ISSET(fd, &fds)) {
          ul_dbuff_set_len(rcvbuff, 128);
          nread = recv(fd, rcvbuff->data, rcvbuff->len-1,0);
          rcvbuff->data[nread] = '\0';
        }
      }
    }
  }
  return -5;
}

int
ul_eth_send_msg(FILE *fd,ul_msg_buf_t *sndbuff)
{
  ul_msginfo *msginfo;
  int i;

  do{
    msginfo=&sndbuff->msginfo;
    fprintf(fd,"%02x%02x%02x%04x%08x%04x",
    	        msginfo->sadr,msginfo->dadr,msginfo->cmd,
                msginfo->flg,msginfo->stamp,msginfo->len);
    ul_log(UL_LDOMAIN,UL_LOGL_DEB|UL_LOGL_CONT,"%02x%02x%02x%04x%08x%04x",
    	        msginfo->sadr,msginfo->dadr,msginfo->cmd,
                msginfo->flg,msginfo->stamp,msginfo->len);
    i=0;
    while(i<msginfo->len) {
      fprintf(fd,"%02x",sndbuff->data.data[i]);
      ul_log(UL_LDOMAIN,UL_LOGL_DEB|UL_LOGL_CONT,"%02x",sndbuff->data.data[i]);
      i++;
    }
    if (!sndbuff->tail) 
      return 0;
    sndbuff=sndbuff->tail;
  }while(1);
}


int
ul_eth_demarshal_stream(ul_msg_buf_t *msg_buf,char *stream,int stream_len)
{
  ul_msg_buf_t *tail;
  ul_msginfo *msginfo;
  int i,v;

  ul_msg_buf_destroy(msg_buf);

  do {
    /* header */
    msginfo=&msg_buf->msginfo;
    if (stream_len<22) return -1;
    sscanf(stream,"%02x%02x%02x%04x%08x%04x",
           &msginfo->sadr,&msginfo->dadr,&msginfo->cmd,
           &msginfo->flg,&msginfo->stamp,&msginfo->len);
    stream+=22;
    stream_len-=22;

    if ((stream_len/2)<msginfo->len) return -1;
    if (ul_dbuff_prep(&msg_buf->data,msginfo->len)<msginfo->len)
      return -1;

    /* copy data from txt stream to ul_msg_buf */
    i=0;
    while (i<msginfo->len) {
      sscanf(stream,"%02x",&v);
      *(msg_buf->data.data+i)=v;
      stream+=2;
      i++;
    }
    stream_len-=2*i;
    
    /* prepare tail */
    if (stream_len>0) {
      if(!(tail=malloc(sizeof(ul_msg_buf_t))))
        return -1;
      ul_msg_buf_init(tail);
      msg_buf->tail=tail;
      msg_buf=tail;
    }
  } while(stream_len>0);
  return 1;
} 

