#*******************************************************************
# uLan Communication - Makefile for uLan library compilation
#
# (C) Copyright 2000 by Pavel Pisa 
#
# The uLan driver is distributed under the Gnu General Public Licence. 
# See file COPYING for details.
#
# Author reserves right to use and publish sources for embedded 
# applications under different conditions too.
#*******************************************************************

all : default

default : library

# Select compiler
CC=gcc

# Basic compilation flags
CFLAGS += -Wall -Wstrict-prototypes 
CFLAGS += -O2
CFLAGS += -I. -I..
CFLAGS += -ggdb

# CFLAGS for the uLan library
# note: some macro expansions require at least -O
LCFLAGS=  $(CFLAGS)
LCFLAGS+= $(CPUFLAGS)
# uncomment the next line if you want a shared library
# LIB_SHARED = 1

LIB_OBJS = ul_l_drv.lo ul_l_msg.lo

# DJGPP = 1

ifdef DJGPP

LIB_OBJS += ul_drv.lo

MORE_C_FILES += ../ul_drv/ul_drv.c

ul_drv.lo: ../ul_drv/ul_drv.c
	$(CC) -o $@ $(LCFLAGS) -D UL_WITH_PCI -I ../ul_drv -c $<

endif

ifndef LIB_SHARED

LIBULAN = libulan.a

%.lo: %.c
	$(CC) -o $@ $(LCFLAGS) -c $<

$(LIBULAN): $(LIB_OBJS)
	ar rcs $(LIBULAN) $^
	ranlib $(LIBULAN)

else

%.lo: %.c
	$(CC) --shared -o $@ $(LCFLAGS) -c $<

LIBULAN = libulan.so.0.0.4

$(LIBULAN): $(LIB_OBJS)
	ld --shared --soname=$(LIBULAN) -o $(LIBULAN) $^
	ln -s -f $(LIBULAN) libulan.so

endif

ul_lib/ul_fd.h : ul_lib/ul_fd4dir.h
	cp $< $@

ul_lib/ul_lib_config.h :
	echo "/* Substitute for OMK automatically generated file */" \
		> $@

library : ul_lib/ul_fd.h

library : ul_lib/ul_lib_config.h

library: $(LIBULAN)

.SUFFIXES: .i .s
%.c : %.i
	$(CC) -E $(CFLAGS) $< >$@

%.c : %.s
	$(CC) -S $(CFLAGS) $<

dep:
	$(CC) $(CFLAGS) $(CPPFLAGS) -w -E -M *.c $(MORE_C_FILES) | \
	sed 's/\.o/.lo/g' > depend

depend:
	@touch depend

clean :
	rm -f depend *.o *.lo *~ *.a *.so *.so.*

-include depend
