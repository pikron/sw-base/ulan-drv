/*******************************************************************
  uLan Communication - uL_DRV - multiplatform uLan driver

  ul_ufsm.c	- finite state machine for building and receiving
                  message frames on UART devices

  (C) Copyright 1996-2004 by Pavel Pisa - project originator
        http://cmp.felk.cvut.cz/~pisa
  (C) Copyright 1996-2004 PiKRON Ltd.
        http://www.pikron.com
  (C) Copyright 2002-2004 Petr Smolik
  

  The uLan driver project can be used and distributed 
  in compliance with any of next licenses
   - GPL - GNU Public License
     See file COPYING for details.
   - LGPL - Lesser GNU Public License
   - MPL - Mozilla Public License
   - and other licenses added by project originator

  Code can be modified and re-distributed under any combination
  of the above listed licenses. If contributor does not agree with
  some of the licenses, he/she can delete appropriate line.
  WARNING: if you delete all lines, you are not allowed to
  distribute code or sources in any form.
 *******************************************************************/

/*******************************************************************/
/* uLan main driver automata loop */

ul_call_fnc uld_drvloop_proc_1, uld_drvloop_proc_2;
ul_call_fnc uld_drvloop_rec_1, uld_drvloop_rec_2, uld_drvloop_rec_3, uld_drvloop_rec_4;

int uld_drvloop(ul_drv *udrv, int ret_code)
{
  if(((udrv->char_buff&0x180)==0x100) &&
     (uld_adr_match(udrv,udrv->char_buff&0xff)!=UL_ADR_MATCH_NONE) &&
     (udrv->char_buff==udrv->last_ctrl))
  { /* it seems, that somebody wants us  */
    udrv->con_flg=0;
    udrv->xor_sum=udrv->char_buff+1;
    UL_FCALL2(uld_recbeg_1,uld_drvloop_rec_1); /* Receive begin of frame */
    return UL_RC_PROC;
  };

  if(udrv->prep_bll.first!=NULL) /* we have something to proccess  */
  {
    LOG_CHIO(" call connect ");
    udrv->con_adr_match=UL_ADR_MATCH_NONE;
    UL_FCALL2(*udrv->chip_ops->fnc_connect,uld_drvloop_proc_1); /* Media access arbitration */
    return UL_RC_PROC;
  };

  udrv->char_buff=0;
  UL_FCALL(*udrv->chip_ops->fnc_recch); /* Receive other character */
  return UL_RC_PROC;
};

int uld_drvloop_proc_error(ul_drv *udrv)
{ ul_mem_blk *mes;
  udrv->fnc_timeout=NULL;
  LOG_MESSAGES(KERN_INFO "uLan : send message ERROR\n");
  if((mes=udrv->con_message)!=NULL)
  {udrv->con_message=NULL;
   if((UL_BLK_HEAD(mes).flg&UL_BFL_NORE)||
      (++UL_BLK_HEAD(mes).retry_cnt>=UL_RETRY_CNT))
   { UL_BLK_HEAD(mes).flg|=UL_BFL_FAIL;
     ul_bll_move_mes(&udrv->proc_bll,mes);
     LOG_FAILS(KERN_ERR "uLan : send message (d:%d,s:%d,c:%X,f:%X,l:%d) FAILED after %d attempts\n",
                   UL_BLK_HEAD(mes).dadr,UL_BLK_HEAD(mes).sadr,
                   UL_BLK_HEAD(mes).cmd,UL_BLK_HEAD(mes).flg,
                   UL_BLK_HEAD(mes).len,UL_BLK_HEAD(mes).retry_cnt);
     SCHEDULE_BH(udrv);
   }else if(UL_BLK_HEAD(mes).flg&UL_BFL_REWA)
     ul_bll_move_mes(&udrv->prep_bll,mes);
  };
  udrv->char_buff=0x1FF; 
  return 0;
};

int uld_drvloop_proc_1(ul_drv *udrv, int ret_code)
{
  if(ret_code<0) {UL_FNEXT(uld_drvloop); return UL_RC_PROC;};
  LOG_MESSAGES(KERN_INFO "uLan : begin of send message\n");
  udrv->fnc_timeout=&uld_drvloop_proc_error;
  udrv->con_message=udrv->prep_bll.first;
  UL_FCALL2(uld_prmess,uld_drvloop_proc_2); /* Process outgoing message */
  return UL_RC_PROC;
};

int uld_drvloop_proc_2(ul_drv *udrv, int ret_code)
{
  ul_mem_blk *mes;
  udrv->fnc_timeout=NULL;
  udrv->char_buff=uld_get_arb_addr(udrv)|0x180; 
  UL_FCALL2(*udrv->chip_ops->fnc_sndch,uld_drvloop); /* Release media, send B8+own Adr */
  if(udrv->con_message==NULL) return UL_RC_PROC;
  if(ret_code<0)
  {uld_drvloop_proc_error(udrv);
  }else{
    LOG_MESSAGES(KERN_INFO "uLan : send message finished OK\n");
    mes=udrv->con_message;
    udrv->con_message=NULL;

    if((udrv->con_adr_match!=UL_ADR_MATCH_NONE)||
       (UL_BLK_HEAD(mes).flg&UL_BFL_M2IN))
    {
      ul_bll_move_mes(&udrv->proc_bll,mes);
      SCHEDULE_BH(udrv);
    } else
     ul_bll_free_mes(mes);
  };
  return UL_RC_PROC;
};

int uld_drvloop_rec_error(ul_drv *udrv)
{ ul_mem_blk *mes;
  udrv->fnc_timeout=NULL;
  LOG_MESSAGES(KERN_INFO "uLan : receive message ERROR\n");
  if((mes=udrv->con_message)!=NULL)
  { udrv->con_message=NULL;
    ul_bll_free_mes(mes);
  };
  udrv->char_buff=0x1FF;
  return 0;
};

int uld_drvloop_rec_1(ul_drv *udrv, int ret_code)
{ ul_mem_blk *mes;
  int flg=0;
  if(ret_code<0) {UL_FNEXT(uld_drvloop); return UL_RC_PROC;};
  LOG_MESSAGES(KERN_INFO "uLan : begin of receive message\n");
  if (udrv->con_flg&0x10)
    flg|=UL_BFL_PMES;
  mes=ul_new_frame_head(udrv,udrv->con_dadr,udrv->con_sadr,
                	udrv->con_cmd,flg);
  if(mes==NULL)
  { LOG_FAILS(KERN_ERR "uLan : NO memory for receive\n");
    UL_FNEXT(uld_drvloop); return UL_RC_PROC;
  }
 #ifdef UL_WITH_MULTI_NET
  UL_BLK_HEAD(mes).hops=udrv->con_hops;
  UL_BLK_HEAD(mes).sadr_local=udrv->con_sadr_local;
  if((udrv->con_sadr&UL_ADR_NET_MASK)&&
     ((udrv->con_sadr^udrv->my_net_base)&UL_ADR_NET_MASK))
    UL_BLK_HEAD(mes).flg|=UL_BFL_SANL;
 #endif /*UL_WITH_MULTI_NET*/

  ul_bll_ins(&udrv->work_bll,mes);
  udrv->con_message=mes;
  udrv->fnc_timeout=&uld_drvloop_rec_error;
  udrv->con_frame=mes;
  UL_FCALL2(uld_recdata,uld_drvloop_rec_2); /* Receive frame data */
  return UL_RC_PROC;
};

int uld_drvloop_rec_2(ul_drv *udrv, int ret_code)
{
  if(ret_code<0) 
  { uld_drvloop_rec_error(udrv);
    UL_FNEXT(uld_drvloop); return UL_RC_PROC;
  };
  #ifdef UL_WITH_IAC
    UL_FCALL2(uld_recend,uld_drvloop_rec_3);
  #else 
    UL_FCALL2(uld_recend,uld_drvloop_rec_4); /* Receive frame end */
  #endif /* UL_WITH_IAC  */
  return UL_RC_PROC;
};

#ifdef UL_WITH_IAC
int uld_drvloop_rec_3(ul_drv *udrv, int ret_code)
{ 
  if(ret_code<0) 
  { uld_drvloop_rec_error(udrv);
    UL_FNEXT(uld_drvloop); return UL_RC_PROC;
  };
  UL_FCALL2(uld_prmess_iac,uld_drvloop_rec_4);
  return UL_RC_PROC;
 
}
#endif /* UL_WITH_IAC  */

int uld_drvloop_rec_4(ul_drv *udrv, int ret_code)
{ ul_mem_blk *mes;
  if(ret_code<0) 
  { uld_drvloop_rec_error(udrv);
    UL_FNEXT(uld_drvloop); return UL_RC_PROC;
  };
  mes=udrv->con_message;
  udrv->fnc_timeout=NULL;
  udrv->con_message=NULL;
  if (ret_code==UL_RC_FREEMSG) {
    ul_bll_free_mes(mes);
    UL_FNEXT(uld_drvloop);
    LOG_MESSAGES(KERN_INFO "uLan : receive message was freed\n");
    return UL_RC_PROC;
  }
  UL_BLK_HEAD(mes).stamp=ul_gen_stamp();
  ul_bll_move_mes(&udrv->proc_bll,mes);
  SCHEDULE_BH(udrv);
  UL_FNEXT(uld_drvloop);
  LOG_MESSAGES(KERN_INFO "uLan : end of receive message\n");
  return UL_RC_PROC;
};

int ul_max_cycle_cnt=0;
int ul_max_cycle_time=0;
int ul_irq_seq_num=0;
#ifdef ENABLE_UL_IRQ_STALE_WDG	
int ul_irq_stale_wdg_cnt=0;
#endif /* ENABLE_UL_IRQ_STALE_WDG */

/*** Interrupt entry point ***/

UL_IRQ_HANDLER_FNC(uld_irq_handler)
{
  unsigned int intno = ul_irq_handler_get_irqnum();
  ul_drv* udrv=(ul_drv*)ul_irq_handler_get_context();
  #ifdef UL_GLOBAL_IRQ_LOCK_FINI
   UL_GLOBAL_IRQ_LOCK_FINI
  #endif
  int irq_loop_cnt=0;
  int cycle_cnt=0;
  int cycle_time;
  int ret_code;
  #ifdef UL_LOG_ENABLE
    int seq_num=++ul_irq_seq_num;
  #endif
  (void)intno;
  if(udrv->magic!=UL_DRV_MAGIC)
  {
    #ifdef FOR_LINUX_KERNEL
     panic("uld_irq_handler : BAD udrv magic !!!");
    #elif defined(_WIN32)
     UL_PRINTF("uld_irq_handler : BAD udrv magic !!!\n");
     return FALSE;
    #elif defined(__DJGPP__)||defined(CONFIG_OC_UL_DRV_SYSLESS)
     UL_PRINTF("uld_irq_handler : BAD udrv magic !!!\n");
     return;
    #else
     error("uld_irq_handler : BAD udrv magic !!!");
    #endif
  }
  #ifdef UL_GLOBAL_IRQ_LOCK
   UL_GLOBAL_IRQ_LOCK;
  #endif

  if(uld_test_dfl(udrv,CHIPOK))
  do
  { 
    #ifdef ENABLE_UL_IRQ_STALE_WDG	
    /* last rescue code for stale IRQ !!!!!! */
    if(ul_irq_stale_wdg_cnt++>10000)
    {
      SCHEDULE_BH(udrv); /* Test activation of BH */
      if(ul_irq_stale_wdg_cnt>20000)
      {
	if(udrv->chip_ops->fnc_genirq!=NULL)
	{
	  LOG_FATAL(KERN_CRIT "Stale IRQ uLan IRQ signal !!!\n");
	  udrv->chip_ops->fnc_genirq(udrv,0);
	}
	if(ul_irq_stale_wdg_cnt<25000) break;
	#ifdef FOR_LINUX_KERNEL
	 LOG_FATAL(KERN_CRIT "Disabling IRQ %d forever !!!\n",intno);
	 disable_irq_nosync(intno);
	#endif
	break;
      }
    }
    #endif /* ENABLE_UL_IRQ_STALE_WDG */
    do /* IRQ fully SMP and re-entrant processing */
    {
      uld_set_dfl(udrv,ASK_ISR);
      if(uld_test_and_set_dfl(udrv,IN_ISR))
      { LOG_IRQ(KERN_INFO "uLan IRQ %d arrived, but else is in ISR\n",intno);
        break;
      };

      /* only one CPU in no nested levels at time */
      uld_clear_dfl(udrv,ASK_ISR);

      while((ret_code=udrv->chip_ops->fnc_pool(udrv))!=0)
      {
	uld_tracer_log_fsm_poll(ret_code);
	LOG_SEQNUM("\n<<< seq %d ",seq_num);
	LOG_IRQ(KERN_INFO "uLan IRQ %d arrived, code = %d\n",intno,ret_code);
	cycle_time=RDTSC;
	do {
	  ul_call_fnc *fnc=udrv->fnc_act;
	  ret_code=fnc(udrv,ret_code);
	  uld_tracer_log_fsm_fnc(fnc, ret_code);
	  if((cycle_cnt++>100) && (ret_code!=UL_RC_WIRQ)) 
	    {LOG_FATAL(KERN_CRIT "Infinite loop in uLan IRQ handler !!!\n");break;};
	} while(ret_code!=UL_RC_WIRQ);
	cycle_time=RDTSC-cycle_time;
	if(cycle_cnt>ul_max_cycle_cnt) ul_max_cycle_cnt=cycle_cnt;
	if(cycle_time>ul_max_cycle_time) ul_max_cycle_time=cycle_time;
	LOG_SEQNUM(" seq %d >>>",seq_num);
	if(irq_loop_cnt++>10)
	{ LOG_FATAL(KERN_CRIT "Stale IRQ - uLan state handlers !!!\n");
	  if(udrv->chip_ops->fnc_genirq!=NULL)
	    udrv->chip_ops->fnc_genirq(udrv,0);
	  break;
	};
	uld_atomic_clear_dfl(udrv,NACTIV);
      };
      uld_atomic_clear_dfl(udrv,IN_ISR);
    }
    while(uld_atomic_test_dfl(udrv,ASK_ISR));

  }while(0);
  #ifdef UL_GLOBAL_IRQ_UNLOCK
   UL_GLOBAL_IRQ_UNLOCK;
  #endif
   return UL_IRQ_RETVAL(irq_loop_cnt);
};

/*** Timeout entry point ***/
void uld_timeout(ul_drv *udrv)
{
  #ifdef UL_GLOBAL_IRQ_LOCK_FINI
   UL_GLOBAL_IRQ_LOCK_FINI
  #endif
  #if defined(_WIN32)&&defined(UL_WITH_PCI)
   KIRQL old_irql;
  #endif
  int cycle_cnt=0;
  int cycle_time;
  int ret_code=0;
  #ifdef UL_GLOBAL_IRQ_LOCK
   UL_GLOBAL_IRQ_LOCK;
  #endif
  LOG_IRQ(KERN_INFO "uLan timeout activated\n");
  if(uld_test_dfl(udrv,CHIPOK))
  {
    #ifdef FOR_LINUX_KERNEL
    if(udrv->irq) disable_irq_nosync(udrv->irq);
    #elif defined(_WIN32)&&defined(UL_WITH_PCI)
    KeRaiseIrql(uL_SpinLock_Irql,&old_irql);
    #endif
    do
    {
      if(uld_test_and_set_dfl(udrv,IN_ISR))
      { LOG_IRQ(KERN_INFO "uLan timeout, but else is in ISR\n");
        break;
      };

      /* only one CPU with no nested levels goes there */
      uld_clear_dfl(udrv,ASK_ISR);
      if((ret_code=udrv->chip_ops->fnc_pool(udrv))==0)
      {/* if first pass check for timeout conditions */
	if(!cycle_cnt&&(uld_atomic_test_dfl(udrv,NACTIV)||
            (udrv->last_ctrl&0x80)))
	{ cycle_cnt++;
	  uld_clear_dfl(udrv,WDFORCED);
	  if(udrv->fnc_timeout) ret_code=udrv->fnc_timeout(udrv);
	  uld_tracer_log_fsm_timeout(ret_code);
	  if(ret_code==0)
	  {udrv->fnc_sp=&udrv->fnc_stack[0];
	   udrv->fnc_act=&uld_drvloop;
	   ret_code=UL_RC_ACTIVATE;
	  };
	};
      };
      cycle_time=RDTSC;
      while(ret_code!=UL_RC_WIRQ) {
	ul_call_fnc *fnc=udrv->fnc_act;
	ret_code=fnc(udrv,ret_code);
	uld_tracer_log_fsm_fnc(fnc, ret_code);
	if((cycle_cnt++>100) && (ret_code!=UL_RC_WIRQ)) 
	  {LOG_FATAL(KERN_CRIT "Infinite loop in uLan Timeout handler !!!\n");break;};
      };
      cycle_time=RDTSC-cycle_time;
      if(cycle_cnt>ul_max_cycle_cnt) ul_max_cycle_cnt=cycle_cnt;
      if(cycle_time>ul_max_cycle_time) ul_max_cycle_time=cycle_time;
      uld_clear_dfl(udrv,NACTIV);
      uld_atomic_clear_dfl(udrv,IN_ISR);
    }
    while(uld_atomic_test_dfl(udrv,ASK_ISR));
    #ifdef FOR_LINUX_KERNEL
     if(udrv->irq) enable_irq(udrv->irq);
    #elif defined(_WIN32)&&defined(UL_WITH_PCI)
     KeLowerIrql(old_irql);
    #endif
  };
  #ifdef UL_GLOBAL_IRQ_UNLOCK
   UL_GLOBAL_IRQ_UNLOCK;
  #endif
};

/* link watchdog timer function */
#ifdef _WIN32
VOID NTAPI ulan_wd_dpc(IN PKDPC Dpc,IN PVOID data,
		  IN PVOID arg1,IN PVOID arg2)
#elif defined(FOR_LINUX_KERNEL)
 void ulan_do_wd_timer(KC_TIMER_HANDLER_ARGS)
#elif defined(FOR_NUTTX_KERNEL)
 void ulan_do_wd_timer(wdparm_t data)
#else
 void ulan_do_wd_timer(unsigned long data)
#endif /* _WIN32 */
{
  ul_drv* udrv=TIMER_HANDLER_ARGS2UDRV;
  if(udrv->magic!=UL_DRV_MAGIC)
  {
    #ifdef FOR_LINUX_KERNEL
     panic("ulan_do_wd_timer : BAD udrv magic !!!");
    #elif defined(_WIN32)||defined(__DJGPP__)||defined(CONFIG_OC_UL_DRV_SYSLESS)
     UL_PRINTF("ulan_do_wd_timer : BAD udrv magic !!!\n");
     return;
    #else
     error("ulan_do_wd_timer : BAD udrv magic !!!");
    #endif
  }
  if(!uld_test_dfl(udrv,CHIPOK)) return;

  if((udrv->prep_bll.first!=NULL)||uld_test_dfl(udrv,WDFORCED))
  {
    if(uld_test_and_set_dfl(udrv,NACTIV)||
      (udrv->last_ctrl&0x80)) uld_timeout(udrv);
    #ifndef _WIN32  
     SCHEDULE_UDRV_WDTIM(udrv,jiffies+ULD_HZ/25+1);
    #else /* _WIN32 */
     { LARGE_INTEGER delay;
       delay=RtlConvertLongToLargeInteger(-40*10000);
       KeSetTimer(&udrv->wd_timer,delay,&udrv->wd_timer_dpc);
     }
    #endif /* _WIN32 */
  };
};

#ifdef UL_WITH_CHIP_TIMER

void uld_chip_timer_event(ul_drv *udrv)
{
  #ifdef UL_GLOBAL_IRQ_LOCK_FINI
   UL_GLOBAL_IRQ_LOCK_FINI
  #endif
  #if defined(_WIN32)&&defined(UL_WITH_PCI)
   KIRQL old_irql;
  #endif
  int irq_loop_cnt=0;
  int cycle_cnt=0;
  int cycle_time;
  int ret_code=0;
  #ifdef UL_GLOBAL_IRQ_LOCK
   UL_GLOBAL_IRQ_LOCK;
  #endif
  if(uld_test_dfl(udrv,CHIPOK))
  {
    #ifdef FOR_LINUX_KERNEL
    if(udrv->irq) disable_irq_nosync(udrv->irq);
    #elif defined(_WIN32)&&defined(UL_WITH_PCI)
    KeRaiseIrql(uL_SpinLock_Irql,&old_irql);
    #endif
    uld_set_dfl(udrv,CHIPTIMER);
    do
    {
      uld_set_dfl(udrv,ASK_ISR);
      if(uld_test_and_set_dfl(udrv,IN_ISR))
      { LOG_IRQ(KERN_INFO "uLan chip timer event, but else is in ISR\n");
        break;
      };

      /* only one CPU with no nested levels goes there */
      uld_clear_dfl(udrv,ASK_ISR);
      uld_set_dfl(udrv,IN_CHIPTIMER);
      while((ret_code=udrv->chip_ops->fnc_pool(udrv))!=0)
      {
	uld_tracer_log_fsm_poll(ret_code);
	LOG_SEQNUM("\n<<< in chip timeout ");
	LOG_IRQ(KERN_INFO "uLan IRQ chip driver code = %d\n",ret_code);
	cycle_time=RDTSC;
        do {
	  ul_call_fnc *fnc=udrv->fnc_act;
	  ret_code=fnc(udrv,ret_code);
	  uld_tracer_log_fsm_fnc(fnc, ret_code);
	  if((cycle_cnt++>100) && (ret_code!=UL_RC_WIRQ))
	    {LOG_FATAL(KERN_CRIT "Infinite loop in uLan chip timer event handler !!!\n");break;};
	} while(ret_code!=UL_RC_WIRQ);
        cycle_time=RDTSC-cycle_time;
        if(cycle_cnt>ul_max_cycle_cnt) ul_max_cycle_cnt=cycle_cnt;
        if(cycle_time>ul_max_cycle_time) ul_max_cycle_time=cycle_time;
        uld_clear_dfl(udrv,NACTIV);
        LOG_SEQNUM(" >>>");
	if(irq_loop_cnt++>10)
	{ LOG_FATAL(KERN_CRIT "Stale IRQ - uLan state handlers !!!\n");
	  if(udrv->chip_ops->fnc_genirq!=NULL)
	    udrv->chip_ops->fnc_genirq(udrv,0);
	  break;
	};
	uld_atomic_clear_dfl(udrv,NACTIV);
      };
      uld_clear_dfl(udrv,IN_CHIPTIMER);
      uld_atomic_clear_dfl(udrv,IN_ISR);
    }
    while(uld_atomic_test_dfl(udrv,ASK_ISR));
    #ifdef FOR_LINUX_KERNEL
     if(udrv->irq) enable_irq(udrv->irq);
    #elif defined(_WIN32)&&defined(UL_WITH_PCI)
     KeLowerIrql(old_irql);
    #endif
  };
  #ifdef UL_GLOBAL_IRQ_UNLOCK
   UL_GLOBAL_IRQ_UNLOCK;
  #endif
}

/* event on chip supporting timer, Linux kernel only for now */
enum hrtimer_restart uld_chip_timer_callback(struct hrtimer *timer)
{
  ul_drv *udrv=container_of(timer,ul_drv,chip_timer);

  if(udrv->magic!=UL_DRV_MAGIC)
  {
    #ifdef FOR_LINUX_KERNEL
     panic("uld_chip_timer_callback : BAD udrv magic !!!");
    #elif defined(_WIN32)||defined(__DJGPP__)||defined(CONFIG_OC_UL_DRV_SYSLESS)
     UL_PRINTF("uld_chip_timer_callback : BAD udrv magic !!!\n");
     return;
    #else
     error("uld_chip_timer_callback : BAD udrv magic !!!");
    #endif
  }
  if(!uld_test_dfl(udrv,CHIPOK)) return HRTIMER_NORESTART;

  uld_chip_timer_event(udrv);

  return HRTIMER_NORESTART;
}
#endif /*UL_WITH_CHIP_TIMER*/
