/*******************************************************************
  uLan Communication - uL_DRV - multiplatform uLan driver

  ul_di.c	- message data iterators routines

  (C) Copyright 1996-2004 by Pavel Pisa - project originator
        http://cmp.felk.cvut.cz/~pisa
  (C) Copyright 1996-2004 PiKRON Ltd.
        http://www.pikron.com
  (C) Copyright 2002-2004 Petr Smolik
  

  The uLan driver project can be used and distributed 
  in compliance with any of next licenses
   - GPL - GNU Public License
     See file COPYING for details.
   - LGPL - Lesser GNU Public License
   - MPL - Mozilla Public License
   - and other licenses added by project originator

  Code can be modified and re-distributed under any combination
  of the above listed licenses. If contributor does not agree with
  some of the licenses, he/she can delete appropriate line.
  WARNING: if you delete all lines, you are not allowed to
  distribute code or sources in any form.
 *******************************************************************/

/*******************************************************************/
/* Data iterator */

long ul_di_seek(ul_data_it *di,long pos, int whence)
{
  switch(whence)
  {
    case 1: pos+=di->pos; break;
    case 2: pos+=UL_BLK_HEAD(di->head_blk).len; break;
  };
  if(pos<di->pos)
  {
    di->blk=di->head_blk;
    di->pos=0;
    di->ptr=UL_BLK_HEAD(di->head_blk).fdata;
    if(pos<0) return 0;
  };
  di->ptr+=pos-di->pos;
  di->pos=pos;
  return pos;
};

uchar ul_di_read1(ul_data_it *di)
{
  uchar ch;
  if(ul_di_adjust(di))
    ch=*ul_di_byte(di);
  else ch=0;
  ul_di_inc(di);
  return ch;
};

uchar ul_di_write1(ul_data_it *di, uchar ch)
{
  ul_mem_blk *blk;
  while(!ul_di_adjust(di))
  {
    if(!(blk=ul_alloc_blk(UL_BLK_HEAD(di->head_blk).bll->udrv))) return 0;
    di->blk->next=blk;
  };
  *ul_di_byte(di)=ch;
  ul_di_inc(di);
  return 1;
};

int ul_di_read(ul_data_it *di, char *buf, int count)
{
  int cn;
  int len;

  cn=count;
  while(cn>0)
  {
    if(!ul_di_adjust(di))
    {
      memset(buf,0,cn);
      cn=0;
      break;
    };
    len=ul_di_atonce(di);
    if(len>cn) len=cn;
    memcpy(buf,ul_di_byte(di),len);
    ul_di_add(di,len);
    buf+=len;
    cn-=len;
  };
  return count-cn;
}

int ul_di_write(ul_data_it *di, const char *buf, int count)
{
  int cn;
  int len;
  ul_mem_blk *blk;

  cn=count;
  while(cn>0)
  {
    while(!ul_di_adjust(di))
    {
      if(!(blk=ul_alloc_blk(UL_BLK_HEAD(di->head_blk).bll->udrv))) 
	{count-=cn; return count;};
      memset(UL_BLK_NDATA(blk),0,UL_BLK_SIZE);
      di->blk->next=blk;
    };
    len=ul_di_atonce(di);
    if(len>cn) len=cn;
    memcpy(ul_di_byte(di),buf,len);
    ul_di_add(di,len);
    buf+=len;
    cn-=len;
  };
  return count;
}

#ifdef UL_WITH_FRAME_FSM

/*-- Helper functions for snddata --*/
int uld_snddata_1(ul_drv *udrv, int ret_code)
{
  if(ret_code<0) {UL_FRET;return ret_code;};
  if(udrv->con_di.trans_len--)
  {
    udrv->char_buff=ul_di_read1(&udrv->con_di);
    UL_FCALL(*udrv->chip_ops->fnc_sndch); /* send data byte */
  } else UL_FRET;
  return UL_RC_PROC;
};

/*** Send data ***/
int uld_snddata(ul_drv *udrv, int ret_code)
{
  ul_di_init(&udrv->con_di,udrv->con_frame);
  udrv->con_di.trans_len=UL_BLK_HEAD(udrv->con_frame).len;
  if(!udrv->con_di.trans_len) 
    {UL_FRET; return UL_RC_PROC;};
  UL_FNEXT(uld_snddata_1);
  return UL_RC_PROC;
};

/*-- Helper functions for recdata --*/
int uld_recdata_1(ul_drv *udrv, int ret_code)
{
  if(ret_code<0) {UL_FRET;return ret_code;};
  if(!(udrv->char_buff&0xFF00))
  {
    ret_code=ul_di_write1(&udrv->con_di,(uchar)udrv->char_buff);
    if(!ret_code) return UL_RC_ENOMEM;
    if(--udrv->con_di.trans_len)
    {
      UL_FCALL(*udrv->chip_ops->fnc_recch); /* receive consecutive data byte */
      return UL_RC_PROC;
    };
  };
  if(!(UL_BLK_HEAD(udrv->con_frame).flg&UL_BFL_LNMM))
    UL_BLK_HEAD(udrv->con_frame).len=udrv->con_di.pos;
  UL_FRET;
  return UL_RC_PROC;
};

/*** Receive data ***/
int uld_recdata(ul_drv *udrv, int ret_code)
{
  ul_di_init(&udrv->con_di,udrv->con_frame);
  if(UL_BLK_HEAD(udrv->con_frame).flg&UL_BFL_LNMM)
    udrv->con_di.trans_len=UL_BLK_HEAD(udrv->con_frame).len;
  else
    udrv->con_di.trans_len=-1;
  if(!udrv->con_di.trans_len) {UL_FRET; return UL_RC_PROC;};
  UL_FCALL2(*udrv->chip_ops->fnc_recch,uld_recdata_1); /* Receive first data byte */
  return UL_RC_PROC;
};

#endif /*UL_WITH_FRAME_FSM*/
