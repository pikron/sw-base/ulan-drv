/*******************************************************************
  uLan Communication - uL_DRV - multiplatform uLan driver

  ul_linux.c	- Linux kernel and module specific part

  (C) Copyright 1996-2004 by Pavel Pisa - project originator
        http://cmp.felk.cvut.cz/~pisa
  (C) Copyright 1996-2004 PiKRON Ltd.
        http://www.pikron.com
  (C) Copyright 2002-2004 Petr Smolik
  

  The uLan driver project can be used and distributed 
  in compliance with any of next licenses
   - GPL - GNU Public License
     See file COPYING for details.
   - LGPL - Lesser GNU Public License
   - MPL - Mozilla Public License
   - and other licenses added by project originator

  Code can be modified and re-distributed under any combination
  of the above listed licenses. If contributor does not agree with
  some of the licenses, he/she can delete appropriate line.
  WARNING: if you delete all lines, you are not allowed to
  distribute code or sources in any form.
 *******************************************************************/

/*******************************************************************/
/* Linux kernel and module specific part */ 

#ifdef MODULE_SUPPORTED_DEVICE
MODULE_SUPPORTED_DEVICE("ul_drv");
#endif
MODULE_AUTHOR("Pavel Pisa <pisa@cmp.felk.cvut.cz>");
MODULE_DESCRIPTION("\
uLan communication module ( 9 bit RS-485 protocol )\n\
for communication with embedded controllers.\n\
It uses i82510 addon card or RS-232 dongle");
MODULE_LICENSE("GPL");
EXPORT_NO_SYMBOLS;

static int ulan_major_dev=ULAN_MAJOR;	/* uLan major device number */
struct kc_class *ulan_class;

#define UL_MINORS 9
#ifndef CONFIG_UL_DEFAULT_PORT
#define CONFIG_UL_DEFAULT_PORT 0
#endif
ul_drv *ul_drv_arr[UL_MINORS]={[0 ... UL_MINORS-1]=NULL};
char *chip[UL_MINORS]={NULL,};
int chip_specified;
#if defined(UL_WITH_PCI) || defined(UL_WITH_USB) || defined(UL_WITH_OF)
char *slot[UL_MINORS]={NULL,};
int slot_specified;
#endif
int my_adr[UL_MINORS]={[0 ... UL_MINORS-1]=2};
int my_adr_specified;
int baud[UL_MINORS]={[0 ... UL_MINORS-1]=19200};
int baud_specified;
int baudbase[UL_MINORS]={[0 ... UL_MINORS-1]=0};
int baudbase_specified;
int irq[UL_MINORS]={0,};
int irq_specified;
int port[UL_MINORS]={CONFIG_UL_DEFAULT_PORT
	#if (UL_MINORS>1)
		,[1 ... UL_MINORS-1]=0
	#endif
		};
int port_specified;

int debug=-1;		/* debug flags */
int ul_usb_msg_inpr=0;	/* number of concurrent messages sent over USB */

#if (LINUX_VERSION_CODE >= KERNEL_VERSION(2,6,0))
module_param_array(chip, charp, &chip_specified, 0);
module_param_array(my_adr, int, &my_adr_specified, 0);
module_param_array(baud, int, &baud_specified, 0);
module_param_array(irq, int, &irq_specified, 0);
module_param_array(port, int, &port_specified, 0);
module_param_array(baudbase, int, &baudbase_specified, 0);
#if defined(UL_WITH_PCI) || defined(UL_WITH_USB)
module_param_array(slot, charp, &slot_specified, 0);
#endif
module_param(debug, int, 0);
module_param(ul_usb_msg_inpr, int, 0);
module_param(ulbuffer, int, 0);

#else /* LINUX_VERSION_CODE < 2,6,0 */
MODULE_PARM(chip,"1-" __MODULE_STRING(UL_MINORS) "s");
MODULE_PARM(my_adr,"1-" __MODULE_STRING(UL_MINORS) "i");
MODULE_PARM(baud,"1-" __MODULE_STRING(UL_MINORS) "i");
MODULE_PARM(irq,"1-" __MODULE_STRING(UL_MINORS) "i");
MODULE_PARM(port,"1-" __MODULE_STRING(UL_MINORS) "i");
MODULE_PARM(baudbase,"1-" __MODULE_STRING(UL_MINORS) "i");
#if defined(UL_WITH_PCI) || defined(UL_WITH_USB)
MODULE_PARM(slot,"1-" __MODULE_STRING(UL_MINORS) "s");
#endif
MODULE_PARM(debug,"i");
MODULE_PARM(ul_usb_msg_inpr,"i");
MODULE_PARM(ulbuffer,"i");
#endif /* LINUX_VERSION_CODE < 2,6,0 */

MODULE_PARM_DESC(chip,"chip type: auto, 82510, 16450, 16950-pci");
MODULE_PARM_DESC(my_adr,"uLan address of defined interface");
MODULE_PARM_DESC(baud,"define baudrates");
MODULE_PARM_DESC(irq,"define irqs");
MODULE_PARM_DESC(port,"define ports");
MODULE_PARM_DESC(baudbase,"define UART base clock");
#if defined(UL_WITH_PCI) || defined(UL_WITH_USB)
MODULE_PARM_DESC(slot,"slot type (pci) and specification");
#endif
MODULE_PARM_DESC(debug,"debug flags (FATAL,CHIO,IRQ,MSG,FAILS,"\
		       "SEQ,PORTS,FILE,FILT)");
MODULE_PARM_DESC(ul_usb_msg_inpr,"used length of Tx fifo in conventor");
MODULE_PARM_DESC(ulbuffer,"size of per interface buffer");


#ifdef UL_WITH_PCI

static int /*__devinit*/ ulan_pci_init_one (struct pci_dev *dev,
                                   const struct pci_device_id *ent);

static void /*__devexit*/ ulan_pci_remove_one (struct pci_dev *dev);

#if defined(CONFIG_PM) && (LINUX_VERSION_CODE > VERSION(2,6,18))
static int ulan_pci_suspend_one(struct pci_dev *pdev, pm_message_t state);
static int ulan_pci_resume_one(struct pci_dev *pdev);
#else
#define ulan_pci_suspend_one NULL
#define ulan_pci_resume_one NULL
#endif /* CONFIG_PM */

MODULE_DEVICE_TABLE(pci, ulan_pci_tbl);

static struct pci_driver ulan_pci_driver = {
	name:		"ul_drv",
	id_table:	ulan_pci_tbl,
	probe:		ulan_pci_init_one,
	remove:		ulan_pci_remove_one,
	suspend:	ulan_pci_suspend_one,
	resume:		ulan_pci_resume_one,
};
#endif /*UL_WITH_PCI*/

#ifdef UL_WITH_OF
static int ulan_of_probe(struct platform_device *op);
static KC_OF_REMOVE_RET ulan_of_remove(struct platform_device *op);
#ifdef CONFIG_PM
static int ulan_of_suspend(struct platform_device *op, pm_message_t state);
static int ulan_of_resume(struct platform_device *op);
#endif

MODULE_DEVICE_TABLE(of, ulan_of_tbl);

static struct platform_driver ulan_of_driver = {
        .probe          = ulan_of_probe,
        .remove         = ulan_of_remove,
#ifdef CONFIG_PM
        .suspend        = ulan_of_suspend,
        .resume         = ulan_of_resume,
#endif
        .driver         = {
                .name   = "ul_drv",
                .of_match_table = ulan_of_tbl,
        }
};
#endif /*UL_WITH_OF*/

#ifdef UL_WITH_AMBA
static int ulan_amba_probe(struct amba_device *op, const struct amba_id *id);
static KC_AMBA_REMOVE_RET ulan_amba_remove(struct amba_device *op);
#ifdef CONFIG_PM
static int ulan_amba_suspend(struct device *dev);
static int ulan_amba_resume(struct device *dev);
static SIMPLE_DEV_PM_OPS(ulan_amba_dev_pm_ops, ulan_amba_suspend, ulan_amba_resume);
#endif

MODULE_DEVICE_TABLE(amba, ulan_amba_tbl);

static struct amba_driver ulan_amba_driver = {
	.drv = {
		.name	= "uart-pl011",
#ifdef CONFIG_PM
		.pm	= &ulan_amba_dev_pm_ops,
#endif
		.suppress_bind_attrs = IS_BUILTIN(CONFIG_SERIAL_AMBA_PL011),
	},
	.id_table	= ulan_amba_tbl,
	.probe		= ulan_amba_probe,
	.remove		= ulan_amba_remove,
};
#endif /*UL_WITH_AMBA*/

#ifdef UL_WITH_USB

#if (LINUX_VERSION_CODE < VERSION(2,5,41))
static void *ul_usb_probe(struct usb_device *dev, unsigned int ifnum, const struct usb_device_id *id);
static void ul_usb_disconnect(struct usb_device *dev, void *ptr);
#else /* 2.5.41 */
static int ul_usb_probe(struct usb_interface *intf, const struct usb_device_id *id);
static void ul_usb_disconnect(struct usb_interface *intf);
#endif /* 2.5.41 */

#if defined(CONFIG_PM) && (LINUX_VERSION_CODE > VERSION(2,6,18))
static int ul_usb_suspend(struct usb_interface *intf, pm_message_t message);
static int ul_usb_resume(struct usb_interface *intf);
#else
#define ul_usb_suspend NULL
#define ul_usb_resume NULL
#endif /* CONFIG_PM */

MODULE_DEVICE_TABLE (usb, ulan_usb_tbl);

#ifdef CONFIG_USB_DYNAMIC_MINORS
/* 
 * if the user wants to use dynamic minor numbers, then we can have up to 256
 * devices
 */
#define UL_USB_MINOR_BASE	0
#define MAX_DEVICES		256
#else
/* Get a minor range for your devices from the usb maintainer */
#define UL_USB_MINOR_BASE	200

/* we can have up to this number of device plugged in at once */
#define MAX_DEVICES		16
#endif

/* usb specific object needed to register this driver with the usb subsystem */
static struct usb_driver ul_usb_driver = {
	name:		"ulan_usb",
	probe:		ul_usb_probe,
	disconnect:	ul_usb_disconnect,
     #if (LINUX_VERSION_CODE > VERSION(2,5,8))
	suspend:	ul_usb_suspend,
	resume:		ul_usb_resume,
     #if (LINUX_VERSION_CODE > VERSION(2,6,23))
	reset_resume:	ul_usb_resume,
     #endif
     #endif
	/*fops:		&ul_usb_fops,*/
	/* num_minors:	MAX_DEVICES,*/
     #if (LINUX_VERSION_CODE < VERSION(2,5,8))
	minor:		UL_USB_MINOR_BASE,
     #endif
	id_table:	ulan_usb_tbl,
};

#endif /*UL_WITH_USB*/


static int ulan_open(struct inode *inode, struct file *file);
static CLOSERET ulan_close(struct inode *inode, struct file *file);
static long ulan_ioctl(struct file *file,
			unsigned int cmd, unsigned long arg);
#if (LINUX_VERSION_CODE < VERSION(2,6,36))
static int ulan_oldapi_ioctl(struct inode *inode, struct file *file,
			unsigned int cmd, unsigned long arg);
#endif /* 2.6.36 */
#ifdef CONFIG_COMPAT
static long ulan_compat_ioctl(struct file *file, unsigned int cmd,
			unsigned long arg);
#endif
static RWRET ulan_write(WRITE_PARAMETERS);
static RWRET ulan_read(READ_PARAMETERS);
static loff_t ulan_lseek(LSEEK_PARAMETERS);
static unsigned int ulan_poll(struct file *, poll_table *);

/* static struct file_operations ulan_fops */
KC_CHRDEV_FOPS_BEG(ulan_fops)
	KC_FOPS_LSEEK(ulan_lseek)/* lseek              */
	read:ulan_read,		/* read                */
	write:ulan_write,	/* write               */
	poll:ulan_poll,		/* poll                */
#if (LINUX_VERSION_CODE < VERSION(2,6,36))
	ioctl:ulan_oldapi_ioctl,	/* ioctl               */
#else
#warning uLan driver locking needs adaptation for 2.6.36+ Linux kernel version
	unlocked_ioctl:ulan_ioctl,	/* ioctl               */
#endif
#ifdef CONFIG_COMPAT
        compat_ioctl:ulan_compat_ioctl,
#endif
	mmap:NULL,		/* mmap                */
	open:ulan_open,		/* open                */
	KC_FOPS_FLUSH(NULL)	/* flush               */
	KC_FOPS_RELEASE(ulan_close)/* close/release    */
	fsync:NULL,		/* fsync               */
	fasync:NULL,		/* fasync              */
KC_CHRDEV_FOPS_END;


/* open ulan device */
static int ulan_open(struct inode *inode, struct file *file)
{ 
  ul_drv *udrv;
  ul_opdata *opdata;
  unsigned int minor = kc_dev2minor(inode->i_rdev);
  #ifdef UL_WITH_DEVFS
  if(minor==-1){
    udrv=file->private_data;
  }else
  #endif /* UL_WITH_DEVFS */
  {
    if ((minor >= UL_MINORS)||(minor<0)) return -ENODEV;
    udrv=ul_drv_arr[minor];
  }
  if (!udrv) return -ENODEV;
  if(udrv->magic!=UL_DRV_MAGIC)
  {
    printk(KERN_CRIT "ulan_open: Wrong uLan MAGIC number!!!\n");
    return -ENODEV;
  }

  kc_MOD_INC_USE_COUNT;
  if(!(file->private_data=MALLOC(sizeof(ul_opdata))))
  { kc_MOD_DEC_USE_COUNT;
    return(-ENOMEM);
  };
  opdata=(ul_opdata*)file->private_data;
  opdata->file=file;
  opdata->magic=ULOP_MAGIC;
  opdata->message=NULL;
  opdata->udrv=udrv;
  opdata->subdevidx=0;
  opdata->pro_mode=0;
  init_waitqueue_head(&opdata->wqrec);
  opdata->opprew=NULL;
  opdata->opnext=NULL;
  opdata->recchain=NULL;
  opdata->filtchain=NULL;
  { /* add us onto list of clients of udrv */
    ul_opdata *opptr;
    UL_DRV_LOCK_FINI
    UL_DRV_LOCK;
    opptr=udrv->operators;
    if(opptr) {opptr->opprew=opdata;opdata->opnext=opptr;};
    UL_MB();
    udrv->operators=opdata;
    UL_DRV_UNLOCK;
  };

  return 0;
};

static CLOSERET ulan_close(struct inode *inode, struct file *file)
{
  ul_opchain *opmember;
  ul_opdata *opdata=(ul_opdata*)file->private_data;
  if(opdata->magic!=ULOP_MAGIC) panic("ulan_close : BAD opdata magic !!!");

  if (opdata->message) ulan_freemsg(opdata);
  { /* delete us from list of clients of udrv */
    ul_drv *udrv;
    ul_opdata *opptr;
    UL_DRV_LOCK_FINI
    UL_DRV_LOCK;
    if((udrv=opdata->udrv)!=NULL){
      if((opptr=opdata->opnext)) opptr->opprew=opdata->opprew;
      if((opptr=opdata->opprew)) opptr->opnext=opdata->opnext;
      else udrv->operators=opdata->opnext;
    }
    UL_DRV_UNLOCK;
    if(udrv)
      if(udrv->irq>0) ul_synchronize_irq(udrv->irq);
    while((udrv=opdata->udrv)!=NULL){
      if(!uld_atomic_test_dfl(udrv,IN_BOTTOM))
        break;
      kc_yield();
    }
  }
  schedule();
  while((opmember=opdata->recchain))
  {
    del_from_opchain(&opdata->recchain,opmember);
    if(opmember->message) ul_dec_ref_cnt(opmember->message);
    FREE(opmember);
  };
  while((opmember=opdata->filtchain))
  {
    del_from_opchain(&opdata->filtchain,opmember);
    FREE(opmember);
  };
  FREE(file->private_data);
  kc_MOD_DEC_USE_COUNT;
  return (CLOSERET)0;
};

static long ulan_ioctl(struct file *file, 
			unsigned int cmd, unsigned long arg)
{
  ul_msginfo msginfo;
  int ret;
  ul_opdata *opdata=(ul_opdata*)file->private_data;
  if(opdata->magic!=ULOP_MAGIC) panic("ulan_ioctl : BAD opdata magic !!!");

  LOG_FILEIO("ulan_ioctl:IOCTL %d\n", cmd);

  switch (cmd) {
   case UL_DRV_VER :
	return UL_DRV_VERCODE;
   case UL_NEWMSG :
	if(kc_copy_from_user(&msginfo,(void *)arg,sizeof(msginfo)))
	   return -EFAULT;
  	return ulan_newmsg(opdata,&msginfo);
   case UL_TAILMSG :
	if(kc_copy_from_user(&msginfo,(void *)arg,sizeof(msginfo)))
	   return -EFAULT;
  	return ulan_tailmsg(opdata,&msginfo);
   case UL_FREEMSG :
  	return ulan_freemsg(opdata);
   case UL_ACCEPTMSG :  	
	if(kc_copy_to_user((void *)arg,&msginfo,sizeof(msginfo)))
	   return -EFAULT;
	ret=ulan_acceptmsg(opdata,&msginfo);
	if(kc_copy_to_user((void *)arg,&msginfo,sizeof(msginfo)))
	   return -EFAULT;
  	return  ret;
   case UL_ACTAILMSG :  	
	if(kc_copy_to_user((void *)arg,&msginfo,sizeof(msginfo)))
	   return -EFAULT;
	ret=ulan_actailmsg(opdata,&msginfo);
	if(kc_copy_to_user((void *)arg,&msginfo,sizeof(msginfo)))
	   return -EFAULT;
  	return  ret;
   case UL_ADDFILT :
	if(kc_copy_from_user(&msginfo,(void *)arg,sizeof(msginfo)))
	   return -EFAULT;
  	return ulan_addfilt(opdata,&msginfo);
   case UL_ABORTMSG :
  	return ulan_abortmsg(opdata);
   case UL_REWMSG :
  	return ulan_rewmsg(opdata);
   case UL_KLOGBLL :
        if(!opdata->udrv) return -ENODEV;
	printudrvbll(opdata->udrv);
    #ifdef ENABLE_UL_MEM_CHECK
	printudrvoperators(opdata->udrv);
    #endif /* ENABLE_UL_MEM_CHECK */
	return 0;
   case UL_STROKE :
        if(!opdata->udrv) return -ENODEV;
	ulan_stroke(opdata->udrv, 1);
	return 0;
   case UL_DEBFLG :
	uld_debug_flg=arg;
	return 0;
   case UL_INEPOLL :
        return ulan_inepoll(opdata);
   case UL_SETMYADR :
        return ulan_setmyadr(opdata,arg);
  #ifdef UL_WITH_IAC
   case UL_SETIDSTR :{
	  char idstr[41];
	  char ch;
	  int ln=0;
	  do{
	    if(kc_get_user_byte(ch,(char*)arg))
	      return -EFAULT;
	    arg++;
	    if(ln>=sizeof(idstr)-1)
	      return -EINVAL;
	    idstr[ln++]=ch;
	  }while(ch);
	  idstr[ln]=0;
          return ulan_setidstr(opdata,idstr);
	}
  #endif /*UL_WITH_IAC*/
   case UL_SETBAUDRATE :
        return ulan_setbaudrate(opdata,arg);
   case UL_SETPROMODE :
        return ulan_setpromode(opdata,arg);
  #ifdef CONFIG_OC_UL_DRV_WITH_MULTI_DEV
   case UL_SETSUBDEV :
        return ulan_setsubdev(opdata,arg);
  #endif /*CONFIG_OC_UL_DRV_WITH_MULTI_DEV*/
   case UL_QUERYPARAM :{
          unsigned long value;
          if(ulan_queryparam(opdata,arg,&value) < 0)
            return -EINVAL;
          return value;
        }
  #ifdef CONFIG_OC_UL_DRV_WITH_MULTI_NET
   case UL_ROUTE :{
        ul_route_range_t rr;
	if(kc_copy_from_user(&rr,(void *)arg,sizeof(rr)))
	   return -EFAULT;
        ret=ulan_route(opdata,&rr);
	if(kc_copy_to_user((void *)arg,&rr,sizeof(rr)))
	   return -EFAULT;
        return ret;
        }
  #endif /*CONFIG_OC_UL_DRV_WITH_MULTI_NET*/

   case UL_HWTEST :
        if(!kc_capable(CAP_SYS_ADMIN)) return -EPERM;
	return ulan_hwtest(opdata,arg);
  };


  return -EINVAL;
};

#if (LINUX_VERSION_CODE < VERSION(2,6,36))
static int ulan_oldapi_ioctl(struct inode *inode, struct file *file, 
			unsigned int cmd, unsigned long arg)
{
  return ulan_ioctl(file, cmd, arg);
}
#endif /* 2.6.36 */

#ifdef CONFIG_COMPAT
static long ulan_compat_ioctl(struct file *file, unsigned int cmd,
				unsigned long arg)
{
  LOG_FILEIO("ulan_ioctl:Compatability IOCTL %d\n", cmd);

  return ulan_ioctl(file, cmd, arg);
}
#endif


/* common parameters (struct file *file, const char *buf, 
                      size_t count [, loff_t *ppos]) */
static RWRET ulan_write(WRITE_PARAMETERS)
{
  int cn;
  int len;
  ul_mem_blk *blk;
  ul_opdata *opdata=(ul_opdata*)file->private_data;
  if(opdata->magic!=ULOP_MAGIC) panic("ulan_write : BAD opdata magic !!!");

  if(!opdata->udrv) return -ENODEV;
  if(!opdata->message) return -ENOMSG;

  cn=count;
  while(cn>0)
  {
    while(!ul_di_adjust(&opdata->data))
    {
      if(!(blk=ul_alloc_blk(opdata->udrv))) 
	{count-=cn;cn=0;break;};
      memset(UL_BLK_NDATA(blk),0,UL_BLK_SIZE);
      opdata->data.blk->next=blk;
    };
    len=ul_di_atonce(&opdata->data);
    if(len>cn) len=cn;
    if(kc_copy_from_user(ul_di_byte(&opdata->data),buf,len))
      return -EFAULT;
    ul_di_add(&opdata->data,len);
    buf+=len;
    cn-=len;
  };

  if(opdata->data.pos>UL_BLK_HEAD(opdata->data.head_blk).len)
    UL_BLK_HEAD(opdata->data.head_blk).len=opdata->data.pos;

  return count-cn;
};

/* common parameters (struct file *file, char *buf, 
                      size_t count [, loff_t *ppos]) */
static RWRET ulan_read(READ_PARAMETERS)
{
  int cn;
  int len;
  ul_opdata *opdata=(ul_opdata*)file->private_data;
  if(opdata->magic!=ULOP_MAGIC) panic("ulan_read : BAD opdata magic !!!");

  if(!opdata->udrv) return -ENODEV;
  if(!opdata->message) return -ENOMSG;

  if(opdata->data.pos+count>UL_BLK_HEAD(opdata->data.head_blk).len)
   count=UL_BLK_HEAD(opdata->data.head_blk).len-opdata->data.pos;

  cn=count;
  while(cn>0)
  {
    if(!ul_di_adjust(&opdata->data))
    {
      while(cn--) kc_put_user(0,buf++);
      break;
    };
    len=ul_di_atonce(&opdata->data);
    if(len>cn) len=cn;
    if(kc_copy_to_user(buf,ul_di_byte(&opdata->data),len))
      return -EFAULT;
    ul_di_add(&opdata->data,len);
    buf+=len;
    cn-=len;
  };

  return count-cn;
};

/* common parameters (struct file *file, loff_t pos, int whence) */
static loff_t ulan_lseek(LSEEK_PARAMETERS)
{
  ul_opdata *opdata=(ul_opdata*)file->private_data;
  if(opdata->magic!=ULOP_MAGIC) panic("ulan_lseek : BAD opdata magic !!!");

  if(!opdata->message) return -ENOMSG;

  return ul_di_seek(&opdata->data,pos,whence);
};

static unsigned int ulan_poll(struct file *file, poll_table *wait)
{
  /* POLLIN | POLLOUT | POLLERR | POLLHUP | POLLRDNORM | POLLWRNORM */
  unsigned int mask = 0;
  ul_opdata *opdata=(ul_opdata*)file->private_data;
  if(opdata->magic!=ULOP_MAGIC) panic("ulan_poll : BAD opdata magic !!!");

  kc_poll_wait(file, &opdata->wqrec, wait);

  if (opdata->recchain)
    mask |= (POLLIN | POLLRDNORM);

  if (!opdata->udrv)
    mask |= (POLLIN | POLLOUT | POLLERR);

  return mask;
};

#if defined(UL_WITH_PCI) || defined(UL_WITH_USB) || defined(UL_WITH_OF)

static int /*__devinit*/ 
 ulan_init_find_minor(const char *abus, const char *aslot, const char *asubdev,
                      int *pminor, int *ppar)
{
  int i;
  int abus_len;
  int aslot_len;
  int sub_offs;
  int minor;
  int par;
  int match=0;
  char *islot;
  abus_len=strlen(abus);
  aslot_len=strlen(aslot);
  sub_offs=abus_len+aslot_len;
  *pminor=*ppar=minor=par=-1;
  for(i=0;i<UL_MINORS;i++){
    islot=slot[i];
    if(islot)
      if(!*islot) islot=NULL;
    switch (match){
      case 0: 
        if(!ul_drv_arr[i]&&!islot){
	  minor=i;match=1;
	  if(par==-1) par=i;
	}
      case 1:
        if(!islot) continue;
        if(!strcmp(islot,abus)){
	  par=i;
	  if(!ul_drv_arr[i]){ minor=i; match=2;}
	}
      case 2:
        if(!islot||!aslot) continue;
        if(!strncmp(islot,abus,abus_len)&&
	   !strcmp(islot+abus_len,aslot)){
	  par=i;
	  if(!ul_drv_arr[i]){ minor=i; match=3;}
        }
      case 3:
        if(!islot||!aslot||!asubdev) continue;
        if(!strncmp(islot,abus,abus_len)&&
	   !strncmp(islot+abus_len,aslot,aslot_len)&&
	   (islot[sub_offs]=='.')&&!strcmp(islot+sub_offs+1,asubdev)){
	  par=i;
	  if(!ul_drv_arr[i]){ minor=i; match=4;}
        }
    }
  }
  *pminor=minor;*ppar=par;
  return match;
}

#endif /*defined(UL_WITH_PCI)||defined(UL_WITH_USB)*/


/*** initialize kernel module ***/
int init_module(void)
{
 #ifdef UL_WITH_DEVFS
  kc_devfs_handle_t devfs_handle;
  char devname[32];
 #endif /* UL_WITH_DEVFS */
  ul_drv* udrv;
  int ret;
  int minor;

  printk(KERN_INFO "uLan v" UL_DRV_VERSION " init_module\n");
 #if 0
  /* Disabled due -Werror=date-time used for reproducible builds by kernel */
  printk(KERN_INFO "Compiled at %s %s\n",__DATE__, __TIME__);
 #endif
  if(debug!=-1){
    uld_debug_flg=debug;
  }

  ulan_class = kc_class_create(THIS_MODULE, "ulan");

  do {
   #ifdef UL_WITH_DEVFS
    ret = devfs_register_chrdev(ulan_major_dev, "ulan", &ulan_fops);
   #else /* UL_WITH_DEVFS */
    ret = register_chrdev(ulan_major_dev, "ulan", &ulan_fops);
   #endif /* UL_WITH_DEVFS */
    if (ret < 0) {
      if(ulan_major_dev) {
        printk(KERN_WARNING "uLan init : device major number %d taken, using auto\n",
	       ulan_major_dev);
        ulan_major_dev = 0;
      } else {
        printk(KERN_ERR "uLan init : unable to grab device major number\n");
        goto error_register_chrdev;
      }
    }
  } while(ret < 0);
  if(!ulan_major_dev)
    ulan_major_dev=ret;

  ret=0;
 #ifdef UL_WITH_UARTS
  for(minor=0;minor<UL_MINORS;minor++)
  {
    if(port[minor]||chip[minor])
    { /* get new ul_drv (port, irq, baud, my_adr); */
      udrv=ul_drv_new_paddr(ul_uint2physaddr(port[minor]),irq[minor],
   		      baud[minor],my_adr[minor],chip[minor],baudbase[minor]);
      ul_drv_arr[minor]=udrv;
      if(udrv){
	#ifdef UL_WITH_DEVFS
	  sprintf (devname, "ulan%d", minor);
          devfs_handle=kc_devfs_new_cdev(NULL, MKDEV(ulan_major_dev, minor), 
			S_IFCHR | S_IRUSR | S_IWUSR | S_IRGRP | S_IWGRP, 
			&ulan_fops, udrv, devname);
	  udrv->devfs_handle=devfs_handle;
	#endif /* UL_WITH_DEVFS */
	ret++;
	kc_class_device_create(ulan_class, NULL, MKDEV(ulan_major_dev, minor), NULL, "ulan%d", minor);
      }
    }
  }
 #endif /*UL_WITH_UARTS*/

 #ifdef UL_WITH_PCI
  { int rc;
    rc=pci_register_driver (&ulan_pci_driver);
    ret+=rc;
  }
 #endif  /* UL_WITH_PCI */

 #ifdef UL_WITH_OF
  { int rc;
    rc = platform_driver_register(&ulan_of_driver);
    if (rc < 0) {
      printk(KERN_CRIT "platform_driver_register failed for the "__FILE__" driver. Error number %d",
	 rc);
    } else {
      ret++;
    }
  }
 #endif /*UL_WITH_OF*/

 #ifdef UL_WITH_AMBA
  { int rc;
    rc = amba_driver_register(&ulan_amba_driver);
    if (rc < 0) {
      printk(KERN_CRIT "amba_driver_register failed for the "__FILE__" driver. Error number %d",
	 rc);
    } else {
      ret++;
    }
  }
 #endif /*UL_WITH_AMBA*/

 #ifdef UL_WITH_USB
  /* register this driver with the USB subsystem */
  { int rc;
    rc = usb_register(&ul_usb_driver);
    if (rc < 0) {
      printk(KERN_CRIT "usb_register failed for the "__FILE__" driver. Error number %d",
	 rc);
    } else {
      ret++;
    }
  }
 #endif  /* UL_WITH_USB */

  if(!ret) {
    printk(KERN_ERR "uLan module_init : no hardware initialized, aborting !!!\n");
    ret = -ENODEV;
    goto error_no_hardware;
  }


  printk(KERN_INFO "uLan init : driver succesfully registered as major %d\n",
	          ulan_major_dev);

  #ifdef UL_INT_TST
  udrv=ul_drv_arr[0];
  if(udrv)
  { printk("uLan : geninfoblk\n");
    geninfoblk(udrv);
    printk("uLan : gentestblk\n");
    gentestblk(udrv);
    printk("uLan : uld_timeout\n");
    uld_timeout(udrv);
  };
  #endif /* UL_INT_TST */

  return 0;

error_no_hardware:
 #ifdef UL_WITH_USB
  usb_deregister(&ul_usb_driver);
 #endif  /* UL_WITH_USB */
 #ifdef UL_WITH_OF
  platform_driver_unregister(&ulan_of_driver);
 #endif /*UL_WITH_OF*/
 #ifdef UL_WITH_AMBA
  amba_driver_unregister(&ulan_amba_driver);
 #endif /*UL_WITH_AMBA*/
 #ifdef UL_WITH_PCI
  pci_unregister_driver (&ulan_pci_driver);
 #endif  /* UL_WITH_PCI */

  for(minor=0;minor<UL_MINORS;minor++) {
    udrv=ul_drv_arr[minor];
    if(udrv) {
      ul_drv_arr[minor]=NULL;
     #ifdef UL_WITH_DEVFS
      if(udrv->devfs_handle) kc_devfs_delete(udrv->devfs_handle);
     #endif /* UL_WITH_DEVFS */
      kc_class_device_destroy(ulan_class, MKDEV(ulan_major_dev, minor));
      ul_drv_free(udrv);
    }
  }

  if (ulan_major_dev != 0) {
   #ifdef UL_WITH_DEVFS
    devfs_unregister_chrdev(ulan_major_dev, "ulan");
   #else /* UL_WITH_DEVFS */
    unregister_chrdev(ulan_major_dev, "ulan");
   #endif /* UL_WITH_DEVFS */
  };

error_register_chrdev:

  kc_class_destroy(ulan_class);

  return ret;
}

/*** cleanup kernel module ***/
void cleanup_module(void)
{
  ul_drv* udrv;
  int i;

  if (ulan_major_dev != 0) {
   #ifdef UL_WITH_DEVFS
    devfs_unregister_chrdev(ulan_major_dev, "ulan");
   #else /* UL_WITH_DEVFS */
    unregister_chrdev(ulan_major_dev, "ulan");
   #endif /* UL_WITH_DEVFS */
  };

  udrv=ul_drv_arr[0];
  if(udrv)
  { printk("uLan : printudrvbll\n");
    printudrvbll(udrv);
   #ifdef UL_WITH_FRAME_FSM
    printk("uLan : max cycle cnt  %d\n",ul_max_cycle_cnt);
    printk("uLan : max cycle time %d\n",ul_max_cycle_time);
   #endif /*UL_WITH_FRAME_FSM*/
  }

 #ifdef UL_WITH_USB
  usb_deregister(&ul_usb_driver);
 #endif  /* UL_WITH_USB */
 #ifdef UL_WITH_OF
  platform_driver_unregister(&ulan_of_driver);
 #endif /*UL_WITH_OF*/
 #ifdef UL_WITH_AMBA
  amba_driver_unregister(&ulan_amba_driver);
 #endif /*UL_WITH_AMBA*/
 #ifdef UL_WITH_PCI
  pci_unregister_driver (&ulan_pci_driver);
 #endif  /* UL_WITH_PCI */

  for(i=0;i<UL_MINORS;i++)
  { udrv=ul_drv_arr[i];
    ul_drv_arr[i]=NULL;
    if(udrv){
     #ifdef UL_WITH_DEVFS
     if(udrv->devfs_handle) kc_devfs_delete(udrv->devfs_handle);
     #endif /* UL_WITH_DEVFS */
     kc_class_device_destroy(ulan_class, MKDEV(ulan_major_dev, i));
     ul_drv_free(udrv);
    }
  }

  kc_class_destroy(ulan_class);

 #ifdef ENABLE_UL_MEM_CHECK
  UL_PRINTF("MEM_CHECK: malloc-free=%d\n",
		 (int)atomic_read(&ul_mem_check_counter));
 #endif /* ENABLE_UL_MEM_CHECK */
}

#ifdef ENABLE_UL_MEM_CHECK
void * ul_mem_check_malloc(size_t size)
{void *ptr;
 ptr=kmalloc(size,GFP_KERNEL);
 if(ptr) atomic_inc(&ul_mem_check_counter);
 return ptr;
}

void ul_mem_check_free(void *ptr)
{
  if(ptr) atomic_dec(&ul_mem_check_counter);
  kfree(ptr);
}
#endif /* ENABLE_UL_MEM_CHECK */

/*** IO mapping functions for kernel module ***/

int ul_io_map(ul_physaddr_t physaddr, unsigned long len, const char *name, ul_ioaddr_t *ioaddr)
{
  if ((physaddr.physaddr&~UL_PHYSADDR_PIO_MASK) == 0) {
   #ifndef CONFIG_HAS_IOPORT_MAP
    return -1;
   #else /*CONFIG_HAS_IOPORT_MAP*/
    if (!kc_request_region(physaddr.physaddr, len, name))
      return -1;
    ioaddr->ioaddr=ioport_map(physaddr.physaddr, len);
    return 0;
   #endif /*CONFIG_HAS_IOPORT_MAP*/
  } else {
    ioaddr->ioaddr=ioremap(physaddr.physaddr, len);
    if (ioaddr->ioaddr==NULL)
      return -1;
    return 0;
  }
}

void ul_io_unmap(ul_physaddr_t physaddr, unsigned long len, ul_ioaddr_t ioaddr)
{
  if ((physaddr.physaddr&~UL_PHYSADDR_PIO_MASK) == 0) {
   #ifndef CONFIG_HAS_IOPORT_MAP
    return;
   #else /*CONFIG_HAS_IOPORT_MAP*/
    ioport_unmap(ioaddr.ioaddr);
    kc_release_region(physaddr.physaddr, len);
   #endif /*CONFIG_HAS_IOPORT_MAP*/
  } else {
    iounmap(ioaddr.ioaddr);
  }
}
