/*******************************************************************
  uLan Communication - low level C and driver interface library

  ul_netbase.c	- basic network and module constructions

  (C) Copyright 2001 by Pavel Pisa - Originator

  (C) Copyright 2002-2004 by Pavel Pisa - Originator

  The uLan C interface library can be used, copied and modified
  under next licenses
    - GPL - GNU General Public License
    - LGPL - GNU Lesser General Public License
    - MPL - Mozilla Public License
    - and other licenses added by project originators
  Code can be modified and re-distributed under any combination
  of the above listed licenses. If contributor does not agree with
  some of the licenses, he/she can delete appropriate line.
  Warning, if you delete all lines, you are not allowed to
  distribute source code and/or binaries utilizing code.
  
  See files COPYING and README for details.

 *******************************************************************/

#ifndef _UL_MSG_BUF_H
#define _UL_MSG_BUF_H

#ifdef __cplusplus
extern "C" {
#endif

#include <ul_lib/ulan.h>
#include <ul_dbuff.h>


typedef struct ul_msg_buf{	/* buffer to hold full message taken from driver */
  ul_msginfo msginfo;		/* dadr, sadr, cmd, flg, len, stamp */
  ul_dbuff_t  data;		/* message data */
  struct ul_msg_buf *tail;	/* optional tail message */
} ul_msg_buf_t;


int ul_msg_buf_init(ul_msg_buf_t *buf);
void ul_msg_buf_destroy(ul_msg_buf_t *buf);
int ul_msg_buf_rd_data(ul_msg_buf_t *buf, ul_fd_t ul_fd);
int ul_msg_buf_rd_rest(ul_msg_buf_t *buf, ul_fd_t ul_fd);
int ul_msg_buf_wr(ul_msg_buf_t *buf, ul_fd_t ul_fd);

#ifdef __cplusplus
} /* extern "C"*/
#endif

#endif /* _UL_MSG_BUF_H */
