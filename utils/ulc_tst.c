/*******************************************************************
  uLan Communication - simple test client

  ulc_tst.c	- check of driver interface

  (C) Copyright 1996,1999 by Pavel Pisa 

  The uLan driver is distributed under the Gnu General Public Licence. 
  See file COPYING for details.
 *******************************************************************/

#include <unistd.h>
#include <stdlib.h>
#include <fcntl.h>
#include <stdio.h>
#include <sys/types.h>
#include <sys/time.h>
#include <sys/stat.h>
#include <errno.h>
#include <ul_lib/ulan.h>
#include <sys/select.h>

#ifdef CONFIG_OC_UL_DRV_NUTTX
#ifndef S_IWRITE
#define S_IWRITE 0
#endif
#ifndef S_IREAD
#define S_IREAD 0
#endif
#endif /*CONFIG_OC_UL_DRV_NUTTX*/

char *ul_dev_name = UL_DEV_NAME;

int test1(void)
{
 uchar ch;
 int ul_fd;
 int i;
 ul_msginfo msginfo;
 uchar buf1[40]={1,2,3,4,5,6,7,8,9,10};
 uchar buf2[40]={11,12,13,14,15,16,17,18,19,20};

 printf("\n\n*** Simple message send test with read, write and lseek ***\n");

 ul_fd=open(ul_dev_name, O_RDWR, S_IWRITE | S_IREAD);
 if(ul_fd<0) { perror("test1 : open failed");return -1;};
 
 write(ul_fd,buf2,4);
 perror("test1 : write ");

 memset(&msginfo,0,sizeof(msginfo));
 msginfo.dadr=3; msginfo.cmd=10;
 ioctl(ul_fd,UL_NEWMSG,&msginfo);

 write(ul_fd,buf1,10);
 lseek(ul_fd,100,SEEK_SET);
 write(ul_fd,buf2,10);
 
 memset(buf1,0,sizeof(buf1));
 memset(buf2,0,sizeof(buf1));
 
 lseek(ul_fd,4,SEEK_SET);
 printf("data from pos 4\n");
 for(i=0;i<20;i++) {read(ul_fd,&ch,1); printf(" %d",ch);}
 printf("\n");

 lseek(ul_fd,100,SEEK_SET);
 printf("data from pos 100\n");
 for(i=0;i<20;i++) {if(read(ul_fd,&ch,1)<=0) break; printf(" %d",ch);}
 printf("\n");

 ioctl(ul_fd,UL_FREEMSG);

 ioctl(ul_fd,UL_STROKE);

 sleep(5);

 ioctl(ul_fd,UL_KLOGBLL);

 close(ul_fd);
 return 0;
}

int test2(void)
{
 int ul_fd;
 int num_act;
 int ret;
 ul_msginfo msginfo;
 fd_set set;
 uchar buf[0x40];
 struct timeval timeout;

 printf("\n\n*** Tailed processed mesage test ***\n");

 ul_fd=open(ul_dev_name, O_RDWR, S_IWRITE | S_IREAD);
 if(ul_fd<0) { perror("test1 : open failed");return -1;};

 printf("preparing first part\n");
 msginfo.dadr=3;
 msginfo.cmd=UL_CMD_RDM;
 msginfo.flg=UL_BFL_ARQ|UL_BFL_PRQ|UL_BFL_M2IN;
 ioctl(ul_fd,UL_NEWMSG,&msginfo);
 buf[0]=(uchar)2;	buf[1]=2>>8;
 buf[2]=(uchar)0x8800;	buf[3]=0x8800>>8;
 buf[4]=(uchar)0x40;	buf[5]=0x40>>8;
 if(write(ul_fd,buf,6)!=6)
   perror("test2 : write ");

 if(0)
 {
  int i;
  uchar ch;
  lseek(ul_fd,0,SEEK_SET);
  printf("data from pos 0\n");
  for(i=0;i<20;i++) {if(read(ul_fd,&ch,1)<=0) break; printf(" %d",ch);}
  printf("\n");
 };

 printf("preparing second part\n");
 msginfo.dadr=3;
 msginfo.cmd=UL_CMD_RDM&0x7F;
 msginfo.flg=UL_BFL_REC|UL_BFL_LNMM|UL_BFL_M2IN;
 msginfo.len=0x40;
 ioctl(ul_fd,UL_TAILMSG,&msginfo);

 ioctl(ul_fd,UL_FREEMSG);

 ioctl(ul_fd,UL_KLOGBLL);

 ioctl(ul_fd,UL_STROKE);

 FD_ZERO (&set);
 FD_SET (ul_fd, &set);
 timeout.tv_sec = 10;
 timeout.tv_usec = 0;

 printf("Sending tailed message an waiting for input\n");

 while ((num_act=select(FD_SETSIZE,&set, NULL, NULL,&timeout))==-1
         &&errno==-EINTR);

 printf("select returned %d\n",num_act);

 memset(&msginfo,0,sizeof(msginfo));
 ret=ioctl(ul_fd,UL_ACCEPTMSG,&msginfo);
 printf("accept message returned %d\n",ret);
 printf("  flg=0x%X, dadr=%d, sadr=%d, cmd=%d, len=%d, stamp=%d\n",
           msginfo.flg,msginfo.dadr,msginfo.sadr,msginfo.cmd,
           msginfo.len,msginfo.stamp);
 
 memset(&msginfo,0,sizeof(msginfo));
 ret=ioctl(ul_fd,UL_ACTAILMSG,&msginfo);
 printf("accept tail returned %d\n",ret);
 printf("  flg=0x%X, dadr=%d, sadr=%d, cmd=%d, len=%d, stamp=%d\n",
           msginfo.flg,msginfo.dadr,msginfo.sadr,msginfo.cmd,
           msginfo.len,msginfo.stamp);

 close(ul_fd);

 return 0;
}

int test3(void)
{
 int ul_fd;
 int ret;
 ul_fd=open(ul_dev_name, O_RDWR, S_IWRITE | S_IREAD);
 if(ul_fd<0) { perror("test1 : open failed");return -1;};

 ret=ioctl(ul_fd,UL_HWTEST,(unsigned long)0x12);
 printf("get msr/lsr %06X\n",ret);
 ret=ioctl(ul_fd,UL_HWTEST,(unsigned long)0x10);
 printf("set break   %06X\n",ret);
 sleep(2);
 ret=ioctl(ul_fd,UL_HWTEST,(unsigned long)0x12);
 printf("get msr/lsr %06X\n",ret);
 ret=ioctl(ul_fd,UL_HWTEST,(unsigned long)0x13);
 printf("set tx off  %06X\n",ret);
 sleep(2);
 ret=ioctl(ul_fd,UL_HWTEST,(unsigned long)0x12);
 printf("get msr/lsr %06X\n",ret);
 ret=ioctl(ul_fd,UL_HWTEST,(unsigned long)0x11);
 printf("set one     %06X\n",ret);
 sleep(2);
 ret=ioctl(ul_fd,UL_HWTEST,(unsigned long)0x12);
 printf("get msr/lsr %06X\n",ret);
 sleep(2);
 ret=ioctl(ul_fd,UL_HWTEST,(unsigned long)0x13);
 printf("set tx off  %06X\n",ret);
 sleep(2);
 ret=ioctl(ul_fd,UL_HWTEST,(unsigned long)0x12);
 printf("get msr/lsr %06X\n",ret);

 close(ul_fd);
 return 0;
}


int main(int argc,char *argv[])
{

 if(argc>=2)
  ul_dev_name=argv[1];
 /* test1(); */
 /* test2(); */
 test3();

 return 0;
}
