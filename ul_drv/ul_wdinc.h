#ifndef _UL_WDINC_H
#define _UL_WDINC_H

//
// Device State definitions
//
// These values are used to track the state of the device
// with regard to PnP events.
//
// NOTE: The code makes assumptions about the order of these
// states, and the fact that their values are in the high
// word of a longword.
//
//

//
// We use a set of dummy states to delineate the actions we take
// on receipt and completion of an IRP.  These also appear below.
//

//
// In STATE_REMOVED, we immediately fail any received I/O requests
//
#define STATE_REMOVED           0X00000000

//
// In STATE_SURPRISE_REMOVED, we immediately fail all incoming requests
//
#define STATE_SURPRISE_REMOVED   0x00010000

//
// In STATE_NEVER_STARTED, we also immediately fail all incoming requests
//
#define STATE_NEVER_STARTED     0x00020000

//
// Dummy State -- When the state is < this value, no H/W access is
// allowed
//
#define STATE_ALL_BELOW_NO_HW   0x00030000	// ******************

//
// In STATE_REMOVE_PENDING, we also immediately fail all incoming requests
//
#define STATE_REMOVE_PENDING    0x00100000

//
// Dummy state -- When an IRP arrives at the driver, if the current
// device state is below this value, it is immediately failed
//
#define STATE_ALL_BELOW_FAIL    0x00FF0000	// ******************

//
// In STATE_STARTED, requests are processed and removed from the
// queues normally
//
#define STATE_STARTED           0X01000000

//
// Dummy state -- When an IRP arrives at the driver, if the current
// device state is above this value, it is queued, not initiated on
// the device (even if the device is free)
//
#define STATE_ALL_ABOVE_QUEUE   0x0FFF0000	// ******************

//
// Dummy State -- When an IRP is COMPLETED on the device, if
// the device state is below this value, we'll start another
// IRP in an attempt to drain the queue of pending requests.
//
#define STATE_ALL_BELOW_DRAIN   STATE_ALL_ABOVE_QUEUE	// ******************

//
// In STATE_STOP_PENDING, we queue incoming requests
//
#define STATE_STOP_PENDING      0x10000000

//
// In STATE_STOPPED, we queue incoming requests
//
#define STATE_STOPPED           0x10010000


//
// Forward Declarations
//
//typedef struct ul_drv *PULAN_DEVICE_EXTENSION;
NTSTATUS NTAPI DriverEntry(PDRIVER_OBJECT DriverObj, PUNICODE_STRING RegistryPath);
NTSTATUS NTAPI DispatchPower(PDEVICE_OBJECT DeviceObject, PIRP Irp);
VOID NTAPI DriverUnload(PDRIVER_OBJECT DriverObject);
static VOID ReturnPool(PPCI_COMMON_CONFIG  configInfo, PDEVICE_DESCRIPTION
            deviceDescription,  PCM_RESOURCE_LIST resources);
NTSTATUS NTAPI DispatchPnp(PDEVICE_OBJECT DeviceObject, PIRP Irp);
NTSTATUS NTAPI DispatchSystemControl(PDEVICE_OBJECT DeviceObject, PIRP Irp);
NTSTATUS NTAPI ShutdownRoutine(PDEVICE_OBJECT DeviceObject, PIRP Irp);
NTSTATUS NTAPI AddDevice(IN PDRIVER_OBJECT DriverObject,
                   IN PDEVICE_OBJECT PhysicalDeviceObject);
NTSTATUS MyAllocUnicodeString(PUNICODE_STRING UStr, PUNICODE_STRING InUStr, int Len);
VOID ReturnResources(PULAN_DEVICE_EXTENSION devExt);
VOID RemoveDevice(IN PDEVICE_OBJECT devObj);

BOOLEAN
SetDevicePowerState(
    IN PDEVICE_OBJECT DeviceObject,
    IN DEVICE_POWER_STATE DeviceState
    );
VOID NTAPI
PoRequestCompletion(
    IN PDEVICE_OBJECT       DeviceObject,
    IN UCHAR                MinorFunction,
    IN POWER_STATE          PowerState,
    IN PVOID                Context,
    IN PIO_STATUS_BLOCK     IoStatus
    );
NTSTATUS
PowerIrp_Complete(
    IN PDEVICE_OBJECT NullDeviceObject,
    IN PIRP Irp,
    IN PVOID Context
    );
NTSTATUS
SelfSuspendOrActivate(
    IN PDEVICE_OBJECT DeviceObject,
    IN BOOLEAN fSuspend
    );
VOID NTAPI
PoSelfRequestCompletion(
    IN PDEVICE_OBJECT       DeviceObject,
    IN UCHAR                MinorFunction,
    IN POWER_STATE          PowerState,
    IN PVOID                Context,
    IN PIO_STATUS_BLOCK     IoStatus
    );
NTSTATUS
SelfRequestPowerIrp(
    IN PDEVICE_OBJECT DeviceObject,
    IN POWER_STATE PowerState
    );

//
// Globals used by uLan WDM Driver 
//
UNICODE_STRING uL_WDM_RegistryPath;

#endif /* _UL_WDINC_H */
