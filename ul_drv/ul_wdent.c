/*******************************************************************
  uLan Communication - uL_DRV - multiplatform uLan driver

  ul_wdent.c	- Windows WDM entry and unload code

  (C) Copyright 1996-2004 by Pavel Pisa - project originator
        http://cmp.felk.cvut.cz/~pisa
  (C) Copyright 1996-2004 PiKRON Ltd.
        http://www.pikron.com
  (C) Copyright 2002-2004 Petr Smolik
  

  The uLan driver project can be used and distributed 
  in compliance with any of next licenses
   - GPL - GNU Public License
     See file COPYING for details.
   - LGPL - Lesser GNU Public License
   - MPL - Mozilla Public License
   - and other licenses added by project originator

  Code can be modified and re-distributed under any combination
  of the above listed licenses. If contributor does not agree with
  some of the licenses, he/she can delete appropriate line.
  WARNING: if you delete all lines, you are not allowed to
  distribute code or sources in any form.
 *******************************************************************/

// Unicode String Manipulation routines
NTSTATUS
MyAllocUnicodeString(PUNICODE_STRING UStr, PUNICODE_STRING InUStr, int Len)
{
  NTSTATUS Status;
  ULONG Size;
  if(!Len&&InUStr){
    Len=InUStr->Length;
  }
  RtlInitUnicodeString(UStr, NULL);
  if(Len){
    Size=Len+2;
    UStr->Buffer = ExAllocatePoolWithTag(PagedPool,Size,'uL3');
	if(!UStr->Buffer) return STATUS_UNSUCCESSFUL;
    RtlZeroMemory(UStr->Buffer,Size);
    UStr->MaximumLength = Len;
    if(InUStr)
      RtlCopyUnicodeString(UStr,InUStr);
  }
  return STATUS_SUCCESS;
}

///////////////////////////////////////////////////////////////////////////////
//
//  MyGetRegistryKeyValue
//
//    Reads a registry key value from an already opened registry key
//
//  INPUTS:
//	Handle		- handle to the opened registry key
//	KeyNameString	- WCHAR string to the desired key
//  OUTPUTS:
//	KeyNameStringLength	- length of above
//	Data		- buffer for the data
//	DataLength	- length of the buffer
//
//  RETURNS:
//	NT Status code
//
//  IRQL:
//	IRQL PASSIVE_LEVEL
//
///////////////////////////////////////////////////////////////////////////////
NTSTATUS 
MyGetRegistryKeyValue (
    IN HANDLE	Handle,
    IN PWCHAR	KeyNameString,
    IN PVOID	Data,
    IN ULONG	DataLength
    )
{
    UNICODE_STRING   keyName;
    ULONG            length;
    PKEY_VALUE_FULL_INFORMATION fullInfo;
    NTSTATUS         status = STATUS_INSUFFICIENT_RESOURCES;

    RtlInitUnicodeString (&keyName, KeyNameString);
	
    length = sizeof(KEY_VALUE_FULL_INFORMATION)+keyName.Length+4+DataLength;
    fullInfo = ExAllocatePoolWithTag(PagedPool, length, 'uL4'); 

    if (fullInfo) {
      status = ZwQueryValueKey (Handle,&keyName,KeyValueFullInformation,
	  			fullInfo,length,&length);
		
      if (NT_SUCCESS(status)) {

	// If there is enough room in the data buffer, copy the output
	if (DataLength >= fullInfo->DataLength) 
	{
	  RtlCopyMemory (Data,((PUCHAR) fullInfo) + fullInfo->DataOffset, 
			 fullInfo->DataLength);
	}
      }
      ExFreePool(fullInfo);
    }

    return status;
}


//
// The following pragma allows the DriverEntry code to be discarded once
// initialization is completed
//
//#pragma alloc_text(INIT,DriverEntry)

///////////////////////////////////////////////////////////////////////////////
//
//  DriverEntry
//
//      This routine is called by NT when the driver is first loaded.  It is the
//    responsibility of this routine to find it's device and create whatever
//    device objects it needs.
//
//  INPUTS:
//
//      DriverObj - Address of the DRIVER_OBJECT created by NT for this driver.
//
//      RegistryPath - UNICODE_STRING which represents this drivers KEY in the
//                   Registry.  
//
//  OUTPUTS:
//
//      None.
//
//  RETURNS:
//
//      STATUS_SUCCESS. Otherwise an error indicating why the driver could not
//                    Load.
//
//  IRQL:
//
//    This routine is called at IRQL_PASSIVE_LEVEL.
//
//  NOTES:
//
//
///////////////////////////////////////////////////////////////////////////////
NTSTATUS NTAPI
DriverEntry(PDRIVER_OBJECT DriverObj, PUNICODE_STRING RegistryPath)
{
    ULD_LARGE_INTEGER_0=RtlConvertLongToLargeInteger(0);
    
    uLan_DbgPrint("uLan v" UL_DRV_VERSION " Enter the driver!\n");
    //uLan_DbgPrint("uLan: " __DATE__ " " __TIME__ "\n");

    //
    // Establish dispatch entry points for the functions we support
    //
    DriverObj->MajorFunction[IRP_MJ_CREATE] = DispatchRoutine;
    DriverObj->MajorFunction[IRP_MJ_CLOSE] = DispatchRoutine;
    DriverObj->MajorFunction[IRP_MJ_READ] = DispatchRoutine;
    DriverObj->MajorFunction[IRP_MJ_WRITE] = DispatchRoutine;
    DriverObj->MajorFunction[IRP_MJ_DEVICE_CONTROL] = DispatchRoutine;
    DriverObj->MajorFunction[IRP_MJ_CLEANUP] = DispatchRoutine;

    //
    // PnP and Power entry points
    //
    DriverObj->MajorFunction[IRP_MJ_PNP]            =  DispatchPnp;
#ifdef UL_WITH_WIN_PWR
    DriverObj->MajorFunction[IRP_MJ_POWER]          =  DispatchPower;
#endif /* UL_WITH_WIN_PWR */
    DriverObj->MajorFunction[IRP_MJ_SHUTDOWN]       =  ShutdownRoutine;

    //
    // WMI entry point
    //
    DriverObj->MajorFunction[IRP_MJ_SYSTEM_CONTROL] =  DispatchSystemControl;

    //
    // AddDevice Function
    //
    DriverObj->DriverExtension->AddDevice = AddDevice;

    //
    // Unload function
    //
    DriverObj->DriverUnload = DriverUnload;

    //
    // Save the registry path for later use.
    //
    MyAllocUnicodeString(&uL_WDM_RegistryPath,RegistryPath,0);

    //
    // Initial reasonable value for driver spinlock
    //
    uL_SpinLock_Irql=DISPATCH_LEVEL+1;
  #ifdef UL_KERNEL_SPINLOCK
    KeInitializeSpinLock(&uL_SpinLock);
  #endif
    uLan_DbgPrint("uLan: All initialized!\n");
    
    return(STATUS_SUCCESS);
    
}

///////////////////////////////////////////////////////////////////////////////
//
//  DriverUnload
//
//      This routine is our dynamic unload entry point.  We are called here when
//    the OS wants to unload our driver.  It is our responsibility to release any
//    resources we allocated.
//
//  INPUTS:
//
//      DriverObj - Address of our DRIVER_OBJECT.
//
//  OUTPUTS:
//
//      None.
//
//  RETURNS:
//
//      None.
//
//  IRQL:
//
//    This routine is called at IRQL_PASSIVE_LEVEL.
//
//  NOTES:
//
//    No doubt we pool leak at this entry point by not properly returning everything.
//
///////////////////////////////////////////////////////////////////////////////
VOID NTAPI DriverUnload(PDRIVER_OBJECT DriverObject)
{
    PULAN_DEVICE_EXTENSION devExt;
    PDEVICE_OBJECT devObj,nextDevObj;
    IO_RESOURCE_REQUIREMENTS_LIST reqList;
    NTSTATUS code;
    UNICODE_STRING linkName;
    CM_RESOURCE_LIST returnResources;
    BOOLEAN conflictDetected;
            
    uLan_DbgPrint("uLan: DriverUnload\n");

    //
    // For THIS driver, there will only ever be a single device object.
    // Because of this, we just get it from the DriverObj.  If this were
    // a multiple device driver, we would do this in a while loop...
    //
    nextDevObj = DriverObject->DeviceObject;
    while(nextDevObj){
      devObj=nextDevObj;
      nextDevObj=devObj->NextDevice;

      // Find uLan Device Extension
      devExt=(PULAN_DEVICE_EXTENSION)devObj->DeviceExtension;

      // Set State
      devExt->State = STATE_REMOVED;

      // Return uLan system resources
      ReturnResources(devExt);

      // Destroy DeviceObject
      RemoveDevice(devObj);
    }

    if(uL_WDM_RegistryPath.Buffer)
   	  ExFreePool(uL_WDM_RegistryPath.Buffer);
}
