/*******************************************************************
  uLan Communication - uL_DRV - multiplatform uLan driver

  ul_devtab.c	- Linux and Windows PnP PCI and USB device tables

  (C) Copyright 1996-2004 by Pavel Pisa - project originator
        http://cmp.felk.cvut.cz/~pisa
  (C) Copyright 1996-2004 PiKRON Ltd.
        http://www.pikron.com
  (C) Copyright 2002-2004 Petr Smolik
  

  The uLan driver project can be used and distributed 
  in compliance with any of next licenses
   - GPL - GNU Public License
     See file COPYING for details.
   - LGPL - Lesser GNU Public License
   - MPL - Mozilla Public License
   - and other licenses added by project originator

  Code can be modified and re-distributed under any combination
  of the above listed licenses. If contributor does not agree with
  some of the licenses, he/she can delete appropriate line.
  WARNING: if you delete all lines, you are not allowed to
  distribute code or sources in any form.
 *******************************************************************/

/*******************************************************************/
/* Supported devices table */ 

static const ul_chip_type_ent ul_chip_types[]={
    #ifdef UL_WITH_UART_510
	{"82510",u510_init,UL_CHIPT_RQPORT,1,0},
    #endif /*UL_WITH_UART_450*/
    #ifdef UL_WITH_UART_450
	{"16450",u450_init,UL_CHIPT_RQPORT,1,0},
     #ifdef CONFIG_OC_UL_DRV_U450_VARPINS
	{"16450-dirneg",u450_init,UL_CHIPT_RQPORT,1,U450_CHOPT_DIRNEG},
	{"16450-msrswap",u450_init,UL_CHIPT_RQPORT,1,U450_CHOPT_MSRSWAP},
	{"16450-dirneg-msrswap",u450_init,UL_CHIPT_RQPORT,1,U450_CHOPT_DIRNEG|U450_CHOPT_MSRSWAP},
     #endif /*CONFIG_OC_UL_DRV_U450_VARPINS*/
    #endif /*UL_WITH_UART_450*/
    #ifdef UL_WITH_PCI
	{"16950-adv",u950pci_init,UL_CHIPT_PCI|UL_CHIPT_NOPROBE,4,
		0x16954000|U950PCI_CHOPT_TXDTRNEG|U950PCI_CHOPT_HSPDOSC},
	{"16950-pci",u950pci_init,UL_CHIPT_PCI|UL_CHIPT_NOPROBE,4,
		0x16954000|U950PCI_CHOPT_RXDONRI},
	{"16950-pcie",u950pci_init,UL_CHIPT_PCI|UL_CHIPT_NOPROBE,4,
		0x16954000|U950PCI_CHOPT_SP512F4K},
    #endif  /* UL_WITH_PCI */
    #ifdef CONFIG_OC_UL_DRV_WITH_VIRTUAL
	{"virtual",ul_virtual_init,UL_CHIPT_NOPROBE,1,0},
    #endif /*CONFIG_OC_UL_DRV_WITH_VIRTUAL*/
	{NULL,NULL},
};

#ifdef CONFIG_OC_UL_DRV_WITH_VIRTUAL
/* For Windows support */
#define UL_VIRTUAL_HW		0x7f010000
#endif /*CONFIG_OC_UL_DRV_WITH_VIRTUAL*/

#ifdef UL_WITH_PCI

static struct pci_device_id ulan_pci_tbl[] /*__devinitdata*/ = {
	/* Advantech PCI-1602A, negated DTR */
	{ /* PCI_VENDOR_ID */ 0x13fe, /* PCI_DEVICE_ID */ 0x1600,
	  /* PCI_VENDOR_ID */ PCI_ANY_ID, /* PCI_SUBDEVICE_ID */ PCI_ANY_ID,
	  0, 0, 0x16954000|U950PCI_CHOPT_TXDTRNEG|U950PCI_CHOPT_HSPDOSC},
	/* Tedia PCI-1482 */
	{ /* PCI_VENDOR_ID */ 0x1415, /* PCI_DEVICE_ID */ 0x950A,
	  /* PCI_VENDOR_ID */ PCI_ANY_ID, /* PCI_SUBDEVICE_ID */ PCI_ANY_ID,
	  0, 0, 0x16954000|U950PCI_CHOPT_RXDONRI},
	/* Tedia PCI-1482 with PiKRON signature */
	{ /* PCI_VENDOR_ID */ 0x1760, /* PCI_DEVICE_ID */ 0x8004,
	  /* PCI_VENDOR_ID */ PCI_ANY_ID, /* PCI_SUBDEVICE_ID */ PCI_ANY_ID,
	  0, 0, 0x16954000|U950PCI_CHOPT_RXDONRI},
	/* STL Sunrich Technology IP-160 PCIe RS-422/485 2-Port Card w/isolation */
	{ /* PCI_VENDOR_ID */ 0x1415, /* PCI_DEVICE_ID */ 0xc158,
	  /* PCI_VENDOR_ID */ PCI_ANY_ID, /* PCI_SUBDEVICE_ID */ PCI_ANY_ID,
	  0, 0, 0x16954000|U950PCI_CHOPT_SP512F4K|U950PCI_CHOPT_OSC62_5},
	/* SUNIX - SUN2212 */
	{ /* PCI_VENDOR_ID */ 0x1fd4, /* PCI_DEVICE_ID */ 0x1999,
	  /* PCI_VENDOR_ID */ PCI_ANY_ID, /* PCI_SUBDEVICE_ID */ PCI_ANY_ID,
	  0, 0, 0x16450000|U450_CHOPT_HSPDOSC},
	{ 0, } /* terminate list */
};

#endif /*UL_WITH_PCI*/

#define UL_MPC52XX_DEVID 0x1234
#define UL_UART_450_DW_APB_DEVID 0x1300
#define UL_UART_PL011_DEVID 0x1411

#ifdef UL_WITH_OF
static const struct of_device_id ulan_of_tbl[] = {
   #ifdef UL_WITH_UART_MPC52xx_PSC
        { .compatible = "fsl,mpc5200-psc-ulan", .data = (void *)UL_MPC52XX_DEVID, },
   #endif /*UL_WITH_UART_MPC52xx_PSC*/
   #ifdef UL_WITH_UART_450
        { .compatible = "snps,dw-apb-uart", .data = (void *)UL_UART_450_DW_APB_DEVID, },
   #endif /*UL_WITH_UART_450*/
   #ifdef UL_WITH_UART_PL011
        { .compatible = "arm,pl011-ulan", .data = (void *)UL_UART_PL011_DEVID, },
   #endif /*UL_WITH_UART_PL011*/
        {},
};
#endif /*UL_WITH_OF*/

#ifdef UL_WITH_AMBA
static const struct amba_id ulan_amba_tbl[] = {
	{
		.id     = 0x00041011,
		.mask   = 0x000fffff,
		.data   = (void *)UL_UART_450_DW_APB_DEVID,
	},
	{ 0, 0 },
};
#endif /*UL_WITH_AMBA*/

#ifdef UL_WITH_USB

/* Define these values to match your device */
#define UL_USB_PS1_VENDOR_ID	0xDEAD
#define UL_USB_PS1_PRODUCT_ID	0x1001
#define UL_USB_HW_PS1		(0x1234<<8)  

#ifndef USB_VENDOR_PIKRON
#define USB_VENDOR_PIKRON	0x1669
#endif

#ifdef FOR_LINUX_KERNEL
/* table of devices that work with this driver */
static const struct usb_device_id ulan_usb_tbl [] = {
	{ USB_DEVICE(UL_USB_PS1_VENDOR_ID, UL_USB_PS1_PRODUCT_ID), driver_info:UL_USB_HW_PS1 },
	{ USB_DEVICE(USB_VENDOR_PIKRON, UL_USB_PS1_PRODUCT_ID), driver_info:UL_USB_HW_PS1 },
	{ USB_DEVICE(USB_VENDOR_PIKRON, 0x1002), driver_info:UL_USB_HW_PS1 },
	{ USB_DEVICE(USB_VENDOR_PIKRON, 0x1003), driver_info:UL_USB_HW_PS1 },
	{ }					/* Terminating entry */
};
#else
static const struct usb_device_id ulan_usb_tbl [] = {
	{ UL_USB_PS1_VENDOR_ID, UL_USB_PS1_PRODUCT_ID, UL_USB_HW_PS1 },
	{ USB_VENDOR_PIKRON, UL_USB_PS1_PRODUCT_ID, UL_USB_HW_PS1 },
	{ USB_VENDOR_PIKRON, 0x1002, UL_USB_HW_PS1 },
	{ USB_VENDOR_PIKRON, 0x1003, UL_USB_HW_PS1 },
	{ 0, }					/* Terminating entry */	
};
#endif /* FOR_LINUX_KERNEL */

#endif /* UL_WITH_USB */

