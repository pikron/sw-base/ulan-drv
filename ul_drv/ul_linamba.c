/*******************************************************************
  uLan Communication - uL_DRV - multiplatform uLan driver

  ul_drv.c	- uLan driver main source file

  (C) Copyright 1996-2004 by Pavel Pisa - project originator
        http://cmp.felk.cvut.cz/~pisa
  (C) Copyright 1996-2004 PiKRON Ltd.
        http://www.pikron.com
  (C) Copyright 2002-2004 Petr Smolik
  (C) Copyright 2009 Martin Samek


  The uLan driver project can be used and distributed
  in compliance with any of next licenses
   - GPL - GNU Public License
     See file COPYING for details.
   - LGPL - Lesser GNU Public License
   - MPL - Mozilla Public License
   - and other licenses added by project originator

  Code can be modified and re-distributed under any combination
  of the above listed licenses. If contributor does not agree with
  some of the licenses, he/she can delete appropriate line.
  WARNING: if you delete all lines, you are not allowed to
  distribute code or sources in any form.
 *******************************************************************/

#include <linux/platform_device.h>
#include <linux/of_irq.h>
#include <linux/of_address.h>

#ifdef UL_WITH_UART_MPC52xx_PSC
/* in kernel 2.6.31 some functions was moved from mpc52xx.h to mpc5xxx.h */
#if LINUX_VERSION_CODE >= KERNEL_VERSION(2,6,31)
#include <asm/mpc5xxx.h>
#else
#include <asm/mpc52xx.h>
#endif
#endif /*UL_WITH_UART_MPC52xx_PSC*/

/* default invocation of chip_init for OF devices */
static int /*__devinit*/
  ulan_amba_init_chan(struct amba_device *op,char *subdev,
                 char *chip_name, ul_drv **pudrv,
                 ul_chip_init_fnc *chip_init,
                 ul_physaddr_t physaddr, int irq, long abaudbase, int options)
{
  ul_drv *udrv;
  int amy_adr=0;
  int abaud=0;
  int match;
  int minor;
  int ret;
  int i;
#ifdef UL_WITH_DEVFS
  kc_devfs_handle_t devfs_handle;
  char dev_name[32];
#endif /* UL_WITH_DEVFS */

  *pudrv=NULL;
  /* try to find best minor and parameters */
  match=ulan_init_find_minor("amba",dev_name(&op->dev),subdev,&minor,&i);
  if(i>=0){
    abaud=baud[i];amy_adr=my_adr[i];
  }

  /* mem for ul_drv */
  if(!(udrv=MALLOC(sizeof(ul_drv)))) return -ENOMEM;
  /* clear memory */
  memset(udrv,0,sizeof(ul_drv));
  /* set initial state */
  ul_drv_new_init_state(udrv, amy_adr);
  udrv->dev=(struct ul_phys_dev_ptr *)op;
  /* init chip driver */
  if((ret=(*chip_init)(udrv, physaddr, irq, abaud, abaudbase, options))<0){
    printk(KERN_CRIT "ulan_init_chan: ERROR - chip_init returned %d\n",ret);
    FREE(udrv);
    return -EIO;
  }
  /* setups buffers, ports and irq for sucesfully detected device */
  if((ret=ul_drv_new_start(udrv,ulbuffer))<0){
    printk(KERN_CRIT "ulan_init_chan: ERROR - ul_drv_new_start returned %d\n",ret);
    FREE(udrv);
    return -EIO;
  }
  #ifdef UL_WITH_DEVFS
  sprintf (dev_name, "ulan%d", minor);
  devfs_handle=kc_devfs_new_cdev(NULL, MKDEV(ulan_major_dev, minor),
			S_IFCHR | S_IRUSR | S_IWUSR | S_IRGRP | S_IWGRP,
			&ulan_fops, udrv, dev_name);
  udrv->devfs_handle=devfs_handle;
  #endif /* UL_WITH_DEVFS */

  printk(KERN_INFO "ulan_init_chan: chip=%s minor=%d baud=%d my_adr=%d ready\n",
	 chip_name,/*dev_name*/minor,udrv->baud_val,udrv->my_adr_arr[0]);

  if(minor>=0) ul_drv_arr[minor]=udrv;
  *pudrv=udrv;
  udrv->next_chan=dev_get_drvdata(&op->dev);
  dev_set_drvdata(&op->dev, udrv);
  kc_class_device_create(ulan_class, NULL, MKDEV(ulan_major_dev, minor),
			&op->dev, "ulan%d", minor);
  return 0;
}

static int ulan_amba_probe(struct amba_device *op, const struct amba_id *id)
{
	struct clk *clk;
	long baudbase;
	struct resource *res;
	int ret;
	resource_size_t mapbase;
	ul_physaddr_t physbase;
	unsigned int irq;
	int chip_options;
	ul_drv *udrv;

	chip_options = (uintptr_t)id->data;

	dev_info(&op->dev, "ulan_amba_probe(op=%p, chip_options=0x%08x)\n", op, chip_options);
	res = &op->res;

	mapbase = res->start;
	physbase = ul_uint2physaddr(mapbase);

	irq = op->irq[0];

	dev_info(&op->dev, "of uart at %p, irq=%x\n",
		(void *)mapbase, irq);

	/* get the clock - this also enables the HW */
	clk = devm_clk_get(&op->dev, NULL);
	ret = PTR_ERR_OR_ZERO(clk);
	if (ret) {
		dev_err(&op->dev, "could not get clk: %d\n", ret);
		return ret;
	}

	/* enable the clock as a last step */
	ret = clk_prepare_enable(clk);
	if (ret) {
		dev_err(&op->dev, "unable to enable uart clock\n");
		return ret;
	}

	baudbase = clk_get_rate(clk);

	ret = -1;
   #if defined(UL_WITH_UART_PL011)
	switch(chip_options & ~0xff) {
     #ifdef UL_WITH_UART_PL011
	   case (UL_UART_450_DW_APB_DEVID  & ~0xff) :
		/* (dev,subdev,chip_name,pudrv,chip_init,port,irq,options) */
		ret = ulan_amba_init_chan(op,"0","pl011-ulan",&udrv,&upl011_init,physbase,irq, baudbase, chip_options);
		break;
     #endif /*UL_WITH_UART_PL011*/
	}
   #endif /*UL_WITH_UART_PL011*/
	return ret>=0? 0: -ENODEV;
}

static KC_AMBA_REMOVE_RET ulan_amba_remove(struct amba_device *op)
{
	int i;
	ul_drv *udrv = (ul_drv *)dev_get_drvdata(&op->dev);

        dev_info(&op->dev, "ulan_amba_remove\n");

	if((struct ul_phys_dev_ptr *)op!=udrv->dev){
		printk(KERN_CRIT "ulan_remove_one: BAD - cross OF device remove\n");
		return (KC_AMBA_REMOVE_RET)-1;
	}
	for(i=0;i<UL_MINORS;i++){
		if (udrv==ul_drv_arr[i]){
			kc_class_device_destroy(ulan_class, MKDEV(ulan_major_dev, i));
			ul_drv_arr[i]=NULL;
		}
	}

	#ifdef UL_WITH_DEVFS
	if(udrv->devfs_handle) kc_devfs_delete(udrv->devfs_handle);
	#endif /* UL_WITH_DEVFS */
	ul_drv_free(udrv);

	return (KC_AMBA_REMOVE_RET)0;
}

#ifdef CONFIG_PM
static int ulan_amba_suspend(struct device *dev)
{
	int ret;
	ul_drv *udrv = (ul_drv *)dev_get_drvdata(dev);

	ret=ul_drv_stop(udrv);
	if(ret<0) {
		UL_PRINTF(KERN_CRIT "ulan_amba_suspend : ul_drv_stop failed\n");
	}
	return 0;
}

static int ulan_amba_resume(struct device *dev)
{
	int ret;
	ul_drv *udrv = (ul_drv *)dev_get_drvdata(dev);

	if(!uld_atomic_test_dfl(udrv,CHIPOK)) {
		ret=ul_drv_start(udrv,0);
		if(ret<0) {
			UL_PRINTF(KERN_CRIT "ulan_amba_resume : ul_drv_start failed\n");
			return -EIO;
		}
	}
	return 0;
}

#endif
