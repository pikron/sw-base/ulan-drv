
		uLan Driver for NT v. 1.1.2
		(c) 1999-2009 Pavel Pisa
		(c) 2002-2009 Petr Smolik

The uLan provides 9-bit character message oriented communication protocol,
which is transfered over RS-485 link. Physical layer consist of one
twisted pair of leads and RS-485 transceivers.

ul_drv is Linux and Windows device driver designed to access uLan network.
Supported hardware is i82510 RS-485 card or simple active converter
dongle for standard PC RS-232 ports. OX16C950 based PCI cards are
supported as well.

The uLan driver is developed under GNU General Public Licence.

The uLan driver is used by associated project Chromulan

Chromulan is a freeware data acquisition and devices
control program on uLan network for Win98/NT/2000.

  (C) 2000 - 2001 Jindrich Jindrich, Pavel Pisa, PiKRON Ltd.

  Originators of the CHROMuLAN project:

  Jindrich Jindrich - http://www.jindrich.com
                      http://orgchem.natur.cuni.cz/Chromulan
                      software developer, project coordinator
  Pavel Pisa        - http://cmp.felk.cvut.cz/~pisa
                      embeded software developer
  PiKRON Ltd.       - http://www.pikron.com
                      project initiator, sponsor, instrument developer

  The CHROMuLAN project is distributed under the GNU General Public Licence.

Windows NT version of driver is Kernel Mode Driver and its parameters
are controlled by next registry keys

  [HKEY_LOCAL_MACHINE\SYSTEM\CurrentControlSet\Services\ul_drv]
  "Type"=dword:00000001
  "Start"=dword:00000002
  "ErrorControl"=dword:00000001
  "DisplayName"="UL_DRV"
  "Group"="port"
  "Tag"=dword:00000001
  "ImagePath"=hex(2):5c,3f,3f,5c,44,3a,...

  [HKEY_LOCAL_MACHINE\SYSTEM\CurrentControlSet\Services\ul_drv\Parameters]
  "Port Address"=dword:000003e8
  "IRQ Line"=dword:00000005
  "Baud Rate"=dword:00004b00
  "Buffer Size"=dword:00010000
  "My Addr"=dword:00000002
  "Debug"=dword:00000019
  "ScanForPCI"=dword:00000001

More information about driver, its implementation and meaning
of parameters can be found in UL_DRV.HTML file.
