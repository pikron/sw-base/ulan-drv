/*******************************************************************
  uLan Communication - low level C and driver interface library

  ul_msg_buf.c	- store uLAN message into memory buffer

  (C) Copyright 1996-2016 by Pavel Pisa - project originator
        http://cmp.felk.cvut.cz/~pisa
  (C) Copyright 1996-2016 PiKRON Ltd.
        http://www.pikron.com
  (C) Copyright 2002-2016 Petr Smolik


  The uLan driver project can be used and distributed
  in compliance with any of next licenses
   - GPL - GNU Public License
     See file COPYING for details.
   - LGPL - Lesser GNU Public License
   - MPL - Mozilla Public License
   - and other licenses added by project originator

  Code can be modified and re-distributed under any combination
  of the above listed licenses. If contributor does not agree with
  some of the licenses, he/she can delete appropriate line.
  WARNING: if you delete all lines, you are not allowed to
  distribute code or sources in any form.
 *******************************************************************/

#include <stdlib.h>
#include <string.h>
#include <ul_msg_buf.h>

int
ul_msg_buf_init(ul_msg_buf_t *buf)
{
  memset(buf,0,sizeof(ul_msg_buf_t));
  ul_dbuff_init(&buf->data, 0);
  return 0;
}

void
ul_msg_buf_destroy(ul_msg_buf_t *buf)
{
  ul_msg_buf_t *tail;
  buf->msginfo.len=0;
  buf->msginfo.flg=0;
  ul_dbuff_destroy(&buf->data);
  tail=buf->tail;
  buf->tail=0;
  while(tail){
    buf=tail;
    tail=buf->tail;
    ul_dbuff_destroy(&buf->data);
    free(buf);
  }
}

int
ul_msg_buf_rd_data(ul_msg_buf_t *buf, ul_fd_t ul_fd)
{
  ssize_t len, rd_len;
  len=buf->msginfo.len;
  if (ul_dbuff_prep(&buf->data,len)<len)
    return -1;
  if(!len) return 0;
  rd_len=ul_read(ul_fd,buf->data.data, len);
  if(rd_len!=len) return -1;
  return rd_len;
}

int
ul_msg_buf_rd_rest(ul_msg_buf_t *buf, ul_fd_t ul_fd)
{
  int frames=0;
  ul_msg_buf_t *tail;
  do{
    if(buf->msginfo.len)
      if(ul_msg_buf_rd_data(buf,ul_fd)<0)
        break;
    frames++;
    if(!(buf->msginfo.flg&UL_BFL_TAIL)){
      ul_freemsg(ul_fd);
      return frames;
    }
    if(!(tail=malloc(sizeof(ul_msg_buf_t))))
      break;
    ul_msg_buf_init(tail);
    if(ul_actailmsg(ul_fd,&tail->msginfo)<0)
    {
      ul_msg_buf_destroy(tail);
      free(tail);
      break;
    }
    buf->tail=tail;
    buf=tail;
  }while(1);
  ul_freemsg(ul_fd);
  return -1;
}

int
ul_msg_buf_wr(ul_msg_buf_t *buf, ul_fd_t ul_fd)
{
  int l;
  if (!buf) return -1;
  l=buf->msginfo.len;
  buf->msginfo.len=0;
  if (ul_newmsg(ul_fd,&buf->msginfo)<0) return -1;
  do{
    if (ul_write(ul_fd,buf->data.data,l)!=l) 
      break;
    if(!(buf->msginfo.flg&UL_BFL_TAIL)){
      return ul_freemsg(ul_fd);
    }
    buf=buf->tail;
    l=buf->msginfo.len;
    buf->msginfo.len=0;
    if(ul_tailmsg(ul_fd,&buf->msginfo)<0)
      break;
  }while(1);
  ul_abortmsg(ul_fd);
  return -1;
}
